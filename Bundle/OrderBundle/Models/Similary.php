<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 21:46
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle\Models;


use Shopware\Models\Article\Detail as ArticleDetail;
use Shopware\Models\Article\Unit;
use Shopware\Models\Order\DetailStatus;
use Shopware\Models\Tax\Tax;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use Shopware\Models\Order\Detail as Position;

/**
 * Class Similary
 * @package WundeDcompanion\Bundle\OrderBundle\Models
 */
class Similary
{
    /**
     * @var int $uid
     */
    private $uid = 0;

    /**
     * @var int $instock
     */
    private $instock = 0;

    /**
     * @var int $dcInstock
     */
    private $dcInstock = 0;

    /**
     * @var float $price
     */
    private $price = 0;

    /**
     * @var int $articleDetailId
     */
    private $articleDetailId;

    /**
     * @var ArticleDetail $articleDetail
     */
    private $articleDetail;

    /**
     * @var int $articleId
     */
    private $articleId;

    /**
     * @var string $ordernumber
     */
    private $ordernumber;

    /**
     * @var string $ean
     */
    private $ean;

    /**
     * @var string $articleName
     */
    private $articleName;

    /**
     * @var int $positionId
     */
    private $positionId;

    /**
     * @var int $quantity
     */
    private $quantity;

    /**
     * @var float $shippingCost
     */
    private $shippingCost;

    /**
     * @var Tax $tax
     */
    private $tax;

    /**
     * @var Unit $unit
     */
    private $unit;

    /**
     * @var string $packUnit
     */
    private $packUnit;

    /**
     * @var DetailStatus $detailStatus
     */
    private $detailStatus;

    /**
     * @var float $purchasePrice
     */
    private $purchasePrice;

    /**
     * @var Wholesaler|null $wholesaler
     */
    private $wholesaler;

    /**
     * @var int $expectedQuantity
     */
    private $expectedQuantity;

    /**
     * Similary constructor.
     */
    public function __construct()
    {
        $this->instock = 0;
        $this->price = 0;
        $this->quantity = 0;
        $this->shippingCost = 0;
    }

    /**
     * @return int
     */
    public function getInstock()
    {
        return $this->instock;
    }

    /**
     * @param int $instock
     * @return Similary
     */
    public function setInstock($instock)
    {
        $this->instock = $instock;
        return $this;
    }

    /**
     * @return int
     */
    public function getDcInstock()
    {
        return $this->dcInstock;
    }

    /**
     * @param int $dcInstock
     * @return Similary
     */
    public function setDcInstock($dcInstock)
    {
        $this->dcInstock = $dcInstock;
        return $this;
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @return float
     */
    public function getTotalPurchasePrice()
    {
        return $this->purchasePrice * $this->quantity;
    }

    /**
     * @param float $purchasePrice
     * @return Similary
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
        return $this;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param Unit $unit
     * @return Similary
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return string
     */
    public function getPackUnit()
    {
        return $this->packUnit;
    }

    /**
     * @param string $packUnit
     * @return Similary
     */
    public function setPackUnit($packUnit)
    {
        $this->packUnit = $packUnit;
        return $this;
    }

    /**
     * @return DetailStatus
     */
    public function getDetailStatus()
    {
        return $this->detailStatus;
    }

    /**
     * @param DetailStatus $detailStatus
     * @return Similary
     */
    public function setDetailStatus($detailStatus)
    {
        $this->detailStatus = $detailStatus;
        return $this;
    }

    /**
     * @return Tax
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param Tax $tax
     * @return Similary
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }

    /**
     * @return string
     */
    public function getArticleName()
    {
        return $this->articleName;
    }

    /**
     * @param string $articleName
     * @return Similary
     */
    public function setArticleName($articleName)
    {
        $this->articleName = $articleName;
        return $this;
    }

    /**
     * @return int
     */
    public function getArticleId()
    {
        return $this->articleId;
    }

    /**
     * @param int $articleId
     * @return Similary
     */
    public function setArticleId($articleId)
    {
        $this->articleId = $articleId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     * @return Similary
     */
    public function setEan($ean)
    {
        $this->ean = $ean;
        return $this;
    }

    /**
     * @return ArticleDetail
     */
    public function getArticleDetail()
    {
        return $this->articleDetail;
    }

    /**
     * @param ArticleDetail $articleDetail
     * @return Similary
     */
    public function setArticleDetail($articleDetail)
    {
        $this->articleDetail = $articleDetail;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Similary
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int
     */
    public function getArticleDetailId()
    {
        return $this->articleDetailId;
    }

    /**
     * @param int $articleDetailId
     * @return Similary
     */
    public function setArticleDetailId($articleDetailId)
    {
        $this->articleDetailId = $articleDetailId;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrdernumber()
    {
        return $this->ordernumber;
    }

    /**
     * @param string $ordernumber
     * @return Similary
     */
    public function setOrdernumber($ordernumber)
    {
        $this->ordernumber = $ordernumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * @param int $positionId
     * @return Similary
     */
    public function setPositionId($positionId)
    {
        $this->positionId = $positionId;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Similary
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getExpectedQuantity(): int
    {
        return $this->expectedQuantity ?: 0;
    }

    /**
     * @param int $expectedQuantity
     */
    public function setExpectedQuantity(int $expectedQuantity): void
    {
        $this->expectedQuantity = $expectedQuantity;
    }

    /**
     * @return Wholesaler|null
     */
    public function getWholesaler()
    {
        return $this->wholesaler;
    }

    /**
     * @param Wholesaler|null $wholesaler
     * @return Similary
     */
    public function setWholesaler($wholesaler)
    {
        $this->wholesaler = $wholesaler;
        return $this;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param float $shippingCost
     * @return Similary
     */
    public function setShippingCost($shippingCost)
    {
        $this->shippingCost = $shippingCost;
        return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
        return !is_null($this->wholesaler) ? $this->wholesaler->getShortname() : 'shop';
    }

    /**
     * @return float|int
     */
    public function getTotalPrice()
    {
        return $this->price * ($this->tax->getTax() / 100 + 1);
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return md5(serialize($this->__debugInfo()));
    }

    /**
     * @return array
     */
    public function __debugInfo()
    {
        return [
            'id' => $this->getUid(),
            'instock' => $this->getInstock(),
            'dcInstock' => $this->getDcInstock(),
            'price' => $this->getPrice(),
            'purchasePrice' => $this->getPurchasePrice(),
            'totalPurchasePrice' => $this->getTotalPurchasePrice(),
            'articleDetailId' => $this->getArticleDetailId(),
            'ordernumber' => $this->getOrdernumber(),
            'positionId' => $this->getPositionId(),
            'orderQuantity' => $this->getQuantity(),
            'expectedQuantity' => $this->getExpectedQuantity(),
            'shippingCost' => $this->getShippingCost(),
            'context' => $this->getContext()
        ];
    }
}