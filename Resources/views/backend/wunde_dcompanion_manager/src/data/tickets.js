import { firebase } from '../plugins/firebase';

export default {
  subject: 'Testticket',
  content: '<h1>Testticket</h1>',
  rating: 0,
  creatorId: 0,
  editor: null,
  deleted: false,
  updatedAt: null,
  createdAt: firebase.firestore.FieldValue.serverTimestamp(),
  receivedAt: null,
  deletedAt: null,
  comments: [],
  state: 10,
  priority: 2
}
