{include file="string:{config name=emailheaderplain}"}

-- Dropshippers Companion Statusmail - BESTELLEINGANG --

Hallo,

es ist eine neue Bestellung eingegangen, diese beinhaltet einen oder mehrere Dropshipping-Artikel.

Die Bestellnummer lautet: {$orderNumber}

{if $dc_auto_order && $payment.name == "prepayment"}
Achtung: Ihr Kunde hat {$payment.description} als Art der Bezahlung gewählt
Der automatisierte Dropshipping-Prozess wurde vorerst unterbrochen. Er wird fortgesetzt, sobald der Bezahlstatus von Ihnen auf "Komplett bezahlt" geändert wurde!
{else}
Ist der Dropshippers Companion auf "Automatik" eingestellt, so ist keine weitere Aktion nötig, andernfalls kontrollieren Sie den Bestelleingang bitte umgehend.
{/if}

