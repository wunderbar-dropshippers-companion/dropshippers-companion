<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 18:52
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Seeder;

use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Helpers\Database\Database;
use WundeDcompanion\Models\Ticket\Priority;

/**
 * Class PrioritySeeder
 * @package WundeDcompanion\Seeder
 */
abstract class PrioritySeeder extends DatabaseSeeder
{
    private static $priorities = [
        Priority::PRIORITY_LOW => ['name' => 'low', 'color' => 'grey lighten-3'],
        Priority::PRIORITY_NORMAL => ['name' => 'normal', 'color' => 'green lighten-3'],
        Priority::PRIORITY_HIGH => ['name' => 'high', 'color' => 'amber lighten-3'],
        Priority::PRIORITY_VERY_HIGH => ['name' => 'very_high', 'color' => 'red lighten-3'],
    ];

    public static function seed()
    {
        $em = Shopware()->Container()->get('models');
        $logger = Shopware()->Container()->get('pluginlogger');

        if (Database::entityTableExists(Priority::class)) {
            foreach (self::$priorities as $id => $data) {
                try {
                    /** @var Priority $priority */
                    $priority = $em->getRepository(Priority::class)->find($id);

                    if (is_null($priority)) {
                        $priority = new Priority($id, $data['name'], $data['color']);
                        $em->persist($priority);
                    } else {
                        $priority->setName($data['name']);
                        $priority->setColor($data['color']);
                    }
                } catch (Exception $e) {
                    $logger->error($e->getMessage(), $e->getTrace());
                }
            }
        }
    }
}