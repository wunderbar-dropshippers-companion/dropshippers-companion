// Packages
import Vue from 'vue'

import { router, vuetify, i18n } from './plugins'
import store from './store'

const showEditor = false

import App from './App.vue'
import Editor from './Editor.vue'

const root = showEditor ? Editor : App

Vue.config.productionTip = false
Vue.config.devtools = process.env.NODE_ENV !== 'production'
Vue.config.debug = process.env.NODE_ENV !== 'production'
Vue.config.silent = process.env.NODE_ENV === 'production'

store.dispatch('snippets/load')
  .finally(() => {
    new Vue({
      router,
      store,
      vuetify,
      i18n,
      render: h => h(root)
    }).$mount('#app')
  })
