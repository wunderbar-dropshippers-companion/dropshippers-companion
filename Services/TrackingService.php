<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 22/03/2021 13:40
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;

use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Order;
use WundeDcompanion\Models\Order\Carrier;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Tracking;
use WundeDcompanion\Models\Wholesaler\Wholesaler;

class TrackingService
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * @var MailService $mailService
     */
    private $mailService;

    /**
     * Test constructor.
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param MailService $mailService
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(ModelManager $em, LoggerService $logger, MailService $mailService, WholesalerRepository $wholesalerRepository)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mailService = $mailService;
        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * @param Order $order
     * @return string
     */
    public function handleOrderTrackings(Order $order)
    {
        $result = 'Order (' . $order->getNumber() . ')';

        try {
            if ($this->hasOrderMissingTrackingCodes($order) || true) {
                $wholesalers = $this->getUsedOrderWholesalers($order);

                $trackings = [];
                $trackingCodes = [];

                /** @var Carrier[] $carriers */
                $carriers = $this->em->getRepository(Carrier::class)
                    ->createQueryBuilder('carrier')
                    ->indexBy('carrier', 'carrier.name')
                    ->getQuery()
                    ->getResult();

                foreach ($wholesalers as $shortname => $wholesaler)
                {
                    $client = $wholesaler->getClient();

                    if (!is_null($client)) {
                        $response = $client->getTrackings($order);

                        if ($response->getStatusCode() === 200) {
                            $wholesalerTrackings = json_decode($response->getContent(), true);

                            foreach ($wholesalerTrackings as $positionId => $positionTrackings)
                            {
                                foreach ($positionTrackings as $positionTracking)
                                {
                                    if (array_key_exists('code', $positionTracking) && array_key_exists('carrier', $positionTracking)) {
                                        $tracking = new Tracking($carriers);

                                        $tracking->setCode($positionTracking['code']);
                                        $tracking->setCarrier($positionTracking['carrier']);

                                        $trackings[$positionId][] = $tracking;
                                    }
                                }
                            }
                        }
                    }
                }

                /** @var Position $position */
                foreach ($order->getDetails()->getValues() as $position)
                {
                    if (array_key_exists($position->getId(), $trackings)) {

                        $positionTrackings = $trackings[$position->getId()];
                        $attributes = $position->getAttribute();

                        if (
                            method_exists($attributes, 'setDcDropshipTrackingCarrier') &&
                            method_exists($attributes, 'setDcDropshipTrackingCode')
                        ) {
                            $codes = [];
                            $carriers = [];

                            /** @var Tracking $positionTracking */
                            foreach ($positionTrackings as $positionTracking)
                            {
                                $codes[] = [
                                    'value' => $positionTracking->getCode(),
                                    'url' => $positionTracking->getTrackingUrl(),
                                    'carrier' => $positionTracking->getCarrier()
                                ];

                                $carriers[] = [
                                    'value' => $positionTracking->getCarrier(),
                                    'url' => $positionTracking->getCarrierUrl()
                                ];

                                if (!array_key_exists($positionTracking->getCode(), $trackingCodes)) {
                                    $trackingCodes[] = [
                                        'value' => $positionTracking->getCode(),
                                        'url' => $positionTracking->getTrackingUrl(),
                                        'carrier' => $positionTracking->getCarrier()
                                    ];
                                }
                            }
                            $attributes->setDcDropshipTrackingCarrier(json_encode($carriers));
                            $attributes->setDcDropshipTrackingCode(json_encode($codes));
                        }
                    }
                }

                if (count($trackingCodes)) {
                    $order->setTrackingCode(json_encode(array_values($trackingCodes)));
                }

                if (count($trackings)) {
                    foreach ($trackings as $positionId => $positionTrackings)
                    {
                        $position = $this->findPosition($order, $positionId);

                        if (!is_null($position)) {
                            $result .= "\n\t" . 'Article (' . $position->getArticleNumber() . ') | PosId ('.$positionId.')';

                            /** @var Tracking $positionTracking */
                            foreach ($positionTrackings as $positionTracking)
                            {
                                $result .= "\n\t\t" . $positionTracking->getCarrier() . ': ' . $positionTracking->getCode();
                            }
                        }
                    }
                }
            } else {
                $result = 'Order (' . $order->getNumber() . '): nothing to handle';
            }

//            $this->mailService->sendTrackingMail($order);
            $this->mailService->sendTrackingMailToCustomer($order);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
            $result = 'error';
        }

        return $result;
    }

    /**
     * @param Order $order
     * @param int $positionId
     * @return Position|null
     */
    private function findPosition($order, $positionId)
    {
        /** @var Position $position */
        foreach ($order->getDetails()->getValues() as $position)
        {
            if ($position->getId() === $positionId) {
                return $position;
            }
        }

        return null;
    }

    /**
     * @param Order $order
     * @return Wholesaler[]
     */
    private function getUsedOrderWholesalers(Order $order): array
    {
        $shortnames = array_map(function ($position) {
            /** @var Position $position */
            $attributes = $position->getAttribute();
            return method_exists($attributes, 'getDcNameShort')
                ? $attributes->getDcNameShort()
                : '';
        }, $order->getDetails()->getValues());

        $filteredShortnames = array_filter($shortnames, function($shortname) {
            return !empty($shortname);
        });

        return array_filter($this->wholesalers, function ($shortname) use($filteredShortnames) {
            return in_array($shortname, $filteredShortnames);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function hasOrderMissingTrackingCodes(Order $order): bool
    {
        try {
            if (empty($order->getTrackingCode()) || is_null($order->getTrackingCode())) {
                return true;
            }

            /** @var Position $position */
            foreach ($order->getDetails()->getValues() as $position)
            {
                $attributes = $position->getAttribute();

                if (
                    method_exists($attributes, 'getDcDropshipTrackingCarrier') &&
                    method_exists($attributes, 'getDcDropshipTrackingCode')
                ) {
                    $carriers = $attributes->getDcDropshipTrackingCarrier();
                    $codes = $attributes->getDcDropshipTrackingCode();

                    if (empty($carriers) || empty($codes)) {
                        return true;
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
            return true;
        }

        return false;
    }
}