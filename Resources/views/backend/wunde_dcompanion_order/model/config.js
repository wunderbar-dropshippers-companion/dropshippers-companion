Ext.define('Shopware.apps.WundeDcompanionOrder.model.Config', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    autoLoad: true,

    /**
     * The fields used for this model
     * @array
     */
    fields: [
        { name: 'dc_auto_order', type: 'boolean' },
        { name: 'dc_combine_tracking_codes', type: 'boolean' },
        { name: 'dc_debug_logging', type: 'boolean' },
        { name: 'dc_delete_log_after', type: 'boolean' },
        { name: 'dc_finish_order', type: 'boolean' },
        { name: 'dc_mail_error_recipients', type: 'boolean' },
        { name: 'dc_mail_notification', type: 'boolean' },
        { name: 'dc_mail_recipients', type: 'boolean' },
        { name: 'dc_override_stocks', type: 'boolean' },
        { name: 'dc_overwrite_purchaseprice', type: 'boolean' },
        { name: 'dc_overwrite_purchaseprice_cheapest', type: 'boolean' },
        { name: 'dc_send_mail_to_customer', type: 'boolean' },
        { name: 'dc_test_mode', type: 'boolean' }
    ],
});