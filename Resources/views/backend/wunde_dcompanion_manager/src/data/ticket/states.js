export default [
  {
    "id": -10,
    "color": "red lighten-3",
    "snippet": "state.no_solution",
    "selectable": false
  },
  {
    "id": 0,
    "color": "deep-purple lighten-4",
    "snippet": "state.waiting",
    "selectable": false
  },
  {
    "id": 10,
    "color": "green lighten-3",
    "snippet": "state.assigned",
    "selectable": true
  },
  {
    "id": 20,
    "color": "amber lighten-3",
    "snippet": "state.in_progress",
    "selectable": false
  },
  {
    "id": 30,
    "color": "green lighten-2",
    "snippet": "state.solved",
    "selectable": true
  }
]
