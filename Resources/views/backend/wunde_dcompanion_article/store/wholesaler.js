/*
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 01/09/2020 16:10
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

Ext.define('Shopware.apps.WundeDcompanionArticle.store.Wholesaler', {

    /**
     * Extend for the standard ExtJS 4
     * @string
     */
    extend:'Ext.data.Store',

    autoLoad: true,

    remoteSort: false,

    sorters: [{
        property: 'longname',
        direction: 'ASC'
    }],

    /**
     * Define the used model for this store
     * @string
     */
    model: 'Shopware.apps.WundeDcompanionArticle.model.Wholesaler',

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '{url controller=WundeDcompanionArticle action="list"}',
            update: '{url controller=WundeDcompanionArticle action="update"}',
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'wholesalers'
        },
    }
});