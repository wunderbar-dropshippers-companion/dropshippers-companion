//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/view/list/list"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionOrder.view.list.List', {

    override: 'Shopware.apps.Order.view.list.List',

    dcSnippets: {
        sendOrderButtonText: '{s name=button/text/sendorder}Ordernumber{/s}',
        sendOrdersButtonText: '{s name=button/text/sendorders}Ordernumber{/s}'
    },

    registerEvents: function() {
        var me = this;
        me.callParent(arguments);

        this.addEvents(
            'sendOrderPositions',
        );
    },

    getColumns: function () {
        var me = this;
        var columns = me.callParent(arguments);

        columns.unshift({
            header: 'Trackingcodes',
            width: 200,
            sortable: false,
            resizable: true,
            groupable: false,
            hidden: false,
            hideable: true,
            dataIndex: 'trackingCode',
            flex: 1,
            renderer: me.linkRenderer
        });

        columns.unshift(
            {
                header: 'DC',
                width: 30,
                sortable: true,
                resizable: false,
                groupable: false,
                hidden: false,
                hideable: false,
                align: 'center',
                dataIndex: 'is_dropship',
                flex: 1,
                renderer: me.getDCColumnInfo
            },
        );

        return columns;
    },

    getDCColumnInfo: function (value, metaData, record) {
        var error = '<div style="width: 16px; height: 16px; background: red; margin: 0 auto; border-radius: 2px; padding-top: 0; color: white;"></div>'
        return record.get("dcDropshipActive") || error;
    },

    getGridSelModel:function () {
        var me = this;
        var selModel = me.callOverridden(arguments);

        selModel.on('selectionchange', function(sm, selections) {
            var buttonText = selections.length > 1
                ? me.dcSnippets.sendOrdersButtonText
                : me.dcSnippets.sendOrderButtonText

            me.sendOrdersButton.setText(buttonText);
            me.sendOrdersButton.setDisabled(selections.length === 0);

            me.abortOrdersButton.setDisabled(selections.length === 0);
        })

        return selModel;
    },

    getToolbar:function () {
        var me = this;
        var toolbar = me.callOverridden(arguments);

        Ext.create('Shopware.apps.WundeDcompanionOrder.store.Config', {
            listeners: {
                load: function(store, records) {
                    if (records.length) {
                        var record = records[0];

                        me.sendOrdersButton = Ext.create('Ext.button.Button', {
                            text: me.dcSnippets.sendOrderButtonText,
                            iconCls: 'sprite-truck-box-label',
                            disabled: true,
                            hidden: record.get('dc_auto_order'),
                            listeners: {
                                click: function () {
                                    me.fireEvent('sendOrderPositions', me.selModel.getSelection(), me)
                                }
                            }
                        });

                        me.abortOrdersButton = Ext.create('Ext.button.Button', {
                            text: 'Automatischen Bestellvorgang abbrechen',
                            iconCls: 'sprite-cross-button',
                            disabled: true,
                            hidden: !record.get('dc_auto_order'),
                            listeners: {
                                click: function () {
                                    me.fireEvent('abortOrderProcess', me.selModel.getSelection(), me)
                                }
                            }
                        })

                        toolbar.insert(1, me.sendOrdersButton);
                        toolbar.insert(2, me.abortOrdersButton);
                    }
                }
            }
        });

        return toolbar;
    },

    linkRenderer: function (value) {
        try {
            var data = JSON.parse(value);

            var links = Ext.Array.map(data, function (item) {
                return item.hasOwnProperty('carrier')
                    ? '<a href="'+item.url+'" target="_blank" title="'+item.carrier+'">'+item.value+'</a>'
                    : '<a href="'+item.url+'" target="_blank">'+item.value+'</a>';
            })

            return links.length ? links.join(', ') : '---'
        } catch (error) {
            console.log(error)
            return '---'
        }
    }
});
//{/block}