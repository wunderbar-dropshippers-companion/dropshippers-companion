{include file="string:{config name=emailheadertml}"}

{if $order.shipping.salutation == "mr"}
    <b>Sehr geehrter Herr {$order.shipping.lastName},</b>
{else}
    <b>Sehr geehrte Frau {$order.shipping.lastName},</b>
{/if}

wir möchten Sie darüber informieren, dass zu Ihrer Bestellung {$order.number} vom {$orderDate|date_format:"%d.%m.%Y"} folgende Trackinginformationen hinterlegt wurden:


{foreach item=trackingCode from=$trackingCodes key=index}
    {$trackingCode.value}
    {if index != count($trackingCodes) - 1}
        ,
    {/if}
{/foreach}

<p><b>Wir bedanken uns für Ihre Bestellung!</b></p>
