<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 17:08
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Email;


use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Mail\Mail;
use SplFileInfo;
use WundeDcompanion\Services\MailService;

/**
 * Class Template
 * @package WundeDcompanion\Helpers\Email
 */
class Template
{
    const PREFIX = 'DC_';

    /**
     * @var string $name
     */
    private $name = '';

    /**
     * @var array $config
     */
    private $config = [];

    /**
     * @var string $txt
     */
    private $txt = '';

    /**
     * @var $html
     */
    private $html = '';

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * Template constructor.
     */
    public function __construct()
    {
        $this->em = Shopware()->Container()->get('models');
        $this->logger = Shopware()->Container()->get('pluginlogger');
    }

    /**
     * @param SplFileInfo $fileInfo
     */
    public function setValue($fileInfo)
    {
        $extension = $fileInfo->getExtension();
        $basename = $fileInfo->getBasename('.' . $extension);
        $content = file_get_contents($fileInfo->getPathname());

        $this->name = self::PREFIX . strtoupper($basename);

        switch (strtolower($extension)) {
            case 'html':
                $this->html = $content;
                break;
            case 'txt':
                $this->txt = $content;
                break;
            case 'json':
                $this->config = json_decode($content, true);
                break;
        }
    }

    /**
     * @return string
     */
    private function getSubject()
    {
        return array_key_exists('subject', $this->config) && is_string($this->config['subject'])
            ? $this->config['subject']
            : '';
    }

    /**
     * @return integer
     */
    private function getType()
    {
        return array_key_exists('type', $this->config) && is_integer($this->config['type'])
            ? $this->config['type']
            : 1;
    }

    /**
     * @return bool
     */
    private function isValid()
    {
        return
            !empty($this->config) &&
            !empty($this->name) &&
            !empty($this->html) &&
            !empty($this->txt);
    }

    /**
     * @param Mail[] $registeredTemplates
     * @return Mail|null
     */
    public function register($registeredTemplates)
    {
        if ($this->isValid() && !in_array($this->name, $registeredTemplates)) {
            $mail = array_key_exists($this->name, $registeredTemplates)
                ? $registeredTemplates[$this->name]
                : new Mail();

            $mail->setName($this->name);
            $mail->setFromMail('{config name=mail}');
            $mail->setFromName('{config name=shopName}');
            $mail->setContent($this->txt);
            $mail->setContentHtml($this->html);
            $mail->setIsHtml(true);
            $mail->setSubject($this->getSubject());
            $mail->setMailtype($this->getType());

            try {
                if (is_null($mail->getId())) {
                    $this->em->persist($mail);

                    if ($this->name === 'DC_INSTALL') {
                        $mailService = new MailService();
                        $mailService->sendInstallMail($mail);
                    }
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e->getTrace());
            }

            return $mail;
        }

        return null;
    }

    public function __debugInfo()
    {
        return [
            'name' => $this->name,
            'txt' => $this->txt,
            'html' => $this->html,
            'config' => $this->config
        ];
    }


}