import ThemeBackup from "../../data/themes";
import { vuetify } from '../../plugins'

export default {
  namespaced: true,
  mutations: {
    setThemes: (state, themes) => {
      for (const [theme, colors] of Object.entries(themes)) {
        if (vuetify.framework.theme.themes.hasOwnProperty(theme)) {
          vuetify.framework.theme.themes[theme] = Object.assign(
            vuetify.framework.theme.themes[theme],
            colors
          )
        }
      }
    }
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getThemes'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let themes = response.data.hasOwnProperty('themes') ? response.data.themes : {};

          if (success) {
            commit('setThemes', themes)
          } else {
            throw new Error();
          }
        })
        .catch(() => {
          console.warn('themes could not be loaded')
          commit('setThemes', ThemeBackup)
        })
    },
  }
}
