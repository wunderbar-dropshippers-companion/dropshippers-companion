<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 02/11/2020 11:15
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use Shopware\Components\CacheManager;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\CustomModels\ViisonPickwareERP\Warehouse\BinLocation;
use Shopware\CustomModels\ViisonPickwareERP\Warehouse\Warehouse;
use Shopware\CustomModels\ViisonPickwareERP\Warehouse\WarehouseRepository;
use Shopware\Models\Article\Detail;
use Shopware\Models\Config\Element;
use Shopware\Models\Config\ElementTranslation;
use Shopware\Models\Config\Form;
use Shopware\Models\Plugin\Plugin;
use Shopware\Models\Shop\Locale;
use Shopware_Components_Snippet_Manager;
use Shopware\Plugins\ViisonPickwareERP\Components\StockLedger\StockChangeList\StockChangeListFactoryService;
use Shopware\Plugins\ViisonPickwareERP\Components\StockLedger\StockLedgerService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Config;
use WundeDcompanion\Models\Version;
use Shopware\Plugins\ViisonPickwareERP\Components\StockLedger\ArticleDetailStockInfoProviderService;

/**
 * Class PickwareService
 * @package WundeDcompanion\Services
 */
class PickwareService
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_pickware/main';
    const DEFAULT_LABEL = 'Pickware/storage location';
    const DEFAULT_DESCRIPTION = 'Enter the name or the abbreviation of the Pickware stock plate if you want to reconcile stocks in Pickware.';

    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var Plugin $plugin
     */
    private $plugin;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var array $locales;
     */
    private $locales;

    /**
     * @var Shopware_Components_Snippet_Manager $snippetManager
     */
    private $snippetManager;

    /**
     * @var CacheManager $cache
     */
    private $cache;

    /**
     * PickwareService constructor.
     * @param ContainerInterface $container
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param $snippetManager
     * @param CacheManager $cache
     * @param array $locales
     * @param string $pluginName
     */
    public function __construct($container, $em, $logger, $snippetManager, $cache, $locales, $pluginName)
    {
        $this->container = $container;
        $this->em = $em;
        $this->logger = $logger;
        $this->locales = $locales;
        $this->cache = $cache;
        $this->snippetManager = $snippetManager;

        $this->plugin = $em->getRepository(Plugin::class)->findOneBy([
            'name' => $pluginName
        ]);
    }

    /**
     * @return Plugin|null
     */
    public function getPlugin()
    {
        return $this->plugin;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return !is_null($this->plugin)
            ? $this->plugin->getActive()
            : false;
    }

    /**
     * @return bool
     */
    public function isInstalled()
    {
        return !is_null($this->plugin)
            ? (bool)$this->plugin->getInstalled()
            : false;
    }

    /**
     * @param Form $form
     * @param string $name
     * @return Element|boolean
     */
    private function getExtensionConfig($form, $name)
    {
        /** @var Element $element */
        foreach ($form->getElements()->getValues() as $element)
        {
            if ($element->getName() === $name) {
                return $element;
            }
        }

        return false;
    }

    /**
     * @param Wholesaler $wholesaler
     * @return bool
     */
    public function removeExtensionConfig($wholesaler)
    {
        /** @var Form $form */
        $form = $wholesaler->getPlugin()->getConfigForms()->first();
        $shortname = trim(strtolower($wholesaler->getShortname()));
        $name = 'dc_'.$shortname.'_pickware_warehousename';

        if ($element = $this->getExtensionConfig($form, $name)) {
            try {
                $this->em->remove($element);
                $this->em->flush();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
                return false;
            }
        }

        return true;
    }

    /**
     * @param Element $element
     */
    public function setElementTranslations($element)
    {
        $locales = array_keys(Shopware()->Container()->getParameter('wunde_dcompanion.pickware.locales'));

        /** @var Locale[] $locales */
        $locales = Shopware()->Models()->getRepository(Locale::class)
            ->createQueryBuilder('locale')
            ->where('locale.locale IN (:locales)')
            ->setParameter('locales', $locales)
            ->getQuery()
            ->getResult();

        foreach ($locales as $locale)
        {
            $this->snippetManager->setLocale($locale);

            $snippets = $this->snippetManager->getNamespace(self::SNIPPET_NAMESPACE);

            $label = $snippets->get('extension/config/label', self::DEFAULT_LABEL);
            $description = $snippets->get('extension/config/description', self::DEFAULT_DESCRIPTION);

            $translation = new ElementTranslation();
            $translation->setLocale($locale);
            $translation->setLabel($label);
            $translation->setElement($element);
            $translation->setDescription($description);

            try {
                $this->em->persist($translation);
                $this->em->flush();

                $this->clearCache();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }
    }

    /**
     * @param Form $form
     * @return int
     */
    private function getNextElementPosition($form)
    {
        /** @var Element[] $elements */
        $elements = $form->getElements()->getValues();

        $positions = array_map(function($element) {
            return $element->getPosition();
        }, $elements);

        return max($positions) + 1;
    }

    /**
     * @param Form $form
     * @param string $name
     * @return int
     */
    private function getPositionAfter($form, $name)
    {
        /** @var Element[] $elements */
        $elements = $form->getElements()->getValues();

        $targetPosition = 0;

        foreach ($elements as $element)
        {
            if ($element->getName() === $name) {
                $targetPosition = $element->getPosition();
            }
        }

        foreach ($elements as $element)
        {
            if ($element->getPosition() > $targetPosition) {
                $element->setPosition($element->getPosition() + 1);
            }
        }

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return $targetPosition + 1;
    }

    /**
     * @param Wholesaler $wholesaler
     * @return Warehouse
     */
    private function getWarehouse(Wholesaler $wholesaler)
    {
        /** @var Warehouse $warehouse */
        $warehouse = $this->em->getRepository(Warehouse::class)->findOneBy([
            'code' => strtoupper(trim($wholesaler->getShortname()))
        ]);

        if (is_null($warehouse)) {
            $warehouse = new Warehouse();
            $warehouse->setCode(strtoupper(trim($wholesaler->getShortname())));
            $warehouse->setName($wholesaler->getLongname());

            try {
                $this->em->persist($warehouse);

                $nullBinLocation = new BinLocation($warehouse, BinLocation::NULL_BIN_LOCATION_CODE);
                $warehouse->setNullBinLocation($nullBinLocation);

                $this->em->persist($nullBinLocation);
                $this->em->flush();

                return $warehouse;
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);

                return null;
            }
        }

        return $warehouse;
    }

    /**
     * @param Wholesaler $wholesaler
     * @return bool
     */
    public function addExtensionConfig(Wholesaler $wholesaler)
    {
        /** @var Form $form */
        $form = $wholesaler->getPlugin()->getConfigForms()->first();
        $shortname = trim(strtolower($wholesaler->getShortname()));
        $name = 'dc_'.$shortname.'_pickware_warehousename';
        $afterPositionElement = 'dc_'.$shortname.'_shipping_cost_exemption_limit';

        /** @var WarehouseRepository $warehouseRepository */
        $warehouseRepository = $this->em->getRepository(Warehouse::class);

        if (!$this->getExtensionConfig($form, $name)) {
            $snippets = $this->snippetManager->getNamespace(self::SNIPPET_NAMESPACE);

            $label = $snippets->get('extension/config/label', self::DEFAULT_LABEL);
            $description = $snippets->get('extension/config/description', self::DEFAULT_DESCRIPTION);

            $warehouse = $this->getWarehouse($wholesaler);
            $defaultWarehouse = $warehouseRepository->getDefaultWarehouse();

            $warehouseId = (!is_null($warehouse)
                ? $warehouse->getId()
                : (!is_null($defaultWarehouse)
                    ? $defaultWarehouse->getId()
                    : 1));

            $element = new Element('select', $name, []);
            $element->setValue($warehouseId);
            $element->setLabel($label);
            $element->setDescription($description);
            $element->setPosition($this->getPositionAfter($form, $afterPositionElement));
            $element->setScope(Element::SCOPE_SHOP);

            try {
                $this->em->persist($element);
                $form->addElement($element);
                $this->em->flush();

                $this->setElementTranslations($element);

                $this->clearCache();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
                return false;
            }
        }

        return true;
    }

    /**
     * @param bool $config
     * @param bool $array
     * @return Warehouse[]
     */
    public function getWarehouses($config = false, $array = false)
    {
        $warehouses = [];

        if ($this->isActive()) {
            if ($config) {
                $warehouses = $this->em->getRepository(Warehouse::class)
                    ->createQueryBuilder('warehouse')
                    ->select([
                        'warehouse.id',
                        'warehouse.name',
                        'warehouse.code'
                    ])
                    ->getQuery()
                    ->getArrayResult();

                foreach ($warehouses as &$warehouse)
                {
                    $warehouse['displayName'] = $warehouse['name'] . ' (' . $warehouse['code'] . ')';
                }
            } else {
                $warehouses = $this->em->getRepository(Warehouse::class)->findAll();

                if ($array) {
                    $warehouses = $this->em->toArray($warehouses);
                }
            }
        }

        return $warehouses;
    }

    /**
     * @param string $version
     * @return bool
     */
    public function isMinVersion($version)
    {
        return version_compare($this->plugin->getVersion(), $version, '>=');
    }

    /**
     * @param Detail $articleDetail
     * @param Warehouse $warehouse
     * @param int $clientInStock
     * @param string $purchasePrice
     * @param bool $remove
     */
    public function setStock($articleDetail, $warehouse, $clientInStock, $purchasePrice, $remove = false)
    {
        try {
            if ($this->isActive() && $this->isMinVersion(5) && !is_null($warehouse) && $warehouse instanceof Warehouse) {
                /** @var StockChangeListFactoryService $stockChangeListFactory */
                $stockChangeListFactory = $this->container->get('pickware.erp.stock_change_list_factory_service');

                /** @var StockLedgerService $stockLedger */
                $stockLedger = $this->container->get('pickware.erp.stock_ledger_service');

                /** @var ArticleDetailStockInfoProviderService $articleDetailStockInfoProvider */
                $articleDetailStockInfoProvider = $this->container->get('pickware.erp.article_detail_stock_info_provider_service');

                // Get stockinfo
                $stockInfo = $articleDetailStockInfoProvider->getStockInWarehouse($articleDetail, $warehouse);

                // Delete from Stock (set to zero)
                if ($remove && $stockInfo > 0) {
                    $stockChangeList = $stockChangeListFactory->createStockChangeList($warehouse, $articleDetail, -$stockInfo);
                    $stockLedger->recordOutgoingStock($articleDetail, $stockChangeList, 'Artikel nicht mehr verfügbar');
                    $this->logger->pickwareLog([
                        'context' => 'Löschen',
                        'ordernumber' => $articleDetail->getNumber(),
                        'warehouse' => $warehouse->getName()
                    ], true);
                } // Initialisierung
                else if ($stockInfo == 0 && $clientInStock > 0) {
                    $stockChangeList = $stockChangeListFactory->createStockChangeList($warehouse, $articleDetail, $clientInStock);
                    $stockLedger->recordIncomingStock($articleDetail, $stockChangeList, $purchasePrice, 'Initialisierung');
                    $this->logger->pickwareLog([
                        'context' => 'Initialisierung',
                        'ordernumber' => $articleDetail->getNumber(),
                        'warehouse' => $warehouse->getName(),
                        'purchasePrice' => $purchasePrice
                    ], true);
                } else {
                    // Incoming
                    if ($stockInfo < $clientInStock) {
                        $addStock = $clientInStock - $stockInfo;
                        $stockChangeList = $stockChangeListFactory->createStockChangeList($warehouse, $articleDetail, $addStock);
                        $stockLedger->recordIncomingStock($articleDetail, $stockChangeList, $purchasePrice, 'Einlagern');
                        $this->logger->pickwareLog([
                            'context' => 'Einlagern',
                            'ordernumber' => $articleDetail->getNumber(),
                            'warehouse' => $warehouse->getName(),
                            'addStock' => $addStock,
                            'purchasePrice' => $purchasePrice
                        ], true);
                    } // Outgoing
                    elseif ($stockInfo > $clientInStock) {
                        $reduceStock = $clientInStock - $stockInfo;
                        $stockChangeList = $stockChangeListFactory->createStockChangeList($warehouse, $articleDetail, $reduceStock);
                        $stockLedger->recordOutgoingStock($articleDetail, $stockChangeList, 'Auslagern');
                        $this->logger->pickwareLog([
                            'context' => 'Auslagern',
                            'ordernumber' => $articleDetail->getNumber(),
                            'warehouse' => $warehouse->getName(),
                            'reduceStock' => $reduceStock
                        ], true);
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }
    }

    private function clearCache()
    {
        $this->cache->clearTemplateCache();
        $this->cache->clearHttpCache();
        $this->cache->clearProxyCache();
        $this->cache->clearConfigCache();
    }
}