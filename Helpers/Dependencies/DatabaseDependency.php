<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 14:41
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Dependencies;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Tools\SchemaTool;
use Exception;
use Shopware\Bundle\AttributeBundle\Service\ConfigurationStruct;
use Shopware\Bundle\AttributeBundle\Service\CrudService;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelEntity;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Helpers\Database\AttributeTableManager as AttributeManager;
use WundeDcompanion\Helpers\Database\Column;
use WundeDcompanion\Helpers\Database\Database;
use WundeDcompanion\Helpers\Database\DatabaseTable;
use WundeDcompanion\Helpers\Database\TableManager;
use WundeDcompanion\Seeder\DatabaseSeeder;

/**
 * Class DatabaseDependency
 * @package WundeDcompanion\Helpers\Dependencies
 */
class DatabaseDependency
{
    const TABLE_PATH = __DIR__ . '/../../Data/tables.json';

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Expr $expr
     */
    private $expr;

    /**
     * @var AbstractSchemaManager $schemaManager
     */
    private $schemaManager;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var SchemaTool $schemaTool
     */
    private $schemaTool;

    /**
     * @var CrudService $crud
     */
    private $crud;

    /**
     * DatabaseDependency constructor.
     */
    public function __construct()
    {
        $di = Shopware()->Container();

        $this->em = $di->get('models');
        $this->logger = $di->get('pluginlogger');
        $this->crud = $di->get('shopware_attribute.crud_service');

        $this->expr = $this->em->getExpressionBuilder();
        $this->schemaManager = $this->em->getConnection()->getSchemaManager();

        $this->schemaTool = new SchemaTool($this->em);
    }

    /**
     * @param string[] $entities
     * @return ClassMetadata[]
     */
    private function getSchemas($entities)
    {
        return array_map(function ($entity) {
            return $this->em->getClassMetadata($entity);
        }, $entities);
    }

    /**
     * @param string[] $entities
     * @return ClassMetadata[]
     */
    public function getUnregisteredSchemas($entities)
    {
        return array_filter($this->getSchemas($entities), function ($schema) {
            return !Database::tableExists($schema->getTableName());
        });
    }

    /**
     * @param string[] $entities
     * @return ClassMetadata[]
     */
    public function getRegisteredSchemas($entities)
    {
        return array_filter($this->getSchemas($entities), function ($schema) {
            return Database::tableExists($schema->getTableName());
        });
    }

    /**
     * @param string[] $entities
     */
    public function registerEntities($entities = [])
    {
        $schemas = $this->getUnregisteredSchemas($entities);
        $this->schemaTool->updateSchema($schemas, true);
    }

    /**
     * @param ModelEntity[] $entities
     */
    public function unregisterEntities($entities = [])
    {
        $schemas = $this->getRegisteredSchemas($entities);
        $this->schemaTool->dropSchema($schemas);
    }

    /**
     * @param array $columns
     * @param string|null $shortname
     * @return array
     */
    private function getExpectedColumnTypes(array $columns, $shortname)
    {
        $data = [];

        foreach ($columns as $key => $column)
        {
            $name = !is_null($shortname)
                ? str_replace('%shortname%', $shortname, $column['name'])
                : $column['name'];

            $data[$name] = [
                'expected' => $column['type'],
                'rename' => isset($column['oldName'])
                    ? (!is_null($shortname)
                        ? str_replace('%shortname%', $shortname, $column['oldName'])
                        : $column['oldName'])
                    : null
            ];
        }

        return $data;
    }

    /**
     * @param string $table
     * @param array $columns
     * @param string|null $shortname
     * @return array
     */
    private function getExpectedColumns(string $table, array $columns, $shortname)
    {
        $schemaManager = $this->em->getConnection()->getSchemaManager();

        $existingColumns = $schemaManager->listTableColumns($table);
        $expectedColumns = $this->getExpectedColumnTypes($columns, $shortname);

        $expectedColumns = array_filter($expectedColumns, function ($options, $expectedColumn) use($existingColumns) {
            return !is_null($options['rename'])
                ? array_key_exists($options['rename'], $existingColumns)
                : array_key_exists($expectedColumn, $existingColumns);
        }, ARRAY_FILTER_USE_BOTH);

        array_walk($expectedColumns, function (&$expectedColumn, $columnName) use($table) {

            /** @var ConfigurationStruct $struct */
            $struct = !is_null($expectedColumn['rename']) && !is_null($this->crud->get($table, $expectedColumn['rename']))
                ? $this->crud->get($table, $expectedColumn['rename'])
                : $this->crud->get($table, $columnName);

            $expectedColumn['provided'] = $struct->getColumnType();
        });

        foreach ($expectedColumns as $key => &$expectedColumn)
        {
            if ($expectedColumn['expected'] === $expectedColumn['provided']) {
                unset($expectedColumns[$key]);
            }

            if (!is_null($expectedColumn['rename']) && !array_key_exists($expectedColumn['rename'], $existingColumns)) {
                $expectedColumn['rename'] = null;
            }
        }

        return $expectedColumns;
    }

    /**
     * @param string|null $shortname
     * @param string $context
     * @return array
     */
    private function getExpectedTables($shortname, string $context)
    {
        $tables = json_decode(file_get_contents(self::TABLE_PATH), true)[$context];

        array_walk($tables, function (&$columns, $table) use($shortname) {
            $columns = $this->getExpectedColumns($table, $columns, $shortname);
        });

        foreach ($tables as $key => $columns)
        {
            if (empty($columns)) {
                unset($tables[$key]);
            }
        }

        return $tables;
    }

    /**
     * @param string|null $shortname
     * @param string $context
     * @return array
     */
    private function getDataBackup($shortname, string $context)
    {
        $expectedTables = $this->getExpectedTables($shortname, $context);

        foreach ($expectedTables as $tableName => &$values)
        {
            $fields = ['id'];

            foreach ($values as $name => $options)
            {
                $fields[] = !is_null($options['rename'])
                    ? $options['rename'] . ' AS ' . $name
                    : $name;
            }

            $items = $this->em->getConnection()->createQueryBuilder()
                ->select($fields)
                ->from($tableName)
                ->execute()
                ->fetchAll();

            $data = [];

            foreach ($items as &$columns)
            {
                foreach ($columns as $columnName => &$value)
                {
                    if ($columnName !== 'id') {
                        $type = $values[$columnName]['expected'];

                        switch($type) {
                            case 'integer': $value = (int)$value; break;
                            case 'float': $value = (float)(str_replace(',', '.', $value)); break;
                            case 'boolean': $value = (bool)$value; break;
                            default: $value = (string)$value;
                        }
                    }
                }

                $data[$columns['id']] = $columns;
                unset($data[$columns['id']]['id']);
            }

            $values = $data;
        }

        return $expectedTables;
    }

    /**
     * @param string $context
     * @param string|null $shortname
     */
    public function registerAttributes(string $context, string $shortname = null)
    {
        try {
            if ($context === AttributeManager::CONTEXT_EXTENSION && is_null($shortname)) {
                throw new Exception('Missing param shortname');
            }

            $attributeTableManager = Database::getAttributeTables();

            if (method_exists($attributeTableManager, $context)) {
                $dataBackup = $this->getDataBackup($shortname, $context);

                /** @var DatabaseTable $table */
                foreach ($attributeTableManager->$context() as $table)
                {
                    if (Database::tableExists($table->getName())) {
                        foreach ($table->getColumns() as $column) {
                            if (
                                !is_null($column->getOldName($shortname)) &&
                                (bool)$this->crud->get($table->getName(), $column->getOldName($shortname))
                            ) {
                                $this->crud->update(
                                    $table->getName(),
                                    $column->getOldName($shortname),
                                    $column->getType(),
                                    $column->getValues(),
                                    $column->getName($shortname),
                                    false,
                                    $column->getDefault()
                                );
                            } else {
                                $this->crud->update(
                                    $table->getName(),
                                    $column->getName($shortname),
                                    $column->getType(),
                                    $column->getValues(),
                                    null,
                                    false,
                                    $column->getDefault()
                                );
                            }
                        }
                    } else {
                        throw new Exception('Database table '.$table->getName().' not exists');
                    }
                }

                $attributeTableManager->generateAttributeModels();
                $this->updateCastedData($dataBackup);

//                /** @var DatabaseTable $table */
//                foreach ($attributeTableManager->$context() as $table)
//                {
//                    if ($table->getName() === 's_articles_attributes') {
//                        $this->updateReceiveDataCronjob($shortname);
//                    }
//                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    public function updateReceiveDataCronjob()
    {
        $cronJobs = $this->em->getConnection()->createQueryBuilder()
            ->select(['id'])
            ->from('s_crontab')
            ->where('action = :action')
            ->setParameter('action', 'Shopware_CronJob_DcReceiveData')
            ->execute()
            ->fetchAll();

        if (!empty($cronJobs)) {
            $id = $cronJobs[0]['id'];

            $next = new \DateTime('now');
            $next->modify('+1 min');

            $start = clone $next;
            $start->modify('-1 hour');

            $end = clone $next;
            $end->modify('+1 min');

            $this->em->getConnection()->createQueryBuilder()
                ->update('s_crontab')
                ->set('next', ':next')
                ->set('start', ':start')
                ->set('end', ':end')
                ->where('id = :id')
                ->setParameter('next', $next->format('Y-m-d H:i:s'))
                ->setParameter('start', $start->format('Y-m-d H:i:s'))
                ->setParameter('end', $end->format('Y-m-d H:i:s'))
                ->setParameter('id', $id)
                ->execute();
        }
    }

    /**
     * @param array $dataBackup
     */
    private function updateCastedData($dataBackup)
    {
        $conn = $this->em->getConnection();

        foreach ($dataBackup as $table => $records)
        {
            foreach ($records as $id => $record)
            {
                /** @var Connection $conn */
                $query = $conn->createQueryBuilder()->update($table);

                foreach ($record as $column => $value)
                {
                    $query->set($column, ':' . $column);
                    $query->setParameter($column, $value);
                }

                $query
                    ->where('id = :id')
                    ->setParameter('id', $id)
                    ->execute();
            }
        }
    }

    /**
     * @param string $context
     * @param string|null $shortname
     */
    public function unregisterAttributes(string $context, string $shortname = null)
    {
        try {
            if ($context === AttributeManager::CONTEXT_EXTENSION && is_null($shortname)) {
                throw new Exception('Missing param shortname');
            }

            $attributeTableManager = Database::getAttributeTables();

            if (method_exists($attributeTableManager, $context)) {
                /** @var DatabaseTable $table */
                foreach ($attributeTableManager->$context() as $table) {
                    if (Database::tableExists($table->getName())) {
                        foreach ($table->getColumns() as $column) {
                            if ((bool)$this->crud->get($table->getName(), $column->getName($shortname))) {
                                $this->crud->delete($table->getName(), $column->getName($shortname));
                            }
                        }
                    } else {
                        throw new Exception('Database table ' . $table->getName() . ' not exists');
                    }
                }

                $attributeTableManager->generateAttributeModels();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param string[] $seeders
     */
    public function seed($seeders)
    {
        foreach ($seeders as $seeder)
        {
            call_user_func($seeder .'::seed');
        }
    }
}