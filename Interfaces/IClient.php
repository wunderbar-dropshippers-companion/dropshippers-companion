<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 13/08/2020 14:38
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Interfaces;

use Shopware\Models\Order\Order;
use WundeDcompanion\Models\Wholesaler\Config\Shipper;

/**
 * Interface IClient
 * @package WundeDcompanion\Interfaces
 */
interface IClient
{

    /**
     * @param string $ordernumber
     * @return IJsonResponse
     */
    public function getQuantities($ordernumber = null);

    /**
     * @param string $ordernumber
     * @return IJsonResponse
     */
    public function getQuantity($ordernumber);

    /**
     * @param string $ordernumber
     * @return IJsonResponse
     */
    public function getProducts($ordernumber = null);

    /**
     * @param string $ordernumber
     * @return IJsonResponse
     */
    public function getProduct($ordernumber);

    /**
     * @param string $ordernumber
     * @return IJsonResponse
     */
    public function getAll($ordernumber = null);

    /**
     * @param Order $order
     * @return IJsonResponse
     */
    public function getTrackings(Order $order): IJsonResponse;

    /**
     * @param Order $order
     * @param Shipper $shipper
     * @param string $requestName
     * @return mixed
     */
    public function sendOrder(Order $order, $shipper, $requestName = 'dropship');

    /**
     * @param boolean $testMode
     */
    public function setTestMode($testMode);

    /**
     * @param array $config
     */
    public function setConfig($config);
}