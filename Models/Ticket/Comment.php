<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 09/10/2020 13:48
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Ticket;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\User\User;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_comments")
 * @ORM\HasLifecycleCallbacks()
 */
class Comment extends ModelEntity
{
    const DATETIME_FORMAT = 'd.m.Y H:i:s';

    /**
     * Primary Key - autoincrement value
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $creator
     * @ORM\Column(name="creator", type="string", nullable=false)
     */
    private $creator;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", nullable=false)
     */
    private $email;

    /**
     * @var string $content
     * @ORM\Column(name="content", type="text", nullable=false)
     */
    private $content;

    /**
     * @var string $updatedAt
     * @ORM\Column(name="updated_at", type="string")
     */
    private $updatedAt;

    /**
     * @var string $createdAt
     * @ORM\Column(name="created_at", type="string")
     */
    private $createdAt;

    /**
     * @var boolean $developerAnswer
     * @ORM\Column(name="developer_answer", type="boolean", options={"default": 0})
     */
    private $developerAnswer = false;

    /**
     * @var boolean $shopAnswer
     * @ORM\Column(name="shop_answer", type="boolean", options={"default": 0})
     */
    private $shopAnswer = false;

    /**
     * Comment constructor.
     * @param string $creator
     * @param string $email
     * @param string $content
     */
    public function __construct($creator = null, $email = null, $content = null)
    {
        $this->creator = $creator;
        $this->email = $email;
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param string $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function isDeveloperAnswer()
    {
        return $this->developerAnswer;
    }

    /**
     * @param bool $developerAnswer
     */
    public function setDeveloperAnswer($developerAnswer)
    {
        $this->developerAnswer = $developerAnswer;
    }

    /**
     * @return bool
     */
    public function isShopAnswer()
    {
        return $this->shopAnswer;
    }

    /**
     * @param bool $shopAnswer
     */
    public function setShopAnswer($shopAnswer)
    {
        $this->shopAnswer = $shopAnswer;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $date = (new DateTime())->format(self::DATETIME_FORMAT);

        $this->createdAt = $date;
        $this->updatedAt = $date;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = (new DateTime())->format(self::DATETIME_FORMAT);
    }
}