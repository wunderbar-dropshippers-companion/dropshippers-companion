//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/controller/list"}
Ext.define('Shopware.apps.WundeDcompanionOrder.controller.List', {
    override: 'Shopware.apps.Order.controller.List',

    init: function() {
        var me = this;

        me.control({
            'order-dropship-grid': {
                openArticle: me.onOpenArticle,
            }
        });

        me.callParent(arguments);
    },

    onOpenArticle: function(record) {
        Shopware.app.Application.addSubApplication({
            name: 'Shopware.apps.Article',
            action: 'detail',
            params: {
                articleId: record.get('articleId')
            }
        });
    },
});
//{/block}