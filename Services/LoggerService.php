<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 13:13
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;

use DateTime;
use Exception;
use Monolog\Handler\HandlerInterface;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Models\Log\Log;
use WundeDcompanion\Models\Log\Type;

/**
 * Class LoggerService
 * @package WundeDcompanion\Services
 */
class LoggerService
{

    /**
     * @var string $toFileLogPath
     */
    private $toFileLogPath;

    /**
     * @var string $pickwareLogPath
     */
    private $pickwareLogPath;

    /**
     * @var string $pickwareShortLogPath
     */
    private $pickwareShortLogPath;

    /**
     * @var array $formattings
     */
    private $formattings;

    /**
     * @var UtilsService $utils
     */
    private $utils;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var MailService $mailService
     */
    private $mailService;

    /**
     * LoggerService constructor.
     * @param string $toFileLogPath
     * @param string $pickwareLogPath
     * @param string $pickwareShortLogPath
     * @param array $formattings
     * @param UtilsService $utils
     * @param Logger $logger
     * @param MailService $mailService
     */
    public function __construct($toFileLogPath, $pickwareLogPath, $pickwareShortLogPath, $formattings, $utils, $logger, $mailService)
    {
        $this->toFileLogPath = $toFileLogPath;
        $this->pickwareLogPath = $pickwareLogPath;
        $this->pickwareShortLogPath = $pickwareShortLogPath;
        $this->formattings = $formattings;
        $this->utils = $utils;
        $this->logger = $logger;
        $this->mailService = $mailService;
    }

    /**
     * @param Exception $exception
     * @return array
     */
    private function getTraceArray($exception)
    {
        $trace = [];

        if ($exception instanceof Exception) {
            $trace = $exception->getTrace();

            array_walk_recursive($trace, function (&$element) {
                if (is_object($element) && get_class($element) === 'Closure') {
                    $element = 'Closure';
                }
            });
        }

        return $trace;
    }

    /**
     * @param Exception $exception
     * @return array
     */
    private function getTrace($exception)
    {
        return $exception instanceof Exception
            ? $exception->getTrace()
            : [];
    }

    /**
     * @param string $message
     * @param Exception $exception
     * @param array $content
     * @param string $identifier
     * @return bool
     */
    public function error($message, $exception = null, $content = [], $identifier = null)
    {
        $this->createLog(Type::TYPE_ERROR, $message, $exception, $content, $identifier);

        $this->mailService->sendErrorMail($exception);

        return $this->logger->error($message, $this->getTrace($exception));
    }

    /**
     * @param string $message
     * @param array $content
     * @param string $identifier
     * @return bool
     */
    public function info($message, $content = [], $identifier = null)
    {
        $this->createLog(Type::TYPE_INFO, $message, null, $content, $identifier);

        return $this->logger->info($message, $content);
    }

    /**
     * @param int $logType
     * @param string $message
     * @param array $content
     * @param string $identifier
     * @param Exception $exception
     */
    private function createLog($logType, $message, $exception = null, $content = [], $identifier = null)
    {
        $code = 0;
        $file = '';
        $line = 0;
        $trace = json_encode($content);

        if ($exception instanceof Exception) {
            $code = $exception->getCode();
            $file = $exception->getFile();
            $line = $exception->getLine();
            $trace = json_encode($this->getTraceArray($exception));
        }

        try {
            $em = Shopware()->Models();

            /** @var Type $type */
            $type = $em->getRepository(Type::class)->find($logType);

            if (!is_null($type)) {
                $log = new Log();
                $log->setCode($code);
                $log->setFile($file);
                $log->setLine($line);
                $log->setMessage($message);
                $log->setIdentifier($identifier);
                $log->setContent($trace);
                $log->setType($type);

                $em->persist($log);
                $em->flush();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param mixed $message
     * @param bool $append
     */
    public function toFile($message = '', $append = false)
    {
        try {
            $date = new DateTime();
            $today = $date->format($this->formattings['date_only']);
            $time = $date->format($this->formattings['time_only']);

            $today = str_replace('.', '-', $today);

            $path = str_replace('%date%', $today, $this->toFileLogPath);

            $message = !empty($message)
                ? '['.$time.']'.PHP_EOL.PHP_EOL.print_r($message, true)
                : '';

            if ($append && file_exists($path)) {
                $content = trim(file_get_contents($path));

                $message = strlen($content)
                    ? $content . PHP_EOL . PHP_EOL . PHP_EOL . $message
                    : $message;
            }

            file_put_contents($path, $message);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param mixed $message
     * @param bool $append
     */
    public function pickwareLog($message = '', $append = false)
    {
        try {
            $date = new DateTime();
            $today = $date->format($this->formattings['date_only']);
            $time = $date->format($this->formattings['time_only']);

            $today = str_replace('.', '-', $today);

            $path = str_replace('%date%', $today, $this->pickwareLogPath);

            $this->pickwareShortLog($message);

            $message = !empty($message)
                ? '['.$time.']'.PHP_EOL.PHP_EOL.print_r($message, true)
                : '';

            if ($append && file_exists($path)) {
                $content = trim(file_get_contents($path));

                $message = strlen($content)
                    ? $content . PHP_EOL . PHP_EOL . PHP_EOL . $message
                    : $message;
            }

            file_put_contents($path, $message);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param mixed $message
     */
    private function pickwareShortLog($message = '')
    {
        try {
            if (
                is_array($message) &&
                array_key_exists('context', $message) &&
                array_key_exists('ordernumber', $message) &&
                array_key_exists('warehouse', $message)
            ) {
                $date = new DateTime();
                $today = $date->format($this->formattings['date_only']);
                $dateTime = $date->format($this->formattings['time_and_date']);

                $today = str_replace('.', '-', $today);

                $path = str_replace('%date%', $today, $this->pickwareShortLogPath);

                $shortMessage = sprintf(
                    '%s %s %s %s',
                    str_pad($dateTime . ':', 30, ' ', STR_PAD_RIGHT),
                    str_pad(sprintf('Context: %s', $message['context']), 30, ' ', STR_PAD_RIGHT),
                    str_pad(sprintf('Bestellnummer: %s', $message['ordernumber']), 40, ' ', STR_PAD_RIGHT),
                    str_pad(sprintf('Pickwarelager: %s', $message['warehouse']), 35, ' ', STR_PAD_RIGHT)
                );

                if (array_key_exists('addStock', $message)) {
                    $shortMessage .= sprintf(' | Add: %s', $message['addStock']);
                } else if (array_key_exists('reduceStock', $message)) {
                    $shortMessage .= sprintf(' | Remove: %s', $message['reduceStock']);
                }

                if (file_exists($path)) {
                    $content = trim(file_get_contents($path));

                    $shortMessage = strlen($content)
                        ? $content . PHP_EOL . $shortMessage
                        : $shortMessage;
                }

                file_put_contents($path, $shortMessage);
            } else {
                $this->toFile([
                    is_array($message),
                    array_key_exists('context', $message),
                    array_key_exists('ordernumber', $message),
                    array_key_exists('warehouse', $message)
                ]);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }
}