//{namespace name=backend/wunde_dcompanion_article/main}
//{block name="backend/article/view/detail/window"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionArticle.view.detail.Window', {
    override: 'Shopware.apps.Article.view.detail.Window',

    width: '100%',

    requires: [
        'Shopware.apps.WundeDcompanionArticle.view.detail.List'
    ],

    dcSnippets: {
        dcWholesaler: '{s name=dc/wholesalers}DC Wholesalers{/s}'
    },

    config: {},

    createMainTabPanel: function() {
        var me = this;

        me.callParent(arguments);
        me.registerAdditionalTab(
            {
                title: me.dcSnippets.dcWholesaler,
                contentFn: me.createWholesalerTab,
                insertIndex: 1,
                tabConfig: {
                    autoscroll: true,
                    name: 'article-wholesaler-tab',
                    border: false
                }
            },
            'Ext.form.Panel'
        );

        return me.mainTab;
    },

    createWholesalerTab: function(article, stores, eOpts) {
        var me = this;
        var wholesalerStore = Ext.create('Shopware.apps.WundeDcompanionArticle.store.Wholesaler', {
            listeners: {
                load: function(store, records) {
                    var message = 'Folgende Großhändler wurden aktualisiert: <br><br> [0]';
                    var wholesaler = '';

                    Ext.Array.each(records, function(record) {
                        if (record.get('show_update_Message')) {
                            wholesaler += record.get('longname') + (record.get('live') ? ' (OK)<br>' : ' (Fehler)<br>');
                        }
                    }, me)

                    if (wholesaler.length) {
                        Shopware.Notification.createStickyGrowlMessage({
                            title: 'Success',
                            text: Ext.String.format(message, wholesaler),
                            log: true,
                            width: 450
                        }, 'Wholesaler');
                    }
                }
            }
        });

        wholesalerStore.getProxy().setExtraParam('detailId', me.article.get('mainDetailId'));
        wholesalerStore.getProxy().setExtraParam('showUpdateMessage', false);
        wholesalerStore.getProxy().setExtraParam('live', false);

        me.articleContainer = Ext.create('Ext.container.Container', {
            title: me.dcSnippets.dcWholesaler,
            layout: 'fit',
            name: 'article-wholesaler-container',
            items: [
                Ext.create('Shopware.apps.WundeDcompanionArticle.view.detail.List', {
                    store: wholesalerStore,
                    height: '100%',
                    detailId: me.article.get('mainDetailId'),
                    variant: false
                }),
            ]
        });

        eOpts.tab.add(me.articleContainer);
        eOpts.tab.setDisabled(false);
    }
});
//{/block}