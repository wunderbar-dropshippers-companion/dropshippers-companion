<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 20/08/2020 21:08
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Commands;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\DBAL\Query\QueryBuilder;
use GuzzleHttp\Client;
use Shopware\Commands\ShopwareCommand;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Article;
use Shopware\Models\Shop\Shop;
use Shopware\Models\User\User;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;

class ArticleImport extends ShopwareCommand
{
    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var QueryBuilder $builder
     */
    private $builder;

    /**
     * @var ExpressionBuilder $expr
     */
    private $expr;

    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var array $options
     */
    private $options;

    /**
     * ArticleImport constructor.
     * @param WholesalerRepository $wholesalerRepository
     * @param ModelManager $em
     */
    public function __construct($wholesalerRepository, $em)
    {
        parent::__construct();
        $this->wholesalerRepository = $wholesalerRepository;
        $this->em = $em;
        $this->builder = $em->getConnection()->createQueryBuilder();
        $this->expr = $em->getConnection()->getExpressionBuilder();

        $this->setClient();
    }

    private function setClient()
    {
        /** @var Shop $shop */
        $shop = $this->em->getRepository(Shop::class)->getActiveDefault();

        $api = $this->builder
            ->select(['username', 'apiKey'])
            ->from('s_core_auth')
            ->where($this->expr->isNotNull('apiKey'))
            ->andWhere($this->expr->eq('active', 'active'))
            ->setParameter('active', true)
            ->execute()
            ->fetch();

        if (empty($api)) {
            echo 'Keine Shopware Api Zugangsdaten gefunden!' . PHP_EOL;
            die;
        }

        if (
            !is_null($shop) &&
            is_array($api) &&
            array_key_exists('username', $api) &&
            array_key_exists('apiKey', $api)
        ) {
            $this->options = [
                'auth' => [
                    $api['username'],
                    $api['apiKey']
                ]
            ];

            $this->client = new Client([
                'base_url' => ($shop->getSecure() ? 'https' : 'http') . '://' . $shop->getHost() . '/api/'
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dc:import')
            ->setDescription('Import wholesaler articles.')
            ->addArgument(
                'count',
                InputArgument::OPTIONAL,
                'Article count per wholesaler to import'
            );
    }

    private function get($query)
    {
        if ($client = $this->client) {
            $result = $client->get($query, $this->options);

            if ($result->getStatusCode() === 200) {
                return $result->json()['data'];
            }
        }

        return [];
    }

    private function post($query, $options = [])
    {
        if ($client = $this->client) {
            $options = array_merge($this->options, $options);
            return $client->post($query, $options);
        }

        return false;
    }

    private function put($query, $options = [])
    {
        if ($client = $this->client) {
            $options = array_merge($this->options, $options);
            return $client->put($query, $options);
        }

        return false;
    }

    private function getCategoryId()
    {
        $categories = $this->get('categories');

        if (!empty($categories)) {
            foreach ($categories as $category)
            {
                if ($category['name'] === 'DCIMPORT') {
                    return $category['id'];
                }
            }

            return $this->post('categories', [
                'json' => [
                    'name' => 'DCIMPORT',
                    'parentId' => 3,
                    'active' => true
                ]
            ]);
        }

        return null;
    }

    private function deleteAll()
    {
        try {
            /** @var Article[] $articles */
            $articles = $this->em->getRepository(Article::class)->findAll();

            foreach ($articles as $article)
            {
                $this->em->remove($article);
            }

            $this->em->flush();
            return true;
        } catch (Exception $e) {

        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $count = (int)$input->getArgument('count');
            $max = 50000;

            if ($this->deleteAll()) {

                $data = [];
                $categoryId = $this->getCategoryId();

                $current = 1;

                foreach ($this->wholesalerRepository->findAll() as $shortname => $wholesaler)
                {
                    if (($count && count($data) >= ($count * $current)) || !$count && count($data) >= ($max * $current)) break;
                    $output->writeln('<info>'.print_r($wholesaler->getLongname(), true).'</info>');

                    $response = $wholesaler->getClient()->getAll();

                    if ($response->getStatusCode() === 200) {
                        $articles = json_decode($response->getContent(), true);

                        foreach ($articles as $index => $article)
                        {
                            if (($count && count($data) >= ($count * $current)) || !$count && count($data) >= ($max * $current)) break;

                            if (
                                array_key_exists('ordernumber', $article) &&
                                array_key_exists('quantity', $article) &&
                                array_key_exists('name', $article) &&
                                array_key_exists('purchasePrice', $article) &&
                                array_key_exists('priceRecommended', $article) &&
//                            array_key_exists('image', $article) &&
//                            array_key_exists('manufacturer', $article) &&
                                array_key_exists('ean', $article) &&
                                !is_array($article['ordernumber']) &&
                                !is_array($article['quantity']) &&
                                !is_array($article['name']) &&
                                !is_array($article['purchasePrice']) &&
                                !is_array($article['priceRecommended']) &&
//                            !is_array($article['image']) &&
//                            !is_array($article['manufacturer']) &&
                                !is_array($article['ean'])
                            ) {
                                $description = array_key_exists('description', $article)
                                    ? $article['description']
                                    : 'Testbeschreibung';

                                $data[$index] = [
                                    'name' => $article['name'],
                                    'descriptionLong' => $description,
                                    'active' => true,
                                    'tax' => 19,
                                    'categories' => [
                                        [
                                            'id' => $categoryId
                                        ],
                                    ],
                                    'mainDetail' => [
                                        'instock' => 0,
                                        'lastStock' => 1,
                                        'number' => $article['ordernumber'],
                                        'ean' => $article['ean'],
                                        'active' => true,
                                        'purchasePrice' => $article['purchasePrice'],
                                        'prices' => [
                                            [
                                                'customerGroupKey' => 'EK',
                                                'price' => $article['priceRecommended'],
                                            ],
                                        ],
                                        'attribute' => [
                                            'dc' . ucfirst(strtolower($shortname)) . 'Instock' => $article['quantity'],
                                            'dc' . ucfirst(strtolower($shortname)) . 'Active' => true,
                                            'dc' . ucfirst(strtolower($shortname)) . 'PurchasingPrice' => $article['purchasePrice'],
                                            'dc' . ucfirst(strtolower($shortname)) . 'PriceRecommended' => $article['priceRecommended'],
                                            'dc' . ucfirst(strtolower($shortname)) . 'Ordernumber' => $article['ordernumber'],
                                            'dc' . ucfirst(strtolower($shortname)) . 'Articlename' => $article['name'],
                                            'dc' . ucfirst(strtolower($shortname)) . 'Description' => $description,
                                            'dc' . ucfirst(strtolower($shortname)) . 'OverridePrice' => false,
                                            'dc' . ucfirst(strtolower($shortname)) . 'OverrideName' => false,
                                        ]
                                    ]
                                ];
                            }

                            if (array_key_exists('image', $article) && !is_array($article['image'])) {
                                $data[$index]['images'] = [
                                    [
                                        'link' => $article['image'],
                                        'description' => $article['name']
                                    ]
                                ];
                            } else {
                                $data[$index]['images'] = [
                                    [
                                        'link' => 'https://dropshippers-companion.de/wp-content/uploads/2020/02/dc_mit-dropshipping-starten.jpg',
                                        'description' => $article['name']
                                    ]
                                ];
                            }

                            if (array_key_exists('manufacturer', $article) && !is_array($article['manufacturer'])) {
                                $data[$index]['supplier'] = $article['manufacturer'];
                            } else {
                                $data[$index]['supplier'] = $wholesaler->getBasename();
                            }
                        }

                        $current++;
                    }
                }

                $response = $this->put('articles', ['json' => $data]);
//

//                $article = $data[array_keys($data)[0]];

//                $response = $this->put('articles', ['json' => $article]);

                $result = [
                    'data' => $data,
//                    'baseurl' => $response->getEffectiveUrl(),
//                    'options' => $this->options,
//                    'code' => $response->getStatusCode(),
//                    'message' => json_decode($response->getBody()->getContents(), true)
                ];

//                $output->writeln('<info>'.print_r($response->getBody()->getContents(), true).'</info>');

            }
        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
//            $output->writeln('<comment>'.print_r($e->getTraceAsString(), true).'</comment>');
        }
    }
}