<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 28/12/2020 14:28
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services\Decorates;

use Shopware\Components\Model\ModelManager;
use SwagBackendOrder\Components\Order\Struct\OrderStruct;
use SwagBackendOrder\Components\Order\Validator\Constraints\LastStock;
use SwagBackendOrder\Components\Order\Validator\OrderValidator as OriginalService;
use SwagBackendOrder\Components\Order\Validator\OrderValidatorInterface;
use SwagBackendOrder\Components\Order\Validator\ValidationResult;
use SwagBackendOrder\Components\Order\Validator\Validators\ProductValidator;
use Shopware_Components_Snippet_Manager;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;

/**
 * Class OrderValidator
 * @package WundeDcompanion\Services\Decorates
 */
class OrderValidator implements OrderValidatorInterface
{
    /**
     * @var OriginalService $originalService
     */
    private $originalService;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Shopware_Components_Snippet_Manager
     */
    private $snippetManager;

    /**
     * @var ProductValidator
     */
    private $productValidator;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * OrderValidator constructor.
     * @param OriginalService $service
     * @param ProductValidator $productValidator
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param Shopware_Components_Snippet_Manager $snippetManager
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(
        OriginalService $service,
        ProductValidator $productValidator,
        ModelManager $em,
        LoggerService $logger,
        Shopware_Components_Snippet_Manager $snippetManager,
        WholesalerRepository $wholesalerRepository
    )
    {
        $this->originalService = $service;
        $this->em = $em;
        $this->logger = $logger;
        $this->snippetManager = $snippetManager;
        $this->productValidator = $productValidator;
        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * @param OrderStruct $order
     * @return ValidationResult
     */
    public function validate(OrderStruct $order)
    {
        $result = $this->originalService->validate($order);

        $violations = $result->getMessages();

        $positions = $order->getPositions();

        foreach ($positions as $position)
        {
            $ordernumber = $position->getNumber();

            $wholesalerStocks = array_sum(array_map(function($wholesaler) use($ordernumber) {
                $article = $wholesaler->articles()->findByOrdernumber($ordernumber);

                return !is_null($article) && isset($article['dc_' . $wholesaler->getShortname() . '_instock'])
                    ? $article['dc_' . $wholesaler->getShortname() . '_instock']
                    : 0;
            }, $this->wholesalers));

            $lastStock = new LastStock();

            /** @var Shopware_Components_Snippet_Manager $snippetManager */
            $snippetManager = Shopware()->Container()->get('snippets');
            $snippets = $snippetManager->getNamespace($lastStock->namespace);
            $snippet = $snippets->get($lastStock->snippet, '');

            $message = sprintf($snippet, $ordernumber);

            if ($wholesalerStocks > 0 && in_array($message, $violations) && ($key = array_search($message, $violations)) !== false) {
                unset($violations[$key]);
                $violations = array_values($violations);
            }
        }

        $result->setMessages($violations);

        return $result;
    }
}