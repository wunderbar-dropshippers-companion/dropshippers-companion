<?php

namespace WundeDcompanion\Commands;

use DateTime;
use DirectoryIterator;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query\Expr;
use Exception;
use Shopware\Commands\ShopwareCommand;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\CustomModels\ViisonPickwareERP\Warehouse\Warehouse;
use Shopware\Models\Article\Detail;
use Shopware\Models\Order\Order;
use Shopware\Models\Plugin\Plugin;
use Shopware\Plugins\ViisonPickwareERP\Components\StockLedger\ArticleDetailStockInfoProviderService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Bundle\OrderBundle\SortService;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\ConfigService;
use Shopware\Models\Order\Detail as Position;
use WundeDcompanion\Services\PickwareService;
use function Clue\StreamFilter\fun;

/**
 * Class Test
 * @package WundeDcompanion\Commands
 */
class Test extends ShopwareCommand
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var SortService $sortService
     */
    private $sortService;

    /**
     * @var PickwareService $pickwareService
     */
    private $pickwareService;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * @var Expr $expr
     */
    private $expr;

    /**
     * Test constructor.
     * @param ModelManager $em
     * @param Logger $logger
     * @param SortService $sortService
     * @param PickwareService $pickwareService
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(
        ModelManager $em,
        Logger $logger,
        SortService $sortService,
        PickwareService $pickwareService,
        WholesalerRepository $wholesalerRepository
    )
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $logger;
        $this->sortService = $sortService;
        $this->pickwareService = $pickwareService;
        $this->wholesalers = $wholesalerRepository->findAllActive();

        $this->expr = $em->getExpressionBuilder();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dc:test')
            ->setDescription('make a functionality test')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

    private function clearConsole()
    {
        echo sprintf("\033\143");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->clearConsole();

        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}