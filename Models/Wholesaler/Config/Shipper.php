<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/09/2020 23:44
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler\Config;


/**
 * Class Shipper
 * @package WundeDcompanion\Models\Wholesaler\Config
 */
class Shipper
{

    /**
     * @var string $company
     */
    private $company = '';

    /**
     * @var string $company2
     */
    private $company2 = '';

    /**
     * @var string $firstname
     */
    private $firstname = '';

    /**
     * @var string $lastname
     */
    private $lastname = '';

    /**
     * @var string $street
     */
    private $street = '';

    /**
     * @var string $zip
     */
    private $zip = '';

    /**
     * @var string $city
     */
    private $city = '';

    /**
     * @var string $countrycode
     */
    private $countrycode = '';

    /**
     * @var string $email
     */
    private $email = '';

    /**
     * @var string $phone
     */
    private $phone = '';

    /**
     * Shipper constructor.
     * @param string $company
     * @param string $company2
     * @param string $firstname
     * @param string $lastname
     * @param string $street
     * @param string $zip
     * @param string $city
     * @param string $countrycode
     * @param string $email
     * @param string $phone
     */
    public function __construct(
        $company = '',
        $company2 = '',
        $firstname = '',
        $lastname = '',
        $street = '',
        $zip = '',
        $city = '',
        $countrycode = '',
        $email = '',
        $phone = ''
    )
    {
        $this->company = $company;
        $this->company2 = $company2;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->street = $street;
        $this->zip = $zip;
        $this->city = $city;
        $this->countrycode = $countrycode;
        $this->email = $email;
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function company()
    {
        return $this->company;
    }

    /**
     * @return string
     */
    public function company2()
    {
        return $this->company2;
    }

    /**
     * @return string
     */
    public function firstname()
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function lastname()
    {
        return $this->lastname;
    }

    /**
     * @return string
     */
    public function street()
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function zip()
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function city()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function countrycode()
    {
        return $this->countrycode;
    }

    /**
     * @return string
     */
    public function email()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function phone()
    {
        return $this->phone;
    }
}