<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 14:58
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Database;


use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\Mapping\ClassMetadata;
use Shopware\Components\Model\ModelManager;

/**
 * Class Database
 * @package WundeDcompanion\Helpers
 */
abstract class Database
{

    /**
     * @return Table[]
     */
    public static function getTables()
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');
        $schemaManager = $em->getConnection()->getSchemaManager();

        return $schemaManager->listTables();
    }

    /**
     * @return AttributeTableManager
     */
    public static function getAttributeTables()
    {
        return new AttributeTableManager();
    }

    /**
     * @param string $entity
     * @return ClassMetadata
     */
    public static function getMetaData(string $entity)
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');
        return $em->getClassMetadata($entity);
    }

    /**
     * @param string $entity
     * @return bool
     */
    public static function entityTableExists(string $entity)
    {
        return self::tableExists(self::getMetaData($entity)->getTableName());
    }

    /**
     * @param string[] $entities
     * @return bool
     */
    public static function entityTablesExists(array $entities)
    {
        $tables = array_map(function ($entity) {
            return self::getMetaData($entity)->getTableName();
        }, $entities);

        return self::tablesExists($tables);
    }

    /**
     * @param string $table
     * @return bool
     */
    public static function tableExists(string $table)
    {
        return self::tablesExists([$table]);
    }

    /**
     * @param array $tables
     * @return bool
     */
    public static function tablesExists($tables = [])
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');
        $schemaManager = $em->getConnection()->getSchemaManager();

        return $schemaManager->tablesExist($tables);
    }
}