{include file="string:{config name=emailheaderplain}"}

-- Dropshippers Companinon Statusmail - BESTELLSTATUS --

Hallo,

die Bestellung {$orderNumber} wurde an den/die Großhändler übermittelt.

{if $status == 'OK'}
Der Status der Übermittlung lautet: Der Dropshipping-Auftrag wurde erfolgreich ausgeführt ({$orderInfo.status}).

Datum der Übertragung: {$orderInfo.date}
Dropship-Bestellnummer: {$orderInfo.orderId}
Dropship-ID: {$orderInfo.dropshipId}
{else}
Der Status der Übermittlung lautet: <b>Es ist ein Fehler bei der Übermittlung des Dropship-Auftrages aufgetreten ({$orderInfo.status}).</b>

Datum der Übertragung: {$orderInfo.date}
Meldung des Großhändlers: {$orderInfo.message}
Fehlercode / Grund des Abbruchs:  {$orderInfo.info}

*Bitte kontrollieren Sie den Vorgang und setzen Sie sich gegebenenfalls mit Ihrem Großhändler in Verbindung.*
{/if}

Nachfolgend eine Übersicht der Bestellung zu Kontrollzwecken:

{foreach $articles as $article}
Ihre Artikelnummer: {$article.articleordernumber}
Bestellmenge: {$article.quantity}

Bestellung ausgeführt bei: {$article.dc_name_long}
Artikelnr. des Großhändlers: {$article.articleNumber}
{/foreach}