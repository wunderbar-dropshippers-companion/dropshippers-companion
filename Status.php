<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 19/02/2021 11:12
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion;


class Status
{

    /**
     * Consts defining order states
     */
    const ORDER_STATE_CANCELLED = -1;
    const ORDER_STATE_OPEN = 0;
    const ORDER_STATE_IN_PROCESS = 1;
    const ORDER_STATE_COMPLETED = 2;
    const ORDER_STATE_PARTIALLY_COMPLETED = 3;
    const ORDER_STATE_CANCELLED_REJECTED = 4;
    const ORDER_STATE_READY_FOR_DELIVERY = 5;
    const ORDER_STATE_PARTIALLY_DELIVERED = 6;
    const ORDER_STATE_COMPLETELY_DELIVERED = 7;
    const ORDER_STATE_CLARIFICATION_REQUIRED = 8;

    /**
     * Consts defining payment states
     */
    const PAYMENT_STATE_PARTIALLY_INVOICED = 9;
    const PAYMENT_STATE_COMPLETELY_INVOICED = 10;
    const PAYMENT_STATE_PARTIALLY_PAID = 11;
    const PAYMENT_STATE_COMPLETELY_PAID = 12;
    const PAYMENT_STATE_1ST_REMINDER = 13;
    const PAYMENT_STATE_2ND_REMINDER = 14;
    const PAYMENT_STATE_3RD_REMINDER = 15;
    const PAYMENT_STATE_ENCASHMENT = 16;
    const PAYMENT_STATE_OPEN = 17;
    const PAYMENT_STATE_RESERVED = 18;
    const PAYMENT_STATE_DELAYED = 19;
    const PAYMENT_STATE_RE_CREDITING = 20;
    const PAYMENT_STATE_REVIEW_NECESSARY = 21;
    const PAYMENT_STATE_NO_CREDIT_APPROVED = 30;
    const PAYMENT_STATE_THE_CREDIT_HAS_BEEN_PRELIMINARILY_ACCEPTED = 31;
    const PAYMENT_STATE_THE_CREDIT_HAS_BEEN_ACCEPTED = 32;
    const PAYMENT_STATE_THE_PAYMENT_HAS_BEEN_ORDERED = 33;
    const PAYMENT_STATE_A_TIME_EXTENSION_HAS_BEEN_REGISTERED = 34;
    const PAYMENT_STATE_THE_PROCESS_HAS_BEEN_CANCELLED = 35;

    public function handleOrder() {
        



        if ($statusId === Status::PAYMENT_STATE_COMPLETELY_PAID && (
                $statusId === Status::ORDER_STATE_OPEN  ||
                $statusId === Status::ORDER_STATE_IN_PROCESS ||
                $statusId === Status::ORDER_STATE_PARTIALLY_COMPLETED
            )
        ) {
            // Bestellung wird abgearbeitet
        }
    }
}