import DefaultProperties from '../../../data/ticket/priorities'

export default {
  namespaced: true,
  state: {
    priorities: []
  },
  getters: {
    getPriorities: (state) => state.priorities
  },
  mutations: {
    setPriorities: (state, priorities) => state.priorities = priorities,
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getTicketPriorities'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let priorities = response.data.hasOwnProperty('priorities') ? response.data.priorities : [];

          if (success) {
            commit('setPriorities', priorities)
            return priorities
          } else {
            throw new Error();
          }
        })
        .catch(() => {
          console.warn('Priorities could not be loaded')
          commit('setPriorities', DefaultProperties)
          return DefaultProperties
        })
    },
  }
}
