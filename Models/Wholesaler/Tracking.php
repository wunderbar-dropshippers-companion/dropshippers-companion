<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 22/03/2021 11:08
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler;

use WundeDcompanion\Models\Order\Carrier;

/**
 * Class Tracking
 * @package WundeDcompanion\Models\Wholesaler
 */
class Tracking
{
    /**
     * @var string $code
     */
    private $code;

    /**
     * @var string $carrier
     */
    private $carrier;

    /**
     * @var Carrier[] $carriers
     */
    private $carriers;

    /**
     * Tracking constructor.
     * @param Carrier[] $carriers
     */
    public function __construct(array $carriers)
    {
        $this->carriers = $carriers;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Tracking
     */
    public function setCode(string $code): Tracking
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarrier(): string
    {
        return $this->carrier;
    }

    /**
     * @param string $carrier
     * @return Tracking
     */
    public function setCarrier(string $carrier): Tracking
    {
        $this->carrier = $carrier;
        return $this;
    }

    /**
     * @return string
     */
    public function getCarrierUrl(): string
    {
        return array_key_exists($this->carrier, $this->carriers)
            ? $this->carriers[$this->carrier]->getDomain()
            : '#';
    }

    /**
     * @return string
     */
    public function getTrackingUrl(): string
    {
        return array_key_exists($this->carrier, $this->carriers)
            ? $this->carriers[$this->carrier]->getTrackingUrl($this->code)
            : '#';
    }
}