<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:46
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use Doctrine\Common\EventSubscriber;
use Exception;
use Shopware\Components\Logger;
use Doctrine\ORM\Events;
use WundeDcompanion\Models\Change\Change;
use WundeDcompanion\Models\Change\ChangeSet;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use DateTime;

class ModelSubscriber implements EventSubscriber
{
    const EXCLUDE_TABLES = [
        's_dcompanion_changes',
        's_dcompanion_changesets',
        's_dcompanion_changeet_changes'
    ];

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var ChangeSet[] $changeSets
     */
    private $changeSets = [];

    /**
     * @var array $watches
     */
    private $watches;

    /**
     * @var int $maxAge
     */
    private $maxAge;

    /**
     * RouteSubscriber constructor.
     * @param Logger $logger
     * @param array $watches
     * @param string $maxAge
     */
    public function __construct($logger, $watches, $maxAge)
    {
        $this->logger = $logger;
        $this->watches = $watches;
        $this->maxAge = (int)$maxAge;
    }

    /**
     * Event subscriber
     *
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate,
            Events::postFlush
        ];
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        try {
            $entity = $args->getEntity();
            $em = $args->getEntityManager();
            $meta = $em->getClassMetadata(get_class($entity));
            $table = $meta->getTableName();

            if (!in_array($table, self::EXCLUDE_TABLES) && array_key_exists($table, $this->watches)) {

                $changes = [];

                foreach ($args->getEntityChangeSet() as $column => $values) {
                    if ($values[0] != $values[1] && in_array($column, $this->watches[$table])) {

                        $changes[] = new Change(
                            $column,
                            $values[0],
                            $values[1]
                        );
                    }
                }

                if (!empty($changes)) {
                    $recordId = method_exists($entity, 'getId')
                        ? $entity->getId()
                        : 0;

                    $this->changeSets[] = new ChangeSet($table, $recordId, $changes);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        try {
            if (!empty($this->changeSets)) {
                $em = $args->getEntityManager();

                /** @var ChangeSet[] $changeSets */
                $changeSets = $em->getRepository(ChangeSet::class)->findAll();

                foreach ($changeSets as $changeSet)
                {
                    if ($changeSet->getCreatedAt()->diff(new DateTime())->days > $this->maxAge) {
                        $em->remove($changeSet);
                    }
                }

                foreach ($this->changeSets as $key => $changeSet) {
                    $em->persist($changeSet);
                    unset($this->changeSets[$key]);
                }

                $em->flush();
                $em->clear();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}