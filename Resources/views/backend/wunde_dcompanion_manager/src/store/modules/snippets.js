import {i18n, setI18nLanguage } from "../../plugins";

export default {
  namespaced: true,
  state: {
    privacy: {
      locale: 'en',
      text: ''
    },
    imprint: {
      locale: 'en',
      text: ''
    }
  },
  getters: {
    getPrivacyLocale: (state) => state.privacy.locale,
    getPrivacyText: (state) => state.privacy.text,
    getImprintLocale: (state) => state.privacy.locale,
    getImprintText: (state) => state.privacy.text
  },
  mutations: {
    setPrivacyLocale: (state, locale) => state.privacy.locale = locale,
    setPrivacyText: (state, text) => state.privacy.text = text,
    setImprintLocale: (state, locale) => state.imprint.locale = locale,
    setImprintText: (state, text) => state.imprint.text = text
  },
  actions: {
    imprint({rootGetters, commit, getters}) {
      if (getters['getImprintText'].length === 0 || getters['getImprintLocale'] !== i18n.locale) {
        return axios.get(rootGetters.getUrl('getImprint'))
          .then(response => {
            let success = response.data.hasOwnProperty('success') ? response.data.success : false;
            let snippets = response.data.hasOwnProperty('snippets') ? response.data.snippets : [];

            if (success) {
              if (snippets.hasOwnProperty(i18n.locale)) {
                commit('setImprintText', snippets[i18n.locale])
                commit('setImprintLocale', i18n.locale)
                return snippets[i18n.locale]
              } else if (snippets.hasOwnProperty('en')) {
                commit('setImprintText', snippets['en'])
                commit('setImprintLocale', 'en')
                return snippets['en']
              }
            }

            return getters['getImprintText']
          })
          .catch(error => {
            console.warn(error.message)
          })
      }

      return getters['getImprintText']
    },
    privacy({rootGetters, commit, getters}) {
      if (getters['getPrivacyText'].length === 0 || getters['getPrivacyLocale'] !== i18n.locale) {
        return axios.get(rootGetters.getUrl('getPrivacy'))
          .then(response => {
            let success = response.data.hasOwnProperty('success') ? response.data.success : false;
            let snippets = response.data.hasOwnProperty('snippets') ? response.data.snippets : [];

            if (success) {
              if (snippets.hasOwnProperty(i18n.locale)) {
                commit('setPrivacyText', snippets[i18n.locale])
                commit('setPrivacyLocale', i18n.locale)
                return snippets[i18n.locale]
              } else if (snippets.hasOwnProperty('en')) {
                commit('setPrivacyText', snippets['en'])
                commit('setPrivacyLocale', 'en')
                return snippets['en']
              }
            }

            return getters['getPrivacyText']
          })
          .catch(error => {
            console.warn(error.message)
          })
      }

      return getters['getPrivacyText']
    },
    load({rootGetters}) {
      return axios.get(rootGetters.getUrl('getSnippets'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let messages = response.data.hasOwnProperty('messages') ? response.data.messages : [];
          let fallback = response.data.hasOwnProperty('fallback') ? response.data.fallback : 'en';
          let locale = response.data.hasOwnProperty('locale') ? response.data.locale : 'en';

          if (success) {
            i18n.locale = locale
            i18n.fallbackLocale = fallback
            i18n.setLocaleMessage(locale, messages)
            setI18nLanguage(locale)
          } else {
            console.error('snippets could not be loaded')
          }
        })
        .catch(() => {
          console.warn('snippets could not be loaded')
        })
    },
  }
}
