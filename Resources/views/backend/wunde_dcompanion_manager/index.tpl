<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>DC Manager</title>
        <link rel="stylesheet" href="{link file="backend/_resources/css/dc-manager.css"}" >

    </head>
    <body>
        <div id="app"></div>
        <script type="text/javascript" src="{link file="backend/base/frame/postmessage-api.js"}"></script>
        <script type="text/javascript" src="{link file="backend/_resources/js/dc-manager.js"}"></script>
    </body>
</html>