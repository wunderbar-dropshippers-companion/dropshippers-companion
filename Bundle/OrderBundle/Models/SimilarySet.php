<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 22:28
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle\Models;


use WundeDcompanion\Bundle\OrderBundle\Interfaces\PermutableInterface;

/**
 * Class SimilarySet
 * @package WundeDcompanion\Bundle\OrderBundle\Models
 */
class SimilarySet implements PermutableInterface
{
    /**
     * @var Similary[] $similaries
     */
    private $similaries;

    /**
     * SimilarySet constructor.
     * @param Similary[] $similaries
     */
    public function __construct(array $similaries)
    {
        $this->similaries = $similaries;
    }

    /**
     * @return Similary[]
     */
    public function getSimilaries()
    {
        return $this->similaries;
    }

    /**
     * @return Similary[]
     */
    public function getItems()
    {
        return $this->getSimilaries();
    }
}