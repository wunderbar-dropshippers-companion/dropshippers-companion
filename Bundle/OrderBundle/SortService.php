<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 20:24
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle;

use Doctrine\DBAL\ConnectionException;
use Exception;
use Shopware\Models\Attribute\OrderDetail;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\DetailStatus;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use WundeDcompanion\Bundle\AbstractService;
use WundeDcompanion\Bundle\OrderBundle\Models\Combination;
use WundeDcompanion\Bundle\OrderBundle\Models\Similary;

/**
 * Class SortService
 * @package WundeDcompanion\Bundle\OrderBundle
 */
class SortService extends AbstractService
{

    /**
     * @param Order $order
     * @return Position[]
     */
    private function getMergedPositions(Order $order): array
    {
        $positionSets = [];
        $positions = [];

        /**
         * @var int $key
         * @var Position $position
         */
        foreach ($order->getDetails()->getValues() as $key => $position)
        {
            $articleDetailId = $position->getArticleDetail()->getId();

            $positionSets[$articleDetailId][] = $position;
        }

        /** @var Position[] $positionSet */
        foreach ($positionSets as $positionSet)
        {
            $master = array_shift($positionSet);

            foreach ($positionSet as $position)
            {
                $master->setQuantity($master->getQuantity() + $position->getQuantity());

                try {
                    $this->em->remove($position);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), $e);
                }
            }

            $positions[] = $master;

            try {
                $this->em->flush();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }

        return $positions;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function sort(Order $order)
    {
        if (
            $this->config->autoOrder() &&
            !$this->orderIsComplete($order) &&
            !$this->transmissionAttemptLimitIsReached($order)
        ) {
            $mergedPositions = $this->getMergedPositions($order);

            $similarySets = array_map(function($position) {
                return $this->similaryService->getPositionSimilaries($position);
            }, $mergedPositions);

            $optimalCombination = $this->combinationService->getOptimalCombination($similarySets, $order);

            return $this->saveOptimalCombination($optimalCombination, $order, $mergedPositions);
        }

        return $order;
    }

    /**
     * @param Combination $optimalCombination
     * @param Order $order
     * @param Position[] $mergedPositions
     * @return Order
     */
    private function saveOptimalCombination(Combination $optimalCombination, Order $order, array $mergedPositions): Order
    {
        $positions = [];
        $similaries = $optimalCombination->getSimilaries();
        $dropshipActive = $this->hasDropshippingArticles($similaries);

        $dropshipStatus = $dropshipActive ? 0 : null;

        $order->getAttribute()->setDcDropshipActive($dropshipActive);
        $order->getAttribute()->setDcDropshipStatus($dropshipStatus);

        foreach ($mergedPositions as $mergedPosition)
        {
            try {
                $this->em->remove($mergedPosition);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }

        /** @var Position $position */
        foreach ($order->getDetails()->getValues() as $position)
        {
            try {
                $this->em->remove($position);
                $this->em->flush();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }

        foreach ($similaries as $similary)
        {
            $positionDropshipStatus = !is_null($similary->getWholesaler()) ? 0 : null;

            $attribute = new OrderDetail();
            $attribute->setDcNameShort($similary->getContext());
            $attribute->setDcStock($similary->getDcInstock());
            $attribute->setDcDropshipStatus($positionDropshipStatus);

            if (!is_null($similary->getWholesaler())) {
                $attribute->setDcNameLong($similary->getWholesaler()->getLongname());
            }

            $position = new Position();
            $position->setOrder($order);
            $position->setNumber($order->getNumber());
            $position->setArticleId($similary->getArticleId());
            $position->setArticleNumber($similary->getOrdernumber());
            $position->setArticleName($similary->getArticleName());
            $position->setArticleDetail($similary->getArticleDetail());
            $position->setPrice($similary->getTotalPrice());
            $position->setQuantity($similary->getQuantity());
            $position->setStatus($similary->getDetailStatus());
            $position->setEan($similary->getEan());
            $position->setTax($similary->getTax());
            $position->setTaxRate($similary->getTax()->getTax());
            $position->setAttribute($attribute);

            try {
                $this->em->persist($position);
                $positions[] = $position;
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }

        $order->setDetails($positions);

        $orderAttribute = $order->getAttribute();

        if (method_exists($orderAttribute, 'setDcSorted')) {
            $orderAttribute->setDcSorted(true);
        }

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return $order;
    }

    /**
     * @param Similary[] $similaries
     * @return bool
     */
    private function hasDropshippingArticles(array $similaries)
    {
        foreach ($similaries as $similary)
        {
            if (!is_null($similary->getWholesaler())) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function orderIsComplete(Order $order)
    {
        return $order->getOrderStatus()->getId() === Status::ORDER_STATE_COMPLETED;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function paymentIsComplete(Order $order)
    {
        return $order->getPaymentStatus()->getId() === Status::PAYMENT_STATE_COMPLETELY_PAID;
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function transmissionAttemptLimitIsReached(Order $order)
    {
        return method_exists($order->getAttribute(), 'getDcDropshipTransmissionAttempt')
            ? $order->getAttribute()->getDcDropshipTransmissionAttempt() >= 3
            : false;
    }
}