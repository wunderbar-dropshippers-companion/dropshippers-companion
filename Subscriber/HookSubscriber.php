<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:46
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use Doctrine\DBAL\Driver\ResultStatement;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\QueryException;
use Enlight\Event\SubscriberInterface;
use Enlight_Hook_HookArgs;
use Exception;
use PDO;
use Shopware\Bundle\AttributeBundle\Service\ConfigurationStruct;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Article;
use Shopware\Models\Article\Detail;
use Shopware\Models\Config\Element;
use Shopware\Models\Config\Form;
use Shopware\Models\Config\Value;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use Shopware_Controllers_Backend_Config;
use SwagBackendOrder\Components\Order\Validator\Constraints\LastStock;
use WundeDcompanion\Bundle\OrderBundle\OrderService;
use WundeDcompanion\Bundle\OrderBundle\SortService;
use WundeDcompanion\Models\Core\Config;
use WundeDcompanion\Models\Order\Trackingcode;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\ArticleService;
use WundeDcompanion\Services\CronService;
use WundeDcompanion\Services\LoggerService;
use Shopware_Controllers_Backend_Order;
use Shopware_Controllers_Backend_AttributeData;
use Shopware\Models\Article\Detail as ArticleDetail;
use Doctrine\DBAL\Connection;
use Shopware_Controllers_Backend_Base;
use Shopware_Components_Snippet_Manager;
use WundeDcompanion\Services\PickwareService;
use Shopware_Controllers_Frontend_Detail;
use Shopware_Controllers_Frontend_Listing;
use Shopware_Controllers_Frontend_Checkout;
use Shopware_Controllers_Backend_SwagBackendOrder;
use Shopware_Controllers_Backend_Application;
use Shopware_Controllers_Frontend_Payment;
use Shopware_Controllers_Frontend_PaypalUnified;
use Shopware_Controllers_Backend_CanceledOrder;
use ArrayObject;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedEvent;

/**
 * Class HookSubscriber
 * @package WundeDcompanion\Subscriber
 */
class HookSubscriber implements SubscriberInterface
{

    const PAYPAL_HOOK = 'https://sw560-2.wmadc-dev.de/PaypalUnifiedWebhook/execute';

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var ArticleService $articleService
     */
    private $articleService;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Connection $connection
     */
    private $connection;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var string $pluginDir
     */
    private $pluginDir;

    /**
     * @var Shopware_Components_Snippet_Manager $snippetManager
     */
    private $snippetManager;

    /**
     * @var PickwareService $pickwareService
     */
    private $pickwareService;

    /**
     * @var Expr $expr
     */
    private $expr;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * @var CronService $cron
     */
    private $cron;

    /**
     * @var SortService $sortService
     */
    private $sortService;

    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * @var array $colors
     */
    private $colors = [
        'active' => '#32DC32',
        'inactive' => '#CCCCCC'
    ];

    /**
     * RouteSubscriber constructor.
     * @param LoggerService $logger
     * @param WholesalerRepository $wholesalerRepository
     * @param ArticleService $articleService
     * @param PickwareService $pickwareService
     * @param SortService $sortService
     * @param OrderService $orderService
     * @param CronService $cron
     * @param ModelManager $em
     * @param Connection $connection
     * @param Config $config
     * @param Shopware_Components_Snippet_Manager $snippetManager
     * @param string $pluginDir
     */
    public function __construct(
        LoggerService $logger,
        WholesalerRepository $wholesalerRepository,
        ArticleService $articleService,
        PickwareService $pickwareService,
        SortService $sortService,
        OrderService $orderService,
        CronService $cron,
        ModelManager $em,
        Connection $connection,
        Config $config,
        Shopware_Components_Snippet_Manager $snippetManager,
        string $pluginDir
    )
    {
        $this->logger = $logger;
        $this->wholesalerRepository = $wholesalerRepository;
        $this->articleService = $articleService;
        $this->pickwareService = $pickwareService;
        $this->sortService = $sortService;
        $this->orderService = $orderService;
        $this->cron = $cron;
        $this->em = $em;
        $this->connection = $connection;
        $this->config = $config;
        $this->pluginDir = $pluginDir;
        $this->snippetManager = $snippetManager;
        $this->expr = $em->getExpressionBuilder();
        $this->wholesalers = $wholesalerRepository->findAllActive();

        if (Shopware()->Container()->getParameterBag()->has('wunde_dcompanion.colors')) {
            $this->colors = Shopware()->Container()->getParameter('wunde_dcompanion.colors');
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Shopware_Controllers_Backend_ArticleList::filterAction::after' => 'onBackendArticleListFilterHook',
            'Shopware_Controllers_Backend_Article::detailListAction::after' => 'onBackendArticleDetailListHook',
            'Shopware_Controllers_Backend_Order::savePositionAction::before' => 'onBackendOrderSavePositionBeforeHook',
            'Shopware_Controllers_Backend_Order::savePositionAction::after' => 'onBackendOrderSavePositionAfterHook',
            'Shopware_Controllers_Backend_Order::getList::after' => 'onBackendOrderGetListAfterHook',
            'Shopware_Controllers_Backend_AttributeData::listAction::after' => 'onBackendAttributeDataListAfterHook',
            'Shopware_Controllers_Backend_Base::getVariantsAction::after' => 'onBackendBaseGetVariantsAfterHook',
            'Shopware_Controllers_Backend_Config::getFormAction::after' => 'onBackendConfigGetFormAfter',

            'Shopware_Controllers_Frontend_Checkout::ajaxAddArticleCartAction::before' => 'onFrontendCheckoutAjaxAddArticleCartBefore',
            'Shopware_Controllers_Frontend_Checkout::ajaxAddArticleCartAction::after' => 'onFrontendCheckoutAjaxAddArticleCartAfter',
//            'Shopware_Controllers_Frontend_Checkout::confirmAction::before' => 'onFrontendCheckoutConfirmBefore',
            'Shopware_Controllers_Frontend_Checkout::confirmAction::after' => 'onFrontendCheckoutConfirmAfter',
            'Shopware_Controllers_Frontend_Checkout::cartAction::after' => 'onFrontendCheckoutCartActionAfter',

            'Shopware_Controllers_Frontend_Checkout::finishAction::after' => 'onFrontendCheckoutFinishActionAfter',

//            'Shopware_Controllers_Backend_SwagBackendOrder::getProductAction::after' => 'onBackendSwagOrderGetProductAfter',
//            'Shopware_Controllers_Backend_SwagBackendOrder::validateEditAction::after' => 'onBackendSwagOrderValidateEditAfter',
//            'Shopware_Controllers_Backend_SwagBackendOrder::createOrderAction::after' => 'onBackendSwagOrderCreateOrderAfter',
        ];
    }

    public function onFrontendCheckoutFinishActionAfter()
    {
        $orderVariables = Shopware()->Session()['sOrderVariables']->getArrayCopy();
        $ordernumber = $orderVariables['sOrderNumber'];

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->findOneBy([
            'number' => $ordernumber
        ]);

        if (
            !is_null($order) &&
            $this->sortService->paymentIsComplete($order) &&
            !$this->sortService->orderIsComplete($order) &&
            !$this->sortService->transmissionAttemptLimitIsReached($order)
        ) {
            $order = $this->sortService->sort($order);

            if (
                !is_null($order) &&
                $this->sortService->orderIsComplete($order)
            ) {
                $this->orderService->send([$order]);
            }
        }
    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onFrontendCheckoutGetAvailableStockAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Frontend_Checkout $controller */
//        $controller = $args->getSubject();
//        $request = $controller->Request();
//        $return = $args->getReturn();
//
//        $ordernumber = $request->getParam('sAdd');
//
//        $wholesalerStocks = array_sum(array_map(function ($wholesaler) use($ordernumber) {
//            $article = $wholesaler->articles()->findByOrdernumber($ordernumber);
//
//            return !is_null($article) && isset($article['dc_' . $wholesaler->getShortname() . '_instock'])
//                ? (int)$article['dc_' . $wholesaler->getShortname() . '_instock']
//                : 0;
//        }, $this->wholesalers));
//
//        if (isset($return['instock'])) {
//            $return['instock'] += $wholesalerStocks;
//        }
//
//        $args->setReturn($return);
//    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onFrontendCheckoutCartActionAfter(Enlight_Hook_HookArgs  $args)
    {
        /** @var Shopware_Controllers_Frontend_Checkout $controller */
        $controller = $args->getSubject();
        $view = $controller->View();

        $basket = $view->getAssign('sBasket');

        if (is_array($basket) && isset($basket['content']) && is_array($basket['content'])) {
            foreach ($basket['content'] as &$position)
            {
                $sArticle = $position['additional_details'];

                $position['maxpurchase'] += array_sum(array_map(function ($wholesaler) use($sArticle) {
                    return isset($sArticle['dc_' . $wholesaler->getShortname() . '_instock'])
                        ? (int)$sArticle['dc_' . $wholesaler->getShortname() . '_instock']
                        : 0;
                }, $this->wholesalers));
            }

            $view->clearAssign('sBasket');
            $view->assign('sBasket', $basket);
        }
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onFrontendCheckoutConfirmAfter(Enlight_Hook_HookArgs  $args)
    {
        /** @var Shopware_Controllers_Frontend_Checkout $controller */
        $controller = $args->getSubject();
        $view = $controller->View();

        $sBasket = $view->getAssign('sBasket');
        $lastStock = $view->getAssign('sLaststock');

        if (isset($sBasket['content'])) {
            foreach ($sBasket['content'] as $key => &$val)
            {
                if (isset($val['instock']) && isset($val['additional_details'])) {
                    $sArticle = $val['additional_details'];

                    $val['instock'] += array_sum(array_map(function ($wholesaler) use($sArticle) {
                        return isset($sArticle['dc_' . $wholesaler->getShortname() . '_instock'])
                            ? (int)$sArticle['dc_' . $wholesaler->getShortname() . '_instock']
                            : 0;
                    }, $this->wholesalers));

                    $val['maxpurchase'] = $val['instock'];
                }
            }

            $view->clearAssign('sBasket');
            $view->assign('sBasket', $sBasket);
        }
    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onFrontendCheckoutCartAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Frontend_Checkout $controller */
//        $controller = $args->getSubject();
//        $view = $controller->View();
//
//        $sBasket = $view->getAssign('sBasket');
//
//        if (isset($sBasket['content'])) {
//            foreach ($sBasket['content'] as $key => &$val)
//            {
//                if (isset($val['instock']) && isset($val['additional_details'])) {
//                    $sArticle = $val['additional_details'];
//
//                    $val['instock'] += array_sum(array_map(function ($wholesaler) use($sArticle) {
//                        return isset($sArticle['dc_' . $wholesaler->getShortname() . '_instock'])
//                            ? (int)$sArticle['dc_' . $wholesaler->getShortname() . '_instock']
//                            : 0;
//                    }, $this->wholesalers));
//
//                    $val['maxpurchase'] = $val['instock'];
//                }
//            }
//        }
//
//        $view->clearAssign('sBasket');
//        $view->assign('sBasket', $sBasket);
//    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onFrontendCheckoutAjaxCartAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Frontend_Checkout $controller */
//        $controller = $args->getSubject();
//        $view = $controller->View();
//
//        $sBasket = $view->getAssign('sBasket');
//
//        if (isset($sBasket['content'])) {
//            foreach ($sBasket['content'] as $key => &$val)
//            {
//                if (isset($val['instock']) && isset($val['additional_details'])) {
//                    $sArticle = $val['additional_details'];
//
//                    $val['instock'] += array_sum(array_map(function ($wholesaler) use($sArticle) {
//                        return isset($sArticle['dc_' . $wholesaler->getShortname() . '_instock'])
//                            ? (int)$sArticle['dc_' . $wholesaler->getShortname() . '_instock']
//                            : 0;
//                    }, $this->wholesalers));
//                }
//            }
//        }
//
//        $view->clearAssign('sBasket');
//        $view->assign('sBasket', $sBasket);
//    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onFrontendCheckoutAjaxAddArticleCartBefore(Enlight_Hook_HookArgs  $args)
    {
        /** @var Shopware_Controllers_Frontend_Checkout $controller */
        $controller = $args->getSubject();
        $request = $controller->Request();

        $ordernumber = $request->getParam('sAdd');

        /** @var Detail $detail */
        $detail = $this->em->getRepository(Detail::class)->findOneBy([
            'number' => $ordernumber
        ]);

        if (!is_null($detail) && $detail instanceof Detail && $detail->getInStock() <= 0) {
            $wholesalerStocks = array_map(function ($wholesaler) use($detail) {
                $shortname = ucfirst(strtolower($wholesaler->getShortname()));
                $attribute = $detail->getAttribute();
                return method_exists($attribute, 'getDc' . $shortname . 'Instock')
                    ? $detail->getAttribute()->{'getDc' . $shortname . 'Instock'}()
                    : 0;
            }, $this->wholesalers);

            if (array_sum($wholesalerStocks) > 0) {
                try {
                    $request->setParam('lastStock', $detail->getLastStock());
                    $detail->setLastStock(false);
                    $this->em->flush();
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), $e);
                }
            }
        }
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onFrontendCheckoutAjaxAddArticleCartAfter(Enlight_Hook_HookArgs  $args)
    {
        /** @var Shopware_Controllers_Frontend_Checkout $controller */
        $controller = $args->getSubject();
        $request = $controller->Request();
        $view = $controller->View();

        $ordernumber = $request->getParam('sAdd');
        $lastStock = (int)$request->getParam('lastStock');

        $id = Shopware()->Modules()->Articles()->sGetArticleIdByOrderNumber($ordernumber);

        /** @var Detail $detail */
        $detail = $this->em->getRepository(Detail::class)->find($id);

        if (!is_null($detail) && $detail instanceof Detail && $detail->getLastStock() !== $lastStock) {
            try {
                $request->setParam('lastStock', $detail->getLastStock());
                $detail->setLastStock($lastStock);
                $this->em->flush();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }
    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onBackendSwagOrderCreateOrderAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Backend_SwagBackendOrder $controller */
//        $controller = $args->getSubject();
//        $request = $controller->Request();
//        $view = $controller->View();
//
//        $data = $request->getParam('data');
//        $violations = $view->getAssign('violations');
//        $success = $view->getAssign('success');
//
//        if (
//            isset($data['position']) &&
//            is_array($data['position']) &&
//            !is_null($violations) &&
//            is_array($violations) &&
//            !$success
//        ) {
//            $positions = $data['position'];
//
//            $lastStock = new LastStock();
//
//            /** @var Shopware_Components_Snippet_Manager $snippetManager */
//            $snippetManager = Shopware()->Container()->get('snippets');
//            $snippets = $snippetManager->getNamespace($lastStock->namespace);
//            $snippet = $snippets->get($lastStock->snippet, '');
//
//            foreach ($positions as $position)
//            {
//                if (isset($position['inStock']) && isset($position['articleNumber'])) {
//                    $stock = (int)$position['inStock'];
//                    $ordernumber = $position['articleNumber'];
//
//                    $message = sprintf($snippet, $ordernumber);
//
//                    if ($stock) {
//                        foreach ($violations as $key => $violation)
//                        {
//                            if (strpos($violation, $message) !== false) {
//                                unset($violations[$key]);
//                            }
//                        }
//                    }
//                }
//            }
//
//            if (empty($violations)) {
//                $view->clearAssign('success');
//                $view->assign('success', true);
//            }
//
//            $view->clearAssign('violations');
//            $view->assign('violations', $violations);
//        }
//    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onBackendSwagOrderValidateEditAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Backend_SwagBackendOrder $controller */
//        $controller = $args->getSubject();
//        $request = $controller->Request();
//        $view = $controller->View();
//
//        $ordernumber = $request->getParam('articleNumber', null);
//        $inStock = (int)$request->getParam('inStock', 0);
//        $violations = $view->getAssign('violations');
//
//        if (!is_null($violations) && is_array($violations) && $inStock > 0) {
//
//            $lastStock = new LastStock();
//
//            /** @var Shopware_Components_Snippet_Manager $snippetManager */
//            $snippetManager = Shopware()->Container()->get('snippets');
//            $snippets = $snippetManager->getNamespace($lastStock->namespace);
//            $message = sprintf($snippets->get($lastStock->snippet, ''), $ordernumber);
//
//            foreach ($violations as $key => $violation)
//            {
//                if (strpos($violation, $message) !== false) {
//                    unset($violations[$key]);
//                }
//            }
//
//            if (empty($violations)) {
//                $view->clearAssign('success');
//                $view->assign('success', true);
//            }
//
//            $view->clearAssign('violations');
//            $view->assign('violations', $violations);
//        }
//    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onBackendSwagOrderGetProductAfter(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Backend_SwagBackendOrder $controller */
//        $controller = $args->getSubject();
//        $view = $controller->View();
//
//        $data = $view->getAssign('data');
//
//        if (isset($data['id']) && isset($data['inStock']) && !$this->config->overrideStocks()) {
//
//            /** @var Detail $article */
//            $article = $this->em->getRepository(Detail::class)->find($data['id']);
//
//            if (!is_null($article)) {
//                $attributes = $article->getAttribute();
//
//                $data['inStock'] += array_sum(array_map(function ($wholesaler) use($attributes) {
//                    $shortname = ucfirst($wholesaler->getShortname());
//
//                    return method_exists($attributes, 'getDc' . $shortname . 'Instock')
//                        ? $attributes->{'getDc' . $shortname . 'Instock'}()
//                        : 0;
//                }, $this->wholesalers));
//
//                $view->clearAssign('data');
//                $view->assign('data', $data);
//            }
//        }
//    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onFrontendCheckoutAjaxAddArticleCartAfterHook(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Frontend_Checkout $controller */
//        $controller = $args->getSubject();
//        $request = $controller->Request();
//        $view = $controller->View();
//
//        $ordernumber = $request->getParam('sAdd');
//        $quantity = (int)$request->getParam('sQuantity');
//
//        try {
//            /** @var Detail $detail */
//            $detail = $this->em->getRepository(Detail::class)
//                ->createQueryBuilder('details')
//                ->select(['details'])
//                ->leftJoin('details.attribute', 'attribute')
//                ->where($this->expr->eq('details.number', ':ordernumber'))
//                ->setParameter('ordernumber', $ordernumber)
//                ->addSelect(['attribute'])
//                ->getQuery()
//                ->getOneOrNullResult();
//
//            $wholesalerStocks = array_map(function ($wholesaler) use($detail) {
//                $shortname = ucfirst(strtolower($wholesaler->getShortname()));
//                $attribute = $detail->getAttribute();
//                return method_exists($attribute, 'getDc' . $shortname . 'Instock')
//                    ? $detail->getAttribute()->{'getDc' . $shortname . 'Instock'}()
//                    : 0;
//            }, $this->wholesalers);
//
//            $totalStock = !$this->config->overrideStocks()
//                ? $detail->getInStock() + array_sum($wholesalerStocks)
//                : 0;
//
//            if ($totalStock >= $quantity) {
//                $view->clearAssign('basketInfoMessage');
//                $view->assign('basketInfoMessage', null);
//            }
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), $e);
//        }
//    }

//    /**
//     * @param Enlight_Hook_HookArgs $args
//     */
//    public function onFrontendDetailIndexAfterHook(Enlight_Hook_HookArgs  $args)
//    {
//        /** @var Shopware_Controllers_Frontend_Listing $controller */
//        $controller = $args->getSubject();
//        $view = $controller->View();
//
//        $article = $view->getAssign('sArticle');
//
//        /** @var Detail $detail */
//        $detail = $this->em->getRepository(Detail::class)->find($article['articleDetailsID']);
//
//        $wholesalerStocks = array_map(function ($wholesaler) use($detail) {
//            $shortname = ucfirst(strtolower($wholesaler->getShortname()));
//            $attribute = $detail->getAttribute();
//            return method_exists($attribute, 'getDc' . $shortname . 'Instock')
//                ? $detail->getAttribute()->{'getDc' . $shortname . 'Instock'}()
//                : 0;
//        }, $this->wholesalers);
//
//        $totalStock = !$this->config->overrideStocks()
//            ? $detail->getInStock() + array_sum($wholesalerStocks)
//            : 0;
//
//        $article['instock'] = $totalStock;
//        $article['isAvailable'] = $totalStock > 0;
//
//        $view->clearAssign('sArticle');
//        $view->assign('sArticle', $article);
//    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onBackendConfigGetFormAfter(Enlight_Hook_HookArgs  $args)
    {
        /** @var Shopware_Controllers_Backend_Config $controller */
        $controller = $args->getSubject();
        $view = $controller->View();
        $request = $controller->Request();

        $filters = $request->getParam('filter', []);

        foreach ($filters as $filter)
        {
            if (isset($filter['property']) && isset($filter['value']) && $filter['property'] === 'id') {
                $formId = (int)$filter['value'];
                $data = $view->getAssign('data');
                $elements = $data['elements'];
                $wholesalers = $this->wholesalerRepository->findAll();

                /** @var Form $form */
                $form = $this->em->getRepository(Form::class)->find($formId);

                foreach ($wholesalers as $wholesaler)
                {
                    if (!is_null($form->getPlugin()) && $wholesaler->getName() === $form->getPlugin()->getName()) {
                        foreach ($elements as &$element)
                        {
                            $options = $element['options'];

                            if ($this->pickwareService->isActive()) {
                                if (strpos($element['name'], 'pickware_warehousename') !== false) {
                                    $warehouses = $this->pickwareService->getWarehouses(true);

                                    $ids = array_column($warehouses, 'id');

                                    $flush = false;

                                    if (isset($element['value']) && !in_array($element['value'], $ids)) {
                                        /** @var Element $elementEntity */
                                        $elementEntity = $this->em->getRepository(Element::class)->find($element['id']);

                                        $elementEntity->setValue(0);
                                        $element['value'] = 0;
                                    }

                                    if (isset($element['values'])) {
                                        /** @var Value $value */
                                        foreach ($element['values'] as &$value)
                                        {
                                            if (!in_array($value['value'], $ids)) {
                                                /** @var Value $elementValue */
                                                $elementValue = $this->em->getRepository(Value::class)->find($value['id']);

                                                $elementValue->setValue(0);
                                                $value['value'] = 0;
                                            }
                                        }
                                    }

                                    if ($flush) {
                                        try {
                                            $this->em->flush();
                                        } catch (Exception $e) {
                                            $this->logger->error($e->getMessage(), $e);
                                        }
                                    }

                                    $options['editable'] = false;
                                    $options['displayField'] = 'displayName';
                                    $options['valueField'] = 'id';
                                    $options['store'] = [
                                        'fields' => [
                                            'id', 'displayName'
                                        ],
                                        'data' => $warehouses
                                    ];
                                }
                            }

                            $element['options'] = $options;
                        }

                        $data['elements'] = $elements;

                        $view->clearAssign('data');
                        $view->assign('data', $data);
                    }
                }
            }
        }
    }

    public function onBackendBaseGetVariantsAfterHook(Enlight_Hook_HookArgs $args)
    {
        /** @var Shopware_Controllers_Backend_Base $controller */
        $controller = $args->getSubject();
        $view = $controller->View();

        $variants = $view->getAssign('data');
        $total = $view->getAssign('total');
        $success = $view->getAssign('success');

        try {
            /** @var ArticleDetail[] $articleDetails */
            $articleDetails = $this->em->getRepository(ArticleDetail::class)
                ->createQueryBuilder('detail')
                ->indexBy('detail', 'detail.id')
                ->getQuery()
                ->getResult();

            $wholesalerVariants = [];

            foreach ($variants as &$variant)
            {
                $variant['purchasePrice'] = (float)$articleDetails[$variant['id']]->getPurchasePrice();
            }

            foreach ($this->wholesalers as $wholesaler)
            {
                $builder = $this->em->getConnection()->createQueryBuilder();

                $fields = [
                    'details.id',
                    'articles.name',
                    'articles.description',
                    'articles.active',
                    'attribute.dc_'.$wholesaler->getShortname().'_ordernumber as ordernumber',
                    'articles.id as articleId',
                    'attribute.dc_'.$wholesaler->getShortname().'_instock as inStock',
                    'supplier.name as supplierName',
                    'supplier.id as supplierId',
                    'details.additionalText',
                ];

                $builder->select($fields);
                $builder->from('s_articles_details', 'details');
                $builder->innerJoin('details', 's_articles', 'articles', 'details.articleID = articles.id');
                $builder->innerJoin('details', 's_articles_attributes', 'attribute', 'attribute.articledetailsID = details.id');
                $builder->innerJoin('articles', 's_articles_supplier', 'supplier', 'supplier.id = articles.supplierID');
                $builder->where(
                    $builder->expr()->eq(
                        'attribute.dc_'.$wholesaler->getShortname().'_active',
                        ':active'
                    )
                );
                $builder->setParameter(':active', true);

                $filters = $controller->Request()->getParam('filter', []);
                foreach ($filters as $filter) {
                    if ($filter['property'] === 'free') {
                        $builder->andWhere(
                            $builder->expr()->orX(
                                'attribute.dc_'.$wholesaler->getShortname().'_ordernumber LIKE :free',
                                'attribute.dc_'.$wholesaler->getShortname().'_articlename LIKE :free',
                                'details.ordernumber LIKE :free',
                                'articles.name LIKE :free',
                                'supplier.name LIKE :free'
                            )
                        );
                        $builder->setParameter(':free', $filter['value']);
                    } else {
                        $builder->addFilter($filter);
                    }
                }

                $properties = $this->prepareVariantParam($controller->Request()->getParam('sort', []), $fields);
                foreach ($properties as $property) {
                    $builder->addOrderBy($property['property'], $property['direction']);
                }

                $builder->setFirstResult($controller->Request()->getParam('start'))
                    ->setMaxResults($controller->Request()->getParam('limit'));

                /** @var ResultStatement $statement */
                $statement = $builder->execute();
                $result = $statement->fetchAll(PDO::FETCH_ASSOC);

                $variantOrdernumbers = array_column($variants, 'ordernumber');
                $wholesalerOrdernumbers = array_column($wholesalerVariants, 'ordernumber');

                $result = array_filter($result, function($item) use($variantOrdernumbers, $wholesalerOrdernumbers) {
                    return
                        !in_array($item['ordernumber'], $variantOrdernumbers) &&
                        !in_array($item['ordernumber'], $wholesalerOrdernumbers);
                });

                $wholesalerVariants = array_merge($wholesalerVariants, $result);

                $total += count($result);
            }

            $variants = array_merge($variants, $wholesalerVariants);

            $view->clearAssign();

            $view->assign('success', $success);
            $view->assign('data', $variants);
            $view->assign('total', $total);

        } catch (QueryException $e) {
            $this->logger->error($e->getMessage(), $e);
        }
    }

    public function onBackendOrderGetListAfterHook(Enlight_Hook_HookArgs $args)
    {
        $assigned = $args->getReturn();

        try {
            $orders = $assigned['data'];
            $orderAttributeColumns = [];

            $tables = json_decode(file_get_contents($this->pluginDir . '/Data/tables.json'), true);
            $coreTables = $tables['core'];

            if (array_key_exists('s_order_details_attributes', $coreTables)) {
                $orderAttributeColumns = array_column($coreTables['s_order_attributes'], 'name');
            }

            foreach ($orders as $orderKey => $order)
            {
                $activeColor = $this->colors['active'];
                $inactiveColor = $this->colors['inactive'];

                $orderAttributes = $this->connection->createQueryBuilder()
                    ->select($orderAttributeColumns)
                    ->from('s_order_attributes')
                    ->where('id = :id')
                    ->setParameter('id', $order['id'])
                    ->execute()
                    ->fetch();

                $orders[$orderKey] = array_merge($order, $orderAttributes);
                $orders[$orderKey]['dc_auto_order'] = $this->config->autoOrder();

                foreach ($order['details'] as $detailKey => $detail)
                {
                    $orderDetailAttributes = $detail['attribute'];

                    $inStock = 0;
                    $purchasePrice = 0;
                    $longname = null;
                    $active = false;

                    if ($wholesaler = $this->wholesalerRepository->findByShortname($orderDetailAttributes['dcNameShort'])) {
                        $active = true;

                        $articleAttributes = $wholesaler->articles()->findByDetailId($detail['articleDetailID']);

                        if (!is_null($articleAttributes)) {
                            $ordernumber = $articleAttributes['dc_' . $wholesaler->getShortname() . '_ordernumber'];

                            $response = $wholesaler->getClient()->getAll($ordernumber);

                            if ($response->getStatusCode() === 200) {
                                $article = json_decode($response->getContent(), true)[$ordernumber];

                                $inStock = (int)$article['quantity'];
                                $purchasePrice = (float)$article['purchasePrice'];
                                $longname = $wholesaler->getLongname();
                            }
                        }
                    }

                    /** @var Trackingcode[] $trackingCodes */
                    $trackingCodes = $this->em->getRepository(Trackingcode::class)->findBy([
                        'positionId' => $detail['id']
                    ]);

                    $orders[$orderKey]['details'][$detailKey]['dc_tracking_codes'] = [];

                    foreach ($trackingCodes as $trackingCode)
                    {
                        $trackingUrl = null;

                        if (!is_null($carrier = $trackingCode->getCarrier())) {
                            $code = $trackingCode->getCode();
                            $trackingUrl = $carrier->getTrackingUrl($code);
                        }

                        $orders[$orderKey]['details'][$detailKey]['dc_tracking_codes'][] = [
                            'code' => $trackingCode->getCode(),
                            'url' => $trackingUrl
                        ];
                    }

                    $orders[$orderKey]['details'][$detailKey]['articleDetailId'] = $detail['articleDetailID'];
                    $orders[$orderKey]['details'][$detailKey]['attribute']['dcStock'] = (int)$inStock;
                    $orders[$orderKey]['details'][$detailKey]['attribute']['dcPurchasePrice'] = (float)$purchasePrice;
                    $orders[$orderKey]['details'][$detailKey]['attribute']['dcNameLong'] = $longname;
                    $orders[$orderKey]['details'][$detailKey]['attribute']['dcDropshipActive'] = $active
                        ? '<div style="width: 16px; height: 16px; background: '.$activeColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>'
                        : '<div style="width: 16px; height: 16px; background: '.$inactiveColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>';
                }

                $orders[$orderKey]['dc_dropship_infos'] = $this->getDropshipInfo($orders[$orderKey]);

                $orders[$orderKey]['dc_dropship_active'] = (bool)$orders[$orderKey]['dc_dropship_active']
                    ? '<div style="width: 16px; height: 16px; background: '.$activeColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>'
                    : '<div style="width: 16px; height: 16px; background: '.$inactiveColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>';
            }

            $assigned['data'] = $orders;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return $assigned;
    }

    /**
     * @param array $order
     * @return array
     */
    private function getDropshipInfo($order)
    {
        $infos = [];

        $snippets = $this->snippetManager->getNamespace('backend/wunde_dcompanion_order/main');

        /** @var Order $order */
        $order = $this->em->getRepository(Order::class)->find($order['id']);

        $orderDropshipStatus = !is_null($order->getAttribute()->getDcDropshipStatus())
            ? (int)$order->getAttribute()->getDcDropshipStatus()
            : null;

        if (!is_null($order)) {
            /** @var Position[] $positions */
            $positions = $order->getDetails()->getValues();

            if (!$order->getAttribute()->getDcDropshipActive() || is_null($order->getAttribute()->getDcDropshipStatus())) {
                $infos[] = [
                    'color' => '#434343',
                    'message' => $snippets->get(
                        'dropship_info/order_has_no_dropship',
                        'Order without Dropshipping articles.'
                    ),
                ];
            } else {
                if ($order->getPaymentStatus()->getId() !== Status::PAYMENT_STATE_COMPLETELY_PAID) {
                    $changePaidStatus = $snippets->get(
                        'dropship_info/change_status_to_paid_in_full',
                        'Please change the status to "paid in full" on receipt of payment.'
                    );

                    $message = $this->config->autoOrder()
                        ? $changePaidStatus . ' ' . $snippets->get(
                            'dropship_info/order_is_then_automatically_sent_to_wholesaler_partner',
                            'The order is then automatically sent to the wholesale partner.'
                        )
                        : $changePaidStatus . ' ' . $snippets->get(
                            'dropship_info/can_send_order_to_wholesaler_partner_in_position_tab',
                            'You can then send the order to the wholesale partner in the Positions tab.'
                        );

                    $infos[] = [
                        'message' => $message,
                        'color' => '#ff7e00'
                    ];
                } else {
                    if ($order->getAttribute()->getDcDropshipActive()) {
                        if ($orderDropshipStatus === 100) {
                            $infos[] = [
                                'color' => 'limegreen',
                                'message' => $snippets->get(
                                    'dropship_info/dropshipping_order_to_wholesaler_partners',
                                    'Dropshipping order to wholesale partners.'
                                )
                            ];
                        } else if ($orderDropshipStatus === 200) {
                            $infos[] = [
                                'color' => '#ff0090',
                                'message' => $snippets->get(
                                    'dropship_info/tracking_info_available_check_mail',
                                    'Dropshipping order tracking information available.<br>Please check your emails.'
                                )
                            ];
                        } else if ($orderDropshipStatus === 0) {

                            $wholesalers = [];

                            foreach ($positions as $detail)
                            {
                                $shortname = trim($detail->getAttribute()->getDcNameShort());

                                if (!is_null($shortname) && !in_array($shortname, $wholesalers) && $shortname !== 'shop') {
                                    $wholesaler = $this->wholesalerRepository->findByShortname($shortname);
                                    $wholesalers[$shortname] = $wholesaler->getLongname();
                                }
                            }

                            if (!empty($wholesalers)) {
                                $message = $snippets->get(
                                    'dropship_info/order_is_flagged_for_transmission_to_wholesale_partner',
                                    'Dropshipping order is marked for transmission to the following wholesalers:'
                                ) . '<br>';

                                foreach ($wholesalers as $wholesaler)
                                {
                                    $message .= '<br>' .$wholesaler;
                                }

                                $infos[] = [
                                    'message' => $message,
                                    'color' => '#009fe3'
                                ];
                            }
                        }
                    } else {
                        $infos[] = [
                            'color' => '#ff7e00',
                            'message' => $snippets->get(
                                'dropship_info/order_transmission_to_wholesale_partner_is_possible',
                                'A transmission of the Dropshipping order to the wholesale partner is now possible.<br>Please check the order items of the order and release the order with a click on "Dropshipping Order" for the order.'
                            )
                        ];
                    }

                    foreach ($positions as $position)
                    {
                        $attribute = $position->getAttribute();

                        if (
                            $attribute instanceof Position &&
                            method_exists($attribute, 'getDcDropshipStatus') &&
                            $attribute->getDcDropshipStatus() === 'NOK' &&
                            $orderDropshipStatus === -100
                        ) {
                            $infos[] = [
                                'color' => '#b10000',
                                'message' => $snippets->get(
                                    'dropship_info/order_transmission_error_please_check_your_mails'
                                )
                            ];
                        }
                    }
                }
            }
        }

        return $infos;
    }

    public function onBackendOrderSavePositionBeforeHook(Enlight_Hook_HookArgs $args)
    {
        /** @var Shopware_Controllers_Backend_Order $controller */
        $controller = $args->getSubject();
        $request = $controller->Request();

        $detailId = (int)$controller->Request()->getParam('id', null);
        $nameShort = $request->getParam('dc_name_short', '');
        $articleNumber = $request->getParam('articleNumber', null);
        $articleDetailId = $request->getParam('articleDetailId', null);

        if (empty($nameShort)) $nameShort = null;

        if (!$detailId) $detailId = null;
        if (!$articleNumber) $articleNumber = null;

        if (!is_null($articleNumber)) $this->articleService->updateAll($articleNumber, $articleDetailId);

        if (!is_null($detailId)) {
            $this->connection->createQueryBuilder()
                ->update('s_order_details_attributes')
                ->set('dc_name_short', ':shortname')
                ->where('detailID = :detailId')
                ->setParameter('shortname', $nameShort)
                ->setParameter('detailId', $detailId)
                ->execute();
        }
    }

    public function onBackendOrderSavePositionAfterHook(Enlight_Hook_HookArgs $args)
    {
        /** @var Shopware_Controllers_Backend_Order $controller */
        $controller = $args->getSubject();
        $view = $controller->View();

        $data = $view->getAssign('data');

        if (!empty($data['id']) && !empty($data['articleDetailId'])) {
            /** @var Position $position */
            $position = $this->em->getRepository(Position::class)->find($data['id']);

            /** @var ArticleDetail $articleDetail */
            $articleDetail = $this->em->getRepository(ArticleDetail::class)->find($data['articleDetailId']);

            if (
                !is_null($position) &&
                !is_null($articleDetail) &&
                is_null($position->getArticleDetail())
            ) {
                try {
                    $position->setArticleDetail($articleDetail);
                    $this->em->flush($position);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), $e);
                }
            }
        }

        $tables = json_decode(file_get_contents(__DIR__ . '../Data/tables.json'), true);
        $coreTables = $tables['core'];
        $extensionTables = $tables['extension'];

        if (array_key_exists('s_order_details_attributes', $coreTables)) {
            $orderDetailAttributeColumns = array_column($coreTables['s_order_details_attributes'], 'name');

            foreach ($orderDetailAttributeColumns as &$column)
            {
                $upper = lcfirst(implode('', array_map('ucfirst', explode('_', $column))));
                $column = 'attribute.' . $column .= ' as ' . $upper;
            }

            $orderDetailAttributeColumns = array_merge(
                $orderDetailAttributeColumns,
                ['detail.articleDetailID']
            );

            /** @var Wholesaler $wholesaler */
            $wholesaler = $this->wholesalerRepository->findByShortname($data['dc_name_short']);

            $orderDetailAttribute = $this->connection->createQueryBuilder()
                ->select($orderDetailAttributeColumns)
                ->from('s_order_details_attributes', 'attribute')
                ->join('attribute', 's_order_details', 'detail', 'detail.id = attribute.detailID')
                ->where('attribute.detailID = :detailId')
                ->setParameter('detailId', $data['id'])
                ->execute()
                ->fetch();

            $orderDetailAttribute['dcStock'] = 0;
            $orderDetailAttribute['dcPurchasePrice'] = 0;

            if (
                !empty($data['dc_name_short']) &&
                !empty($orderDetailAttribute['articleDetailID']) &&
                array_key_exists('s_articles_attributes', $extensionTables)
            ) {
                $articleDetailAttributeColumns = array_map(function($value) use($data) {
                    return str_replace('%shortname%', $data['dc_name_short'], $value);
                }, array_column($extensionTables['s_articles_attributes'], 'name'));

                $articleDetailAttribute = $this->connection->createQueryBuilder()
                    ->select($articleDetailAttributeColumns)
                    ->from('s_articles_attributes')
                    ->where('articledetailsID = :articleDetailId')
                    ->setParameter('articleDetailId', $orderDetailAttribute['articleDetailID'])
                    ->execute()
                    ->fetch();

                $orderDetailAttribute['dcStock'] = $articleDetailAttribute['dc_'.$data['dc_name_short'].'_instock'];
                $orderDetailAttribute['dcPurchasePrice'] = $articleDetailAttribute['dc_'.$data['dc_name_short'].'_purchasing_price'];
            }

            $orderDetailAttribute['dcNameLong'] = !is_null($wholesaler)
                ? $wholesaler->getLongname()
                : '';

            $activeColor = $this->colors['active'];
            $inactiveColor = $this->colors['inactive'];

            $orderDetailAttribute['dcDropshipActive'] = !is_null($wholesaler)
                ? '<div style="width: 16px; height: 16px; background: '.$activeColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>'
                : '<div style="width: 16px; height: 16px; background: '.$inactiveColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>';

            $data = array_merge($data, ['attribute' => $orderDetailAttribute]);
        }

        $view->clearAssign('data');
        $view->assign('data', $data);
    }

    /**
     * Readme Hint 1
     * @param Enlight_Hook_HookArgs $args
     */
    public function onBackendAttributeDataListAfterHook(Enlight_Hook_HookArgs $args) {
        /** @var Shopware_Controllers_Backend_AttributeData $controller */
        $controller = $args->getSubject();
        $view = $controller->View();

        /** @var ConfigurationStruct[] $attributes */
        $attributes = $view->getAssign('data');

        foreach ($attributes as $key => $attribute)
        {
            $attribute->getColumnName();

            if (strpos($attribute->getColumnName(), 'dc_') !== false) {
                unset($attributes[$key]);
            }
        }

        $attributes = array_values($attributes);

        $view->clearAssign('data');
        $view->assign('data', $attributes);
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onBackendArticleDetailListHook(Enlight_Hook_HookArgs $args)
    {
        $view = $args->getSubject()->View();
        $articleList = $view->getAssign('data');

        foreach ($articleList as &$article) {
            $wholesalers = $this->wholesalerRepository->findAll();

            $isDcArticle = false;

            foreach ($wholesalers as $wholesaler) {
                $ordernumber =  !empty($article['attribute']['dc' . ucfirst($wholesaler->getShortname()) . 'Ordernumber']);
                $active = $article['attribute']['dc' . ucfirst($wholesaler->getShortname()) . 'Active'];

                if ($ordernumber && $active) {
                    $isDcArticle = true;
                    break;
                }
            }

            $activeColor = $this->colors['active'];
            $inactiveColor = $this->colors['inactive'];

            $article['dc_has_article'] = $isDcArticle
                ? '<div style="width: 16px; height: 16px; background: '.$activeColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>'
                : '<div style="width: 16px; height: 16px; background: '.$inactiveColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>';
        }

        $view->clearAssign('data');
        $view->assign('data', $articleList);
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onBackendArticleListFilterHook(Enlight_Hook_HookArgs $args)
    {
        $view = $args->getSubject()->View();
        $request = $args->getSubject()->Request();

        $articleList = $view->getAssign('data');

        foreach ($articleList as &$article) {
            $wholesalers = $this->wholesalerRepository->findAll();
            $isDcArticle = false;

            $totalDcStock = 0;

            foreach ($wholesalers as $wholesaler) {
                $ordernumber = !empty($article['Attribute_dc' . ucfirst($wholesaler->getShortname()) . 'Ordernumber']);
                $active = $article['Attribute_dc' . ucfirst($wholesaler->getShortname()) . 'Active'] === '1';
                $totalDcStock += $article['Attribute_dc' . ucfirst($wholesaler->getShortname()) . 'Instock'];

                if ($ordernumber && $active) {
                    $isDcArticle = true;
                }
            }

            $article['dc_total_stock'] = $totalDcStock;

            $activeColor = $this->colors['active'];
            $inactiveColor = $this->colors['inactive'];

            $article['dc_has_article'] = $isDcArticle
                ? '<div style="width: 16px; height: 16px; background: '.$activeColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>'
                : '<div style="width: 16px; height: 16px; background: '.$inactiveColor.'; margin: 0 auto; border-radius: 2px; padding-top: 0;"></div>';
        }

        $view->clearAssign('data');
        $view->assign('data', $articleList);
    }

    /**
     * Prepares the sort params for the variant search
     *
     * @param array $properties
     * @param array $fields
     * @return array
     */
    private function prepareVariantParam(array $properties, array $fields)
    {
        // Maps the fields to the correct table
        foreach ($properties as $key => $property) {
            foreach ($fields as $field) {
                $asStr = ' as ';
                $dotPos = strpos($field, '.');
                $asPos = strpos($field, $asStr, 1);

                if ($asPos) {
                    $fieldName = substr($field, $asPos + strlen($asStr));
                } else {
                    $fieldName = substr($field, $dotPos + 1);
                }

                if ($fieldName == $property['property']) {
                    $properties[$key]['property'] = $field;
                }
            }
        }

        return $properties;
    }

    /**
     * Adds the additional text for variants
     *
     * @param array $data
     */
    private function addAdditionalTextForVariant($data)
    {
        $variantIds = [];
        $tmpVariant = [];

        // Checks if an additional text is available
        foreach ($data as $key => $variantData) {
            if (!empty($variantData['additionalText'])) {
                $data[$key]['name'] = $variantData['name'] . ' ' . $variantData['additionalText'];
            } else {
                $variantIds[$variantData['id']] = $variantData['id'];
            }

            $tmpVariant[$variantData['id']] = $variantData;
        }

        if (empty($variantIds)) {
            return $data;
        }

        $builder = $this->connection->createQueryBuilder();

        $builder->select([
            'details.id',
            'config_groups.name AS groupName',
            'config_options.name AS optionName',
        ]);

        $builder->from('s_articles_details', 'details');

        $builder->innerJoin('details', 's_article_configurator_option_relations', 'mapping', 'mapping.article_id = details.id');
        $builder->innerJoin('mapping', 's_article_configurator_options', 'config_options', 'config_options.id = mapping.option_id');
        $builder->innerJoin('config_options', 's_article_configurator_groups', 'config_groups', 'config_options.group_id = config_groups.id');

        $builder->where('details.id IN (:detailsId)');
        $builder->setParameter('detailsId', $variantIds, Connection::PARAM_INT_ARRAY);

        $statement = $builder->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        $tmpVariant = $this->buildDynamicText($tmpVariant, $result);

        // Maps the associative data array back to an normal indexed array
        $data = [];
        foreach ($tmpVariant as $variant) {
            $data[] = $variant;
        }

        return $data;
    }

    /**
     * Helper function to generate the additional text dynamically
     *
     * @param array $data
     * @param array $variantsWithoutAdditionalText
     * @return array
     */
    private function buildDynamicText(array $data, array $variantsWithoutAdditionalText)
    {
        foreach ($variantsWithoutAdditionalText as $variantWithoutAdditionalText) {
            $variantData = &$data[$variantWithoutAdditionalText['id']];

            if (empty($variantData['additionalText'])) {
                $variantData['additionalText'] .= $variantWithoutAdditionalText['optionName'];
                $variantData['name'] .= ' ' . $variantWithoutAdditionalText['optionName'];
            } else {
                $variantData['additionalText'] .= ' / ' . $variantWithoutAdditionalText['optionName'];
                $variantData['name'] .= ' / ' . $variantWithoutAdditionalText['optionName'];
            }
        }

        return $data;
    }
}
//{/block}