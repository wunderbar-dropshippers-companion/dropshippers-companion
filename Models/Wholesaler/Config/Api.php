<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/09/2020 23:28
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler\Config;


class Api
{
    /**
     * @var string $endpoint
     */
    private $endpoint = '';

    /**
     * @var string $username
     */
    private $username = '';

    /**
     * @var string $password
     */
    private $password = '';

    /**
     * Api constructor.
     * @param string $endpoint
     * @param string $username
     * @param string $password
     */
    public function __construct($endpoint, $username, $password)
    {
        $this->endpoint = $endpoint;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function endpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function password()
    {
        return $this->password;
    }
}