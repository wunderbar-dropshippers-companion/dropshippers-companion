<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 02/11/2020 11:08
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use Enlight\Event\SubscriberInterface;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Config\Element;
use Shopware\Models\Config\Value;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;
use Shopware_Controllers_Backend_PluginInstaller;
use Enlight_Hook_HookArgs;
use WundeDcompanion\Services\PickwareService;

class InstallSubscriber implements SubscriberInterface
{

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * @var PickwareService $pickware
     */
    private $pickware;

    /**
     * InstallSubscriber constructor.
     * @param LoggerService $logger
     * @param ModelManager $em
     * @param WholesalerRepository $wholesalerRepository
     * @param PickwareService $pickware
     */
    public function __construct(
        LoggerService $logger,
        ModelManager $em,
        WholesalerRepository $wholesalerRepository,
        PickwareService $pickware
    ) {
        $this->logger = $logger;
        $this->em = $em;
        $this->wholesalers = $wholesalerRepository->findAllActive();
        $this->pickware = $pickware;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Shopware_Controllers_Backend_PluginInstaller::installPluginAction::after' => 'onInstallAfter',
            'Shopware_Controllers_Backend_PluginInstaller::activatePluginAction::after' => 'onInstallAfter',
            'Shopware_Controllers_Backend_PluginInstaller::deactivatePluginAction::after' => 'onUninstallAfter',
            'Shopware_Controllers_Backend_PluginInstaller::secureUninstallPluginAction::after' => 'onUninstallAfter',
            'Shopware_Controllers_Backend_PluginInstaller::uninstallPluginAction::after' => 'onUninstallAfter',
        ];
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onInstallAfter(Enlight_Hook_HookArgs $args)
    {
        /** @var Shopware_Controllers_Backend_PluginInstaller $controller */
        $controller = $args->getSubject();
        $technicalName = $controller->Request()->getParam('technicalName', null);

        if (
            !is_null($this->pickware->getPlugin()) &&
            $this->pickware->isActive() &&
            $technicalName === $this->pickware->getPlugin()->getName()
        ) {
            $this->disableOverrideStockConfiguration();

            foreach ($this->wholesalers as $wholesaler)
            {
                $this->pickware->addExtensionConfig($wholesaler);
            }
        }
    }

    /**
     * @param Enlight_Hook_HookArgs $args
     */
    public function onUninstallAfter(Enlight_Hook_HookArgs $args)
    {
        /** @var Shopware_Controllers_Backend_PluginInstaller $controller */
        $controller = $args->getSubject();
        $technicalName = $controller->Request()->getParam('technicalName', null);

        if (
            !is_null($this->pickware->getPlugin()) &&
            !$this->pickware->isActive() &&
            $technicalName === $this->pickware->getPlugin()->getName()
        ) {
            $this->enableOverrideStockConfiguration();

            foreach ($this->wholesalers as $wholesaler)
            {
                $this->pickware->removeExtensionConfig($wholesaler);
            }
        }
    }

    private function disableOverrideStockConfiguration()
    {
        /** @var Element $element */
        $element = Shopware()->Models()->getRepository(Element::class)->findOneBy([
            'name' => 'dc_override_stocks'
        ]);

        $element->setValue(false);
        $element->setOptions(['disabled' => true]);

        /** @var Value $value */
        foreach ($element->getValues()->getValues() as $value)
        {
            $value->setValue(false);
        }

        try {
            Shopware()->Models()->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    private function enableOverrideStockConfiguration()
    {
        /** @var Element $element */
        $element = Shopware()->Models()->getRepository(Element::class)->findOneBy([
            'name' => 'dc_override_stocks'
        ]);

        $element->setOptions(['disabled' => false]);

        try {
            Shopware()->Models()->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }
}