<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 22/01/2021 02:21
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use WundeDcompanion\Bundle\OrderBundle\Interfaces\PermutableInterface;

/**
 * Class PermutationService
 * @package WundeDcompanion\Services
 */
class PermutationService
{
    /**
     * @param PermutableInterface[] $arrays
     * @param int $i
     * @return array
     */
    public function permute($arrays, $i = 0) {
        if (!isset($arrays[$i])) return [];
        if ($i == count($arrays) - 1) return $arrays[$i]->getItems();

        $tmp = $this->permute($arrays, $i + 1);

        $result = [];

        foreach ($arrays[$i]->getItems() as $v) {
            foreach ($tmp as $t) {
                $result[] = is_array($t) ?
                    array_merge(array($v), $t) :
                    array($v, $t);
            }
        }

        return $result;
    }

    /**
     * @param array $array
     * @return array[]
     */
    public function combine($array)
    {
        $results = [[]];

        foreach ($array as $element)
            foreach ($results as $combination)
                array_push($results, array_merge(array($element), $combination));

        return $results;
    }
}