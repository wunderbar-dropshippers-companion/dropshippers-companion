import DefaultConfig from '../../data/config'

export default {
  namespaced: true,
  state: {
    config: {}
  },
  getters: {
    getConfig: (state) => state.config,
    getPluginConfig: (state) => state.config.hasOwnProperty('plugins') ? state.config.plugins : [],
  },
  mutations: {
    setConfig: (state, config) => state.config = config,
  },
  actions: {
    savePluginConfigElement({rootGetters, getters}, fields) {
      return axios.post(rootGetters.getUrl('saveConfigElements'), fields)
        .then(response => {
          return true
          // return response.data.hasOwnProperty('success') ? response.data.success : false;
        })
        .catch(() => {
          return true
          // console.warn('elements could not be saved')
          // return false
        })
    },
    load({commit, rootGetters, getters}) {
      if (Object.keys(getters['getConfig']).length === 0) {
        return axios.get(rootGetters.getUrl('getConfig'))
          .then(response => {
            let success = response.data.hasOwnProperty('success') ? response.data.success : false;
            let config = response.data.hasOwnProperty('config') ? response.data.config : {};

            if (success) {
              commit('setConfig', config)
              return config
            } else {
              throw new Error()
            }
          })
          .catch(() => {
            console.warn('config could not be loaded')

            if (process.env.NODE_ENV === 'development') {
              commit('setConfig', DefaultConfig)
              return DefaultConfig
            } else {
              return false
            }
          })
      } else {
        return getters['getConfig']
      }
    },
  }
}
