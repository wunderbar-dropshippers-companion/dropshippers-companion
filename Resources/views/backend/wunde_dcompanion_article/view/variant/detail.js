/*
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 07/09/2020 09:55
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

//{namespace name=backend/wunde_dcompanion_article/main}
//{block name="backend/article/view/variant/detail"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionArticle.view.variant.Detail', {
    override: 'Shopware.apps.Article.view.variant.Detail',

    requires: [
        'Shopware.apps.WundeDcompanionArticle.view.detail.List'
    ],

    dcSnippets: {
        dcWholesaler: '{s name=dc/wholesalers}DC Wholesalers{/s}'
    },

    initComponent: function() {
        var me = this;

        me.callParent(arguments);

        me.width = '80%';
        me.height = '90%';

        me.items.insert(0, Ext.create('Ext.tab.Panel', {
            items: [
                me.getMainTab(),
                me.getWholesalerTab()
            ]
        }));
    },

    getMainTab: function() {
        var me = this;

        return Ext.create('Ext.container.Container', {
            title: '{s name=base_data namespace=backend/article/view/main}Base data{/s}',
            layout: 'fit',
            name: 'variant-main-tab',
            items: [
                me.formPanel
            ]
        });
    },

    getWholesalerTab: function() {
        var me = this;
        var store = Ext.create('Shopware.apps.WundeDcompanionArticle.store.Wholesaler');

        store.getProxy().setExtraParam('detailId', me.record.get('id'));

        return Ext.create('Ext.container.Container', {
            title: me.dcSnippets.dcWholesaler,
            layout: 'fit',
            name: 'variant-wholesaler-tab',
            items: [
                Ext.create('Shopware.apps.WundeDcompanionArticle.view.detail.List', {
                    store: store.load(),
                    height: '100%',
                    article: me.record,
                    detailId: me.record.get('id'),
                    variant: true,
                    region: 'center',
                }),
            ]
        });
    }
});
//{/block}