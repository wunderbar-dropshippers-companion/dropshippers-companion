export default {
  "plugins": [
    {
      "id": 81,
      "name": "WundeDcInnoCigs",
      "label": "Dropshippers Companion (InnoCigs)",
      "namespace": "ShopwarePlugins",
      "source": "Community",
      "description": null,
      "active": true,
      "added": {
        "date": "2020-12-23 10:23:01.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "installed": {
        "date": "2021-03-31 23:23:16.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "updated": {
        "date": "2021-03-31 23:23:16.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "refreshed": {
        "date": "2021-05-02 21:47:02.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "author": "wunderbarmedien",
      "copyright": "Copyright © 2012, shopware AG",
      "license": null,
      "version": "2.5.0",
      "support": "--",
      "changes": null,
      "link": "https://wunderbar-medienagentur.de",
      "updateVersion": null,
      "updateSource": null,
      "capabilityUpdate": true,
      "capabilityInstall": true,
      "capabilityEnable": true,
      "capabilitySecureUninstall": true,
      "translations": {
        "de": {
          "label": "Dropshippers Companion (InnoCigs)"
        },
        "en": {
          "label": "Dropshippers Companion (InnoCigs)"
        }
      },
      "inSafeMode": false,
      "configForms": {
        "id": 356,
        "parentId": 92,
        "name": "WundeDcInnoCigs",
        "label": "Dropshippers Companion (InnoCigs)",
        "description": null,
        "pluginId": 81,
        "position": 0,
        "elements": [
          {
            "id": 2413,
            "name": "dc_ic_endpoint",
            "value": "https://www.innocigs.com/xmlapi/api.php",
            "description": null,
            "label": "API endpoint",
            "type": "text",
            "required": false,
            "position": 0,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1970,
                "description": null,
                "label": "API-Endpunkt",
                "elementId": 2413,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1971,
                "description": null,
                "label": "API endpoint",
                "elementId": 2413,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2414,
            "name": "dc_ic_username",
            "value": "MS083179",
            "description": null,
            "label": "API-Username",
            "type": "text",
            "required": false,
            "position": 1,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1972,
                "description": null,
                "label": "API-Benutzername",
                "elementId": 2414,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1973,
                "description": null,
                "label": "API-Username",
                "elementId": 2414,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2415,
            "name": "dc_ic_password",
            "value": "b64UZ.%3DKB83DrOSc",
            "description": null,
            "label": "API-Password",
            "type": "text",
            "required": false,
            "position": 2,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1974,
                "description": null,
                "label": "API-Passwort",
                "elementId": 2415,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1975,
                "description": null,
                "label": "API-Password",
                "elementId": 2415,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2416,
            "name": "dc_ic_shipper_company",
            "value": "DC Development Test Company",
            "description": "Enter your company name here as it was stored in the system for InnoCigs.",
            "label": "Company name",
            "type": "text",
            "required": false,
            "position": 3,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1976,
                "description": "Geben Sie hier Ihren Firmenname an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Firmenname",
                "elementId": 2416,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1977,
                "description": "Enter your company name here as it was stored in the system for InnoCigs.",
                "label": "Company name",
                "elementId": 2416,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2417,
            "name": "dc_ic_shipper_company2",
            "value": null,
            "description": "Enter your company name2 here, as it was stored in the system at InnoCigs.",
            "label": "Company name 2",
            "type": "text",
            "required": false,
            "position": 4,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1978,
                "description": "Geben Sie hier Ihren Firmenname2 an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Firmenname 2",
                "elementId": 2417,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1979,
                "description": "Enter your company name2 here, as it was stored in the system at InnoCigs.",
                "label": "Company name 2",
                "elementId": 2417,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2418,
            "name": "dc_ic_shipper_firstname",
            "value": "Andreas",
            "description": "Enter your first name here as it was stored in the system for InnoCigs.",
            "label": "Firstname",
            "type": "text",
            "required": false,
            "position": 5,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1980,
                "description": "Geben Sie hier Ihren Vorname an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Vorname",
                "elementId": 2418,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1981,
                "description": "Enter your first name here as it was stored in the system for InnoCigs.",
                "label": "Firstname",
                "elementId": 2418,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2419,
            "name": "dc_ic_shipper_lastname",
            "value": "Gerhardt",
            "description": "Enter your last name here as it was stored in the system for InnoCigs.",
            "label": "Lastname",
            "type": "text",
            "required": false,
            "position": 6,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1982,
                "description": "Geben Sie hier Ihren Nachnamen an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Nachname",
                "elementId": 2419,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1983,
                "description": "Enter your last name here as it was stored in the system for InnoCigs.",
                "label": "Lastname",
                "elementId": 2419,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2420,
            "name": "dc_ic_shipper_street",
            "value": "Junglasstraße 27",
            "description": "Enter your street here as it was stored in the system for InnoCigs.",
            "label": "Street",
            "type": "text",
            "required": false,
            "position": 7,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1984,
                "description": "Geben Sie hier Ihre Straße an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Straße",
                "elementId": 2420,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1985,
                "description": "Enter your street here as it was stored in the system for InnoCigs.",
                "label": "Street",
                "elementId": 2420,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2421,
            "name": "dc_ic_shipper_postcode",
            "value": 56203,
            "description": "Enter your postal code here as it was stored in the system for InnoCigs.",
            "label": "Postcode",
            "type": "text",
            "required": false,
            "position": 8,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1986,
                "description": "Geben Sie hier Ihre Postleizahl an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Postleitzahl",
                "elementId": 2421,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1987,
                "description": "Enter your postal code here as it was stored in the system for InnoCigs.",
                "label": "Postcode",
                "elementId": 2421,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2422,
            "name": "dc_ic_shipper_city",
            "value": "Höhr-Grenzhausen",
            "description": "Enter your location here as it was stored in the system for InnoCigs.",
            "label": "City",
            "type": "text",
            "required": false,
            "position": 9,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1988,
                "description": "Geben Sie hier Ihren Ort an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Ort",
                "elementId": 2422,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1989,
                "description": "Enter your location here as it was stored in the system for InnoCigs.",
                "label": "City",
                "elementId": 2422,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2423,
            "name": "dc_ic_shipper_countrycode",
            "value": "DE",
            "description": "Enter your country code here as it was stored in the system for InnoCigs.",
            "label": "Country code (e.g. EN)",
            "type": "text",
            "required": false,
            "position": 10,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1990,
                "description": "Geben Sie hier Ihr Länderkürzel an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Länderkürzel (z.B. DE)",
                "elementId": 2423,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1991,
                "description": "Enter your country code here as it was stored in the system for InnoCigs.",
                "label": "Country code (e.g. EN)",
                "elementId": 2423,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2424,
            "name": "dc_ic_shipper_telephone",
            "value": 342391,
            "description": "Enter your telephone number here as it was stored in the system for InnoCigs.",
            "label": "Telephone number",
            "type": "text",
            "required": false,
            "position": 11,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1992,
                "description": "Geben Sie hier Ihre Telefonnummer an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "Telefonnummer",
                "elementId": 2424,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1993,
                "description": "Enter your telephone number here as it was stored in the system for InnoCigs.",
                "label": "Telephone number",
                "elementId": 2424,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2425,
            "name": "dc_ic_shipper_email",
            "value": "a.gerhardt@dropshippers-companion.de",
            "description": "Enter your e-mail address here as it was stored in the system at InnoCigs.",
            "label": "Email address",
            "type": "text",
            "required": false,
            "position": 12,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1994,
                "description": "Geben Sie hier Ihre E-Mailadresse an, wie dieser bei InnoCigs im System hinterlegt wurde.",
                "label": "E-Mailadresse",
                "elementId": 2425,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1995,
                "description": "Enter your e-mail address here as it was stored in the system at InnoCigs.",
                "label": "Email address",
                "elementId": 2425,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2426,
            "name": "dc_ic_shipping_cost",
            "value": 0,
            "description": null,
            "label": "Shipping cost",
            "type": "number",
            "required": false,
            "position": 13,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1996,
                "description": null,
                "label": "Versandkosten",
                "elementId": 2426,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1997,
                "description": null,
                "label": "Shipping cost",
                "elementId": 2426,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2427,
            "name": "dc_ic_shipping_cost_exemption_limit",
            "value": 0,
            "description": null,
            "label": "Shipping cost exemption limit",
            "type": "number",
            "required": false,
            "position": 14,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 1998,
                "description": null,
                "label": "Versandkosten Freigrenze",
                "elementId": 2427,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1999,
                "description": null,
                "label": "Shipping cost exemption limit",
                "elementId": 2427,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2428,
            "name": "dc_ic_pickware_warehouse",
            "value": null,
            "description": null,
            "label": "Pickware warehouse",
            "type": "select",
            "required": false,
            "position": 17,
            "scope": 0,
            "formId": 356,
            "translations": [
              {
                "id": 2000,
                "description": null,
                "label": "Pickwarelager",
                "elementId": 2428,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2001,
                "description": null,
                "label": "Pickware warehouse",
                "elementId": 2428,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2429,
            "name": "dc_ic_api_check",
            "value": null,
            "description": null,
            "label": "Start Api Test",
            "type": "button",
            "required": false,
            "position": 18,
            "scope": 1,
            "formId": 356,
            "translations": [
              {
                "id": 2002,
                "description": null,
                "label": "Starte Api Test",
                "elementId": 2429,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2003,
                "description": null,
                "label": "Start Api Test",
                "elementId": 2429,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          }
        ]
      },
      "fieldSet": {
        "api": {
          "main": [
            "endpoint",
            "username",
            "password"
          ]
        },
        "address": {
          "left": [
            "shipper_company",
            "shipper_company2",
            "shipper_firstname",
            "shipper_lastname"
          ],
          "right": [
            "shipper_street",
            "shipper_postcode",
            "shipper_city",
            "shipper_countrycode"
          ]
        },
        "contact": {
          "main": [
            "shipper_telephone",
            "shipper_email"
          ]
        },
        "shipping": {
          "main": [
            "shipping_cost",
            "shipping_cost_exemption_limit"
          ]
        },
        "pickware": {
          "main": [
            "pickware_warehousename"
          ]
        }
      },
      "wholesaler": "ic"
    },
    {
      "id": 80,
      "name": "WundeDcompanion",
      "label": "Dropshippers Companion",
      "namespace": "ShopwarePlugins",
      "source": "Community",
      "description": null,
      "active": true,
      "added": {
        "date": "2020-12-23 10:20:32.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "installed": {
        "date": "2021-04-27 14:47:16.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "updated": {
        "date": "2021-04-27 14:47:16.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "refreshed": {
        "date": "2021-05-02 21:47:02.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "author": "wunderbarmedien",
      "copyright": "Copyright © 2012, shopware AG",
      "license": null,
      "version": "2.5.0",
      "support": "--",
      "changes": "{\"2.5.0\":{\"en\":[\"\\n            - Multiple wholesalers are now possible\\n            - Tracking information is automatically entered\\n            - Completely new wholesaler management in the article overview\\n        \"],\"de\":[\"\\n            - Mehrere Gro\\u00dfh\\u00e4ndler sind nun m\\u00f6glich\\n            - Trackinginformationen werden automatisch eingepflegt\\n            - Komplett neues Gro\\u00dfhandelsmanagement in der Artikel\\u00fcbersicht\\n        \"]}}",
      "link": "https://wunderbar-medienagentur.de",
      "updateVersion": null,
      "updateSource": null,
      "capabilityUpdate": true,
      "capabilityInstall": true,
      "capabilityEnable": true,
      "capabilitySecureUninstall": true,
      "translations": {
        "de": {
          "label": "Dropshippers Companion"
        },
        "en": {
          "label": "Dropshippers Companion"
        }
      },
      "inSafeMode": false,
      "configForms": {
        "id": 366,
        "parentId": 92,
        "name": "WundeDcompanion",
        "label": "Dropshippers Companion",
        "description": null,
        "pluginId": 80,
        "position": 0,
        "elements": [
          {
            "id": 2573,
            "name": "dc_auto_order",
            "value": true,
            "description": "Would you like to order Dropshipping products automatically from the respective suppliers or process them manually in the backend order?.",
            "label": "Process orders automatically",
            "type": "boolean",
            "required": false,
            "position": 0,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2192,
                "description": "Möchten Sie Dropshipping-Produkte bei den jeweiligen Anbietern automatisch bestellen oder manuell in der Backendbestellung bearbeiten?.",
                "label": "Bestellungen automatisch verarbeiten",
                "elementId": 2573,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2193,
                "description": "Would you like to order Dropshipping products automatically from the respective suppliers or process them manually in the backend order?.",
                "label": "Process orders automatically",
                "elementId": 2573,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2574,
            "name": "dc_combine_tracking_codes",
            "value": true,
            "description": "With this option, tracking codes are stored comma separated in the backend of your shop.",
            "label": "Summarize tracking codes",
            "type": "boolean",
            "required": false,
            "position": 1,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2194,
                "description": "Mit dieser Option werden Trackingcodes kommasepariert im Backend Ihres Shops hinterlegt",
                "label": "Tracking-Codes zusammenfassen",
                "elementId": 2574,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2195,
                "description": "With this option, tracking codes are stored comma separated in the backend of your shop.",
                "label": "Summarize tracking codes",
                "elementId": 2574,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2575,
            "name": "dc_send_mail_to_customer",
            "value": true,
            "description": "Would you like your customer to automatically receive tracking information by e-mail?",
            "label": "Send tracking information to customers by email",
            "type": "boolean",
            "required": false,
            "position": 2,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2196,
                "description": "Möchten Sie, dass Ihr Kunde automatisch Tracking-Informationen per E-Mail erhält?",
                "label": "Tracking-Informationen per E-Mail an Kunden versenden",
                "elementId": 2575,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2197,
                "description": "Would you like your customer to automatically receive tracking information by e-mail?",
                "label": "Send tracking information to customers by email",
                "elementId": 2575,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2576,
            "name": "dc_finish_order",
            "value": true,
            "description": "With this option, orders can be automatically set to \"Completely completed\".",
            "label": "Complete orders",
            "type": "boolean",
            "required": false,
            "position": 3,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2198,
                "description": "Mit dieser Option können Bestellungen automatisch auf \"Komplett abgeschlossen\" gesetzt werden.",
                "label": "Bestellungen abschließen",
                "elementId": 2576,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2199,
                "description": "With this option, orders can be automatically set to \"Completely completed\".",
                "label": "Complete orders",
                "elementId": 2576,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2577,
            "name": "dc_mail_notification",
            "value": true,
            "description": "Would you like to be informed by DC about special offers by e-mail?",
            "label": "E-mail notifications",
            "type": "boolean",
            "required": false,
            "position": 4,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2200,
                "description": "Möchten Sie von DC über Aktionen per E-Mail informiert werden?",
                "label": "E-Mail Benachrichtungen",
                "elementId": 2577,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2201,
                "description": "Would you like to be informed by DC about special offers by e-mail?",
                "label": "E-mail notifications",
                "elementId": 2577,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2578,
            "name": "dc_mail_recipients",
            "value": null,
            "description": "Enter here, separated by a comma, the recipients (CC) for DC notifications.",
            "label": "E-mail recipient for notifications (CC)",
            "type": "text",
            "required": false,
            "position": 5,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2202,
                "description": "Geben Sie hier, durch ein Komma getrennt, die Empfänger (CC) für DC Benachrichtigungen ein.",
                "label": "E-Mail Empfänger für Benachrichtigungen (CC)",
                "elementId": 2578,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2203,
                "description": "Enter here, separated by a comma, the recipients (CC) for DC notifications.",
                "label": "E-mail recipient for notifications (CC)",
                "elementId": 2578,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2579,
            "name": "dc_override_stocks",
            "value": false,
            "description": "Overwrites all inventory of shops without pick goods with the inventories of wholesalers. IMPORTANT: If this option is deactivated, all stocks must be adjusted manually to 0.",
            "label": "Overwrite stocks",
            "type": "boolean",
            "required": false,
            "position": 6,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2204,
                "description": "Überschreibt sämtlichen Warenbestand von Shops ohne Pickware mit den Beständen der Großhändler. WICHTIG: Wird diese Option deaktiviert, so müssen alle Bestände manuell auf 0 angepasst werden.",
                "label": "Bestände überschreiben",
                "elementId": 2579,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2205,
                "description": "Overwrites all inventory of shops without pick goods with the inventories of wholesalers. IMPORTANT: If this option is deactivated, all stocks must be adjusted manually to 0.",
                "label": "Overwrite stocks",
                "elementId": 2579,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2580,
            "name": "dc_test_mode",
            "value": false,
            "description": null,
            "label": "Test mode",
            "type": "boolean",
            "required": false,
            "position": 7,
            "scope": 1,
            "formId": 366,
            "translations": [
              {
                "id": 2206,
                "description": null,
                "label": "Test Modus",
                "elementId": 2580,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2207,
                "description": null,
                "label": "Test mode",
                "elementId": 2580,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          }
        ]
      },
      "fieldSet": {
        "mainsettings": {
          "main": [
            "auto_order",
            "combine_tracking_codes",
            "send_mail_to_customer",
            "finish_order",
            "mail_notification",
            "mail_recipients",
            "override_stocks",
            "test_mode"
          ]
        }
      }
    },
    {
      "id": 84,
      "name": "WundeDcompanionVaporExMachina",
      "label": "Dropshippers Companion (VaporExMachina)",
      "namespace": "ShopwarePlugins",
      "source": "",
      "description": null,
      "active": true,
      "added": {
        "date": "2021-01-01 23:33:43.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "installed": {
        "date": "2021-04-06 11:00:02.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "updated": {
        "date": "2021-04-06 11:00:02.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "refreshed": {
        "date": "2021-05-02 21:47:02.000000",
        "timezone_type": 3,
        "timezone": "Europe/Berlin"
      },
      "author": "wunderbarmedien",
      "copyright": null,
      "license": null,
      "version": "2.5.0",
      "support": null,
      "changes": null,
      "link": "https://wunderbar-medienagentur.de",
      "updateVersion": null,
      "updateSource": null,
      "capabilityUpdate": true,
      "capabilityInstall": true,
      "capabilityEnable": true,
      "capabilitySecureUninstall": true,
      "translations": {
        "de": {
          "label": "Dropshippers Companion (VaporExMachina)"
        },
        "en": {
          "label": "Dropshippers Companion (VaporExMachina)"
        }
      },
      "inSafeMode": false,
      "configForms": {
        "id": 317,
        "parentId": 92,
        "name": "WundeDcompanionVaporExMachina",
        "label": "Dropshippers Companion (VaporExMachina)",
        "description": null,
        "pluginId": 84,
        "position": 0,
        "elements": [
          {
            "id": 1923,
            "name": "dc_vex_endpoint",
            "value": "https://vaporexmachina.de/api",
            "description": null,
            "label": "API endpoint",
            "type": "text",
            "required": false,
            "position": 0,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1384,
                "description": null,
                "label": "API-Endpunkt",
                "elementId": 1923,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1385,
                "description": null,
                "label": "API endpoint",
                "elementId": 1923,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1924,
            "name": "dc_vex_username",
            "value": "info@wunderbar-medienagentur.de",
            "description": null,
            "label": "API-Username",
            "type": "text",
            "required": false,
            "position": 1,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1386,
                "description": null,
                "label": "API-Benutzername",
                "elementId": 1924,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1387,
                "description": null,
                "label": "API-Username",
                "elementId": 1924,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1925,
            "name": "dc_vex_password",
            "value": "!DC2019API",
            "description": null,
            "label": "API-Password",
            "type": "text",
            "required": false,
            "position": 2,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1388,
                "description": null,
                "label": "API-Passwort",
                "elementId": 1925,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1389,
                "description": null,
                "label": "API-Password",
                "elementId": 1925,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1926,
            "name": "dc_vex_shipper_company",
            "value": "DC Development Test Company",
            "description": "Enter your company name here as it was stored in the system for VaporExMachina.",
            "label": "Company name",
            "type": "text",
            "required": false,
            "position": 3,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1390,
                "description": "Geben Sie hier Ihren Firmenname an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Firmenname",
                "elementId": 1926,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1391,
                "description": "Enter your company name here as it was stored in the system for VaporExMachina.",
                "label": "Company name",
                "elementId": 1926,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1927,
            "name": "dc_vex_shipper_company2",
            "value": null,
            "description": "Enter your company name2 here, as it was stored in the system at VaporExMachina.",
            "label": "Company name 2",
            "type": "text",
            "required": false,
            "position": 4,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1392,
                "description": "Geben Sie hier Ihren Firmenname2 an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Firmenname 2",
                "elementId": 1927,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1393,
                "description": "Enter your company name2 here, as it was stored in the system at VaporExMachina.",
                "label": "Company name 2",
                "elementId": 1927,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1928,
            "name": "dc_vex_shipper_firstname",
            "value": "Andreas",
            "description": "Enter your first name here as it was stored in the system for VaporExMachina.",
            "label": "Firstname",
            "type": "text",
            "required": false,
            "position": 5,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1394,
                "description": "Geben Sie hier Ihren Vorname an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Vorname",
                "elementId": 1928,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1395,
                "description": "Enter your first name here as it was stored in the system for VaporExMachina.",
                "label": "Firstname",
                "elementId": 1928,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1929,
            "name": "dc_vex_shipper_lastname",
            "value": "Gerhardt",
            "description": "Enter your last name here as it was stored in the system for VaporExMachina.",
            "label": "Lastname",
            "type": "text",
            "required": false,
            "position": 6,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1396,
                "description": "Geben Sie hier Ihren Nachnamen an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Nachname",
                "elementId": 1929,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1397,
                "description": "Enter your last name here as it was stored in the system for VaporExMachina.",
                "label": "Lastname",
                "elementId": 1929,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1930,
            "name": "dc_vex_shipper_street",
            "value": "Junglasstraße 27",
            "description": "Enter your street here as it was stored in the system for VaporExMachina.",
            "label": "Street",
            "type": "text",
            "required": false,
            "position": 7,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1398,
                "description": "Geben Sie hier Ihre Straße an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Straße",
                "elementId": 1930,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1399,
                "description": "Enter your street here as it was stored in the system for VaporExMachina.",
                "label": "Street",
                "elementId": 1930,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1931,
            "name": "dc_vex_shipper_postcode",
            "value": 56203,
            "description": "Enter your postal code here as it was stored in the system for VaporExMachina.",
            "label": "Postcode",
            "type": "text",
            "required": false,
            "position": 8,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1400,
                "description": "Geben Sie hier Ihre Postleizahl an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Postleitzahl",
                "elementId": 1931,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1401,
                "description": "Enter your postal code here as it was stored in the system for VaporExMachina.",
                "label": "Postcode",
                "elementId": 1931,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1932,
            "name": "dc_vex_shipper_city",
            "value": "Höhr-Grenzhausen",
            "description": "Enter your location here as it was stored in the system for VaporExMachina.",
            "label": "City",
            "type": "text",
            "required": false,
            "position": 9,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1402,
                "description": "Geben Sie hier Ihren Ort an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Ort",
                "elementId": 1932,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1403,
                "description": "Enter your location here as it was stored in the system for VaporExMachina.",
                "label": "City",
                "elementId": 1932,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1933,
            "name": "dc_vex_shipper_countrycode",
            "value": "DE",
            "description": "Enter your country code here as it was stored in the system for VaporExMachina.",
            "label": "Country code (e.g. EN)",
            "type": "text",
            "required": false,
            "position": 10,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1404,
                "description": "Geben Sie hier Ihr Länderkürzel an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Länderkürzel (z.B. DE)",
                "elementId": 1933,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1405,
                "description": "Enter your country code here as it was stored in the system for VaporExMachina.",
                "label": "Country code (e.g. EN)",
                "elementId": 1933,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1934,
            "name": "dc_vex_shipper_telephone",
            "value": 342391,
            "description": "Enter your telephone number here as it was stored in the system for VaporExMachina.",
            "label": "Telephone number",
            "type": "text",
            "required": false,
            "position": 11,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1406,
                "description": "Geben Sie hier Ihre Telefonnummer an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "Telefonnummer",
                "elementId": 1934,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1407,
                "description": "Enter your telephone number here as it was stored in the system for VaporExMachina.",
                "label": "Telephone number",
                "elementId": 1934,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1935,
            "name": "dc_vex_shipper_email",
            "value": "a.gerhardt@dropshippers-companion.de",
            "description": "Enter your e-mail address here as it was stored in the system at VaporExMachina.",
            "label": "Email address",
            "type": "text",
            "required": false,
            "position": 12,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1408,
                "description": "Geben Sie hier Ihre E-Mailadresse an, wie dieser bei VaporExMachina im System hinterlegt wurde.",
                "label": "E-Mailadresse",
                "elementId": 1935,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1409,
                "description": "Enter your e-mail address here as it was stored in the system at VaporExMachina.",
                "label": "Email address",
                "elementId": 1935,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1936,
            "name": "dc_vex_shipping_cost",
            "value": 0,
            "description": null,
            "label": "Shipping cost",
            "type": "number",
            "required": false,
            "position": 13,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1410,
                "description": null,
                "label": "Versandkosten",
                "elementId": 1936,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1411,
                "description": null,
                "label": "Shipping cost",
                "elementId": 1936,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1937,
            "name": "dc_vex_shipping_cost_exemption_limit",
            "value": 0,
            "description": null,
            "label": "Shipping cost exemption limit",
            "type": "number",
            "required": false,
            "position": 14,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1412,
                "description": null,
                "label": "Versandkosten Freigrenze",
                "elementId": 1937,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1413,
                "description": null,
                "label": "Shipping cost exemption limit",
                "elementId": 1937,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 2437,
            "name": "dc_ic_pickware_warehouse",
            "value": null,
            "description": null,
            "label": "Pickware warehouse",
            "type": "select",
            "required": false,
            "position": 17,
            "scope": 0,
            "formId": 317,
            "translations": [
              {
                "id": 2004,
                "description": null,
                "label": "Pickwarelager",
                "elementId": 2437,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 2005,
                "description": null,
                "label": "Pickware warehouse",
                "elementId": 2437,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          },
          {
            "id": 1938,
            "name": "dc_vex_api_check",
            "value": null,
            "description": null,
            "label": "Start Api Test",
            "type": "button",
            "required": false,
            "position": 18,
            "scope": 1,
            "formId": 317,
            "translations": [
              {
                "id": 1414,
                "description": null,
                "label": "Starte Api Test",
                "elementId": 1938,
                "localeId": 1,
                "locale": {
                  "id": 1,
                  "locale": "de_DE",
                  "language": "Deutsch",
                  "territory": "Deutschland"
                }
              },
              {
                "id": 1415,
                "description": null,
                "label": "Start Api Test",
                "elementId": 1938,
                "localeId": 2,
                "locale": {
                  "id": 2,
                  "locale": "en_GB",
                  "language": "Englisch",
                  "territory": "Vereinigtes Königreich"
                }
              }
            ]
          }
        ]
      },
      "fieldSet": {
        "api": {
          "main": [
            "endpoint",
            "username",
            "password"
          ]
        },
        "address": {
          "left": [
            "shipper_company",
            "shipper_company2",
            "shipper_firstname",
            "shipper_lastname"
          ],
          "right": [
            "shipper_street",
            "shipper_postcode",
            "shipper_city",
            "shipper_countrycode"
          ]
        },
        "contact": {
          "main": [
            "shipper_telephone",
            "shipper_email"
          ]
        },
        "shipping": {
          "main": [
            "shipping_cost",
            "shipping_cost_exemption_limit"
          ]
        },
        "pickware": {
          "main": [
            "pickware_warehousename"
          ]
        }
      },
      "wholesaler": "vex"
    }
  ]
}
