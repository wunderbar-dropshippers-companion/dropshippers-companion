<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 22/01/2021 00:19
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle;


use Shopware\Models\Order\Order;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Bundle\OrderBundle\Interfaces\CombinableInterface;
use WundeDcompanion\Bundle\OrderBundle\Interfaces\PermutableInterface;
use WundeDcompanion\Bundle\OrderBundle\Models\Combination;
use WundeDcompanion\Bundle\OrderBundle\Models\SimilarySet;
use WundeDcompanion\Services\PermutationService;

/**
 * Class CombinationService
 * @package WundeDcompanion\Bundle\OrderBundle
 */
class CombinationService
{
    /**
     * @var PermutationService $permutationService
     */
    private $permutationService;

    /**
     * @var OutputInterface|null $output
     */
    private $output;

    /**
     * CombinationService constructor.
     * @param PermutationService $permutationService
     */
    public function __construct(PermutationService $permutationService)
    {
        $this->permutationService = $permutationService;
    }

    /**
     * @param CombinableInterface[] $similarySets
     * @param Order $order
     * @return Combination[]
     */
    public function getAll($similarySets, $order) {
        $combinations = [];
        $similaries = [];

        foreach ($similarySets as $similarySet)
        {
            foreach ($similarySet->getItems() as $item)
            {
                $similaries[] = $item;
            }
        }

        foreach ($this->permutationService->combine($similaries) as $key => $items)
        {
            $combination = new Combination($order, $this->output);
            $combination->setUid($key);
            $combination->setSimilaries($items);

            $hash = $combination->getHash();

            if (!array_key_exists($hash, $combinations)) {
                $combinations[$hash] = $combination;
            }
        }

//        foreach ($combinations as $key => $combination)
//        {
//            if ($combination->getUid() === 91) {
//                $this->print($combination, 'comment');
//            }
//        }

        return array_values($combinations);
    }

    /**
     * @param array|SimilarySet[]|CombinableInterface[] $similarySets
     * @param Order $order
     * @return Combination
     */
    public function getOptimalCombination(array $similarySets, Order $order): Combination
    {
        $combinations = $this->getAll($similarySets, $order);

        $combinations = array_filter($combinations, function ($combination) {
            return $combination->allArticlesAvailable();
        });

        usort($combinations, function (Combination $a, Combination $b) use($combinations) {
            $aPackageFactor = $a->getPackageCount() > 1 ? $a->getPackageCount() / 10 + 1 : 1;
            $bPackageFactor = $b->getPackageCount() > 1 ? $b->getPackageCount() / 10 + 1 : 1;

            return
                (($a->getTotalPurchasePrice() * $aPackageFactor) <=> ($b->getTotalPurchasePrice() * $bPackageFactor));
        });

        return count($combinations)
            ? array_values($combinations)[0]
            : new Combination($order, $this->output);
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output): void
    {
        $this->output = $output;
    }

    protected function clearConsole()
    {
        echo sprintf("\033\143");
    }

    /**
     * @param mixed $value
     * @param string $color
     */
    protected function print($value, $color = 'info')
    {
        if (!is_null($this->output)) {
            $this->output->writeln('<'.$color.'>'.print_r($value, true).'</'.$color.'>');
        } else {
            echo "\e[4m\033[31mOUTPUTINTERFACE NOT FOUND!\033\e[0m" . PHP_EOL . PHP_EOL;
            echo print_r($value, true) . PHP_EOL;
        }
    }
}