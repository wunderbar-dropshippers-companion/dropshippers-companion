<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 18/10/2020 11:32
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Log;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_logs")
 * @ORM\HasLifecycleCallbacks()
 */
class Log extends ModelEntity
{
    const DATETIME_FORMAT = 'd.m.Y H:i:s';

    /**
     * Primary Key - autoincrement value
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $message
     * @ORM\Column(type="text", nullable=true)
     */
    private $message;

    /**
     * @var string $identifier
     * @ORM\Column(nullable=true)
     */
    private $identifier;

    /**
     * @var string $file
     * @ORM\Column(nullable=true)
     */
    private $file;

    /**
     * @var int $line
     * @ORM\Column(type="integer", nullable=true)
     */
    private $line;

    /**
     * @var int $code
     * @ORM\Column(type="integer", nullable=true)
     */
    private $code;

    /**
     * @var string $content
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var int $typeId
     * @ORM\Column(name="type_id", type="integer", nullable=false, options={"default": 4})
     */
    private $typeId = 4;

    /**
     * @var Type $type
     *
     * @ORM\ManyToOne(targetEntity="WundeDcompanion\Models\Log\Type")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @var string $updatedAt
     * @ORM\Column(name="updated_at", type="string", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string $createdAt
     * @ORM\Column(name="created_at", type="string", nullable=true)
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param int $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Type $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $date = (new DateTime())->format(self::DATETIME_FORMAT);

        $this->createdAt = $date;
        $this->updatedAt = $date;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = (new DateTime())->format(self::DATETIME_FORMAT);
    }
}