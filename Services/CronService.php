<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 06/10/2020 14:26
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;

use DateTime;
use Enlight_Components_Cron_Job;
use Enlight_Components_Cron_Manager;
use Exception;
use RuntimeException;
use Throwable;
use Zend_Date;

/**
 * Class CronService
 * @package WundeDcompanion\Services
 */
class CronService
{
    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Enlight_Components_Cron_Manager $cron
     */
    private $cron;

    /**
     * CronService constructor.
     * @param LoggerService $logger
     * @param Enlight_Components_Cron_Manager $cron
     */
    public function __construct(LoggerService $logger, Enlight_Components_Cron_Manager $cron)
    {
        $this->logger = $logger;
        $this->cron = $cron;

        Shopware()->Container()->load('plugins');
    }

    public function runSendOrderCronjob()
    {
        $this->run('Shopware_CronJob_DcSendOrder');
    }

    public function runReceiveDataCronjob()
    {
        $this->run('Shopware_CronJob_DcReceiveData');
    }

    /**
     * @param string $name
     */
    private function run(string $name)
    {
        $job = $this->getJobByActionName($name);

        if (!is_null($job)) {
            try {
                $this->cron->runJob($job);
            } catch (Throwable $e) {
                $this->logger->error($e->getMessage(), $e);
            }
        }
    }

    /**
     * @param string $name
     * @return Enlight_Components_Cron_Job
     */
    private function getJobByActionName(string $name)
    {
        try {
            if (strpos($name, 'Shopware_') !== 0) {
                $name = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
                $name = 'Shopware_CronJob_' . $name;
            }

            $job = $this->cron->getJobByAction($name);

            if ($job !== null) {
                return $job;
            }

            throw new RuntimeException(sprintf('Cron not found by action name "%s".', $name));
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return null;
    }
}