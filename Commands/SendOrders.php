<?php

namespace WundeDcompanion\Commands;

use Exception;
use Shopware\Commands\ShopwareCommand;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Order;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use Shopware\Models\Order\Status;
use WundeDcompanion\Services\OrderService;

/**
 * Class Test
 * @package WundeDcompanion\Commands
 */
class SendOrders extends ShopwareCommand
{
    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * SendOrders constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        parent::__construct();
        $this->orderService = $orderService;
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dc:send')
            ->setDescription('Send orders')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->orderService->sendOrders();
        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}