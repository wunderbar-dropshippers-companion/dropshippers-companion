<?php
/**
 * @project Dropshippers Companion
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/05/2020 12:45
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;

use Doctrine\ORM\Query\Expr;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Article;
use Shopware\Models\Article\Detail;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\PickwareService;
use Enlight\Event\SubscriberInterface;
use Enlight_Hook_HookArgs;
use Shopware\Plugins\ViisonPickwareERP\Components\StockLedger\StockChangeList\StockInitializingService;
use Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;

class PickwareSubscriber implements SubscriberInterface
{
    const UNIINITIALIZED_ARTICLES_FILE = '/Data/Pickware/uninitialized_articles.json';

    /**
     * @var PickwareService $pickware
     */
    private $pickware;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var Expr
     */
    private $expr;

    /**
     * PickwareSubscriber constructor.
     * @param PickwareService $pickware
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct($pickware, $em, $logger, $wholesalerRepository)
    {
        $this->pickware = $pickware;
        $this->em = $em;
        $this->logger = $logger;
        $this->expr = $em->getExpressionBuilder();
        $this->wholesalerRepository = $wholesalerRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
//            'Shopware_Controllers_Backend_ViisonPickwareERPWarehouseManagement::deleteWarehousesAction::after' => 'onChangeWarehouse',
//            'Shopware_Controllers_Backend_ViisonPickwareERPWarehouseManagement::createWarehousesAction::after' => 'onChangeWarehouse',
//            'Shopware_Controllers_Backend_ViisonPickwareERPWarehouseManagement::updateWarehousesAction::after' => 'onChangeWarehouse',
            'Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization::getArticlesAction::after' => 'onGetArticles',
//            'Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization::initializeStocksForABatchOfUninitializedArticlesAction::after' => 'onInitializeArticles',
//            'Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization::createStockEntryAction::after' => 'onCreateStockEntry'
        ];
    }

//    public function onCreateStockEntry(Enlight_Hook_HookArgs $args)
//    {
//        try {
//            /** @var Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization $controller */
//            $controller = $args->getSubject();
//            $detailIds = $controller->Request()->getParam('detailIds', []);
//
//            $this->file->saveToJson(self::UNIINITIALIZED_ARTICLES_FILE, $detailIds);
//            $this->initializeArticles();
//
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), $e);
//        }
//    }
//
//    public function onInitializeArticles(Enlight_Hook_HookArgs $args)
//    {
//        $this->initializeArticles();
//    }
//
//    private function initializeArticles()
//    {
//        try {
//            $detailIds = $this->file->file_exists(self::UNIINITIALIZED_ARTICLES_FILE)
//                ? $this->file->loadFromJson(self::UNIINITIALIZED_ARTICLES_FILE)
//                : [];
//
//            $wholesalers = $this->repo->wholesaler()->findAll();
//
//            /** @var Detail[] $details */
//            $details = $this->repo->articleDetail()->createQueryBuilder('detail')
//                ->where($this->db->expr()->in('detail.id', ':detailIds'))
//                ->setParameter('detailIds', $detailIds)
//                ->getQuery()
//                ->getResult();
//
//            foreach ($details as $detail) {
//                $attribute = $detail->getAttribute();
//
//                foreach ($wholesalers as $wholesaler) {
//                    $instockMethod = 'getDc' . ucfirst(strtolower($wholesaler->getShortname())) . 'Instock';
//                    $priceMethod = 'getDc' . ucfirst(strtolower($wholesaler->getShortname())) . 'PurchasingPrice';
//                    $isActiveMethod = 'getDc' . ucfirst(strtolower($wholesaler->getShortname())) . 'Active';
//
//                    if (
//                        method_exists($attribute, $instockMethod) &&
//                        method_exists($attribute, $priceMethod) &&
//                        method_exists($attribute, $isActiveMethod) &&
//                        $attribute->{$isActiveMethod}()
//                    ) {
//                        $this->pickware->setStock(
//                            $detail,
//                            $wholesaler->getConfig()->pickwareWarehouse(),
//                            $attribute->{$instockMethod}(),
//                            $attribute->{$priceMethod}()
//                        );
//                    }
//                }
//            }
//        } catch (Exception $e) {
//            $this->logger->error($e->getMessage(), $e);
//        }
//    }

    public function onGetArticles(Enlight_Hook_HookArgs $args)
    {
        try {
            /** @var Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization $controller */
            $controller = $args->getSubject();
            $view = $controller->View();
            $articles = $view->getAssign('data');

            $ids = array_column($articles, 'id');

            /** @var Detail[] $details */
            $details = $this->em->getRepository(Detail::class)
                ->createQueryBuilder('detail')
                ->indexBy('detail', 'detail.id')
                ->where($this->expr->in('detail.id', ':ids'))
                ->setParameter('ids', $ids)
                ->getQuery()
                ->getResult();

            $wholesalers = $this->wholesalerRepository->findAllActive();

            foreach ($articles as &$article)
            {
                $detail = $details[$article['id']];

                $sales = $article['openOrders'];
                $instock = $detail->getInStock();

                foreach ($wholesalers as $wholesaler)
                {
                    $shortname = ucfirst(strtolower($wholesaler->getShortname()));
                    $instock += $detail->getAttribute()->{'getDc'.$shortname.'Instock'}();
                }

                $article['instock'] = $instock;
                $article['actualStock'] = $instock + $sales;
            }

            $view->clearAssign('data');
            $view->assign('data', $articles);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }
    }
}