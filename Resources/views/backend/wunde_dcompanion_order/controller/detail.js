//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/controller/detail"}
Ext.define('Shopware.apps.WundeDcompanionOrder.controller.Detail', {
    override: 'Shopware.apps.Order.controller.Detail',

    init: function () {
        var me = this;

        me.control({
            'order-position-panel': {
                changeSource: me.onChangeSource,
                sendOrderPositions: me.onSendOrderPositions
            },
            'order-list': {
                sendOrderPositions: me.onSendOrderPositions,
                abortOrderProcess: me.onAbortOrderProcess
            }
        });

        me.callParent(arguments);
    },

    onAbortOrderProcess: function(orders, view) {
        var me = this;
        var orderIds = [];

        Ext.Array.each(orders, function(order) {
            orderIds.push(order.get('id'));
        }, me)

        view.setLoading(true);

        Ext.Ajax.request({
            url: '{url controller="WundeDcompanionOrder" action="abortOrderProcess"}',
            method: 'POST',
            params: {
                orderIds: Ext.JSON.encode(orderIds)
            },
            success: function (response) {
                view.setLoading(false);

                Shopware.Notification.createGrowlMessage(
                    'Info',
                    'Work in progress',
                    'Order',
                    'growl',
                    false
                );
            },
            failure: function (response) {
                view.setLoading(false);

                Shopware.Notification.createGrowlMessage(
                    'Info',
                    'Work in progress',
                    'Order',
                    'growl',
                    false
                );
            }
        });
    },

    onChangeSource: function(editor, grid, field) {
        var me = this;
        var record = grid.selModel.getLastSelected();
        var selected = field.lastSelection[0];

        me.setDcValues(editor, record, selected);
    },

    setDcValues: function(editor, record, selected) {
        if (selected.get('basename') === 'myShop') {
            record.set('dc_name_long', null);
            record.set('dc_name_short', null);
            record.set('dc_stock', 0);
            record.set('dc_purchasing_price', 0);
            record.set('dc_dropship_date', null);
            record.set('dc_dropship_active', false);
            record.set('dc_dropship_status', null);
            record.set('dc_dropship_status_message', null);
            record.set('dc_dropship_orders_id', null);
            record.set('dc_dropship_id', null);
            record.set('dc_dropship_tracking_carrier', null);
            record.set('dc_dropship_tracking_code', null);
        } else {
            record.set('dc_name_short', selected.get('shortname'))
            record.set('dc_dropship_active', true);
        }
    },

    onBeforeEdit: function(editor, e) {
        var columns = editor.editor.items.items;

        Ext.Array.each(columns, function(column) {
            if (column.name === 'articleNumber') {
                column.setValue(e.record.get('articleNumber'));
            } else if (column.name === 'articleName') {
                column.setValue(e.record.get('articleName'));
            }
        })

        var dcSourceField = Ext.getCmp('dcSourceField');

        dcSourceField.field.store = Ext.create('Shopware.apps.WundeDcompanionOrder.store.Source');
    },

    onArticleSelect: function(editor, value, record) {
        var columns = editor.editor.items.items;
        var updateButton = editor.editor.floatingButtons.items.items[0];

        updateButton.setDisabled(false);

        Ext.Array.each(columns, function(column) {
            if (column.name === 'articleNumber') {
                column.setValue(record.get('number'));
            } else if (column.name === 'articleName') {
                column.setValue(record.get('name'));
            } else if (column.name === 'price'){
                column.setValue(record.get('purchasePrice'));
            }
        })

        editor.context.record.set('articleId', record.get('articleId'));
        editor.context.record.set('articleDetailId', record.get('id'));
    },

    onSendOrderPositions: function(orders, view) {
        var me = this;
        var orderIds = [];

        Ext.Array.each(orders, function(order) {
            orderIds.push(order.get('id'));
        }, me)

        view.setLoading(true);

        Ext.Ajax.request({
            url: '{url controller="WundeDcompanionOrder" action="sendOrder"}',
            method: 'POST',
            params: {
                orderIds: Ext.JSON.encode(orderIds)
            },
            success: function (response) {
                view.setLoading(false);

                Shopware.Notification.createGrowlMessage(
                    'Info',
                    'Work in progress',
                    'Order',
                    'growl',
                    false
                );
            },
            failure: function (response) {
                view.setLoading(false);

                Shopware.Notification.createGrowlMessage(
                    'Info',
                    'Work in progress',
                    'Order',
                    'growl',
                    false
                );
            }
        });
    },
});
//{/block}