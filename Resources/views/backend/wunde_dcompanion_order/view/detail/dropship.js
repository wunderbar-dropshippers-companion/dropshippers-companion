Ext.define('Shopware.apps.WundeDcompanionOrder.view.detail.Dropship', {

    extend: 'Ext.container.Container',
    alias:'widget.order-dropship-panel',
    layout: 'fit',
    bodyPadding: 10,
    autoScroll: true,

    initComponent:function () {
        var me = this;

        me.items = [ me.createDropshipGrid() ];
        me.callParent(arguments);
    },

    createDropshipGrid: function() {
        var me = this;

        return Ext.create('Shopware.apps.Order.view.list.Dropship', {
            name: 'order-dropship-grid',
            store: me.record.getPositions(),
            locale: me.record.getLocale(),
            style: {
                borderTop: '1px solid #A4B5C0'
            },
            viewConfig: {
                enableTextSelection: false
            },
        });
    },
});
