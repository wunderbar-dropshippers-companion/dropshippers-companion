import DefaultSettings from '../../data/settings'

export default {
  namespaced: true,
  state: {
    settings: {},
  },
  getters: {
    isPrivacyAccepted: (state) => state.settings.hasOwnProperty('privacy') ? state.settings.privacy === false : false,
    getSettings: (state) => {
      var localSettings = state.settings

      localSettings.privacy = false

      return window.location.hostname === 'localhost'
        ? localSettings
        : state.settings
    },
  },
  mutations: {
    setSettings: (state, settings) => state.settings = settings,
    setPrivacy: (state, privacy) => state.settings.privacy = privacy
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getSettings'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let settings = response.data.hasOwnProperty('settings') ? response.data.settings : {};

          if (success) {
            commit('setSettings', settings)
            return settings
          } else {
            throw new Error();
          }
        })
        .catch(() => {
          console.warn('settings could not be loaded')
          commit('setSettings', DefaultSettings)
          return DefaultSettings
        })
    },
  }
}
