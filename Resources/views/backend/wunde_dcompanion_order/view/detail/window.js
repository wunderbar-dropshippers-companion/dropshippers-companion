//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/view/detail/window"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionOrder.view.detail.Window', {

    override: 'Shopware.apps.Order.view.detail.Window',

    width:'100%',
    height:'100%',

    createTabPanel: function() {
        var me = this;
        var tabPanel = me.callOverridden(arguments);

        tabPanel.add(
            Ext.create('Shopware.apps.WundeDcompanionOrder.view.detail.Dropship', {
                title: '{s name=dropship_informations}Dropship informations{/s}',
                record: me.record,
            })
        )

        return tabPanel;
    }
});
//{/block}
