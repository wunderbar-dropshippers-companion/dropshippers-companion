import { db } from "../../plugins";
import shopData from '../../data/shop'
import PluginModule from './shop/plugins'

function isSuccess(response) {
  return response.data.hasOwnProperty('success') ? response.data.success : false;
}

export default {
  namespaced: true,
  state: {
    shop: null,
    email: '',
    host: '',
    name: '',
    version: ''
  },
  getters: {
    getShop: (state) => state.shop,
    getdata: (state) => {
      return {
        email: state.email,
        host: state.host,
        name: state.name,
        version: state.version
      }
    }
  },
  mutations: {
    setShop: (state, shop) => state.shop = shop,
    setData: (state, data) => {
      state.email = data.hasOwnProperty('email') ? data.email : '';
      state.host = data.hasOwnProperty('host') ? data.host : '';
      state.name=  data.hasOwnProperty('name') ? data.name : '';
      state.version=  data.hasOwnProperty('version') ? data.version : '';
    }
  },
  actions: {
    refered({}, reference) {
      return reference.get()
        .then(documentSnapshot => {
          return documentSnapshot.data()
        })
    },
    create({commit, dispatch, getters}, data) {
      return db.collection('shops')
        .add(data)
        .then(documentReference => {
          return documentReference
            .get()
            .then(documentSnapshot => {
              commit('setShop', documentSnapshot)
              dispatch('tickets/create', null, {root: true})
              return documentSnapshot.id
            })
            .catch(() => {
              console.warn('shop could not be created')
              return false
            })
        })
        .catch(() => {
          console.warn('shop could not be created')
          return false
        })
    },
    fetch({commit, rootGetters, dispatch, getters}, data) {
      var user = rootGetters['user/getUser']

      return db.collection('shops')
        .where('host', '==', data.host)
        .get()
        .then(querySnapshot => {
          if (querySnapshot.empty) {
            data['users'] = []
            if (user.email.length > 0) data['users'].push(user.email)
            return dispatch('create', data)
          } else {
            var shop = querySnapshot.docs[0]
            if (shop.data().hasOwnProperty('users')) {
              if (shop.data().users.includes(user.email)) {
                commit('setShop', shop)
                return shop.id
              } else {
                var users = shop.data().users

                if (user.email.length > 0) users.push(user.email)

                shop.users = users
                commit('setShop', shop)

                return shop.ref.update({users: users})
                  .finally(() => {
                    return shop.id
                  })
              }
            } else {
              shop['users'] = []

              if (user.email.length > 0) shop['users'].push(user.email)

              commit('setShop', shop)

              return shop.ref.update({users: shop.users})
                .finally(() => {
                  return shop.id
                })
            }
          }
        })
        .catch(() => {
          console.log('message 4')
          console.warn('shop could not be fetched')
          return null
        })
    },
    listen({commit}, shopId) {
      db.collection('shops')
        .doc(shopId)
        .onSnapshot(documentSnapshot => {
          commit('setShop', documentSnapshot)
        })
    },
    load({commit, rootGetters, dispatch, getters}) {
      if (getters['getShop'] === null) {
        return axios.get(rootGetters.getUrl('getShop'))
          .then(response => {
            if (isSuccess(response)) {
              var shop = response.data.hasOwnProperty('shop') ? response.data.shop : null

              console.log('message 1', shop)
              if (shop === null) throw new Error('shop could not be loaded')

              commit('setData', shop)

              if (shop.hasOwnProperty('host')) {
                return dispatch('fetch', shop)
                  .then(shopId => {
                    if (shopId !== null) {
                      dispatch('listen', shopId)
                      return getters['getShop']
                    } else {
                      console.log('message 2', shopId)
                      throw new Error('shop could not be loaded');
                    }
                  })
                  .catch(error => {
                    console.warn(error.message)
                    return false
                  })
              }
            } else {
              console.log('is not success', response)
              throw new Error('shop could not be loaded');
            }
          })
          .catch(error => {
            console.warn(error.message)
            commit('setData', shopData)
            return dispatch('fetch', shopData)
              .then(shopId => {
                if (shopId !== null) {
                  dispatch('listen', shopId)
                  return getters['getShop']
                } else {
                  console.log('message 3', shopId)
                  throw new Error('shop snapshot could not be fetched');
                }
              })
              .catch(error => {
                console.warn(error.message)
                return false
              })
          })
      } else {
        return getters['getShop']
      }
    },
  },
  modules: {
    plugins: PluginModule
  }
}
