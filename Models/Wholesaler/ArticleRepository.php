<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 28/08/2020 17:57
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler;


use WundeDcompanion\Services\SchemaService;
use function Clue\StreamFilter\fun;

/**
 * Class ArticleRepository
 * @package WundeDcompanion\Models\Wholesaler
 */
class ArticleRepository
{
    /**
     * @var Wholesaler $wholesaler
     */
    private $wholesaler;

    /**
     * @var string $pluginDir
     */
    private $pluginDir;

    /**
     * Article constructor.
     * @param Wholesaler $wholesaler
     */
    public function __construct(Wholesaler $wholesaler)
    {
        $this->wholesaler = $wholesaler;
        $this->pluginDir = Shopware()->Container()->getParameter('wunde_dcompanion.plugin_dir');
    }

    /**
     * @param int $articleId
     * @return array
     */
    public function findByArticleId($articleId)
    {
        foreach ($this->findAll() as $article)
        {
            if ((int)$article['articleId'] === (int)$articleId) {
                return $article;
            }
        }

        return null;
    }

    /**
     * @param int $detailId
     * @return array|null
     */
    public function findByDetailId(int $detailId)
    {
        foreach ($this->findAll() as $article)
        {
            if ((int)$article['detailId'] === (int)$detailId) {
                return $article;
            }
        }

        return null;
    }

    /**
     * @param string $ordernumber
     * @return array|null
     */
    public function findByOrdernumber(string $ordernumber)
    {
        foreach ($this->findAll() as $article)
        {
            if (
                isset($article['dc_' . $this->wholesaler->getShortname() . '_ordernumber']) &&
                $article['dc_' . $this->wholesaler->getShortname() . '_ordernumber'] === $ordernumber
            ) {
                return $article;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        /** @var SchemaService $schemaService */
        $schemaService = Shopware()->Container()->get('wunde_dcompanion.services.schema');

        if ($schemaService->tablesExist('s_articles_attributes')) {

            $tables = json_decode(file_get_contents($this->pluginDir . '/Data/tables.json'), true);
            $extensionTables = $tables['extension'];

            $columns = array_map(function($value) {
                return str_replace('%shortname%', $this->wholesaler->getShortname(), $value);
            }, array_column($extensionTables['s_articles_attributes'], 'name'));

            if ($schemaService->columnsExist('s_articles_attributes', $columns)) {
                $builder = Shopware()->Models()->getConnection()->createQueryBuilder();

                $columns = array_map(function($column) {
                    return 'attr.' . $column;
                }, $columns);

                $columns = array_merge($columns, [
                    'attr.id as attributeId',
                    'attr.articledetailsID as detailId',
                    'd.articleID as articleId'
                ]);

                return $builder
                    ->select($columns)
                    ->from('s_articles_attributes', 'attr')
                    ->join('attr', 's_articles_details', 'd', 'd.id = attr.articledetailsID')
                    ->execute()
                    ->fetchAll();
            }
        }

        return [];
    }
}