import firebase from 'firebase/app'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBO5ix2YYXqwbCadexG8tZUy8o_IiYgoq8",
  authDomain: "dc-manager-68665.firebaseapp.com",
  projectId: "dc-manager-68665",
  storageBucket: "dc-manager-68665.appspot.com",
  messagingSenderId: "22522948902",
  appId: "1:22522948902:web:506ac2ee55e77678c025e7"
}
firebase.initializeApp(firebaseConfig)

const db = firebase.firestore()

const useEmulator = true

if (process.env.NODE_ENV === 'development' && useEmulator) {
  db.settings({
    host: 'localhost:8000',
    ssl: false
  })
}

export {db, firebase}
