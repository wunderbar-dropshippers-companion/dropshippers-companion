<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/09/2020 23:52
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler\Config;


use Shopware\CustomModels\ViisonPickwareERP\Warehouse\Warehouse;
use WundeDcompanion\Services\PickwareService;

/**
 * Class Pickware
 * @package WundeDcompanion\Models\Wholesaler\Config
 */
class Pickware
{
    /**
     * @var int $warehouseId
     */
    private $warehouseId;

    /**
     * Pickware constructor.
     * @param string|int $warehouseId
     */
    public function __construct($warehouseId)
    {
        $this->warehouseId = (int)$warehouseId;
    }

    /**
     * @return int
     */
    public function warehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * @return Warehouse|null
     */
    public function warehouse()
    {
        $di = Shopware()->Container();
        $em = Shopware()->Models();

        $warehouse = null;

        if ($di->has('wunde_dcompanion.services.pickware')) {
            /** @var PickwareService $pickwareService */
            $pickwareService = $di->get('wunde_dcompanion.services.pickware');

            if ($pickwareService->isActive()) {
                /** @var Warehouse $warehouse */
                $warehouse = $em->getRepository(Warehouse::class)->find($this->warehouseId);
            }
        }

        return $warehouse;
    }
}