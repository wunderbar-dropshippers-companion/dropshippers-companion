<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 18:52
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Seeder;

use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Helpers\Database\Database;
use WundeDcompanion\Models\Ticket\Status;

/**
 * Class StateSeeder
 * @package WundeDcompanion\Seeder
 */
abstract class StateSeeder extends DatabaseSeeder
{
    /**
     * @var string[] $states
     */
    private static $states = [
        Status::STATE_NO_SOLUTION => [
            'name' => 'no_solution',
            'color' => 'red lighten-3',
            'selectable' => false
        ],
        Status::STATE_WAITING => [
            'name' => 'waiting',
            'color' => 'deep-purple lighten-4',
            'selectable' => false
        ],
        Status::STATE_ASSIGNED => [
            'name' => 'assigned',
            'color' => 'green lighten-3',
            'selectable' => true
        ],
        Status::STATE_IN_PROGRESS => [
            'name' => 'in_progress',
            'color' => 'amber lighten-3',
            'selectable' => false
        ],
        Status::STATE_SOLVED => [
            'name' => 'solved',
            'color' => 'green lighten-2',
            'selectable' => true
        ],
    ];

    public static function seed()
    {
        $em = Shopware()->Container()->get('models');
        $logger = Shopware()->Container()->get('pluginlogger');

        if (Database::entityTableExists(Status::class)) {
            foreach (self::$states as $id => $item) {
                try {
                    /** @var Status $state */
                    $state = $em->getRepository(Status::class)->find($id);

                    if (is_null($state)) {
                        $state = new Status($id, $item['name'], $item['color'], $item['selectable']);
                        $em->persist($state);
                    } else {
                        $state->setName($item['name']);
                        $state->setColor($item['color']);
                        $state->setSelectable($item['selectable']);
                    }
                } catch (Exception $e) {
                    $logger->error($e->getMessage(), $e->getTrace());
                }
            }
        }
    }
}