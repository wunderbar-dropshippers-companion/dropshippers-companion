<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 09/10/2020 13:36
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Ticket;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_states")
 */
class Status extends ModelEntity
{
    /**
     * Consts defining states
     */
    const STATE_NO_SOLUTION = -10;
    const STATE_WAITING = 0;
    const STATE_ASSIGNED = 10;
    const STATE_IN_PROGRESS = 20;
    const STATE_SOLVED = 30;

    const SNIPPET_NAMES = [
        'state/no_solution',
        'state/waiting',
        'state/assigned',
        'state/in_progress',
        'state/solved',
    ];

    /**
     * @var int $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(nullable=false)
     */
    private $name;

    /**
     * @var string $color
     * @ORM\Column(nullable=false)
     */
    private $color;

    /**
     * @var boolean $selectable
     * @ORM\Column(name="selectable", type="boolean")
     */
    private $selectable = false;

    /**
     * Status constructor.
     * @param int $id
     * @param string $name
     * @param string $color
     * @param boolean $selectable
     */
    public function __construct($id, $name, $color, $selectable = false)
    {
        $this->id = $id;
        $this->name = $name;
        $this->color = $color;
        $this->selectable = $selectable;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return bool
     */
    public function isSelectable()
    {
        return $this->selectable;
    }

    /**
     * @param bool $selectable
     */
    public function setSelectable($selectable)
    {
        $this->selectable = $selectable;
    }
}