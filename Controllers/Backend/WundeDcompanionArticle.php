<?php

/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:42
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Detail as ArticleDetail;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\PickwareService;
use WundeDcompanion\Services\SchemaService;

class Shopware_Controllers_Backend_WundeDcompanionArticle extends Shopware_Controllers_Backend_ExtJs
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_article/main';

    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var SchemaService $schema
     */
    private $schema;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    private $snippets;

    /**
     * @var PickwareService $pickwareService
     */
    private $pickwareService;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * Shopware_Controllers_Backend_WundeDcompanionArticle constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->wholesalerRepository = Shopware()->Container()->get('wunde_dcompanion.repositories.wholesalers');

        $this->em = Shopware()->Container()->get('models');
        $this->logger = Shopware()->Container()->get('wunde_dcompanion.services.logger');
        $this->schema = Shopware()->Container()->get('wunde_dcompanion.services.schema');
        $this->pickwareService = Shopware()->Container()->get('wunde_dcompanion.services.pickware');

        $this->snippets = Shopware()->Snippets()->getNamespace(self::SNIPPET_NAMESPACE);
    }

    public function updateAction()
    {
        $success = false;
        $message = 'OK';

        $detailId = $this->Request()->getParam('detailId', null);
        $shortname = $this->Request()->getParam('shortname', null);
        $active = $this->Request()->getParam('active', false);
        $ordernumber = trim($this->Request()->getParam('ordernumber', ''));
        $overrideName = $this->Request()->getParam('override_name', false);
        $overridePrice = $this->Request()->getParam('override_price', false);
        $delete = $this->Request()->getParam('delete', false);

        $wholesaler = $this->wholesalerRepository->findByShortname($shortname);

        /** @var ArticleDetail $article */
        $article = $this->em->getRepository(ArticleDetail::class)->find($detailId);

        if ($delete) {
            Shopware()->Models()->getConnection()->createQueryBuilder()
                ->update('s_articles_attributes')
                ->set('dc_' . $shortname . '_ordernumber', ':ordernumber')
                ->set('dc_' . $shortname . '_articlename', ':articlename')
                ->set('dc_' . $shortname . '_purchasing_price', ':purchasing_price')
                ->set('dc_' . $shortname . '_price_recommended', ':price_recommended')
                ->set('dc_' . $shortname . '_instock', ':instock')
                ->set('dc_' . $shortname . '_active', ':active')
                ->set('dc_' . $shortname . '_override_name', ':override_name')
                ->set('dc_' . $shortname . '_override_price', ':override_price')
                ->set('dc_' . $shortname . '_description', ':description')
                ->where('articledetailsID = :detailId')
                ->setParameter('ordernumber', null)
                ->setParameter('articlename', null)
                ->setParameter('purchasing_price', 0)
                ->setParameter('price_recommended', 0)
                ->setParameter('instock', 0)
                ->setParameter('active', false)
                ->setParameter('override_name', false)
                ->setParameter('override_price', false)
                ->setParameter('description', null)
                ->setParameter('detailId', $detailId)
                ->execute();

            if (!is_null($article) && !is_null($wholesaler) && $this->pickwareService->isActive()) {
                $warehouse = $wholesaler->getConfig()->pickware()->warehouse();

                $this->pickwareService->setStock(
                    $article,
                    $warehouse,
                    0,
                    $article->getPurchasePrice()
                );
            }

            $success = true;
        } else {
            $wholesaler = $this->wholesalerRepository->findByShortname($shortname);

            $exists = (bool) count(array_filter($wholesaler->articles()->findAll(), function ($article) use ($ordernumber, $shortname, $detailId) {
                return (int)$article['detailId'] === (int)$detailId
                    ? false
                    : $article['dc_' . $shortname . '_ordernumber'] === $ordernumber;
            }));

            if ($exists) {
                $message = $this->snippets->get(
                    'error/shop/articleAlreadyExists',
                    'There is already one article with this ordernumber!'
                );
            } else {
                if (!is_null($ordernumber)) {
                    $response = $wholesaler->getClient()->getAll($ordernumber);

                    if ($response->getStatusCode() === 200) {
                        $products = json_decode($response->getContent(), true);

                        if (array_key_exists($ordernumber, $products)) {

                            $data = $products[$ordernumber];

                            Shopware()->Models()->getConnection()->createQueryBuilder()
                                ->update('s_articles_attributes')
                                ->set('dc_' . $shortname . '_ordernumber', ':ordernumber')
                                ->set('dc_' . $shortname . '_articlename', ':articlename')
                                ->set('dc_' . $shortname . '_purchasing_price', ':purchasing_price')
                                ->set('dc_' . $shortname . '_price_recommended', ':price_recommended')
                                ->set('dc_' . $shortname . '_instock', ':instock')
                                ->set('dc_' . $shortname . '_active', ':active')
                                ->set('dc_' . $shortname . '_override_name', ':override_name')
                                ->set('dc_' . $shortname . '_override_price', ':override_price')
                                ->set('dc_' . $shortname . '_description', ':description')
                                ->where('articledetailsID = :detailId')
                                ->setParameter('ordernumber', $ordernumber)
                                ->setParameter('articlename', $data['name'])
                                ->setParameter('purchasing_price', $data['purchasePrice'])
                                ->setParameter('price_recommended', $data['priceRecommended'])
                                ->setParameter('instock', $data['quantity'])
                                ->setParameter('active', $active)
                                ->setParameter('override_name', $overrideName)
                                ->setParameter('override_price', $overridePrice)
                                ->setParameter('description', $data['description'])
                                ->setParameter('detailId', $detailId)
                                ->execute();

                            if (!is_null($article) && !is_null($wholesaler) && $this->pickwareService->isActive()) {
                                $warehouse = $wholesaler->getConfig()->pickware()->warehouse();

//                                $purchasePrice = $overridePrice
//                                    ? (float)$data['purchasePrice']
//                                    : $article->getPurchasePrice();

                                $this->pickwareService->setStock(
                                    $article,
                                    $warehouse,
                                    (int)$data['quantity'],
                                    (float)$data['purchasePrice']
                                );
                            }

                            $success = true;
                        }
                    } else {
                        $message = $this->snippets->get(
                            'error/wholesaler/articleNotExists',
                            'Article is not available at the wholesaler [0]!'
                        );
                    }
                } else {
                    Shopware()->Models()->getConnection()->createQueryBuilder()
                        ->update('s_articles_attributes')
                        ->set('dc_' . $shortname . '_active', ':active')
                        ->where('articledetailsID = :detailId')
                        ->setParameter('active', $active)
                        ->setParameter('detailId', $detailId)
                        ->execute();

                    $success = true;
                }
            }
        }

        if (!$success && $message === 'OK') {
            $message = $this->snippets->get(
                'error/unknown',
                'An unknown error occurred, please check your server logs'
            );
        }

        $this->view->assign([
            'success' => $success,
            'message' => $message,
            'wholesalers' => $this->getArticleWholesalers($detailId, $shortname, true)
        ]);
    }

    private function getArticleWholesalers($detailId, $shortname = null, $live = false, $showUpdateMessage = false)
    {
        $wholesalers = is_null($shortname)
            ? $this->wholesalerRepository->findAll()
            : [$this->wholesalerRepository->findByShortname($shortname)];
        $data = [];

        foreach ($wholesalers as $wholesaler)
        {
            $columns = [
                'dc_' . $wholesaler->getShortname() . '_ordernumber',
                'dc_' . $wholesaler->getShortname() . '_articlename',
                'dc_' . $wholesaler->getShortname() . '_instock',
                'dc_' . $wholesaler->getShortname() . '_active',
                'dc_' . $wholesaler->getShortname() . '_purchasing_price',
                'dc_' . $wholesaler->getShortname() . '_price_recommended',
                'dc_' . $wholesaler->getShortname() . '_override_price',
                'dc_' . $wholesaler->getShortname() . '_override_name',
                'dc_' . $wholesaler->getShortname() . '_description',
            ];

            if ($this->schema->columnsExist('s_articles_attributes', $columns)) {
                $result = Shopware()->Models()->getConnection()->createQueryBuilder()
                    ->select([
                        'dc_' . $wholesaler->getShortname() . '_ordernumber as ordernumber',
                        'dc_' . $wholesaler->getShortname() . '_articlename as articlename',
                        'dc_' . $wholesaler->getShortname() . '_instock as instock',
                        'dc_' . $wholesaler->getShortname() . '_active as active',
                        'dc_' . $wholesaler->getShortname() . '_purchasing_price as purchasing_price',
                        'dc_' . $wholesaler->getShortname() . '_price_recommended as price_recommended',
                        'dc_' . $wholesaler->getShortname() . '_override_price as override_price',
                        'dc_' . $wholesaler->getShortname() . '_override_name as override_name',
                        'dc_' . $wholesaler->getShortname() . '_description as description',
                    ])
                    ->from('s_articles_attributes')
                    ->where('articleDetailsID = :detailId')
                    ->setParameter('detailId', $detailId)
                    ->execute()
                    ->fetch();

                if ($result && is_array($result)) {
                    $prefix = 'dc_' . $wholesaler->getShortname() . '_';

                    $result = array_merge($result, [
                        'longname' => $wholesaler->getLongname(),
                        'shortname' => $wholesaler->getShortname(),
                        'basename' => $wholesaler->getBasename(),
                        'live' => false,
                        'show_update_Message' => $showUpdateMessage
                    ]);

                    $result = $this->schema->castValues('s_articles_attributes', $result, $prefix);

                    if (
                        $live &&
                        $result['active'] &&
                        !empty($result['ordernumber']) && !is_null($result['ordernumber']) &&
                        !empty($result['articlename']) && !is_null($result['articlename'])
                    ) {
                        $response = $wholesaler->getClient()->getAll($result['ordernumber']);

                        if ($response->getStatusCode() === 200) {
                            $article = json_decode($response->getContent(), true);

                            if (array_key_exists($result['ordernumber'], $article)) {
                                $ordernumber = $result['ordernumber'];

                                $result['instock'] = $article[$ordernumber]['quantity'];
                                $result['purchasing_price'] = (float)str_replace(',', '.', $article[$ordernumber]['purchasePrice']);
                                $result['live'] = true;

                                Shopware()->Models()->getConnection()->createQueryBuilder()
                                    ->update('s_articles_attributes')
                                    ->set('dc_' . $wholesaler->getShortname() . '_instock', ':instock')
                                    ->set('dc_' . $wholesaler->getShortname() . '_purchasing_price', ':purchasingPrice')
                                    ->where('articleDetailsID = :detailId')
                                    ->setParameter('instock', $result['instock'])
                                    ->setParameter('purchasingPrice', $result['purchasing_price'])
                                    ->setParameter('detailId', $detailId)
                                    ->execute();

                                /** @var ArticleDetail $detail */
                                $detail = $this->em->getRepository(ArticleDetail::class)->find($detailId);

                                if (!is_null($article) && !is_null($wholesaler) && $this->pickwareService->isActive()) {
                                    $warehouse = $wholesaler->getConfig()->pickware()->warehouse();

                                    $this->pickwareService->setStock(
                                        $detail,
                                        $warehouse,
                                        $result['instock'],
                                        $result['purchasing_price']
                                    );
                                }
                            }
                        }
                    }

                    $data[] = $result;
                }
            }
        }

        return $data;
    }


    public function listAction()
    {
        $detailId = $this->Request()->getParam('detailId', null);
        $showUpdateMessage = $this->Request()->getParam('showUpdateMessage', 'false') !== 'false';
        $live = $this->Request()->getParam('live', 'false') !== 'false';

        $data = !is_null($detailId)
            ? $this->getArticleWholesalers($detailId, null, $live, $showUpdateMessage)
            : [];

        $this->view->assign('wholesalers', $data);
    }
}