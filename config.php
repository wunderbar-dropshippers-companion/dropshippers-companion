<?php return [
    'db' => [
        'host' => '127.0.0.1',
        'port' => '3307',
        'username' => 'zone1987',
        'password' => 'Guck16guck!',
        'dbname' => 'sw5602',
    ],
    'front' => [
        'throwExceptions' => true,
        'showException' => true
    ],
    'phpsettings' => [
        'display_errors' => 1
    ],

    'template' => [
        'forceCompile' => true
    ],
    'csrfProtection' => [
        'frontend' => true,
        'backend' => true
    ],
    'httpcache' => [
        'debug' => true
    ],
];