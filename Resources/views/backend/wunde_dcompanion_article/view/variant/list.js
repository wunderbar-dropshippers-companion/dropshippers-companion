//{block name="backend/article/view/variant/list" append}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionArticle.view.variant.List', {
    override: 'Shopware.apps.Article.view.variant.List',

    getColumns: function () {
        let me = this;
        let columns = me.callOverridden(arguments);

        Ext.Array.push(columns, {
            header: 'DC',
            width: 30,
            hidden: false,
            hideable: false,
            renderer: me.getDCColumnInfo
        });

        me.getWholesalers(function(wholesalers) {

            Ext.Array.each(wholesalers, function(wholesaler) {
                let lowerShortname = wholesaler.shortname.toLowerCase();
                let CammelShortname = lowerShortname.charAt(0).toUpperCase() + lowerShortname.slice(1);

                columns.unshift({
                    header: wholesaler.shortname.toUpperCase(),
                    width: 50,
                    align: 'center',
                    tooltip: wholesaler.longname,
                    hidden: false,
                    hideable: false,
                    renderer: function(value, metaData, record) {
                        return me.getColorate(record.raw.attribute['dc' + CammelShortname + 'Instock'])
                    }
                });
            });
        })

        return columns;
    },

    getWholesalers: function(callback) {
        Ext.Ajax.request({
            url: '{url controller=WundeDcompanionArticleList action=list}',
            async: false,
            success: function (response) {
                response = Ext.JSON.decode(response.responseText, true);

                if (response.hasOwnProperty('data')) {
                    callback(response.data);
                } else {
                    callback([]);
                }
            }
        })
    },

    getDCColumnInfo: function(value, metaData, record) {
        return record.raw.dc_has_article;
    },

    getColorate: function(value)
    {
        value = value || 0;
        return (value > 0)
            ? '<span style="color:green;">' + value + '</span>'
            : '<span style="color:red;">' + value + '</span>';
    }
});
//{/block}