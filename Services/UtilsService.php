<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 13:41
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


class UtilsService
{
    /**
     * @param string $string
     * @return string
     */
    public function camelCaseToUnderscore($string)
    {
        return strtolower(ltrim(preg_replace('/[A-Z]/', '_$0', $string), '_'));
    }

    /**
     * @param $string
     * @return string
     */
    public function underscoreToCamelCase($string)
    {
        return lcfirst(implode('', array_map('ucfirst', explode('_', $string))));
    }
}