<?php
/**
 * @project Dropshippers Companion
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 17/06/2020 08:31
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Commands\Pickware;

use Shopware\Components\Model\ModelManager;
use Shopware\CustomModels\ViisonPickwareERP\StockLedger\StockLedgerEntry;
use WundeDcompanion\Services\DatabaseService;
use WundeDcompanion\Services\PickwareService;
use WundeDcompanion\Services\RepoService;
use Shopware\Models\Article\Article;
use Shopware\Models\Article\Detail;
use Exception;
use Shopware\Commands\ShopwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Test
 * @package WundeDcompanion\Commands\ResetInitialization
 */
class ResetInitialization extends ShopwareCommand
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var PickwareService $pickware
     */
    private $pickware;

    /**
     * @param ModelManager $em
     * @param PickwareService $pickware
     */
    public function __construct($em, $pickware)
    {
        parent::__construct();
        $this->em = $em;
        $this->pickware = $pickware;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('pickware:reset:initialization')
            ->setDescription('resets the pickware article initialization')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

        /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if ($this->pickware->isInstalled()) {
                if ($this->pickware->isActive()) {
                    /** @var Article[] $articles */
                    $articles = $this->em->getRepository(Article::class)->findAll();

                    foreach ($articles as $article) {
                        /** @var Detail $detail */
                        foreach ($article->getDetails()->getValues() as $detail) {
                            $detail->getAttribute()->setPickwarePhysicalStockForSale(0);
                            $detail->getAttribute()->setPickwareStockInitialized(false);
                            $detail->getAttribute()->setPickwareStockInitializationTime(null);

                            $output->writeln('<info>initialization for article: "' . $detail->getNumber() . '" reset</info>');
                        }
                    }
                    $output->writeln(PHP_EOL . '<info>Finished</info>');
                } else {
                    $output->writeln('<info>Pickware is not active!</info>');
                }

                /** @var StockLedgerEntry[] $stockLedgerEntries */
                $stockLedgerEntries = $this->em->getRepository(StockLedgerEntry::class)->findAll();

                foreach ($stockLedgerEntries as $stockLedgerEntry)
                {
                    $this->em->remove($stockLedgerEntry);
                }

                $this->em->flush();
            } else {
                $output->writeln('<info>Pickware is not installed!</info>');
            }

        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}