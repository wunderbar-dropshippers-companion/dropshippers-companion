<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/09/2020 23:24
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler;


use WundeDcompanion\Models\Wholesaler\Config\Api;
use WundeDcompanion\Models\Wholesaler\Config\Pickware;
use WundeDcompanion\Models\Wholesaler\Config\Shipper;
use WundeDcompanion\Models\Wholesaler\Config\Shipping;

/**
 * Class Config
 * @package WundeDcompanion\Models\Wholesaler
 */
class Config
{

    /**
     * @var string $shortname
     */
    private $shortname = '';

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var Api $api
     */
    private $api;

    /**
     * @var Shipper $shipper
     */
    private $shipper;

    /**
     * @var Shipping $shipping
     */
    private $shipping;

    /**
     * @var Pickware $pickware
     */
    private $pickware;

    /**
     * Config constructor.
     * @param array $config
     * @param string $shortname
     */
    public function __construct($config, $shortname)
    {
        $this->config = $config;
        $this->shortname = $shortname;

        $this->api = new Api(
            $this->findConfig('endpoint', ''),
            $this->findConfig('username', ''),
            $this->findConfig('password', '')
        );

        $this->shipper = new Shipper(
            $this->findConfig('shipper_company', ''),
            $this->findConfig('shipper_company2', ''),
            $this->findConfig('shipper_firstname', ''),
            $this->findConfig('shipper_lastname', ''),
            $this->findConfig('shipper_street', ''),
            $this->findConfig('shipper_postcode', ''),
            $this->findConfig('shipper_city', ''),
            $this->findConfig('shipper_countrycode', ''),
            $this->findConfig('shipper_email', ''),
            $this->findConfig('shipper_phone', '')
        );

        $this->shipping = new Shipping(
            $this->findConfig('shipping_cost', 0.0),
            $this->findConfig('shipping_cost_exemption_limit', 0.0)
        );

        $this->pickware = new Pickware(
            $this->findConfig('pickware_warehouse', null)
        );
    }

    /**
     * @return Api
     */
    public function api()
    {
        return $this->api;
    }

    /**
     * @return Shipper
     */
    public function shipper()
    {
        return $this->shipper;
    }

    /**
     * @return Shipping
     */
    public function shipping()
    {
        return $this->shipping;
    }

    /**
     * @return Pickware
     */
    public function pickware()
    {
        return $this->pickware;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->config;
    }

    /**
     * @param string $target
     * @param mixed $default
     * @return mixed
     */
    private function findConfig($target, $default)
    {
        return array_key_exists('dc_' . $this->shortname . '_' . $target, $this->config)
            ? $this->config['dc_' . $this->shortname . '_' . $target]
            : $default;
    }
}