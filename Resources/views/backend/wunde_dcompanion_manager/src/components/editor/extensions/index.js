import Redo from './Redo'
import Undo from './Undo'
import Link from './Link'
import Image from './Image'
import Outdent from './Outdent'
import Indent from './Indent'
import Code from './Code'
import FormatBold from './FormatBold'
import FormatItalic from './FormatItalic'
import FormatSize from './FormatSize'
import FormatUnderline from './FormatUnderline'
import FormatListNumbered from './FormatListNumbered'
import FormatListBulleted from './FormatListBulleted'
import FormatStriketrough from './FormatStriketrough'
import FormatAlignCenter from './FormatAlignCenter'
import FormatAlignLeft from './FormatAlignLeft'
import FormatAlignRight from './FormatAlignRight'
import FormatAlignBlock from './FormatAlignBlock'
import HorizontalLine from './HorizontalLine'
import Separator from './Separator'

export default {
  Redo,
  Undo,
  Link,
  Code,
  Image,
  Outdent,
  Indent,
  Separator,
  FormatBold,
  FormatItalic,
  FormatSize,
  FormatUnderline,
  FormatListNumbered,
  FormatListBulleted,
  FormatStriketrough,
  FormatAlignCenter,
  FormatAlignLeft,
  FormatAlignRight,
  FormatAlignBlock,
  HorizontalLine
}
