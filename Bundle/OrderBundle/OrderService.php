<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 20/03/2021 10:57
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle;


use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\MailService;

class OrderService
{
    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var MailService $mailService
     */
    private $mailService;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * OrderService constructor.
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param MailService $mailService
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(ModelManager $em, LoggerService $logger, MailService $mailService, WholesalerRepository $wholesalerRepository)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->mailService = $mailService;
        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * @param Order $order
     * @param string $shortname
     * @return int
     */
    protected function hasWholesalerPositions(Order $order, string $shortname): int
    {
        /** @var Position[] $positions */
        $positions = $order->getDetails()->getValues();

        $wholesalerPositions = array_filter($positions, function($position) use($shortname) {
            $attributes = $position->getAttribute();

            return
                method_exists($attributes,'getDcNameShort') &&
                strtolower($attributes->getDcNameShort()) === strtolower($shortname);
        });

        return count($wholesalerPositions);
    }

    /**
     * @param Order $order
     */
    private function increaseTransmissionAttempt(Order $order)
    {
        $attribute = $order->getAttribute();

        $transmissionAttempt = $attribute->getDcDropshipTransmissionAttempt();

        if (is_null($transmissionAttempt)) {
            $transmissionAttempt = 0;
        }

        $transmissionAttempt++;

        $attribute->setDcDropshipTransmissionAttempt($transmissionAttempt);

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e, [], 'order');
        }
    }

    /**
     * @param Order $order
     */
    private function setOrderState(Order $order)
    {
        /** @var Status $completedState */
        $completedState = $this->em->getRepository(Status::class)->find(Status::ORDER_STATE_COMPLETED);

        if (!is_null($completedState)) {
            $this->em->getConnection()->createQueryBuilder()
                ->update('s_order')
                ->set('status', ':status')
                ->where('id = :id')
                ->setParameter('status', Status::ORDER_STATE_COMPLETED)
                ->setParameter('id', $order->getId())
                ->execute();
        }
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function transmissionAttemptLimitIsReached(Order $order): bool
    {
        return method_exists($order->getAttribute(), 'getDcDropshipTransmissionAttempt')
            ? $order->getAttribute()->getDcDropshipTransmissionAttempt() >= 3
            : false;
    }

    /**
     * @param Order[] $orders
     */
    public function send(array $orders)
    {
        foreach ($orders as $order)
        {
            $attribute = $order->getAttribute();

            if (
                method_exists($attribute, 'getDcSorted') &&
                $attribute->getDcSorted() &&
                $order->getPaymentStatus()->getId() === Status::PAYMENT_STATE_COMPLETELY_PAID
            ) {
                $results = [];

                foreach ($this->wholesalers as $wholesaler)
                {
                    if ($this->hasWholesalerPositions($order, $wholesaler->getShortname())) {
                        $client = $wholesaler->getClient();
                        $shipper = $wholesaler->getConfig()->shipper();

                        try {
                            if (method_exists($client, 'sendOrder')) {

                                $result = $client->sendOrder($order, $shipper);

                                if ($result) {
                                    $this->mailService->sendOrderMail($order);
                                }

                                $results[$wholesaler->getShortname()] = $result;
                            } else {
                                throw new Exception('Method sendOrder does not exist in the '. $wholesaler->getBasename() .' client');
                            }
                        } catch (Exception $e) {
                            $this->logger->error($e->getMessage(), $e, [], 'order');
                            $results[$wholesaler->getShortname()] = false;
                        }
                    }
                }

                if (array_sum($results) === count($results)) {
                    $this->setOrderState($order);
                } else {
                    $this->increaseTransmissionAttempt($order);
                }
            }
        }
    }
}