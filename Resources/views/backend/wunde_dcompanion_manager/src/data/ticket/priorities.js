export default [
  {
    "id": 1,
    "color": "deep-purple lighten-4",
    "snippet": "priority.low"
  },
  {
    "id": 2,
    "color": "green lighten-3",
    "snippet": "priority.normal"
  },
  {
    "id": 3,
    "color": "amber lighten-3",
    "snippet": "priority.high"
  },
  {
    "id": 4,
    "color": "red lighten-3",
    "snippet": "priority.very_high"
  }
]
