//{block name="backend/order/model/order/fields" append}
    { name: 'isDropship', type: 'string', mapping: 'is_dropship' },
    { name: 'dcDropshipStatus', type: 'string', mapping: 'dc_dropship_status' },
    { name: 'dcDropshipActive', type: 'string', mapping: 'dc_dropship_active' },
    { name: 'dcAutoOrder', type: 'boolean', mapping: 'dc_auto_order' },
    { name: 'dcDropshipInfos', mapping: 'dc_dropship_infos', reference: 'Shopware.apps.WundeDcompanionOrder.model.Info' },
//{/block}
