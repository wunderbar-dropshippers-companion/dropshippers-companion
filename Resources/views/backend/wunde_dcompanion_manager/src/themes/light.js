import colors from 'vuetify/lib/util/colors'

export default {
  primary: '#009FE3',
  navigationBg: colors.blueGrey.lighten5,
  secondary: colors.blueGrey.darken2,
  tabbarBg: colors.grey.lighten5,
  buttonbarBg: colors.grey.lighten5,
  extension: colors.grey.lighten5,
  background: colors.grey.lighten5,
  chart1: '#009FE3',
  chart2: '#808080',
  gridlines: colors.grey.lighten5
}
