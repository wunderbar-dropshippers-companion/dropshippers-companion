//{namespace name=backend/wunde_dcompanion_article_list/main}
//{block name="backend/article_list/view/main/grid" append}
Ext.define('Shopware.apps.WundeDcompanionArticleList.view.main.Grid', {

    override: 'Shopware.apps.ArticleList.view.main.Grid',

    dcSnippets: {
        dcInstock: '{s name=dc/instock}DC Instock{/s}',
    },

    getColumns: function () {
        let me = this;
        let columns = me.callOverridden(arguments);

        Ext.Array.push(columns,
            {
                header: 'DC',
                width: 30,
                sortable: true,
                resizable: false,
                groupable: false,
                hidden: false,
                hideable: false,
                align: 'center',
                renderer: me.getDCColumnInfo
            },
            {
                header: me.dcSnippets.dcInstock,
                width: 80,
                sortable: true,
                resizable: false,
                groupable: false,
                hidden: false,
                hideable: true,
                align: 'center',
                renderer: me.getTotalDcStock
            },
        );

        me.getWholesalers(function(wholesalers) {

            Ext.Array.each(wholesalers, function(wholesaler) {
                let lowerShortname = wholesaler.shortname.toLowerCase();
                let CammelShortname = lowerShortname.charAt(0).toUpperCase() + lowerShortname.slice(1);

                columns.unshift({
                    header: wholesaler.shortname.toUpperCase(),
                    width: 50,
                    align: 'center',
                    tooltip: wholesaler.longname,
                    hidden: false,
                    hideable: false,
                    renderer: function(value, metaData, record) {
                        return me.getColorate(record.raw['Attribute_dc' + CammelShortname + 'Instock'])
                    }
                });
            });
        })

        return columns;
    },

    getWholesalers: function(callback) {
        Ext.Ajax.request({
            url: '{url controller=WundeDcompanionArticleList action=list}',
            async: false,
            success: function (response) {
                response = Ext.JSON.decode(response.responseText, true);

                if (response.hasOwnProperty('data')) {
                    callback(response.data);
                } else {
                    callback([]);
                }
            }
        })
    },

    getDCColumnInfo: function(value, metaData, record) {
        return record.raw.dc_has_article;
    },

    getTotalDcStock: function(value, metaData, record) {
        value = record.raw.dc_total_stock;

        return (value > 0)
            ? '<span style="color:green;">' + value + '</span>'
            : '<span style="color:red;">' + value + '</span>';
    },

    getColorate: function(value)
    {
        value = value || 0;
        return (value > 0)
            ? '<span style="color:green;">' + value + '</span>'
            : '<span style="color:red;">' + value + '</span>';
    }
});
//{/block}
