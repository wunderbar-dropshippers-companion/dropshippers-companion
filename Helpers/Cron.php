<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 16/12/2020 20:37
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers;


use Enlight_Components_Cron_Job;

abstract class Cron
{
    public static function sendOrders()
    {
        self::run('Shopware_CronJob_DcSendOrder');
    }

    public static function receiveData()
    {
        self::run('Shopware_CronJob_DcReceiveData');
    }

    /**
     * @param string $name
     */
    private static function run(string $name)
    {
        Shopware()->Container()->load('plugins');

        $cron = Shopware()->Container()->get('cron');
        $logger = Shopware()->Container()->get('pluginlogger');

        $job = self::getJobByActionName($name);

        if (!is_null($job)) {
            try {
                $cron->runJob($job);
            } catch (Throwable $e) {
                $logger->error($e->getMessage(), $e->getTrace());
            }
        }
    }

    /**
     * @param string $name
     * @return Enlight_Components_Cron_Job
     */
    private static function getJobByActionName(string $name)
    {
        $cron = Shopware()->Container()->get('cron');
        $logger = Shopware()->Container()->get('pluginlogger');

        try {
            if (strpos($name, 'Shopware_') !== 0) {
                $name = str_replace(' ', '', ucwords(str_replace('_', ' ', $name)));
                $name = 'Shopware_CronJob_' . $name;
            }

            $job = $cron->getJobByAction($name);

            if ($job !== null) {
                return $job;
            }

            throw new RuntimeException(sprintf('Cron not found by action name "%s".', $name));
        } catch (Exception $e) {
            $logger->error($e->getMessage(), $e->getTrace());
        }

        return null;
    }
}