<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/03/2021 11:48
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers;

use Exception;

/**
 * Class ShopwareConfig
 * @package WundeDcompanion\Helpers
 */
abstract class ShopwareConfig
{

    /**
     * @return mixed|object|LoggerService|null
     */
    private static function logger()
    {
        return Shopware()->Container()->get('pluginlogger');
    }

    /**
     * @return bool
     */
    private static function isDevServer(): bool
    {
        return
            isset($_SERVER['SERVER_NAME']) &&
            strpos($_SERVER['SERVER_NAME'], 'wmadc-dev.de') !== false;
    }

    /**
     * @return bool
     */
    public static function overwrite(): bool
    {
        try {
            $shopwareConfigPath = __DIR__ . '/../../../../config.php';
            $pluginConfigPath = __DIR__ . '/../config.php';

            if (self::isDevServer() && file_exists($shopwareConfigPath) && file_exists($pluginConfigPath)) {
                $shopwareConfig = require $shopwareConfigPath;
                $pluginConfig = require $pluginConfigPath;

                if (md5(serialize($shopwareConfig)) !== md5(serialize($pluginConfig))) {
                    file_put_contents($shopwareConfigPath, file_get_contents($pluginConfigPath));
                }
            } else {
                if (!file_exists($shopwareConfigPath)) {
                    throw new Exception('Shopware config does not exist');
                }

                if (!file_exists($pluginConfigPath)) {
                    throw new Exception('Overwritable config does not exist');
                }
            }

            return true;
        } catch (Exception $e) {
            self::logger()->error($e->getMessage(), $e->getTrace());
        }

        return false;
    }
}