<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 28/12/2020 01:09
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use ArrayObject;
use Doctrine\DBAL\Driver\ResultStatement;
use Enlight\Event\SubscriberInterface;
use Enlight_Components_Db_Adapter_Pdo_Mysql;
use Enlight_Controller_EventArgs;
use Enlight_Event_EventArgs;
use Enlight_Event_EventManager;
use Enlight_Event_Exception;
use Exception;
use sBasket;
use Shopware\Bundle\StoreFrontBundle\Service\Core\AdditionalTextService;
use Shopware\Bundle\StoreFrontBundle\Service\Core\ContextService;
use Shopware\Components\Cart\Struct\CartItemStruct;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\Order;
use Shopware_Components_Modules;
use sOrder;
use sSystem;
use StoreFrontBundle\Service\AdditionalTextServiceInterface;
use StoreFrontBundle\Service\ContextServiceInterface;
use WundeDcompanion\Bundle\OrderBundle\SortService;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;
use Enlight_Hook_HookArgs;
use Enlight_Components_Session_Namespace;

class ModuleSubscriber implements SubscriberInterface
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * @var Enlight_Components_Session_Namespace $session
     */
    private $session;

    /**
     * @var Enlight_Event_EventManager
     */
    private $eventManager;

    /**
     * @var Enlight_Components_Db_Adapter_Pdo_Mysql $db
     */
    private $db;

    /**
     * @var Shopware_Components_Modules
     */
    private $moduleManager;

    /**
     * @var sSystem
     * @deprecated please update
     */
    public $sSYSTEM;

    /**
     * @var ContextService $contextService
     */
    private $contextService;

    /**
     * @var AdditionalTextService
     */
    private $additionalTextService;

    /**
     * ModuleSubscriber constructor.
     * @param LoggerService $logger
     * @param WholesalerRepository $wholesalerRepository
     * @param ModelManager|null $em
     * @param Enlight_Components_Session_Namespace|null $session
     * @param Enlight_Event_EventManager|null $eventManager
     * @param Enlight_Components_Db_Adapter_Pdo_Mysql|null $db
     * @param ContextService|null $contextService
     * @param AdditionalTextService|null $additionalTextService
     */
    public function __construct(
        LoggerService $logger,
        WholesalerRepository $wholesalerRepository,
        ModelManager $em = null,
        Enlight_Components_Session_Namespace $session = null,
        Enlight_Event_EventManager $eventManager = null,
        Enlight_Components_Db_Adapter_Pdo_Mysql $db = null,
        ContextService $contextService = null,
        AdditionalTextService $additionalTextService = null
    )
    {
        $this->em = $em ?: Shopware()->Models();
        $this->session = $session ?: Shopware()->Session();
        $this->eventManager = $eventManager ?: Shopware()->Events();
        $this->db = $db ?: Shopware()->Db();
        $this->moduleManager = Shopware()->Modules();
        $this->sSYSTEM = Shopware()->System();

        $this->logger = $logger;
        $this->contextService = $contextService;
        $this->additionalTextService = $additionalTextService;

        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'sBasket::sCheckBasketQuantities::replace' => 'onCheckBasketQuantities',
//            'sBasket::sAddArticle::after' => 'onBasketAddArticleAfter',
        ];
    }

    public function onCheckBasketQuantities()
    {
        $result = Shopware()->Db()->fetchAll(
            'SELECT (d.instock - b.quantity) as diffStock, b.ordernumber,
                d.laststock, IF(a.active=1, d.active, 0) as active
            FROM s_order_basket b
            LEFT JOIN s_articles_details d
              ON d.ordernumber = b.ordernumber
              AND d.articleID = b.articleID
            LEFT JOIN s_articles a
              ON a.id = d.articleID
            WHERE b.sessionID = ?
              AND b.modus = 0
            GROUP BY b.ordernumber',
            [Shopware()->Session()->get('sessionId')]
        );

        $hideBasket = false;
        $products = [];
        foreach ($result as $product) {
            if (empty($product['active'])
                || (!empty($product['laststock']) && $product['diffStock'] < 0)
            ) {
                $hideBasket = true;
                $products[$product['ordernumber']]['OutOfStock'] = true;
            } else {
                $products[$product['ordernumber']]['OutOfStock'] = false;
            }
        }

        try {
            $products = Shopware()->Events()->filter('Shopware_Modules_Basket_CheckBasketQuantities_ProductsQuantity',
                $products, [
                    'subject' => $this,
                    'hideBasket' => $hideBasket,
                ]);
        } catch (Enlight_Event_Exception $e) {
            $products = [];
        }

        return ['hideBasket' => $hideBasket, 'articles' => $products];
    }
}