<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:42
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

use Shopware\Components\Model\ModelManager;
use Shopware\CustomModels\ViisonPickwareERP\Warehouse\Warehouse;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\PickwareService;


class Shopware_Controllers_Backend_WundeDcompanionConfig extends Shopware_Controllers_Backend_ExtJs
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_config/main';

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    private $snippets;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var mixed|object|PickwareService|null
     */
    private $pickware;

    /**
     * Shopware_Controllers_Backend_WundeDcompanionConfig constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->logger = Shopware()->Container()->get('wunde_dcompanion.services.logger');
        $this->pickware = Shopware()->Container()->get('wunde_dcompanion.services.pickware');
        $this->em = Shopware()->Container()->get('models');
        $this->snippets = Shopware()->Snippets()->getNamespace(self::SNIPPET_NAMESPACE);
    }

    public function getWarehousesAction()
    {
        $warehouses = [];

        if ($this->pickware->isActive()) {
            /** @var Warehouse[] $warehouses */
            $warehouses = $this->em->getRepository(Warehouse::class)
                ->createQueryBuilder('warehouse')
                ->getQuery()
                ->getArrayResult();
        }

        $this->view->assign([
            'success' => true,
            'data' => $warehouses,
            'total' => count($warehouses)
        ]);
    }
}