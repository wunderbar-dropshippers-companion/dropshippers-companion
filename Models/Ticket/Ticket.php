<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 09/10/2020 13:31
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Ticket;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\User\User;

/**
 * @ORM\Entity(repositoryClass="Repository")
 * @ORM\Table(name="s_dcompanion_tickets")
 * @ORM\HasLifecycleCallbacks()
 */
class Ticket extends ModelEntity
{
    const DATETIME_FORMAT = 'd.m.Y H:i:s';

    /**
     * Primary Key - autoincrement value
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $subject
     * @ORM\Column(name="subject", type="string")
     */
    private $subject;

    /**
     * @var string $content
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var int $statusId
     * @ORM\Column(name="status_id", type="integer", nullable=true)
     */
    private $statusId = 0;

    /**
     * @var Status $status
     *
     * @ORM\ManyToOne(targetEntity="WundeDcompanion\Models\Ticket\Status")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @var int $priorityId
     * @ORM\Column(name="priority_id", type="integer", nullable=true)
     */
    private $priorityId = 2;

    /**
     * @var Priority $priority
     *
     * @ORM\ManyToOne(targetEntity="WundeDcompanion\Models\Ticket\Priority")
     * @ORM\JoinColumn(name="priority_id", referencedColumnName="id")
     */
    protected $priority = 2;

    /**
     * @var float $rating
     * @ORM\Column(name="rating", type="float")
     */
    private $rating = 0;

    /**
     * @var int $creatorId
     * @ORM\Column(name="creator_id", type="integer", nullable=true)
     */
    private $creatorId;

    /**
     * @var User $creator
     *
     * @ORM\ManyToOne(targetEntity="Shopware\Models\User\User")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    protected $creator;

    /**
     * @var string $editor
     * @ORM\Column(name="editor", type="string", nullable=true)
     */
    private $editor;

    /**
     * @var boolean $deleted
     * @ORM\Column(name="deleted", type="boolean", options={"default": 0})
     */
    private $deleted = false;

    /**
     * @var string $updatedAt
     * @ORM\Column(name="updated_at", type="string", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string $createdAt
     * @ORM\Column(name="created_at", type="string", nullable=true)
     */
    private $createdAt;

    /**
     * @var string $receivedAt
     * @ORM\Column(name="received_at", type="string", nullable=true)
     */
    private $receivedAt;

    /**
     * @var string $deletedAt
     * @ORM\Column(name="deleted_at", type="string", nullable=true)
     */
    private $deletedAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="WundeDcompanion\Models\Ticket\Comment",cascade={"persist", "remove"})
     * @ORM\JoinTable(name="s_dcompanion_ticket_comments",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="ticket_id",
     *              referencedColumnName="id",
     *              onDelete="CASCADE"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="comment_id",
     *              referencedColumnName="id",
     *              onDelete="CASCADE"
     *          )
     *      }
     * )
     */
    protected $comments;

    /**
     * Ticket constructor.
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param Status $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return Priority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Priority $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     */
    public function setRating($rating)
    {
        if ($rating > 5) $rating = 5;
        if ($rating < 0) $rating = 0;

        $this->rating = $rating;
    }

    /**
     * @return User
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return string
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * @param string $editor
     */
    public function setEditor($editor)
    {
        $this->editor = $editor;
    }

    /**
     * @return string
     */
    public function getReceivedAt()
    {
        return $this->receivedAt;
    }

    /**
     * @param string $receivedAt
     */
    public function setReceivedAt($receivedAt)
    {
        $this->receivedAt = $receivedAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param string $deletedAt
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        $this->deletedAt = $deleted
            ? (new DateTime())->format(self::DATETIME_FORMAT)
            : null;
    }

    /**
     * @return ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param ArrayCollection $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $date = (new DateTime())->format(self::DATETIME_FORMAT);

        $this->createdAt = $date;
        $this->updatedAt = $date;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = (new DateTime())->format(self::DATETIME_FORMAT);
    }
}