import Vue from 'vue'
import Vuex from 'vuex'

import SnippetModule from './modules/snippets'
import ThemeModule from './modules/themes'
import SettingModule from './modules/settings'
import UserModule from './modules/user'
import ShopModule from './modules/shop'
import MenuModule from './modules/menus'
import TicketModule from './modules/tickets'
import EditorModule from './modules/editor'
import ContactModule from './modules/contact'
import ConfigModule from './modules/config'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    title: ''
  },
  getters: {
    getUrl: () => (action) => `/backend/WundeDcompanionManager/${action}`,
    getTitle: (state) => state.title
  },
  mutations: {
    setTitle: (state, title) => state.title = title
  },
  modules: {
    snippets: SnippetModule,
    themes: ThemeModule,
    settings: SettingModule,
    user: UserModule,
    shop: ShopModule,
    menus: MenuModule,
    tickets: TicketModule,
    editor: EditorModule,
    contact: ContactModule,
    config: ConfigModule
  }
})

export default store
