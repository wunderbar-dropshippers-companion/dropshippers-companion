import Vue from 'vue'
import Router from 'vue-router'
import menu from '../data/menu'
import { i18n } from './i18n'
import store from '../store'

Vue.use(Router)

import Dashboard from '../pages/Dashboard'
import Main from '../views/Main'
import Privacy from '../views/Privacy'
import Imprint from '../views/Imprint'
import Tickets from '../views/Tickets'
import Statistics from '../views/Statistics'
import Settings from '../views/Settings'
import Contact from '../views/Contact'
import Trash from '../views/Trash'
import Suggestions from '../views/Suggestions'
import Faq from '../views/Faq'
import ColorSettings from '../views/Settings/ColorSettings'
import PluginSettings from '../views/Settings/PluginSettings'

const router = new Router({
  history: false,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      redirect: {name: 'main'},
      children: [
        {
          path: '/dashboard/main',
          name: 'main',
          components: {
            default: Main,
            privacy: Privacy
          }
        },
        {
          path: '/dashboard/statistics',
          name: 'statistics',
          component: Statistics
        },
        {
          path: '/dashboard/settings',
          name: 'settings',
          component: Settings,
          children: [
            {
              path: '/dashboard/settings/colors',
              name: 'colors',
              component: ColorSettings
            },
            {
              path: '/dashboard/settings/plugins',
              name: 'plugins',
              component: PluginSettings
            },
          ]
        },
        {
          path: '/dashboard/privacy',
          name: 'privacy',
          component: Privacy
        },
        {
          path: '/dashboard/tickets',
          name: 'tickets',
          component: Tickets
        },
        {
          path: '/dashboard/contact',
          name: 'contact',
          component: Contact
        },
        {
          path: '/dashboard/imprint',
          name: 'imprint',
          component: Imprint
        },
        {
          path: '/dashboard/trash',
          name: 'trash',
          component: Trash
        },
        {
          path: '/dashboard/suggestions',
          name: 'suggestions',
          component: Suggestions
        },
        {
          path: '/dashboard/faqs',
          name: 'faqs',
          component: Faq
        },
      ]
    },
    {
      path: '*',
      redirect: {name: 'main'}
    },
  ]
})

router.beforeEach((to, from, next) => {
  menu.main.forEach(group => {
    var target = group.items.find(item => item.to === to.name)

    if (target !== undefined) {
      store.commit('setTitle', i18n.t(target.snippet))
    }
  })

  next()
})

export default router
