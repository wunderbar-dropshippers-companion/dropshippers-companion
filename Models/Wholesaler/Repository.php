<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 17/08/2020 14:50
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler;


use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager as EntityManager;
use Shopware\Components\Plugin\DBALConfigReader;
use Shopware\Models\Plugin\Plugin;
use Symfony\Component\DependencyInjection\ContainerInterface as DependencyInjection;
use WundeDcompanion\Interfaces\IClient;

/**
 * Class Repository
 * @package WundeDcompanion\Models\Wholesaler
 */
class Repository
{

    /**
     * @var DependencyInjection $di
     */
    private $di;

    /**
     * @var EntityManager $em
     */
    private $em;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * @var DBALConfigReader $configReader
     */
    private $configReader;

    /**
     * Repository constructor.
     * @param DependencyInjection $di
     * @param EntityManager $em
     * @param Logger $logger
     * @param DBALConfigReader $configReader
     */
    public function __construct(DependencyInjection $di, EntityManager $em, $logger, $configReader)
    {
        $this->di = $di;
        $this->em = $em;
        $this->logger = $logger;
        $this->configReader = $configReader;

        $this->setWholesalers($di, $em);
    }

    /**
     * @param DependencyInjection $di
     * @param EntityManager $em
     */
    private function setWholesalers($di, $em)
    {
        $parameters = $di->getParameterBag()->all();

        $identifiers = [];

        foreach ($parameters as $key => $value)
        {
            if (is_string($value) && substr($key, 0, 27 ) === 'wunde_dcompanion.extension.') {
                $identifiers[] = $value;
            }
        }

        foreach ($identifiers as $identifier)
        {
            if (
                array_key_exists($identifier . '.shortname', $parameters) &&
                array_key_exists($identifier . '.longname', $parameters) &&
                array_key_exists($identifier . '.basename', $parameters) &&
                array_key_exists($identifier . '.name', $parameters) &&
                Shopware()->Container()->has($identifier . '.client')
            ) {
                $shortname = strtolower($di->getParameter($identifier . '.shortname'));
                $longname = $di->getParameter($identifier . '.longname');
                $basename = $di->getParameter($identifier . '.basename');
                $name = $di->getParameter($identifier . '.name');

                /** @var Plugin $plugin */
                $plugin = $em->getRepository(Plugin::class)->findOneBy([
                    'name' => $name
                ]);

                if ($plugin->getActive()) {
                    /** @var IClient $client */
                    $client = $di->get($identifier . '.client');
                    $client->setConfig($this->configReader->getByPluginName($plugin->getName()));

                    $this->wholesalers[$shortname] = new Wholesaler(
                        $client,
                        $plugin,
                        $shortname,
                        $longname,
                        $basename,
                        $name,
                        $identifier
                    );
                }
            }
        }
    }

    /**
     * @return Wholesaler[]
     */
    public function findAll()
    {
        return $this->wholesalers;
    }

    /**
     * @return Wholesaler[]
     */
    public function findAllActive()
    {
        /** @var Wholesaler $wholesaler */
        return array_filter($this->wholesalers, function($wholesaler) {
            $plugin = $wholesaler->getPlugin();

            return !is_null($plugin) && $plugin->getActive();
        });
    }

    /**
     * @param $shortname
     * @return Wholesaler
     */
    public function findByShortname($shortname)
    {
        return array_key_exists($shortname, $this->wholesalers)
            ? $this->wholesalers[$shortname]
            : null;
    }
}