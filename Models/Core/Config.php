<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 02/10/2020 09:53
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Core;


use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Shop\Repository as ShopRepository;
use Shopware\Models\Shop\Shop;

/**
 * Class Config
 * @package WundeDcompanion\Models\Core
 */
class Config
{
    /**
     * @var string $pluginName
     */
    private $pluginName;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var bool $autoOrder
     */
    private $autoOrder = true;

    /**
     * @var bool $overwritePurchaseprice
     */
    private $overwritePurchaseprice = true;

    /**
     * @var bool $overwritePurchasepriceCheapest
     */
    private $overwritePurchasepriceCheapest = true;

    /**
     * @var bool $combineTrackingCodes
     */
    private $combineTrackingCodes = true;

    /**
     * @var bool $sendMailToCustomer
     */
    private $sendMailToCustomer = true;

    /**
     * @var bool $finishOrder
     */
    private $finishOrder = true;

    /**
     * @var bool $mailNotification
     */
    private $mailNotification = true;

    /**
     * @var string $mailRecipients
     */
    private $mailRecipients = '';

    /**
     * @var string $mailErrorRecipients
     */
    private $mailErrorRecipients = '';

    /**
     * @var bool $debugLogging
     */
    private $debugLogging = false;

    /**
     * @var int $deleteLogAfter
     */
    private $deleteLogAfter = 30;

    /**
     * @var int $overrideStocks
     */
    private $overrideStocks = false;

    /**
     * @var int $testMode
     */
    private $testMode = false;

    /**
     * Config constructor.
     * @param string $pluginName
     */
    public function __construct($pluginName)
    {
        $this->pluginName = $pluginName;
        $this->config = $this->getConfig();

        $this->autoOrder = $this->findConfig('auto_order', true);
        $this->overwritePurchaseprice = $this->findConfig('overwrite_purchaseprice', true);
        $this->overwritePurchasepriceCheapest = $this->findConfig('overwrite_purchaseprice_cheapest', true);
        $this->combineTrackingCodes = $this->findConfig('combine_tracking_codes', true);
        $this->sendMailToCustomer = $this->findConfig('send_mail_to_customer', true);
        $this->finishOrder = $this->findConfig('finish_order', true);
        $this->mailNotification = $this->findConfig('mail_notification', true);
        $this->mailRecipients = $this->findConfig('mail_recipients', '');
        $this->mailErrorRecipients = $this->findConfig('mail_error_recipients', '');
        $this->debugLogging = $this->findConfig('debug_logging', false);
        $this->deleteLogAfter = $this->findConfig('delete_log_after', 30);
        $this->overrideStocks = $this->findConfig('override_stocks', false);
        $this->testMode = $this->findConfig('test_mode', false);
    }

    /**
     * @return array
     */
    private function getConfig()
    {
        /** @var ShopRepository $shopRepository */
        $shopRepository = Shopware()->Models()->getRepository(Shop::class);

        /** @var ConfigReader $configReader */
        $configReader = Shopware()->Container()->get('shopware.plugin.config_reader');

        $shop = Shopware()->Container()->has('shop')
            ? Shopware()->Shop()
            : $shopRepository->getActiveDefault();

        return $configReader->getByPluginName($this->pluginName, $shop);
    }

    /**
     * @param string $target
     * @param mixed $default
     * @return mixed
     */
    private function findConfig($target, $default)
    {
        return array_key_exists('dc_' . $target, $this->config)
            ? $this->config['dc_' . $target]
            : $default;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->config;
    }

    /**
     * @return bool
     */
    public function autoOrder()
    {
        return $this->autoOrder;
    }

    /**
     * @return bool
     */
    public function overwritePurchaseprice()
    {
        return $this->overwritePurchaseprice;
    }

    /**
     * @return bool
     */
    public function overwritePurchasepriceCheapest()
    {
        return $this->overwritePurchasepriceCheapest;
    }

    /**
     * @return bool
     */
    public function combineTrackingCodes()
    {
        return $this->combineTrackingCodes;
    }

    /**
     * @return bool
     */
    public function sendMailToCustomer()
    {
        return $this->sendMailToCustomer;
    }

    /**
     * @return bool
     */
    public function finishOrder()
    {
        return $this->finishOrder;
    }

    /**
     * @return bool
     */
    public function mailNotification()
    {
        return $this->mailNotification;
    }

    /**
     * @return string
     */
    public function mailRecipients()
    {
        return $this->mailRecipients;
    }

    /**
     * @return string
     */
    public function mailErrorRecipients()
    {
        return $this->mailErrorRecipients;
    }

    /**
     * @return bool
     */
    public function debugLogging()
    {
        return $this->debugLogging;
    }

    /**
     * @return int
     */
    public function deleteLogAfter()
    {
        return $this->deleteLogAfter;
    }

    /**
     * @return int
     */
    public function overrideStocks()
    {
        return $this->overrideStocks;
    }

    /**
     * @return int
     */
    public function testMode()
    {
        return $this->testMode;
    }
}