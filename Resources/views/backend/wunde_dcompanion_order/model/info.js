//{namespace name=backend/wunde_dcompanion_order/main}
Ext.define('Shopware.apps.WundeDcompanionOrder.model.Info', {

    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
        { name: 'message', type: 'string' },
        { name: 'color', type: 'string' },
    ],
});