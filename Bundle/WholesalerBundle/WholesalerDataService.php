<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 20:47
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\WholesalerBundle;

use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;

/**
 * Class QuantityService
 * @package WundeDcompanion\Bundle\WholesalerBundle
 */
class WholesalerDataService extends WholesalerRepository
{
    /**
     * @var array $quantities
     */
    private $quantities = [];

    /**
     * @var array $all
     */
    private $all = [];

    /**
     * @param boolean $update
     * @param string $ordernumber
     * @return array
     */
    public function getQuantities($update = false, $ordernumber = null)
    {
        foreach ($this->findAllActive() as $shortname => $wholesaler)
        {
            $response = $wholesaler->getClient()->getQuantities($ordernumber);

            if ($response->getStatusCode() === 200) {
                if (
                    $update ||
                    ($update === false && !array_key_exists($shortname, $this->quantities))
                ) {
                    $this->quantities[$shortname] = json_decode($response->getContent(), true);
                }
            }
        }

        return $this->quantities;
    }

    /**
     * @param boolean $update
     * @param string $ordernumber
     * @return array
     */
    public function getAll($update = false, $ordernumber = null)
    {
        foreach ($this->findAllActive() as $shortname => $wholesaler)
        {
            $response = $wholesaler->getClient()->getAll($ordernumber);

            if ($response->getStatusCode() === 200) {
                if (
                    $update ||
                    ($update === false && !array_key_exists($shortname, $this->all))
                ) {
                    $this->all[$shortname] = json_decode($response->getContent(), true);
                }
            }
        }

        return $this->all;
    }
}