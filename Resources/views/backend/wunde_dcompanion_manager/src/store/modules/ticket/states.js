import DefaultStates from "../../../data/ticket/states";

export default {
  namespaced: true,
  state: {
    states: []
  },
  getters: {
    getStates: (state) => state.states
  },
  mutations: {
    setStates: (state, states) => state.states = states,
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getTicketStates'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let states = response.data.hasOwnProperty('states') ? response.data.states : [];

          if (success) {
            commit('setStates', states)
          } else {
            throw new Error();
          }

          return states
        })
        .catch(() => {
          console.warn('States could not be loaded')
          commit('setStates', DefaultStates)
        })
    },
  }
}
