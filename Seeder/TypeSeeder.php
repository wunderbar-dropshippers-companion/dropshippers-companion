<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 18:52
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Seeder;

use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Helpers\Database\Database;
use WundeDcompanion\Models\Log\Type;

/**
 * Class TypeSeeder
 * @package WundeDcompanion\Seeder
 */
abstract class TypeSeeder extends DatabaseSeeder
{
    /**
     * @var string[] $types
     */
    private static $types = [
        Type::TYPE_DEBUG => 'debug',
        Type::TYPE_WARNING => 'warning',
        Type::TYPE_INFO => 'info',
        Type::TYPE_ERROR => 'error',
        Type::TYPE_EMERGENCY => 'emergency',
        Type::TYPE_CRITICAL => 'critical',
    ];

    public static function seed()
    {
        $em = Shopware()->Container()->get('models');
        $logger = Shopware()->Container()->get('pluginlogger');

        if (Database::entityTableExists(Type::class)) {
            foreach (self::$types as $id => $name) {
                try {
                    /** @var Type $type */
                    $type = $em->getRepository(Type::class)->find($id);

                    if (is_null($type)) {
                        $type = new Type($id, $name);
                        $em->persist($type);
                    } else {
                        $type->setName($name);
                    }
                } catch (Exception $e) {
                    $logger->error($e->getMessage(), $e->getTrace());
                }
            }
        }
    }
}