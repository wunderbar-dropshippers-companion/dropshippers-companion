import colors from 'vuetify/lib/util/colors'

export default {
  primary: '#009FE3',
  navigationBg: colors.grey.darken4,
  secondary: colors.blueGrey.lighten2,
  tabbarBg: colors.grey.darken3,
  buttonbarBg: colors.grey.darken3,
  extension: colors.grey.darken4,
  background: colors.grey.darken4,
  chart1: '#009FE3',
  chart2: '#808080',
  gridlines: colors.grey.darken3
}
