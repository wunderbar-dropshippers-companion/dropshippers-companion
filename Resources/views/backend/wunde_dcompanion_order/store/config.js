Ext.define('Shopware.apps.WundeDcompanionOrder.store.Config', {

    /**
     * Extend for the standard ExtJS 4
     * @string
     */
    extend:'Ext.data.Store',

    autoLoad: true,

    /**
     * Define the used model for this store
     * @string
     */
    model: 'Shopware.apps.WundeDcompanionOrder.model.Config',

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            read: '{url controller=WundeDcompanionOrder action="getConfig"}',
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'data'
        },
    },
});