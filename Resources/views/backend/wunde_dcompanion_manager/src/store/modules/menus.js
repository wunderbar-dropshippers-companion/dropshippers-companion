import MenuBackup from '../../data/menu'
export default {
  namespaced: true,
  state: {
    menus: []
  },
  getters: {
    getMenu: (state) => state.menus,
    getMainMenu: (state) => state.menus.hasOwnProperty('main') ? state.menus['main'] : [],
    getSettingsMenu: (state) => state.menus.hasOwnProperty('settings') ? state.menus['settings'] : []
  },
  mutations: {
    setMenus: (state, menus) => state.menus = menus
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getMenus'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let menus = response.data.hasOwnProperty('menus') ? response.data.menus : [];

          if (success) {
            commit('setMenus', menus)
          } else {
            throw new Error();
          }
        })
        .catch(() => {
          console.warn('menus could not be loaded')
          commit('setMenus', MenuBackup)
        })
    },
  }
}
