<?php
/**
 * @project Dropshippers Companion
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/05/2020 12:45
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion;

use Shopware\Components\CacheManager;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\ActivateContext;
use Shopware\Components\Plugin\Context\DeactivateContext;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;
use WundeDcompanion\Helpers\Database\AttributeTableManager as AttributeManager;
use WundeDcompanion\Helpers\Dependencies\Dependency;
use WundeDcompanion\Models\Change\Change;
use WundeDcompanion\Models\Change\ChangeSet;
use WundeDcompanion\Models\Log\Log;
use WundeDcompanion\Models\Log\Type;
use WundeDcompanion\Models\Order\Carrier;
use WundeDcompanion\Models\Order\Trackingcode;
use WundeDcompanion\Models\Ticket\Comment;
use WundeDcompanion\Models\Ticket\Priority;
use WundeDcompanion\Models\Ticket\Status;
use WundeDcompanion\Models\Ticket\Ticket;
use WundeDcompanion\Seeder\CarrierSeeder;
use WundeDcompanion\Seeder\PrioritySeeder;
use WundeDcompanion\Seeder\StateSeeder;
use WundeDcompanion\Seeder\TypeSeeder;

/**
 * Class WundeDcompanion
 * @package WundeDcompanion
 */
class WundeDcompanion extends Plugin
{
    const CACHE_LIST = [
        'INSTALL' => [
            CacheManager::CACHE_TAG_CONFIG,
            CacheManager::CACHE_TAG_ROUTER,
            CacheManager::CACHE_TAG_PROXY,
            CacheManager::CACHE_TAG_HTTP,
            CacheManager::CACHE_TAG_SEARCH
        ],
        'UNINSTALL' => [
            CacheManager::CACHE_TAG_CONFIG,
            CacheManager::CACHE_TAG_ROUTER,
            CacheManager::CACHE_TAG_PROXY,
            CacheManager::CACHE_TAG_HTTP,
            CacheManager::CACHE_TAG_TEMPLATE,
            CacheManager::CACHE_TAG_THEME,
            CacheManager::CACHE_TAG_SEARCH
        ],
        'UPDATE' => [
            CacheManager::CACHE_TAG_CONFIG,
            CacheManager::CACHE_TAG_ROUTER,
            CacheManager::CACHE_TAG_PROXY,
            CacheManager::CACHE_TAG_HTTP,
            CacheManager::CACHE_TAG_TEMPLATE,
            CacheManager::CACHE_TAG_THEME,
            CacheManager::CACHE_TAG_SEARCH
        ],
        'ACTIVATE' => [
            CacheManager::CACHE_TAG_CONFIG,
            CacheManager::CACHE_TAG_ROUTER,
            CacheManager::CACHE_TAG_PROXY,
            CacheManager::CACHE_TAG_HTTP,
            CacheManager::CACHE_TAG_TEMPLATE,
            CacheManager::CACHE_TAG_THEME,
            CacheManager::CACHE_TAG_SEARCH
        ],
        'DEACTIVATE' => [
            CacheManager::CACHE_TAG_CONFIG,
            CacheManager::CACHE_TAG_ROUTER,
            CacheManager::CACHE_TAG_PROXY,
            CacheManager::CACHE_TAG_HTTP,
            CacheManager::CACHE_TAG_TEMPLATE,
            CacheManager::CACHE_TAG_THEME,
            CacheManager::CACHE_TAG_SEARCH
        ],
    ];

    const SEEDERS = [
        StateSeeder::class,
        PrioritySeeder::class,
        TypeSeeder::class,
        CarrierSeeder::class
    ];

    const ENTITIES = [
        ChangeSet::class,
        Change::class,
        Ticket::class,
        Status::class,
        Priority::class,
        Comment::class,
        Type::class,
        Log::class,
        Trackingcode::class,
        Carrier::class
    ];

    /**
     * @param InstallContext $context
     */
    public function install(InstallContext $context)
    {
        Dependency::database()->registerEntities(self::ENTITIES);
        Dependency::database()->registerAttributes('core');
        Dependency::database()->seed(self::SEEDERS);
        Dependency::email()->registerEmailTemplates();

        $context->scheduleClearCache(self::CACHE_LIST['INSTALL']);
    }

    /**
     * @param UpdateContext $context
     */
    public function update(UpdateContext $context)
    {
        Dependency::database()->registerEntities(self::ENTITIES);
        Dependency::database()->registerAttributes(AttributeManager::CONTEXT_CORE);
        Dependency::database()->seed(self::SEEDERS);
        Dependency::email()->registerEmailTemplates();

        $context->scheduleClearCache(self::CACHE_LIST['UPDATE']);
    }

    /**
     * @param UninstallContext $context
     */
    public function uninstall(UninstallContext $context)
    {
        if (!$context->keepUserData()) {
            Dependency::database()->unregisterEntities(self::ENTITIES);
            Dependency::database()->unregisterAttributes(AttributeManager::CONTEXT_CORE);
            Dependency::email()->unregisterEmailTemplates();
        }

        $context->scheduleClearCache(self::CACHE_LIST['UNINSTALL']);
    }

    /**
     * @param ActivateContext $context
     */
    public function activate(ActivateContext $context)
    {
        $context->scheduleClearCache(self::CACHE_LIST['ACTIVATE']);
    }

    /**
     * @param DeactivateContext $context
     */
    public function deactivate(DeactivateContext $context)
    {
        $context->scheduleClearCache(self::CACHE_LIST['DEACTIVATE']);
    }
}