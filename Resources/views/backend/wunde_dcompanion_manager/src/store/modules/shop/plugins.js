import DefaultPlugins from '../../../data/shop/plugins'

export default {
  namespaced: true,
  state: {
    plugins: []
  },
  getters: {
    getPlugins: (state) => state.plugins
  },
  mutations: {
    setPlugins: (state, plugins) => state.plugins = plugins,
  },
  actions: {
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getPlugins'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let plugins = response.data.hasOwnProperty('plugins') ? response.data.plugins : [];

          if (success) {
            commit('setPlugins', plugins)
          } else {
            throw new Error();
          }

          return plugins
        })
        .catch(() => {
          console.warn('Plugins could not be loaded')
          commit('setPlugins', DefaultPlugins)
          return DefaultPlugins
        })
    },
  }
}
