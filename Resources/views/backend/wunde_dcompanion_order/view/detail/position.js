//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/view/detail/position"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionOrder.view.detail.Position', {

    override: 'Shopware.apps.Order.view.detail.Position',

    dcSnippets: {
        source: '{s name=source}Source{/s}',
        pleaseSelect: '{s name=please_select}Please select{/s}',
        dcPrice: '{s name=dc_price}DC price{/s}',
        dcInstock: '{s name=dc_instock}DC instock{/s}',
        price: '{s name=price}price{/s}',
        instock: '{s name=instock}instock{/s}',
        sendOrderButtonText: '{s name=button/text/sendorder}Ordernumber{/s}',
    },

    registerEvents: function() {
        var me = this;
        me.callParent(arguments);

        me.addEvents(
            'changeSource',
            'sendOrderPositions'
        )
    },

    getColumns: function(grid) {
        var me = this;
        var columns = me.callOverridden(arguments);

        Ext.Array.each(columns, function(column) {
            if (column.dataIndex === 'price') {
                column.editor.id = 'priceField'
            }
        })

        me.dcDropshipActive = {
            header: 'Dropship',
            dataIndex: 'dc_dropship_active',
            width: 60,
            tooltip: 'Dropshipping article',
            align: 'center',
            renderer: function(value, item, record) {
                if (value === 'true' || value === 'false') {
                    return '';
                }

                return record.get('dc_dropship_active');
            }
        };

        me.wholesalerComboBoxEditor = {
            allowBlank: true,
            xtype: 'combo',
            displayField: 'longname',
            valueField: 'longname',
            required: true,
            emptyText: me.dcSnippets.pleaseSelect,
            autoLoadOnValue: true,
            editable: false,
            autoSelect: true,
            cls: 'x-unselectable',
            queryMode: 'remote',
            queryCaching: false,
            listeners: {
                change: function (field) {
                    me.fireEvent('changeSource', me.rowEditor, grid, field)
                },
            }
        };

        me.wholesalerCombobox = {
            header: me.dcSnippets.source,
            id: 'dcSourceField',
            width: 200,
            dataIndex: 'dc_name_long',
            renderer: function(value) {
                return value.length ? value : 'Mein Shop'
            }
        };

        me.dcStock = {
            header: me.dcSnippets.dcInstock,
            dataIndex: 'dc_stock',
            flex:1,
            align: 'center',
            renderer: function(value) {
                return value;
            }
        };

        me.dcPurchasePrice = {
            header: me.dcSnippets.dcPrice,
            dataIndex: 'dc_purchase_price',
            width: 120,
            align: 'center',
            renderer: grid.priceColumn
        };

        Ext.Array.insert(columns, 0,  [me.dcDropshipActive]);

        me.wholesalerCombobox.editor = me.record.get('dcAutoOrder')
            ? false
            : me.wholesalerComboBoxEditor

        return Ext.Array.insert(columns, 9,  [
            me.dcStock,
            me.dcPurchasePrice,
            me.wholesalerCombobox
        ]);
    },

    createGridToolbar: function() {
        var me = this;
        var gridToolbar = me.callParent(arguments);

        gridToolbar.add(Ext.create('Ext.button.Button', {
            text: me.dcSnippets.sendOrderButtonText,
            iconCls: 'sprite-truck-box-label',
            hidden: me.record.get('dcAutoOrder'),
            listeners: {
                click: function () {
                    me.fireEvent('sendOrderPositions', [me.record], me)
                }
            }
        }))

        return gridToolbar;
    },
});
//{/block}