/*
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 01/09/2020 16:04
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

Ext.define('Shopware.apps.WundeDcompanionArticle.model.Wholesaler', {
    /**
     * Extends the standard Ext Model
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields: [
        { name: 'longname', type: 'string' },
        { name: 'shortname', type: 'string' },
        { name: 'basename', type: 'string' },
        { name: 'active', type: 'boolean' },
        { name: 'articlename', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'instock', type: 'integer' },
        { name: 'ordernumber', type: 'string' },
        { name: 'override_name', type: 'boolean' },
        { name: 'override_price', type: 'boolean' },
        { name: 'price_recommended', type: 'number' },
        { name: 'purchasing_price', type: 'number' },
        { name: 'live', type: 'boolean' },
        { name: 'show_update_Message', type: 'boolean' },
    ],

    /**
     * Configure the data communication
     * @object
     */
    proxy:{
        /**
         * Set proxy type to ajax
         * @string
         */
        type:'ajax',

        async : false,

        /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api: {
            update: '{url controller=WundeDcompanionArticle action="update"}',
        },

        /**
         * Configure the data reader
         * @object
         */
        reader:{
            type:'json',
            root:'wholesalers'
        },
    }
});