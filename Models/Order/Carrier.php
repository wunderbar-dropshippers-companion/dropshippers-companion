<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 09/10/2020 13:31
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Order;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\User\User;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_carrier")
 */
class Carrier extends ModelEntity
{

    /**
     * Primary Key - autoincrement value
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var string $trackingUrl
     * @ORM\Column(type="string")
     */
    private $trackingUrl;

    /**
     * @var string $domain
     * @ORM\Column(type="string")
     */
    private $domain;

    /**
     * Carrier constructor.
     * @param string $name
     * @param string $trackingUrl
     * @param string $domain
     */
    public function __construct($name = null, $trackingUrl = null, $domain = null)
    {
        $this->name = $name;
        $this->trackingUrl = $trackingUrl;
        $this->domain = $domain;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $code
     * @return string
     */
    public function getTrackingUrl($code = '')
    {
        return empty($code)
            ? $this->trackingUrl
            : str_replace('{TRACKING_NUMBER}', $code, $this->getTrackingUrl());
    }

    /**
     * @param string $trackingUrl
     */
    public function setTrackingUrl($trackingUrl)
    {
        $this->trackingUrl = $trackingUrl;
    }

    /**
     * @return string
     */
    public function getDomain(): ?string
    {
        return $this->domain;
    }

    /**
     * @param string|null $domain
     */
    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }
}