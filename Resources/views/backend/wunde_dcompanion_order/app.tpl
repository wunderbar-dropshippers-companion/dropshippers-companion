{block name="backend/order/application" append}
    {$smarty.block.parent}
    {include file="backend/wunde_dcompanion_order/controller/detail.js"}
    {include file="backend/wunde_dcompanion_order/controller/list.js"}
    {include file="backend/wunde_dcompanion_order/model/tracking_code.js"}
    {include file="backend/wunde_dcompanion_order/model/source.js"}
    {include file="backend/wunde_dcompanion_order/model/info.js"}
    {include file="backend/wunde_dcompanion_order/store/source.js"}
    {include file="backend/wunde_dcompanion_order/model/config.js"}
    {include file="backend/wunde_dcompanion_order/store/config.js"}
    {include file="backend/wunde_dcompanion_order/view/detail/dropship.js"}
    {include file="backend/wunde_dcompanion_order/view/list/dropship.js"}
{/block}