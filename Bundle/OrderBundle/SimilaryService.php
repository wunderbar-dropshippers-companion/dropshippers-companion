<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 21:43
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle;


use Exception;
use Shopware\Bundle\StoreFrontBundle\Service\Core\ContextService;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Price;
use Shopware\Models\Customer\Group;
use Shopware\Models\Dispatch\ShippingCost;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\DetailStatus;
use WundeDcompanion\Bundle\OrderBundle\Models\Similary;
use WundeDcompanion\Bundle\OrderBundle\Models\SimilarySet;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;

class SimilaryService
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * @var array $wholesalerData
     */
    private $wholesalerData = [];

    /**
     * @var DetailStatus $detailStatus
     */
    private $detailStatus;

    /**
     * @var ContextService $context
     */
    private $context;

    /**
     * @var int $similaryUID
     */
    private $similaryUID = 0;

    /**
     * SimilaryService constructor.
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param DetailStatus $detailStatus
     * @param Wholesaler[] $wholesalers
     * @param array $wholesalerData
     */
    public function __construct(ModelManager $em, LoggerService $logger, DetailStatus $detailStatus, array $wholesalers, array $wholesalerData)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->wholesalers = $wholesalers;
        $this->wholesalerData = $wholesalerData;
        $this->detailStatus = $detailStatus;

        $this->context = Shopware()->Container()->get('shopware_storefront.context_service');
    }

    /**
     * @param Position $position
     * @return SimilarySet
     */
    public function getPositionSimilaries(Position $position)
    {
        $similaries = $this->getPositionSimilary($position);

        foreach ($this->getPositionWholesalerSimilaries($position) as $similary)
        {
            $similaries[] = $similary;
        }

        $similaries = array_filter($similaries, function ($similary) {
            return $similary->getInstock() > 0;
        });

        return new SimilarySet($similaries);
    }

    /**
     * @param Position $position
     * @return Similary[]
     */
    private function getPositionWholesalerSimilaries(Position $position)
    {
        $similaries = [];
        $articleDetail = $position->getArticleDetail();
        $articleDetailAttributes = $articleDetail->getAttribute();
//        $price = $this->getPositionPrice($position);

        $dcInstock = $this->getDcStock($position);

        foreach ($this->wholesalers as $shortname => $wholesaler)
        {
            $upperShortname = $wholesaler->getUpperShortname();

            if (
                method_exists($articleDetailAttributes,'getDc' . $upperShortname . 'Active') &&
                method_exists($articleDetailAttributes,'getDc' . $upperShortname . 'Ordernumber') &&
                $articleDetailAttributes->{'getDc' . $upperShortname . 'Active'}() &&
                !empty($articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}()) &&
                !is_null($articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}()) &&
                array_key_exists($shortname, $this->wholesalerData)
            ) {
                $ordernumber = $articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}();

                if (
                    array_key_exists($ordernumber, $this->wholesalerData[$shortname]) &&
                    array_key_exists('quantity', $this->wholesalerData[$shortname][$ordernumber]) &&
                    array_key_exists('purchasePrice', $this->wholesalerData[$shortname][$ordernumber]) &&
                    (int)$this->wholesalerData[$shortname][$ordernumber]['quantity'] > 0
                ) {
                    $data = $this->wholesalerData[$shortname][$ordernumber];

                    $purchasePrice = (float)str_replace(',', '.', $data['purchasePrice']);
                    $price = (float)str_replace(',', '.', $data['priceRecommended']);
                    $quantity = (int)$data['quantity'];

                    $similary = new Similary();

                    $similary->setUid($this->similaryUID++);
                    $similary->setInstock($quantity);
                    $similary->setDcInstock($dcInstock);
                    $similary->setPrice($price);
//                    $similary->setPrice($price);
                    $similary->setPurchasePrice($purchasePrice);
                    $similary->setArticleDetailId($articleDetail->getId());
                    $similary->setArticleDetail($articleDetail);
                    $similary->setOrdernumber($ordernumber);
                    $similary->setEan($articleDetail->getEan());
                    $similary->setPositionId($position->getId());
                    $similary->setTax($articleDetail->getArticle()->getTax());
                    $similary->setQuantity($position->getQuantity());
                    $similary->setExpectedQuantity($position->getQuantity());
                    $similary->setArticleName($articleDetail->getArticle()->getName());
                    $similary->setArticleId($articleDetail->getArticleId());
                    $similary->setWholesaler($wholesaler);
                    $similary->setShippingCost($wholesaler->getConfig()->shipping()->cost() ?: 0);
                    $similary->setUnit($articleDetail->getUnit());
                    $similary->setPackUnit($articleDetail->getPackUnit());
                    $similary->setDetailStatus($this->detailStatus);

                    $similaries[] = $similary;
                }
            }
        }

        return $similaries;
    }

    /**
     * @param Position $position
     * @return Similary[]
     */
    private function getPositionSimilary(Position $position)
    {
        $articleDetail = $position->getArticleDetail();
        $dcInstock = $this->getDcStock($position);
        $price = $this->getPositionPrice($position);

        $similary = new Similary();

        $similary->setUid($this->similaryUID++);
        $similary->setInstock($articleDetail->getInStock());
        $similary->setDcInstock($dcInstock);
        $similary->setPrice($price);
        $similary->setPurchasePrice($articleDetail->getPurchasePrice());
        $similary->setArticleDetailId($articleDetail->getId());
        $similary->setArticleDetail($articleDetail);
        $similary->setOrdernumber($articleDetail->getNumber());
        $similary->setEan($articleDetail->getEan());
        $similary->setArticleName($articleDetail->getArticle()->getName());
        $similary->setPositionId($position->getId());
        $similary->setTax($articleDetail->getArticle()->getTax());
        $similary->setQuantity($position->getQuantity());
        $similary->setExpectedQuantity($position->getQuantity());
        $similary->setArticleId($articleDetail->getArticleId());
        $similary->setShippingCost($this->getShippingCost($position));
        $similary->setUnit($articleDetail->getUnit());
        $similary->setPackUnit($articleDetail->getPackUnit());
        $similary->setDetailStatus($this->detailStatus);

        return [$similary];
    }

    /**
     * @param Position $position
     * @return float|int
     */
    private function getShippingCost(Position $position)
    {
        /** @var ShippingCost[] $costMatrix */
        $costMatrix = $position->getOrder()->getDispatch()->getCostsMatrix()->getValues();

        return array_sum(array_map(function($cost) {
            return $cost->getValue();
        }, $costMatrix));
    }

    /**
     * @param Position $position
     * @return int
     */
    private function getDcStock(Position $position)
    {
        $stock = 0;
        $attribute = $position->getArticleDetail()->getAttribute();

        foreach ($this->wholesalers as $wholesaler)
        {
            if (method_exists($attribute, 'getDc' . $wholesaler->getUpperShortname() . 'Instock')) {
                $stock += $attribute->{'getDc' . $wholesaler->getUpperShortname() . 'Instock'}();
            }
        }

        return $stock;
    }

    /**
     * @param Position $position
     * @return float
     */
    private function getPositionPrice(Position $position)
    {
        $articleDetail = $position->getArticleDetail();

        try {
            if (!is_null($this->context->getShopContext())) {
                $article = Shopware()->Modules()->Articles()->sGetArticleById($articleDetail->getArticleId());
                return $article['price'];
            } else {
                throw new Exception('shopcontext not available');
            }
        } catch (Exception $e) {
            $customerGroup = $position->getOrder()->getCustomer()->getGroup();

            /** @var Group $defaultCustomerGroup */
            $defaultCustomerGroup = $this->em->getRepository(Group::class)->findOneBy([
                'key' => 'EK'
            ]);

            /** @var Price[] $articleDetailPrices */
            $articleDetailPrices = $articleDetail->getPrices()->getValues();

            $price = null;

            foreach ($articleDetailPrices as $articleDetailPrice)
            {
                if ($articleDetailPrice->getCustomerGroup() === $customerGroup) {
                    return$articleDetailPrice->getPrice();
                }
            }

            foreach ($articleDetailPrices as $articleDetailPrice)
            {
                if ($articleDetailPrice->getCustomerGroup() === $defaultCustomerGroup) {
                    return $articleDetailPrice->getPrice();
                }
            }

            /** @var Price $articleDetailPrice */
            $articleDetailPrice = $articleDetail->getPrices()->first();

            return $articleDetailPrice->getPrice();
        }
    }
}