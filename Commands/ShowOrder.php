<?php

namespace WundeDcompanion\Commands;

use Exception;
use Shopware\Commands\ShopwareCommand;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Article;
use Shopware\Models\Article\Detail;
use Shopware\Models\Order\Order;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use WundeDcompanion\Bundle\OrderBundle\SortService;
use WundeDcompanion\Services\ConfigService;
use WundeDcompanionArticleImport\Models\ImportArticle;

/**
 * Class Test
 * @package WundeDcompanion\Commands
 */
class ShowOrder extends ShopwareCommand
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * Test constructor.
     * @param ModelManager $em
     * @param Logger $logger
     */
    public function __construct(ModelManager $em, Logger $logger)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $logger;
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('show:orders')
            ->setDescription('Show all Orders')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Order[] $orders */
            $orders = $this->em->getRepository(Order::class)->findAll();

            foreach ($orders as $order)
            {
                $output->writeln('<comment>' . $order->getNumber() . '</comment>');

                $rows = [];

                /** @var \Shopware\Models\Order\Detail $position */
                foreach ($order->getDetails()->getValues() as $position)
                {
                    $rows[] = [
                        $position->getArticleNumber(),
                        $position->getQuantity(),
                        $position->getAttribute()->getDcNameShort() ?: 'shop',
                        $position->getArticleDetail()->getId()
                    ];
                }

                $table = new Table($output);

                $table
                    ->setHeaders(['Artikelnummer', 'Anzahl', 'Händler', 'Detail ID'])
                    ->setRows($rows)
                    ->setColumnWidths([20, 20, 20, 20])
                    ->render();

                $output->writeln('');
            }


        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}