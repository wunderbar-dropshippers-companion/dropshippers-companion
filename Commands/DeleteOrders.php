<?php

namespace WundeDcompanion\Commands;

use Exception;
use Shopware\Commands\ShopwareCommand;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Order;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use Shopware\Models\Order\Status;
use WundeDcompanion\Services\OrderService;

/**
 * Class Test
 * @package WundeDcompanion\Commands
 */
class DeleteOrders extends ShopwareCommand
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * DeleteOrders constructor.
     * @param ModelManager $em
     */
    public function __construct(ModelManager $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('orders:delete')
            ->setDescription('Delete all orders')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            /** @var Order[] $orders */
            $orders = $this->em->getRepository(Order::class)->findAll();

            if ($count = count($orders)) {
                $output->writeln('<comment>DELETE ' . $count . ' ORDERS' . PHP_EOL . '</comment>');
            }

            foreach ($orders as $order)
            {
                $output->writeln('<info>Order ' . $order->getNumber() . ' deleted</info>');
                $this->em->remove($order);
            }

            $finish = count($orders) ? PHP_EOL . 'Finish' : 'Finish';

            $output->writeln('<comment>'.$finish.'</comment>');

            $this->em->flush();
            $this->em->clear();
        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}