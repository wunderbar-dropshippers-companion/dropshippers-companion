//{namespace name=backend/wunde_dcompanion_article/main}
Ext.define('Shopware.apps.WundeDcompanionArticle.view.detail.List', {
    extend: 'Ext.grid.Panel',
    scrollable: true,
    stateful: true,
    stateId: 'multiedit-wholesaler-grid',
    alias: 'widget.multi-edit-wholesaler-grid',
    forceFit: true,

    snippets: {
        active: '{s name=active}Active{/s}',
        activateDeactivate: '{s name=activate/deactivate}activate / deactivate{/s}',
        wholesaler: '{s name=wholesaler}Wholesaler{/s}',
        ordernumber: '{s name=ordernumber}Ordernumber{/s}',
        refresh: '{s name="refresh"}Refresh{/s}',
        textfieldOrdernumberPlaceholder: '{s name=textfield/ordernumber/placeholder}Ordernumber{/s}',
        validatorMinThreeCharacters: '{s name=validator/min_three_characters}Min three characters{/s}',
        articlename: '{s name=articlename}Articlename{/s}',
        priceRecommended: '{s name=price/recommended}Suggested retail price{/s}',
        pricePurchase: '{s name=price/purchase}Purchase price{/s}',
        instock: '{s name=instock}Instock{/s}',
        overrideNameAndDescription: '{s name=override/name}Overwrite name and Description{/s}',
        overridePurchasePrice: '{s name=override/purchase_price}Overwrite purchase price{/s}',
        overridePermanently: '{s name=override/permanently}Permanently overwrite{/s}',
        overridePriceTooltip: '{s name="override/price/permanently/tooltip"}Overwrite purchase price permanently{/s}',
        overrideNameTooltip: '{s name="override/name/permanently/tooltip"}Overwrite article name and description permanently{/s}',
        deleteData: '{s name="delete/article/wholesaler/data"}Delete data{/s}'
    },

    initComponent: function () {
        var me = this;

        this.setupStateManager();

        me.columns = me.getColumns();
        me.tbar = me.createGridToolbar();

        me.addEvents(
            'editWholesaler',
            'saveWholesaler',
            'updateArticleNameAndDescription',
            'updatePurchasePrice',
            'setActive',
            'setPermanentlyOverrideNameAndDescription',
            'setPermanentlyOverridePurchasePrice',
            'deleteData',
            'refresh'
        )

        me.rowEditing = Ext.create('Ext.grid.plugin.RowEditing', {
            clicksToEdit: 2,
            autoCancel: true,
            listeners: {
                scope: me,
                edit: function (editor, context) {
                    me.fireEvent('saveWholesaler', context.record, me.detailId, me.variant)
                },
                beforeedit: function(editor, context) {
                    return me.fireEvent('editWholesaler', context.record, me.detailId)
                }
            }
        });

        me.plugins = me.rowEditing;

        me.callParent(arguments)
    },

    setupStateManager: function () {
        var me = this;
        me.stateManager = new Ext.state.LocalStorageProvider({ });

        Ext.state.Manager.setProvider(me.stateManager);
    },

    getColumns: function() {
        var me = this;

        var columns = [
            {
                dataIndex: 'active',
                xtype: 'actioncolumn',
                width: 40,
                align: 'center',
                header: me.snippets.active,
                sortable: true,
                resizable: false,
                groupable: false,
                hideable: false,
                items: [
                    {
                        hideable: false,
                        tooltip: me.snippets.activateDeactivate,
                        handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                            return me.fireEvent('setActive', record, me.detailId, me.variant)
                        },
                        getClass: function(value, metaData, record) {
                            return me.boolClass(record.data.active)
                        }
                    }
                ]
            },
            {
                dataIndex: 'longname',
                header: me.snippets.wholesaler,
                flex: 1,
                groupable: false,
                sortable: true,
                resizable: true,
                renderer: me.boldRenderer
            },
            {
                dataIndex: 'ordernumber',
                header: me.snippets.ordernumber,
                width: 150,
                align: 'center',
                editor: {
                    xtype: 'textfield',
                    allowBlank: false,
                    emptyText: me.snippets.textfieldOrdernumberPlaceholder,
                    validator: function(value) {
                        return (value.length > 2) || me.snippets.validatorMinThreeCharacters;
                    }
                },
                groupable: false,
                sortable: true,
                resizable: true
            },
            {
                dataIndex: 'articlename',
                header: me.snippets.articlename,
                flex: 1,
                groupable: false,
                sortable: true,
                resizable: true,
                renderer: me.defaultRenderer
            },
            {
                dataIndex: 'price_recommended',
                xtype: 'numbercolumn',
                header: me.snippets.priceRecommended,
                width: 100,
                align: 'center',
                groupable: false,
                sortable: true,
                resizable: false,
                renderer: me.priceRenderer
            },
            {
                dataIndex: 'purchasing_price',
                xtype: 'numbercolumn',
                header: me.snippets.pricePurchase,
                width: 100,
                align: 'center',
                groupable: false,
                sortable: true,
                resizable: false,
                renderer: me.priceRenderer
            },
            {
                dataIndex: 'instock',
                header: me.snippets.instock,
                width: 60,
                align: 'center',
                groupable: false,
                sortable: true,
                resizable: false,
                renderer: me.nummberRenderer
            },
        ];

        if (!me.variant) {
            columns.push({
                xtype: 'actioncolumn',
                width: 90,
                align: 'center',
                sortable: false,
                resizable: false,
                groupable: false,
                hideable: false,
                header: me.snippets.overridePermanently,
                items: [
                    {
                        hideable: false,
                        tooltip: me.snippets.overrideNameTooltip,
                        handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                            me.fireEvent('setPermanentlyOverrideNameAndDescription', grid, record, me.detailId)
                        },
                        getClass: function(value, metaData, record) {
                            return me.boolClass(
                                record.get('ordernumber').length && record.get('active'),
                                me.boolClass(record.get('override_name')),
                                ''
                            )
                        }
                    },
                    {
                        hideable: false,
                        tooltip: me.snippets.overridePriceTooltip,
                        handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                            me.fireEvent('setPermanentlyOverridePurchasePrice', grid, record, me.detailId)
                        },
                        getClass: function(value, metaData, record) {
                            return me.boolClass(
                                record.get('ordernumber').length && record.get('active'),
                                me.boolClass(record.get('override_price')),
                                ''
                            )
                        }
                    }
                ]
            });
        }

        columns.push({
            xtype: 'actioncolumn',
            width: 90,
            align: 'center',
            sortable: false,
            resizable: false,
            groupable: false,
            hideable: false,
            items: [
                {
                    hideable: false,
                    tooltip: 'Edit',
                    handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                        if (record.get('active')) {
                            me.rowEditing.startEdit(record, colIndex);
                        }
                    },
                    getClass: function(value, metaData, record) {
                        return me.boolClass(record.get('active'), 'sprite-pencil', '')
                    }
                },
                {
                    hideable: false,
                    tooltip: me.snippets.overrideNameAndDescription,
                    handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                        me.fireEvent('updateArticleNameAndDescription', record)
                    },
                    getClass: function(value, metaData, record) {
                        return me.boolClass(
                            record.get('ordernumber').length && record.get('articlename').length && record.get('active'),
                            'sprite-layer-shape-text',
                            ''
                        );
                    }
                },
                {
                    hideable: false,
                    tooltip: me.snippets.overridePurchasePrice,
                    handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                        me.fireEvent('updatePurchasePrice', record);
                    },
                    getClass: function(value, metaData, record) {
                        return me.boolClass(
                            record.get('ordernumber').length && record.get('purchasing_price') && record.get('active'),
                            'sprite-calculator--arrow',
                            ''
                        );
                    }
                },
                {
                    hideable: false,
                    tooltip: me.snippets.deleteData,
                    handler: function(grid, rowIndex, colIndex, item, eOpts, record) {
                        me.fireEvent('deleteData', record, me.store, me.detailId, me.variant);
                    },
                    getClass: function(value, metaData, record) {
                        return me.boolClass(
                            record.get('ordernumber').length,
                            'sprite-cross-circle',
                            ''
                        );
                    }
                }
            ]
        });

        return columns;
    },

    boldRenderer: function(value) {
        return '<b>' + value + '</b>';
    },

    boolClass: function(condition, trueCls = 'sprite-tick', falseCls = 'sprite-cross') {
        if (condition) {
            return trueCls;
        } else {
            return falseCls;
        }
    },

    nummberRenderer: function(value, item, record) {
        if (record.get('ordernumber').length) {
            if (record.get('active')) {
                if (value) {
                    return '<span style="color: green;">' + value + '</span>';
                } else {
                    return '<span style="color: red;">' + value + '</span>';
                }
            } else {
                return '<span style="opacity: .4;">' + value + '</span>';
            }
        }

        return '';
    },

    defaultRenderer: function(value, item, record) {
        if (record.get('ordernumber').length) {
            if (record.get('active')) {
                return value;
            } else {
                return '<span style="opacity: .4;">' + value + '</span>';
            }
        }

        return '';
    },

    priceRenderer: function(value, item, record) {
        if (record.get('ordernumber').length) {
            if (record.get('active') && value !== Ext.undefined) {
                return Ext.util.Format.currency(value);
            } else {
                return '<span style="opacity: .4;">' + value + '</span>';
            }
        }

        return '';
    },

    createGridToolbar: function() {
        var me = this;

        me.refreshButton = Ext.create('Ext.button.Button', {
            iconCls:'sprite-arrow-circle-135',
            text: me.snippets.refresh,
            handler: function() {
                me.fireEvent('refresh', me.store, me.variant);
            }
        });

        return Ext.create('Ext.toolbar.Toolbar', {
            dock:'top',
            ui: 'shopware-ui',
            items:[
                me.refreshButton,
            ]
        });
    },
});