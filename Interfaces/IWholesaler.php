<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 13/08/2020 14:42
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Interfaces;


interface IWholesaler
{
    /**
     * @return string
     */
    public function getShortname();

    /**
     * @return string
     */
    public function getLongname();

    /**
     * @return string
     */
    public function getBasename();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return IClient
     */
    public function getClient();
}