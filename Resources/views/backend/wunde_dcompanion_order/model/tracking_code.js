//{namespace name=backend/wunde_dcompanion_order/main}
Ext.define('Shopware.apps.WundeDcompanionOrder.model.TrackingCode', {

    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: [
        { name: 'code', type: 'string' },
        { name: 'url', type: 'string' },
    ],
});