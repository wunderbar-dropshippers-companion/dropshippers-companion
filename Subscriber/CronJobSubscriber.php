<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 20/08/2020 20:45
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\Query\Expr;
use Enlight\Event\SubscriberInterface;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\ConfigReader;
use Shopware\Models\Article\Detail;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Status;
use Shopware\Models\Shop\Repository as ShopRepository;
use Shopware\Models\Shop\Shop;
use Shopware_Components_Cron_CronJob;
use Shopware_Components_Snippet_Manager;
use WundeDcompanion\Models\Order\Carrier;
use WundeDcompanion\Models\Order\Trackingcode;
use WundeDcompanion\Models\Wholesaler\Config;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\OrderService;
use WundeDcompanion\Services\PickwareService;
use WundeDcompanion\Services\SchemaService;
use WundeDcompanion\Services\TrackingService;

class CronJobSubscriber implements SubscriberInterface
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_subscriber/main';

    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var SchemaService $schemaService
     */
    private $schemaService;

    /**
     * @var Connection $connection
     */
    private $connection;

    /**
     * @var Expr
     */
    private $expr;

    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var string $pluginName
     */
    private $pluginName;

    /**
     * @var TrackingService $trackingService
     */
    private $trackingService;

    /**
     * @var PickwareService $pickwareService
     */
    private $pickwareService;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    private $snippets;

    /**
     * CronJobSubscriber constructor.
     * @param WholesalerRepository $wholesalerRepository
     * @param SchemaService $schemaService
     * @param PickwareService $pickwareService
     * @param TrackingService $trackingService
     * @param ModelManager $em
     * @param Shopware_Components_Snippet_Manager $snippetManager
     * @param OrderService $orderService
     * @param LoggerService $logger
     * @param string $pluginName
     */
    public function __construct($wholesalerRepository, $schemaService, $pickwareService, $trackingService, $em, $snippetManager, $orderService, $logger, $pluginName)
    {
        $this->wholesalerRepository = $wholesalerRepository;
        $this->schemaService = $schemaService;
        $this->pickwareService = $pickwareService;
        $this->trackingService = $trackingService;
        $this->em = $em;
        $this->snippets = $snippetManager->getNamespace(self::SNIPPET_NAMESPACE);
        $this->connection = $em->getConnection();
        $this->expr = $em->getExpressionBuilder();
        $this->orderService = $orderService;
        $this->logger = $logger;
        $this->pluginName = $pluginName;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Shopware_CronJob_DcTracking' => 'onDcTracking',
            'Shopware_CronJob_DcSendOrder' => 'onDcSendOrder',
            'Shopware_CronJob_DcReceiveData' => 'onDcReceiveData',
            'Shopware_CronJob_DcDeleteLog' => 'onDcDeleteLog'
        ];
    }

    /**
     * @param bool $cached
     * @return array
     */
    public function getConfig($cached = false)
    {
        /** @var ShopRepository $shopRepository */
        $shopRepository = Shopware()->Models()->getRepository(Shop::class);

        /** @var ConfigReader $configReader */
        $configReader = $cached
            ? Shopware()->Container()->get('shopware.plugin.cached_config_reader')
            : Shopware()->Container()->get('shopware.plugin.config_reader');

        $shop = Shopware()->Container()->has('shop')
            ? Shopware()->Shop()
            : $shopRepository->getActiveDefault();

        return $configReader->getByPluginName($this->pluginName, $shop);
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     */
    public function onDcDeleteLog(Shopware_Components_Cron_CronJob $job = null)
    {
        $days = $this->getConfig()['dc_delete_log_after'] || 30;
        $logPath = Shopware()->DocPath() . 'var/log/';

        foreach (scandir($logPath) as $file) {
            if ($file != '.' && $file != '..' && strpos($file, 'wunde_dcompanion') !== false) {

                $file = $logPath . $file;

                $filetime = date('d-m-Y H:i:s', filemtime($file));
                $now = date('d-m-Y H:i:s');

                $duration = $diff = strtotime($now) - strtotime($filetime);

                if ($duration > (60 * 60 * 24 * $days)) {
                    unlink($file);
                }
            }
        }
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     * @return string
     */
    public function onDcTracking(Shopware_Components_Cron_CronJob $job)
    {
        $expr = $this->em->getExpressionBuilder();

        /** @var Order[] $orders */
        $orders = $this->em->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->select(['o'])
            ->leftJoin('o.paymentStatus', 'payment')
            ->leftJoin('o.orderStatus', 'status')
            ->leftJoin('o.attribute', 'attribute')
            ->where($expr->andX(
                $expr->eq('payment.id', ':paymentId'),
                $expr->eq('attribute.dcDropshipActive', ':dropshipActive')
            ))
            ->setParameter('paymentId', Status::PAYMENT_STATE_COMPLETELY_PAID)
            ->setParameter('dropshipActive', true)
            ->getQuery()
            ->getResult();

        $result = '';

        foreach ($orders as $key => $order)
        {
            if ($key) {
                $result .= "\n\n";
            }

            $result .= $this->trackingService->handleOrderTrackings($order);
        }

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return $result;
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     */
    public function onDcSendOrder(Shopware_Components_Cron_CronJob $job)
    {
        try {
            $this->orderService->sendOrders();
        } catch(Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     * @return string
     */
    public function onDcReceiveData(Shopware_Components_Cron_CronJob $job)
    {
        $result = '';

        $logContent = [];
        $allUpdates = [];

        foreach ($this->wholesalerRepository->findAllActive() as $shortname => $wholesaler)
        {
            if (!empty($result)) $result .= PHP_EOL.PHP_EOL;

            $dcProducts = json_decode($wholesaler->getClient()->getAll()->getContent(), true);

            if (!empty($dcProducts)) {

                /** @var Detail[] $articles */
                $articles = $this->em->getRepository(Detail::class)
                    ->createQueryBuilder('detail')
                    ->select(['detail'])
                    ->leftJoin('detail.attribute', 'attribute')
                    ->where($this->expr->andX(
                        $this->expr->eq('attribute.dc'.ucfirst($shortname).'Active', ':active'),
                        $this->expr->isNotNull('attribute.dc'.ucfirst($shortname).'Ordernumber'),
                        $this->expr->isNotNull('attribute.dc'.ucfirst($shortname).'Articlename'),
                        $this->expr->neq('attribute.dc'.ucfirst($shortname).'Articlename', ':empty'),
                        $this->expr->neq('attribute.dc'.ucfirst($shortname).'Articlename', ':empty')
                    ))
                    ->setParameter('active', true)
                    ->setParameter('empty', '')
                    ->getQuery()
                    ->getResult();

                if (count($articles)) {

                    $wholesalerUpdates = [];

                    foreach ($articles as $article)
                    {
                        $ordernumber = $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'Ordernumber'}();
                        if (array_key_exists($ordernumber, $dcProducts)) {

                            $articleUpdates = [];
                            $purchasePrice = 0;

                            if (
                                isset($dcProducts[$ordernumber]['quantity']) &&
                                $dcProducts[$ordernumber]['quantity'] !== $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'Instock'}()
                            ) {
                                $articleUpdates[] = [
                                    'name' => 'instock',
                                    'from' => $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'Instock'}(),
                                    'to' => $dcProducts[$ordernumber]['quantity']
                                ];

                                $article->getAttribute()->{'setDc' . ucfirst($shortname) . 'Instock'}($dcProducts[$ordernumber]['quantity']);
                            }

                            if (
                                isset($dcProducts[$ordernumber]['description']) &&
                                $dcProducts[$ordernumber]['description'] !== $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'Description'}()
                            ) {
                                $article->getAttribute()->{'setDc' . ucfirst($shortname) . 'Description'}($dcProducts[$ordernumber]['description']);
                                $articleUpdates[] = [
                                    'name' => 'description'
                                ];
                            }

                            if (isset($dcProducts[$ordernumber]['purchasePrice'])) {
                                $purchasePrice = number_format($dcProducts[$ordernumber]['purchasePrice'], 2);

                                if ((float)$purchasePrice !== (float)$article->getAttribute()->{'getDc' . ucfirst($shortname) . 'PurchasingPrice'}()) {
                                    $articleUpdates[] = [
                                        'name' => 'purchasePrice',
                                        'from' => $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'PurchasingPrice'}(),
                                        'to' => $purchasePrice
                                    ];

                                    $article->getAttribute()->{'setDc' . ucfirst($shortname) . 'PurchasingPrice'}((float)$purchasePrice);
                                }
                            }

                            if (isset($dcProducts[$ordernumber]['priceRecommended'])) {
                                $priceRecommended = number_format($dcProducts[$ordernumber]['priceRecommended'], 2);

                                if ((float)$priceRecommended !== (float)$article->getAttribute()->{'getDc' . ucfirst($shortname) . 'PriceRecommended'}()) {
                                    $articleUpdates[] = [
                                        'name' => 'priceRecommended',
                                        'from' => $article->getAttribute()->{'getDc' . ucfirst($shortname) . 'PriceRecommended'}(),
                                        'to' => $priceRecommended
                                    ];

                                    $article->getAttribute()->{'setDc' . ucfirst($shortname) . 'PriceRecommended'}((float)$priceRecommended);
                                }
                            }

                            if (!empty($articleUpdates)) {
                                $wholesalerUpdates[$ordernumber] = $articleUpdates;
                                $logContent[$wholesaler->getShortname()][$ordernumber] = $articleUpdates;
                            }

                            $warehouse = $wholesaler->getConfig()->pickware()->warehouse();

                            if (!is_null($warehouse) && (float)$purchasePrice > 0) {
                                $this->pickwareService->setStock(
                                    $article,
                                    $warehouse,
                                    $dcProducts[$ordernumber]['quantity'],
                                    $purchasePrice
                                );
                            }
                        }
                    }

                    if (!empty($wholesalerUpdates)) {
                        $allUpdates[$wholesaler->getLongname()] = $wholesalerUpdates;
                    }

                    try {
                        $this->em->flush();
                    } catch (Exception $e) {
                        $this->logger->error($e->getMessage(), $e, 'cron');
                    }
                }
            }
        }

        if (!empty($allUpdates)) {
            foreach ($allUpdates as $wholesaler => $wholesalerUpdates)
            {
                if (!empty($wholesalerUpdates)) {
                    if (!empty($result)) {
                        $result .= PHP_EOL . PHP_EOL;
                    }

                    $result .= $wholesaler . ':' . PHP_EOL.PHP_EOL;

                    $arrayPointer = 0;

                    foreach ($wholesalerUpdates as $ordernumber => $articleUpdates)
                    {
                        if (!empty($articleUpdates)) {

                            if ($arrayPointer++ !== 0) {
                                $result .= PHP_EOL;
                            }

                            $result .= "\t" . $ordernumber . ':' . PHP_EOL;

                            foreach ($articleUpdates as $update)
                            {
                                $result .= isset($update['from']) && isset($update['to'])
                                    ? "\t\t- " . $this->snippets->get($update['name'], $update['name']) . ' (' . $update['from'] . ' => ' . $update['to'] . ')' . PHP_EOL
                                    : "\t\t- " . $this->snippets->get($update['name'], $update['name']) . PHP_EOL;
                            }
                        }
                    }
                }
            }
        } else {
            $result .= $this->snippets->get('articles/up_to_date', 'All articles are up to date');
            $logContent = ['All articles are up to date'];
        }

        $this->logger->info('CronJob onDcReceiveData', $logContent, 'cron');

        return $result;
    }
}