<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 16:00
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Database;


/**
 * Class Table
 * @package WundeDcompanion\Helpers\Database
 */
class DatabaseTable
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var Column[] $columns
     */
    private $columns;

    /**
     * Table constructor.
     * @param string $name
     * @param Column[] $columns
     */
    public function __construct($name = '', $columns = [])
    {
        $this->name = $name;
        $this->columns = $columns;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }
}