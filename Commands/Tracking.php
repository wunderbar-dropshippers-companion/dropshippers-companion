<?php

namespace WundeDcompanion\Commands;

use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Shopware\Commands\ShopwareCommand;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Bundle\OrderBundle\OrderService;
use WundeDcompanion\Bundle\OrderBundle\SortService;
use WundeDcompanion\Models\Order\Carrier;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\ConfigService;

/**
 * Class Test
 * @package WundeDcompanion\Commands
 */
class Tracking extends ShopwareCommand
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var SortService $sortService
     */
    private $sortService;

    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * Test constructor.
     * @param ModelManager $em
     * @param Logger $logger
     * @param SortService $sortService
     * @param OrderService $orderService
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(ModelManager $em, Logger $logger, SortService $sortService, OrderService $orderService, WholesalerRepository $wholesalerRepository)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $logger;
        $this->sortService = $sortService;
        $this->sortService = $sortService;
        $this->orderService = $orderService;
        $this->wholesalers = $wholesalerRepository->findAllActive();
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dc:tracking')
            ->setDescription('make a functionality test')
            ->setHelp(<<<EOF
The <info>%command.name%</info> Make Tests.
EOF
            )
        ;
    }

    /**
     *
     */
    private function clearConsole()
    {
        echo sprintf("\033\143");
    }

    /**
     * @param Order $order
     * @return Wholesaler[]
     */
    private function getUsedOrderWholesalers(Order $order): array
    {
        $shortnames = array_map(function ($position) {
            /** @var Position $position */
            $attributes = $position->getAttribute();
            return method_exists($attributes, 'getDcNameShort')
                ? $attributes->getDcNameShort()
                : '';
        }, $order->getDetails()->getValues());

        $filteredShortnames = array_filter($shortnames, function($shortname) {
            return !empty($shortname);
        });

        return array_filter($this->wholesalers, function ($shortname) use($filteredShortnames) {
            return in_array($shortname, $filteredShortnames);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function hasOrderMissingTrackingCodes(Order $order)
    {
        if (empty($order->getTrackingCode()) || is_null($order->getTrackingCode())) {
            return true;
        }

        /** @var Position $position */
        foreach ($order->getDetails()->getValues() as $position)
        {
            $attributes = $position->getAttribute();

            if (
                method_exists($attributes, 'getDcDropshipTrackingCarrier') &&
                method_exists($attributes, 'getDcDropshipTrackingCode')
            ) {
                $carriers = $attributes->getDcDropshipTrackingCarrier();
                $codes = $attributes->getDcDropshipTrackingCode();

                if (empty($carriers) || empty($codes)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->clearConsole();

            $trackingService = Shopware()->Container()->get('wunde_dcompanion.services.tracking');

            /** @var Order $order */
            $order = $this->em->getRepository(Order::class)->find(211);

            $trackingService->handleOrderTrackings($order);
        } catch (Exception $e) {
            $output->writeln('<error>'.print_r($e->getMessage(), true).'</error>');
        }
    }
}