<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:46
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Hook_HookArgs;
use Shopware_Controllers_Backend_Config;
use Shopware_Controllers_Backend_ViisonPickwareERPStockInitialization;
use WundeDcompanion\Helpers\ShopwareConfig;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Services\LoggerService;
use Shopware_Controllers_Backend_ArticleList;
use Shopware_Controllers_Frontend_Detail;
use WundeDcompanion\Services\PickwareService;

class RouteSubscriber implements SubscriberInterface
{

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var string $pluginDirectory
     */
    private $pluginDirectory;

    /**
     * @var string $pluginName
     */
    private $pluginName;

    /**
     * @var string $templatePath
     */
    private $templatePath;

    /**
     * @var WholesalerRepository $wholesalerRepository
     */
    private $wholesalerRepository;

    /**
     * @var PickwareService $pickwareService
     */
    private $pickwareService;

    /**
     * RouteSubscriber constructor.
     * @param LoggerService $logger
     * @param PickwareService $pickwareService
     * @param WholesalerRepository $wholesalerRepository
     * @param string $pluginDirectory
     * @param string $pluginName
     * @param string $templatePath
     */
    public function __construct(
        LoggerService $logger,
        PickwareService $pickwareService,
        WholesalerRepository $wholesalerRepository,
        string $pluginDirectory,
        string $pluginName,
        string $templatePath
    )
    {
        $this->logger = $logger;
        $this->pickwareService = $pickwareService;
        $this->wholesalerRepository = $wholesalerRepository;
        $this->pluginName = $pluginName;
        $this->pluginDirectory = $pluginDirectory;
        $this->templatePath = $templatePath;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch' => 'onPostDispatch',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Article' => 'onBackendArticlePostDispatch',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_ArticleList' => 'onBackendArticleListPostDispatch',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Order' => 'onBackendOrderPostDispatch',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Base' => 'onBackendBasePostDispatch',
            'Enlight_Controller_Action_PostDispatchSecure_Backend_Index' => 'onBackendIndexPostDispatch',
        ];
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onPostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $view = $args->getSubject()->View();

        ShopwareConfig::overwrite();

        $view->addTemplateDir($this->pluginDirectory . $this->templatePath);
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onBackendIndexPostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $request = $args->getSubject()->Request();
        $view = $args->getSubject()->View();

        if ($request->getActionName() === 'index') {
            $view->extendsTemplate('backend/wunde_dcompanion_base/header.tpl');
        }
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onBackendBasePostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $request = $args->getSubject()->Request();
        $view = $args->getSubject()->View();

        if ($request->getActionName() === 'index') {
            $view->extendsTemplate('backend/wunde_dcompanion_base/model/variant.js');
        }
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onBackendOrderPostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $request = $args->getSubject()->Request();
        $view = $args->getSubject()->View();

        if ($request->getActionName() === 'index') {
            $view->extendsTemplate('backend/wunde_dcompanion_order/app.tpl');
        }

        if ($request->getActionName() === 'load') {
            $view->extendsTemplate('backend/wunde_dcompanion_order/view/detail/position.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/view/detail/overview.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/view/detail/window.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/view/list/position.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/view/list/list.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/model/position.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/model/tracking_code.js');
            $view->extendsTemplate('backend/wunde_dcompanion_order/model/order.js');
        }
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onBackendArticleListPostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $request = $args->getSubject()->Request();
        $view = $args->getSubject()->View();

        if ($request->getActionName() === 'load') {
            $view->extendsTemplate('backend/wunde_dcompanion_article_list/view/main/grid.js');
        }
    }

    /**
     * @param Enlight_Controller_ActionEventArgs $args
     */
    public function onBackendArticlePostDispatch(Enlight_Controller_ActionEventArgs $args)
    {
        $request = $args->getSubject()->Request();
        $view = $args->getSubject()->View();

        if ($request->getActionName() === 'index') {
            $view->extendsTemplate('backend/wunde_dcompanion_article/app.tpl');
        }

        if ($request->getActionName() === 'load') {
            $view->extendsTemplate('backend/wunde_dcompanion_article/view/detail/window.js');
            $view->extendsTemplate('backend/wunde_dcompanion_article/view/variant/list.js');

            if ($this->pickwareService->isActive()) {
                $view->extendsTemplate('backend/wunde_dcompanion_article/view/variant/pickware_detail.js');
            } else {
                $view->extendsTemplate('backend/wunde_dcompanion_article/view/variant/detail.js');
            }
        }
    }
}