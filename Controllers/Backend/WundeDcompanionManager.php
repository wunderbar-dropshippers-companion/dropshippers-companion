<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 13/04/2021 09:25
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

use Shopware\Components\CSRFWhitelistAware;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Config\Element;
use Shopware\Models\Plugin\Plugin;
use Shopware\Models\User\User;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Services\PickwareService;
use Shopware\Models\Shop\Repository as ShopRepository;
use WundeDcompanion\Models\Ticket\Priority;
use WundeDcompanion\Models\Ticket\Status;
use Shopware\Models\Config\Form;


/**
 * Class Shopware_Controllers_Backend_WundeDcompanionManager
 */
class Shopware_Controllers_Backend_WundeDcompanionManager extends Shopware_Controllers_Backend_ExtJs implements CSRFWhitelistAware
{
    const LOCALES = [
        ['long' => 'en_GB', 'short' => 'en', 'fallback' => true, 'snippet' => 'language.english'],
        ['long' => 'de_DE', 'short' => 'de', 'fallback' => false, 'snippet' => 'language.german'],
    ];

    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_manager/main';
    const KEY_SPACE = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    /**
     * @var Wholesaler[]
     */
    protected $wholesalers = [];

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    protected $snippets;

    /**
     * @var ModelManager $em
     */
    protected $em;

    /**
     * @var PickwareService $pickwareService
     */
    protected $pickwareService;

    /**
     * @var string $pluginDirectory
     */
    private $pluginDirectory;

    /**
     * Shopware_Controllers_Backend_WundeDcompanionManager constructor.
     */
    public function __construct()
    {
        parent::__construct();

        /** @var WholesalerRepository $wholesalerRepository */
        $wholesalerRepository = Shopware()->Container()->get('wunde_dcompanion.repositories.wholesalers');

        $this->em = Shopware()->Container()->get('models');
        $this->logger = Shopware()->Container()->get('wunde_dcompanion.services.logger');
        $this->pickwareService = Shopware()->Container()->get('wunde_dcompanion.services.pickware');
        $this->snippets = Shopware()->Snippets()->getNamespace(self::SNIPPET_NAMESPACE);
        $this->pluginDirectory = Shopware()->Container()->getParameter('wunde_dcompanion.plugin_dir');

        $this->wholesalers = array_values($wholesalerRepository->findAll());
    }

    /**
     * @return \Shopware\Models\Shop\Locale[]
     */
    protected function getLocales()
    {
        $expr = $this->em->getExpressionBuilder();

        /** @var \Shopware\Models\Shop\Locale[] $locales */
        $locales = $this->em->getRepository(\Shopware\Models\Shop\Locale::class)
            ->createQueryBuilder('l')
            ->where($expr->in('l.locale', ':locales'))
            ->setParameter('locales', array_column(self::LOCALES, 'long'))
            ->getQuery()
            ->getResult();

        return $locales;
    }

    /**
     * @return mixed|string
     */
    protected function getFallbackLocale()
    {
        foreach (self::LOCALES as $locale)
        {
            if ($locale['fallback']) {
                return $locale['short'];
            }
        }

        return 'en';
    }

    /**
     * @param $locale
     * @return mixed|null
     */
    protected function getLocaleShortCode($locale)
    {
        foreach (self::LOCALES as $l)
        {
            if ($l['long'] === $locale) {
                return $l['short'];
            }
        }

        return null;
    }

    /**
     * @return User|null
     */
    protected function getBackendUser()
    {
        if (Shopware()->Container()->has('auth')) {
            $auth = Shopware()->Container()->get('auth');

            if (method_exists($auth, 'getIdentity')) {
                $userId = $auth->getIdentity()->id;

                /** @var User $user */
                $user = $this->em->getRepository(User::class)->find($userId);

                return $user;
            }
        }

        return null;
    }

    /**
     * @return mixed|string
     */
    protected function getBackendUserLocale()
    {
        $user = $this->getBackendUser();

        if (!is_null($user)) {
            $localeId = $user->getLocaleId();

            /** @var \Shopware\Models\Shop\Locale $locale */
            $locale = $this->em->getRepository(\Shopware\Models\Shop\Locale::class)->find($localeId);

            if (!is_null($locale)) {
                foreach (self::LOCALES as $l)
                {
                    if ($l['long'] === $locale->getLocale()) {
                        return $l['short'];
                    }
                }
            }
        }

        return 'en';
    }

    /**
     * @param $length
     * @return string|null
     */
    private function randomStr($length)
    {
        $str = null;

        try {
            $max = mb_strlen(self::KEY_SPACE, '8bit') - 1;
            if ($max < 1) {
                throw new Exception(self::KEY_SPACE . ' must be at least two characters long');
            }
            for ($i = 0; $i < $length; ++$i) {
                $str .= self::KEY_SPACE[random_int(0, $max)];
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return $str;
    }

    public function getBackendUserAction()
    {
        $user = $this->getBackendUser();

        if (!is_null($user)) {
            $this->view->assign([
                'success' => true,
                'user' => [
                    'id' => $user->getId(),
                    'name' => $user->getName(),
                    'email' => $user->getEmail()
                ]
            ]);
        } else {
            $this->view->assign([
                'success' => false
            ]);
        }
    }

    public function getSnippetsAction()
    {
        $lang = $this->getBackendUserLocale();

        $locales = $this->getLocales();
        $snippetManager = Shopware()->Snippets();

        $success = false;
        $messages = [];

        try {
            foreach ($locales as $locale)
            {
                $snippetManager->setLocale($locale);

                $snippetNamespace = $snippetManager->getNamespace(self::SNIPPET_NAMESPACE);
                $snippets = $snippetNamespace->toArray();

                $translations = [];

                foreach($snippets as $path => $value) {
                    $parts = explode('/', $path);
                    while ($path = array_pop($parts)) {
                        if ($path === 'vuetify') {
                            $path = '$vuetify';
                        }

                        $value = [$path => $value];
                    }

                    $translations = array_merge_recursive($translations, $value);
                }

                $shortcode = $this->getLocaleShortCode($locale->getLocale());

                $messages[$shortcode] = $translations;
                $success = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        $messages = array_key_exists($lang, $messages)
            ? $messages[$lang]
            : (
            array_key_exists('en', $messages)
                ? $messages['en']
                : []
            );

        $this->view->assign([
            'success' => $success,
            'messages' => $messages,
            'fallback' => $this->getFallbackLocale(),
            'locale' => $lang
        ]);
    }

    public function getThemesAction()
    {
        $themes = $this->container->getParameterBag()->has('wunde_dcompanion.manager.themes')
            ? $this->container->getParameter('wunde_dcompanion.manager.themes')
            : [];

        $this->view->assign([
            'success' => true,
            'themes' => $themes
        ]);
    }

    public function getTicketHeadersAction()
    {
        $success = false;
        $headers = Shopware()->Container()->getParameter('wunde_dcompanion.manager.ticket_headers');

        try {
            $user = $this->getBackendUser();

            if (!is_null($user)) {
                $attributes = $user->getAttribute();

                if (method_exists($attributes, 'getDcmTicketHeaders')) {
                    $dcmHeaders = json_decode($attributes->getDcmTicketHeaders(), true);

                    if (is_array($dcmHeaders) && count($dcmHeaders) > 0) {
                        $headers = $dcmHeaders;
                        $success = true;
                    } else {
                        $attributes->setDcmTicketHeaders(json_encode($headers));
                        $this->em->flush();
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        foreach ($headers as &$header)
        {
            if (isset($header['hidable'])) {
                if ($header['hidable'] === 'true') $header['hidable'] = true;
                if ($header['hidable'] === 'false') $header['hidable'] = false;
            }

            if (isset($header['show'])) {
                if ($header['show'] === 'true') $header['show'] = true;
                if ($header['show'] === 'false') $header['show'] = false;
            }

            if (isset($header['sortable'])) {
                if ($header['sortable'] === 'true') $header['sortable'] = true;
                if ($header['sortable'] === 'false') $header['sortable'] = false;
            }
        }

        $this->view->assign([
            'success' => $success,
            'headers' => $headers
        ]);
    }

    public function saveTicketHeadersAction()
    {
        $success = false;
        $headers = [];

        try {
            $headers = $this->Request()->getParam('headers');

            $user = $this->getBackendUser();

            if (!is_null($user) && count($headers)) {
                $attribute = $user->getAttribute();

                if (method_exists($attribute, 'setDcmTicketHeaders')) {
                    $attribute->setDcmTicketHeaders(json_encode($headers));

                    $this->em->flush();
                    $success = true;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        $this->view->assign([
            'success' => $success,
            'headers' => $headers
        ]);
    }

    public function getSettingsAction()
    {
        $success = false;
        $privacy = false;

        try {
            $shop = $this->getShop();

            if (!is_null($shop)) {
                $attributes = $shop->getAttribute();

                if (method_exists($attributes, 'getDcmPrivacy')) {
                    $privacy = $attributes->getDcmPrivacy() === 1;

                    $success = true;
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
            $this->view->assign('error', $e->getMessage());
        }

        $this->view->assign([
            'success' => $success,
            'settings' => [
                'privacy' => $privacy
            ]
        ]);
    }

    /**
     * @param DateTimeInterface|null $date
     * @return int|null
     */
    private function toTimestamp(?DateTimeInterface $date): ?int
    {
        return !is_null($date) && $date instanceof DateTime
            ? $date->getTimestamp()
            : null;
    }

    public function getPluginsAction()
    {
        $success = false;

        try {
            $plugins = $this->em->getRepository(Plugin::class)
                ->createQueryBuilder('plugin')
                ->select([
                    'plugin.active',
                    'plugin.added',
                    'plugin.author',
                    'plugin.installed',
                    'plugin.label',
                    'plugin.link',
                    'plugin.name',
                    'plugin.translations',
                    'plugin.version',
                ])
                ->getQuery()
                ->getArrayResult();

            foreach ($plugins as &$plugin)
            {
                if (isset($plugin['translations'])) {
                    $plugin['translations'] = json_decode($plugin['translations'], true);
                }

                if (isset($plugin['changes'])) {
                    $plugin['changes'] = json_decode($plugin['changes'], true);
                }

                if (isset($plugin['added'])) {
                    $plugin['added'] = $this->toTimestamp($plugin['added']);
                }

                if (isset($plugin['installed'])) {
                    $plugin['installed'] = $this->toTimestamp($plugin['installed']);
                }

                if (isset($plugin['updated'])) {
                    $plugin['updated'] = $this->toTimestamp($plugin['updated']);
                }

                if (isset($plugin['refreshed'])) {
                    $plugin['refreshed'] = $this->toTimestamp($plugin['refreshed']);
                }
            }

            $success = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);

            $plugins = [];
        }

        $this->view->assign([
            'success' => $success,
            'plugins' => $plugins,
        ]);
    }

    /**
     * @return \Shopware\Models\Shop\DetachedShop|null
     */
    private function getShop()
    {
        $shop = null;

        try {
            $shop = Shopware()->Shop();
        } catch (Exception $e) {
            /** @var ShopRepository $shopRepository */
            $shopRepository = $this->em->getRepository(Shopware\Models\Shop\Shop::class);

            $shop = $shopRepository->getActiveDefault();
        }

        return $shop;
    }

    public function getShopAction()
    {
        try {
            $shop = $this->getShop();

            if (!is_null($shop)) {
                $this->view->assign([
                    'success' => true,
                    'shop' => [
                        'name' => $shop->getName(),
                        'host' => $shop->getHost(),
                        'email' => Shopware()->Config()->get('mail'),
                        'version' => Shopware()->Config()->get('Version')
                    ]
                ]);
            } else {
                throw new Exception('Shop not found');
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());

            $this->view->assign([
                'success' => false
            ]);
        }
    }

    public function getMenusAction()
    {
        $menus = $this->container->getParameterBag()->has('wunde_dcompanion.manager.menus')
            ? $this->container->getParameter('wunde_dcompanion.manager.menus')
            : [];

        foreach ($menus as &$menuGroups)
        {
            foreach ($menuGroups as &$menuGroup)
            {
                if (isset($menuGroup['hidden'])) {
                    $menuGroup['hidden'] = ($menuGroup['hidden'] === 'true');
                }

                foreach ($menuGroup['items'] as &$item)
                {
                    if (isset($item['disabled'])) {
                        $item['disabled'] = ($item['disabled'] === 'true');
                    }
                }

                $menuGroup['items'] = array_filter($menuGroup['items'], function($item) {
                    return !isset($item['hidden']) || $item['hidden'] === 'false';
                });
            }

            $menuGroups = array_filter($menuGroups, function($menuGroup) {
                return count($menuGroup['items']);
            });
        }

        $this->view->assign([
            'success' => true,
            'menus' => $menus
        ]);
    }

    public function getTicketPrioritiesAction()
    {
        $success = false;
        $priorities = [];

        try {

            $priorityRepository = $this->em->getRepository(Priority::class);

            $priorities = $priorityRepository
                ->createQueryBuilder('priority')
                ->getQuery()
                ->getArrayResult();

            foreach ($priorities as &$priority)
            {
                $priority['snippet'] = 'priority.' . $priority['name'];
            }

            $success = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        $this->view->assign([
            'success' => $success,
            'priorities' => $priorities
        ]);
    }

    public function getTicketStatesAction()
    {
        $success = false;
        $states = [];

        try {
            $stateRepository = $this->em->getRepository(Status::class);

            $states = $stateRepository
                ->createQueryBuilder('state')
                ->getQuery()
                ->getArrayResult();

            foreach ($states as &$state)
            {
                $state['snippet'] = 'state.' . $state['name'];
            }

            $success = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        $this->view->assign([
            'success' => $success,
            'states' => $states
        ]);
    }

    public function getPrivacyAction() {
        $success = false;
        $snippets = [];

        try {
            $path = $this->pluginDirectory . '/Snippets/Privacy';

            $iterator = new DirectoryIterator($path);

            $snippets = [];

            foreach ($iterator as $fileInfo)
            {
                if ($fileInfo->isFile() && $fileInfo->getExtension() === 'html') {
                    $locale = str_replace('.' . $fileInfo->getExtension(), '', $fileInfo->getBasename());
                    $snippets[$locale] = file_get_contents($fileInfo->getPathname());
                }
            }

            if (count($snippets)) {
                $success = true;
            }
        } catch(Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        $this->view->assign([
            'success' => $success,
            'snippets' => $snippets
        ]);
    }

    public function getImprintAction() {
        $success = false;
        $snippets = [];

        try {
            $path = $this->pluginDirectory . '/Snippets/Imprint';

            $iterator = new DirectoryIterator($path);

            $snippets = [];

            foreach ($iterator as $fileInfo)
            {
                if ($fileInfo->isFile() && $fileInfo->getExtension() === 'html') {
                    $locale = str_replace('.' . $fileInfo->getExtension(), '', $fileInfo->getBasename());
                    $snippets[$locale] = file_get_contents($fileInfo->getPathname());
                }
            }

            if (count($snippets)) {
                $success = true;
            }
        } catch(Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        $this->view->assign([
            'success' => $success,
            'snippets' => $snippets
        ]);
    }

    public function sendMailAction() {
        $subject = $this->Request()->getParam('subject');
        $content = $this->Request()->getParam('content');

        $success = false;

        if (!is_null($subject) && !is_null($content)) {
            $context = [
                'subject' => $subject,
                'content' => $content
            ];

            $path = $this->pluginDirectory . '/Email/contact.json';
            $data = file_exists($path)
                ? json_decode(file_get_contents($path), true)
                : [];

            $this->logger->toFile($data);

            try {
                $mail = Shopware()->TemplateMail()->createMail('DC_CONTACT', $context);

                if (isset($data['to'])) $mail->addTo($data['to']);
                if (isset($data['cc'])) {
                    if (is_array($data['cc'])) {
                        foreach ($data['cc'] as $cc)
                        {
                            $mail->addCc($cc);
                        }
                    } else {
                        $mail->addTo($data['cc']);
                    }
                }

                $mail->send();

                $success = true;
            } catch (Exception $e) {
                $this->view->assign('error', $e->getMessage());
            }
        }

        $this->view->assign('success', $success);
    }

    /**
     * @return int[]
     */
    private function getExtensionConfigFormIds()
    {
        $plugiName = Shopware()->Container()->getParameter('wunde_dcompanion.plugin_name');

        /** @var Plugin $core */
        $core = $this->em->getRepository(Plugin::class)->findOneBy([
            'name' => $plugiName
        ]);

        $formIds = array_map(function($wholesaler) {
            /** @var Form $form */
            $form = $wholesaler->getPlugin()->getConfigForms()->first();

            return $form->getId();
        }, $this->wholesalers);

        if (!is_null($core)) {
            /** @var Form $form */
            $form = $core->getConfigForms()->first();

            $formIds[] = $form->getId();
        }

        return $formIds;
    }

    /**
     * @return array
     */
    private function getExtensionConfigForms()
    {
        $formIds = $this->getExtensionConfigFormIds();

        $expr = $this->em->getExpressionBuilder();

        return $this->em->getRepository(Form::class)
            ->createQueryBuilder('form')
            ->select('form')
            ->leftJoin('form.elements', 'elements')
            ->leftJoin('elements.translations', 'translations')
            ->where($expr->in('form.id', ':ids'))
            ->setParameter('ids', $formIds)
            ->addSelect([
                'elements',
                'translations'
            ])
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param string $name
     * @return false|string
     */
    private function getWholesalerShortname(string $name)
    {
        foreach ($this->wholesalers as $wholesaler)
        {
            if (strtolower($wholesaler->getName()) === strtolower($name)) {
                return $wholesaler->getShortname();
            }
        }

        return false;
    }

    private function getExtensionConfig()
    {
        $forms = $this->getExtensionConfigForms();

        foreach ($forms as &$form)
        {
            $formName = strtolower($form['name']);

            $fieldSets = $formName === 'wundedcompanion'
                ? Shopware()->Container()->getParameter('wunde_dcompanion.config.core.groups')
                : Shopware()->Container()->getParameter('wunde_dcompanion.config.extension.groups');

            $elements = [];

            $shortname = $formName !== 'wundedcompanion'
                ? $this->getWholesalerShortname($form['name'])
                : '';

            foreach ($form['elements'] as $element)
            {
                $elements[$element['name']] = $element;
            }

            unset($form['elements']);

            $form['groups'] = [];

            $warehouses = $this->pickwareService->getWarehouses(true);

            foreach ($fieldSets as $fieldSetTitle => $fieldSet)
            {
                $containers = [];

                foreach ($fieldSet as $fieldPosition => $items)
                {
                    $configElements = [];

                    foreach ($items as $item)
                    {

                        if ($formName === 'wundedcompanion' && isset($elements['dc_' . $item])) {
                            $configElements[] = $elements['dc_' . $item];
                        } else if ($formName !== 'wundedcompanion' && isset($elements['dc_' . $shortname . '_' . $item])) {

                            $configElement = $elements['dc_' . $shortname . '_' . $item];

                            if ($item === 'pickware_warehousename') {
                                $configElement['options'] = [
                                    'displayField' => 'displayName',
                                    'editable' => false,
                                    'valueField' => 'id',
                                    'store' => $warehouses
                                ];
                            }

                            $configElements[] = $configElement;
                        }
                    }

                    if (count($configElements)) {
                        $containers[] = [
                            'position' => $fieldPosition,
                            'elements' => $configElements
                        ];
                    }
                }

                if (count($containers)) {
                    $title = $this->snippets->get('config/fieldset/' . $fieldSetTitle . '/title', '');

                    $form['groups'][] = [
                        'name' => $title,
                        'containers' => $containers
                    ];
                }
            }
        }

        foreach ($forms as &$form)
        {
            if (array_key_exists('description', $form)) unset($form['description']);
            if (array_key_exists('parentId', $form)) unset($form['parentId']);
            if (array_key_exists('pluginId', $form)) unset($form['pluginId']);
        }

        return $forms;
    }

    private function getExtensions()
    {
        return array_values(array_map(function ($identifier) {
            $plugin = $this->em->getRepository(Plugin::class)
                ->createQueryBuilder('plugin')
                ->select('plugin')
                ->leftJoin('plugin.configForms', 'config_forms')
                ->leftJoin('config_forms.elements', 'elements')
                ->leftJoin('elements.translations', 'translations')
                ->leftJoin('translations.locale', 'locale')
                ->where('plugin.name = :name')
                ->setParameter('name', Shopware()->Container()->getParameterBag()->get($identifier . '.name'))
                ->addSelect([
                    'config_forms',
                    'elements',
                    'translations',
                    'locale'
                ])
                ->getQuery()
                ->getArrayResult()[0];

            if (isset($plugin['configForms'][0])) {
                $plugin['configForms'] = $plugin['configForms'][0];
            }

            if (isset($plugin['translations'][0])) {
                $plugin['translations'] = json_decode($plugin['translations'], true);
            }

            $plugin['fieldSet'] = $plugin['name'] === 'WundeDcompanion'
                ? Shopware()->Container()->getParameter('wunde_dcompanion.config.core.groups')
                : Shopware()->Container()->getParameter('wunde_dcompanion.config.extension.groups');

            $wholesalers = array_values(array_filter($this->wholesalers, function ($wholesaler) use($plugin) {
                return $wholesaler->getName() === $plugin['name'];
            }));

            if (array_key_exists(0, $wholesalers)) {
                $plugin['wholesaler'] = $wholesalers[0]->getShortname();
            }

            foreach ($plugin['configForms']['elements'] as &$element)
            {
                if (array_key_exists('options', $element)) unset($element['options']);
            }

            return json_decode(json_encode($plugin), true);
        }, array_filter(Shopware()->Container()->getParameterBag()->all(), function ($parameter, $key) {
            return
                is_string($parameter) && substr($key, 0, 26 ) === 'wunde_dcompanion.extension' &&
                Shopware()->Container()->getParameterBag()->has($parameter . '.name');
        }, ARRAY_FILTER_USE_BOTH)));
    }

    /**
     *
     */
    public function getConfigAction()
    {
        $this->view->assign([
            'success' => true,
            'config' => [
                'plugins' => $this->getExtensions()
            ]
        ]);
    }

    public function saveConfigElementsAction()
    {
        $success = false;

        $dataSet = $this->Request()->getParam('elements', []);
        $expr = $this->em->getExpressionBuilder();

        try {
            /** @var Element[] $elements */
            $elements = $this->em->getRepository(Element::class)
                ->createQueryBuilder('config')
                ->where($expr->in('config.id', ':ids'))
                ->indexBy('config', 'config.id')
                ->setParameter('ids', array_column($dataSet, 'id'))
                ->getQuery()
                ->getResult();

            foreach ($dataSet as $data)
            {
                if (array_key_exists($data['id'], $elements)) {
                    $element = $elements[$data['id']];

                    $element->setValue($data['value']);

                    /** @var \Shopware\Models\Config\Value[] $values */
                    $values = $element->getValues()->getValues();

                    foreach ($values as $value)
                    {
                        $value->setValue($data['value']);
                    }
                }
            }

            $this->em->flush();
            $success = true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        $this->view->assign('success', $success);
    }

    /**
     * @return string[]
     */
    public function getWhitelistedCSRFActions(): array
    {
        return [
            'index',
            'getSnippets',
            'getThemes',
            'getSettings',
            'getBackendUser',
            'getShop',
            'getPlugins',
            'getMenus',
            'getTicketPriorities',
            'getTicketStates',
            'getTicketHeaders',
            'saveTicketHeaders',
            'getPrivacy',
            'getImprint',
            'getConfig',
            'sendMail',
            'saveConfigElements'
        ];
    }
}