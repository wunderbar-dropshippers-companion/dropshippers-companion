<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 14:48
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Dependencies;


use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;

/**
 * Class Dependency
 * @package WundeDcompanion\Dependencies
 */
abstract class Dependency
{
    /**
     * @return DatabaseDependency
     */
    public static function database()
    {
        return new DatabaseDependency();
    }

    /**
     * @return EmailDependency
     */
    public static function email()
    {
        return new EmailDependency();
    }
}