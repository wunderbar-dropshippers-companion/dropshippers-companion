<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 19/02/2021 11:28
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Validators;


use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\EqualTo;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validation;

class OrderValidator
{
    const PAYMENT_INCOMPLETE = 'Payment incomplete';

    /**
     * @param Order $order
     * @return ValidationResult
     */
    public function validate(Order $order): ValidationResult
    {
        $data = [
            'payment' => $order->getPaymentStatus()->getId(),
            'status' => $order->getOrderStatus()->getId()
        ];

        $validator = Validation::createValidator();

        $constraint = new Collection([
            'payment' => new EqualTo([
                'value' => Status::PAYMENT_STATE_COMPLETELY_PAID,
                'message' => 'Payment inclomplete'
            ]),
            'status' => [
                new Choice([
                    'message' => 'Order is not open',
                    'choices' => [
                        Status::ORDER_STATE_OPEN,
                        Status::ORDER_STATE_IN_PROCESS,
                        Status::ORDER_STATE_PARTIALLY_COMPLETED,
                        Status::ORDER_STATE_PARTIALLY_DELIVERED
                    ]
                ]),
                new NotEqualTo([
                    'message' => 'Order completely delivered',
                    'value' => Status::ORDER_STATE_COMPLETELY_DELIVERED
                ]),
                new NotEqualTo([
                    'message' => 'Order completed',
                    'value' => Status::ORDER_STATE_COMPLETED
                ])
            ]
        ]);

        $violations = $validator->validate($data, $constraint);

        return new ValidationResult($violations);
    }
}