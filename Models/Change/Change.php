<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 08/09/2020 14:12
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Change;

use DateTimeInterface;
use Exception;
use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_changes")
 * @ORM\HasLifecycleCallbacks()
 */
class Change extends ModelEntity
{
    /**
     * Primary Key - autoincrement value
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $field
     * @ORM\Column(nullable=false)
     */
    private $field;

    /**
     * @var mixed $oldValue
     * @ORM\Column(type="text", nullable=true)
     */
    private $oldValue;

    /**
     * @var mixed $newValue
     * @ORM\Column(type="text", nullable=true)
     */
    private $newValue;

    /**
     * @var string $createdAt
     * @ORM\Column(nullable=false)
     */
    private $createdAt;

    /**
     * Change constructor.
     * @param string $field
     * @param mixed $oldValue
     * @param mixed $newValue
     */
    public function __construct($field, $oldValue, $newValue)
    {
        $this->setField($field);
        $this->setOldValue($oldValue);
        $this->setNewValue($newValue);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return Change
     */
    public function setField($field)
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return unserialize($this->oldValue);
    }

    /**
     * @param mixed $oldValue
     * @return Change
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = serialize($oldValue);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return unserialize($this->newValue);
    }

    /**
     * @param mixed $newValue
     * @return Change
     */
    public function setNewValue($newValue)
    {
        $this->newValue = serialize($newValue);
        return $this;
    }

    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    public function getCreatedAt()
    {
        return new DateTime($this->createdAt);
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = (new DateTime())->format('d.m.Y H:i:s');
    }
}