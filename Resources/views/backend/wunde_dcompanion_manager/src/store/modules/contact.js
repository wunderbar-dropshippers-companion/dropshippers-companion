export default {
  namespaced: true,
  actions: {
    send({commit, rootGetters}, data) {
      return axios.post(rootGetters.getUrl('sendMail'), data)
        .then(response => {
          console.log(response)
        })
        .catch(() => {
          console.warn('email could not be sent')
        })
    },
  }
}
