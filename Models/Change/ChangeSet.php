<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 08/09/2020 14:12
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Change;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use Shopware\Components\Model\ModelEntity;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_changesets")
 * @ORM\HasLifecycleCallbacks()
 */
class ChangeSet extends ModelEntity
{
    /**
     * Primary Key - autoincrement value
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $tablename
     * @ORM\Column(nullable=false)
     */
    private $tablename;

    /**
     * @var int $recordId
     * @ORM\Column(type="integer", nullable=false)
     */
    private $recordId;

    /**
     * @var string $createdAt
     * @ORM\Column(nullable=false)
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="WundeDcompanion\Models\Change\Change",cascade={"persist","remove"})
     * @ORM\JoinTable(name="s_dcompanion_changeset_changes",
     *      joinColumns={
     *          @ORM\JoinColumn(
     *              name="changeset_id",
     *              referencedColumnName="id",
     *              onDelete="CASCADE"
     *          )
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(
     *              name="change_id",
     *              referencedColumnName="id",
     *              onDelete="CASCADE"
     *          )
     *      }
     * )
     */
    protected $changes;

    /**
     * ChangeSet constructor.
     * @param string $tablename
     * @param integer $recordId
     * @param array $changes
     */
    public function __construct($tablename, $recordId, array $changes)
    {
        $this->setRecordId($recordId);
        $this->setTablename($tablename);
        $this->setChanges(new ArrayCollection($changes));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRecordId()
    {
        return $this->recordId;
    }

    /**
     * @param int $recordId
     * @return ChangeSet
     */
    public function setRecordId($recordId)
    {
        $this->recordId = $recordId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * @param string $tablename
     * @return ChangeSet
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;
        return $this;
    }

    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    public function getCreatedAt()
    {
        return new DateTime($this->createdAt);
    }

    /**
     * @return ArrayCollection
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * @param ArrayCollection $changes
     * @return ChangeSet
     */
    public function setChanges($changes)
    {
        $this->changes = $changes;
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = (new DateTime())->format('d.m.Y H:i:s');
    }
}