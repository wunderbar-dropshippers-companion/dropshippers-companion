module.exports = {
  outputDir: "Resources/views/backend/_resources",
  filenameHashing: false,
  pages: {
    index: {
      entry: "Resources/views/backend/wunde_dcompanion_manager/src/main.js"
    }
  },
  chainWebpack: config => {
    config.optimization.delete('splitChunks')
    config.plugins.delete('html')
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')
  },
  css: {
    extract: {
      filename: 'css/dc-manager.css',
    },
  },
  configureWebpack: {
    output: {
      filename: 'js/dc-manager.js',
    }
  }
}
