<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 02/12/2020 13:01
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use Mpdf\Mpdf;
use Mpdf\MpdfException;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\DBALConfigReader;
use Shopware\Models\Mail\Mail;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Repository as OrderRepository;
use Shopware\Models\Plugin\Plugin;
use Shopware\Models\Shop\Shop;
use WundeDcompanion\Models\Core\Config;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use Zend_Mime_Part;

/**
 * Class MailService
 * @package WundeDcompanion\Services
 */
class MailService
{
    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * MailService constructor.
     * @param Logger|null $logger
     */
    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger ?: Shopware()->Container()->get('pluginlogger');
    }

    /**
     * @return DateTimeInterface
     */
    private function getDate()
    {
        try {
            return new DateTime('now', new DateTimeZone('Europe/Berlin'));
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return null;
    }

    /**
     * @param Exception $exception
     * @return array
     */
    private function getExceptionData($exception)
    {
        if ($exception instanceof Exception) {
            $trace = $exception->getTrace();

            array_walk_recursive($trace, function (&$element) {
                if (is_object($element) && get_class($element) === 'Closure') {
                    $element = 'Closure';
                }
            });

            return [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile(),
                'trace' => $trace
            ];
        }

        return [];
    }

    private function getServerName()
    {
        return isset($_SERVER['SERVER_NAME']) && !is_null($_SERVER['SERVER_NAME']) && !empty($_SERVER['SERVER_NAME'])
            ? $_SERVER['SERVER_NAME']
            : '#';
    }

    private function getShop()
    {
        /** @var Shop $shop */
        $shop = Shopware()->Models()->getRepository(Shop::class)->findOneBy([
            'host' => $this->getServerName()
        ]);

        if (is_null($shop)) {
            $shop = Shopware()->Models()->getRepository(Shop::class)->getActiveDefault();
        }

        return $shop;
    }

    private function getLanguage()
    {
        $shop = $this->getShop();

        return !is_null($shop) ? $shop->getLocale()->getLanguage() : '';
    }

    private function getShopName()
    {
        $shop = $this->getShop();

        return !is_null($shop) ? $shop->getName() : '';
    }

    private function getDomain()
    {
        $shop = $this->getShop();
        $serverName = $this->getServerName();

        return !is_null($shop)
            ? ($shop->getSecure()
                ? 'https://' . $shop->getHost()
                : 'http://' . $shop->getHost())
            : $serverName;
    }

    /**
     * @param Exception $exception
     */
    public function sendErrorMail($exception = null)
    {
        $language = $this->getLanguage();
        $shopName = $this->getShopName();
        $domain = $this->getDomain();

        $plugins = Shopware()->Models()->getRepository(Plugin::class)
            ->createQueryBuilder('plugin')
            ->select([
                'plugin.name',
                'plugin.version',
                'plugin.label',
                'plugin.installed',
                'plugin.active',
                'plugin.author',
                'plugin.link',
            ])
            ->orderBy('plugin.name')
            ->getQuery()
            ->getArrayResult();

        $trace = !is_null($exception) && $exception instanceof Exception
            ? $exception->getTraceAsString()
            : [];

        $context = [
            'exception' => $trace,
            'domain' => $domain,
            'shop' => $shopName,
            'email' => Shopware()->Config()->get('mail'),
            'version' => Shopware()->Config()->get('Version'),
            'language' => $language,
            'date' => $this->getDate(),
            'plugins' => $plugins
        ];

        try {
            $receipients = Shopware()->Container()->getParameter('wunde_dcompanion.mail.exception');

            $mail = Shopware()->TemplateMail()->createMail('DC_ERROR', $context);
            $mail->addTo($receipients['to']);
            $mail->addCc($receipients['cc']);
            $mail->addBcc($receipients['bcc']);
            $mail->setReplyTo(Shopware()->Config()->get('mail'));
            $mail->send();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Order $order
     */
    public function sendOrderMail($order)
    {
        $context = [
            'orderNumber' => $order->getNumber(),
            'dcAutoOrder' => Shopware()->Config()->get('dc_auto_order'),
            'paymentName' => $order->getPayment()->getName()
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail('DC_ORDER', $context);
            $mail->addTo($receiver);
            $mail->send();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Mail $mail
     */
    public function sendInstallMail(Mail $mail)
    {
        $date = new DateTime('now');

        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');

        try {
            $shop = Shopware()->Shop();
        } catch (Exception $e) {
            /** @var Shop $shop */
            $shop = $em->getRepository(Shop::class)->findOneBy(['host' => $_SERVER['SERVER_NAME']]);
        }

        $language = !is_null($shop) ? $shop->getLocale()->getLanguage() : 'unknown';

        $context = [
            'date' => $date,
            'domain' => $this->getDomain(),
            'language' => $language
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail($mail, $context);

            if (!is_null($mail)) {
                $mail->addTo($receiver);
                $mail->send();
            } else {
                throw new Exception('Mail template not found!');
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Mail $mail
     */
    public function sendUninstallMail(Mail $mail)
    {
        $date = new DateTime('now');

        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');

        try {
            $shop = Shopware()->Shop();
        } catch (Exception $e) {
            /** @var Shop $shop */
            $shop = $em->getRepository(Shop::class)->findOneBy(['host' => $_SERVER['SERVER_NAME']]);
        }

        $language = !is_null($shop) ? $shop->getLocale()->getLanguage() : 'unknown';

        $context = [
            'date' => $date,
            'domain' => $this->getDomain(),
            'language' => $language
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail($mail, $context);

            if (!is_null($mail)) {
                $mail->addTo($receiver);
                $mail->send();
            } else {
                throw new Exception('Mail template not found!');
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Order $order
     */
    public function sendTrackingMail(Order $order)
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');

        $date = $order->getOrderTime();

        $orders = $em->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->select('o')
            ->leftJoin('o.payment', 'payment')
            ->leftJoin('o.attribute', 'attribute')
            ->leftJoin('o.details', 'details')
            ->leftJoin('o.shipping', 'shipping')
            ->leftJoin('shipping.customer', 'customer')
            ->leftJoin('shipping.country', 'country')
            ->leftJoin('details.attribute', 'detailAttributes')
            ->where('o.id = :orderId')
            ->setParameter('orderId', $order->getId())
            ->addSelect([
                'payment',
                'attribute',
                'details',
                'detailAttributes',
                'shipping',
                'customer',
                'country'
            ])
            ->getQuery()
            ->getArrayResult();

        $order = count($orders) ? $orders[0] : [];

        foreach ($order['details'] as &$position)
        {
            $carriers = json_decode($position['attribute']['dcDropshipTrackingCarrier'], true);
            $codes = json_decode($position['attribute']['dcDropshipTrackingCode'], true);

            $position['attribute']['dcDropshipTrackingCarrier'] = $carriers;
            $position['attribute']['dcDropshipTrackingCode'] = $codes;
        }

        $context = [
            'order' => $order,
            'orderDate' => $date,
            'domain' => $this->getDomain()
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail('DC_TRACKING', $context);
            $mail->addTo($receiver);
            $mail->send();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Order $order
     */
    public function sendTrackingMailToCustomer(Order $order)
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');

        $date = $order->getOrderTime();
        $trackingCodes = json_decode($order->getTrackingCode(), true);

        $orders = Shopware()->Models()->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->select('o')
            ->leftJoin('o.payment', 'payment')
            ->leftJoin('o.attribute', 'attribute')
            ->leftJoin('o.details', 'details')
            ->leftJoin('o.shipping', 'shipping')
            ->leftJoin('shipping.customer', 'customer')
            ->leftJoin('shipping.country', 'country')
            ->leftJoin('details.attribute', 'detailAttributes')
            ->where('o.id = :orderId')
            ->setParameter('orderId', $order->getId())
            ->addSelect([
                'payment',
                'attribute',
                'details',
                'detailAttributes',
                'shipping',
                'customer',
                'country'
            ])
            ->getQuery()
            ->getArrayResult();

        $order = count($orders) ? $orders[0] : [];

        $context = [
            'order' => $order,
            'orderDate' => $date,
            'trackingCodes' => $trackingCodes
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail('DC_TRACKING_CUSTOMER', $context);
            $mail->addTo($receiver);
            $mail->send();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }

    /**
     * @param Order $order
     */
    public function sendDropshipMail($order)
    {
        /** @var ModelManager $em */
        $em = Shopware()->Container()->get('models');

        /** @var OrderRepository $orderRepository */
        $orderRepository = $em->getRepository(Order::class);

        /** @var WholesalerRepository $wholesalerRepository */
        $wholesalerRepository = Shopware()->Container()->get('wunde_dcompanion.repositories.wholesalers');

        $wholesalers = $wholesalerRepository->findAllActive();

        $orderWholesalers = [];

        /** @var Detail $position */
        foreach ($order->getDetails()->getValues() as $position)
        {
            $shortname = $position->getAttribute()->getDcNameShort();

            if (
                !is_null($shortname) &&
                array_key_exists($shortname, $wholesalers) &&
                !array_key_exists($shortname, $orderWholesalers)
            ) {
                $wholesalers[$shortname] = $wholesalers[$shortname]->getLongname();
            }
        }

        $orders = Shopware()->Models()->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->select('o')
            ->leftJoin('o.payment', 'payment')
            ->leftJoin('o.attribute', 'attribute')
            ->leftJoin('o.details', 'details')
            ->leftJoin('o.shipping', 'shipping')
            ->leftJoin('shipping.customer', 'customer')
            ->leftJoin('shipping.country', 'country')
            ->leftJoin('details.attribute', 'detailAttributes')
            ->where('o.id = :orderId')
            ->setParameter('orderId', $order->getId())
            ->addSelect([
                'payment',
                'attribute',
                'details',
                'detailAttributes',
                'shipping',
                'customer',
                'country'
            ])
            ->getQuery()
            ->getArrayResult();

        $order = count($orders) ? $orders[0] : [];

        $context = [
            'order' => $order,
            'wholesalers' => $orderWholesalers,
            'domain' => $this->getDomain()
        ];

        $receiver = Shopware()->Config()->get('mail');

        try {
            $mail = Shopware()->TemplateMail()->createMail('DC_DROPSHIP', $context);
            $mail->addTo($receiver);
            $mail->send();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }
    }
}