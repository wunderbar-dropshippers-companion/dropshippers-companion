<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 24/09/2020 00:50
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use Exception;
use Shopware\Components\ContainerAwareEventManager;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Detail as ArticleDetail;
use Shopware\Models\Dispatch\ShippingCost;
use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\DetailStatus;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use Shopware\Models\Tax\Tax;
use WundeDcompanion\Interfaces\IJsonResponse;
use WundeDcompanion\Models\Core\Config as CoreConfig;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use Shopware\Models\Attribute\OrderDetail as PositionAttribute;

/**
 * Class OrderService
 * @package WundeDcompanion\Services
 */
class OrderService
{
    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var CoreConfig $config
     */
    private $config;

    /**
     * @var Order[] $orders
     */
    private $orders = [];

    /**
     * @var ContainerAwareEventManager $events
     */
    private $events;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers = [];

    /**
     * @var IJsonResponse[] $quantities
     */
    private $quantities = [];

    /**
     * @var array $messages
     */
    private $messages = [];

    /**
     * @var MailService $mailService
     */
    private $mailService;

    /**
     * OrderService constructor.
     * @param ModelManager $em
     * @param ContainerAwareEventManager $events
     * @param LoggerService $logger
     * @param CoreConfig $config
     * @param WholesalerRepository $wholesalerRepository
     * @param MailService $mailService
     */
    public function __construct($em, $events, $logger, $config, $wholesalerRepository, $mailService)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->events = $events;
        $this->mailService = $mailService;

        $this->config = $config;

        $this->orders = $this->em->getRepository(Order::class)->findAll();

        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * @param Order $order
     * @param bool $sort
     */
    public function sendOrder($order, $sort = true)
    {
        $isComplete = $order->getOrderStatus()->getId() === Status::ORDER_STATE_COMPLETED;
        $paymentComplete = $order->getPaymentStatus()->getId() === Status::PAYMENT_STATE_COMPLETELY_PAID;
        $maximumTransmissionAttemptsReached = $order->getAttribute()->getDcDropshipTransmissionAttempt() >= 3;

        if (!$isComplete && $paymentComplete && !$maximumTransmissionAttemptsReached) {
            if ($this->config->autoOrder() && $sort) {
                $this->send($this->getSortedOrder($order));
            } else {
                $this->send([
                    'order' => $order,
                    'success' => true,
                    'messages' => []
                ]);
            }
        }
    }

    /**
     * @param bool $sort
     */
    public function sendOrders($sort = true)
    {
        foreach ($this->orders as $order)
        {
            $this->sendOrder($order, $sort);
        }
    }

    /**
     * @param Order $order
     */
    private function updateOrderStatus($order)
    {
        /** @var Position[] $positions */
        $positions = $order->getDetails()->getValues();

        $attributes = $this->em->getConnection()->createQueryBuilder()
            ->select('attribute.*')
            ->from('s_order_details', 'detail')
            ->join('detail', 's_order_details_attributes', 'attribute', 'attribute.detailID = detail.id')
            ->where('detail.orderID = :orderId')
            ->setParameter('orderId', $order->getId())
            ->execute()
            ->fetchAll();

        $count = count(array_filter($attributes, function($attribute) {
            return
                !is_null($attribute['dc_dropship_status']) &&
                !is_null($attribute['dc_dropship_orders_id']) &&
                !is_null($attribute['dc_dropship_date']) &&
                $attribute['dc_dropship_status'] === 'success';
        }));

        try {
            $orderStatusRepository = $this->em->getRepository(Status::class);

            if (!$count) {
                // No position could be ordered successfully
                $order->setOrderStatus($orderStatusRepository->find(Status::ORDER_STATE_OPEN));
            } elseif ($count === count($positions)) {
                // All positions could be ordered successfully
                $order->setOrderStatus($orderStatusRepository->find(Status::ORDER_STATE_COMPLETED));
            } else {
                // The items could only partially be ordered successfully
                $order->setOrderStatus($orderStatusRepository->find(Status::ORDER_STATE_PARTIALLY_COMPLETED));
            }

            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e, [], 'order');
        }
    }

    /**
     * @param Order $order
     */
    private function increaseTransmissionAttempt($order)
    {
        $attribute = $order->getAttribute();

        $transmissionAttempt = $attribute->getDcDropshipTransmissionAttempt();

        if (is_null($transmissionAttempt)) {
            $transmissionAttempt = 0;
        }

        $transmissionAttempt++;

        $attribute->setDcDropshipTransmissionAttempt($transmissionAttempt);

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e, [], 'order');
        }
    }

    /**
     * @param array $data
     */
    private function send($data)
    {
        if ($data['success']) {
            try {
                /** @var Order $order */
                $order = $this->em->getRepository(Order::class)->find($data['order']->getId());

                $results = [];

                $wholesalers = [];

                /** @var Position $position */
                foreach ($order->getDetails()->getValues() as $position)
                {
                    $shortname = $position->getAttribute()->getDcNameShort();

                    if (!in_array($shortname, $wholesalers)) {
                        $wholesalers[] =  $shortname;
                    }
                }

                foreach ($this->wholesalers as $wholesaler)
                {
                    $client = $wholesaler->getClient();
                    $shipper = $wholesaler->getConfig()->shipper();

                    if (!in_array($wholesaler->getShortname(), $wholesalers)) continue;

                    $result = false;

                    try {

                        if (method_exists($client, 'sendOrder')) {
                            $result = $client->sendOrder($order, $shipper);

                            if ($result) {
                                $this->mailService->sendOrderMail($order);
                            }
                        } else {
                            throw new Exception('Method sendOrder does not exist in the '. $wholesaler->getBasename() .' client');
                        }
                    } catch (Exception $e) {
                        $this->logger->error($e->getMessage(), $e, [], 'order');
                    }

                    $results[$wholesaler->getShortname()] = $result;
                }

                if (array_sum($results) === count($wholesalers)) {
                    /** @var Status $completedState */
                    $completedState = $this->em->getRepository(Status::class)->find(Status::ORDER_STATE_COMPLETED);

                    if (!is_null($completedState)) {
                        Shopware()->Models()->getConnection()->createQueryBuilder()
                            ->update('s_order')
                            ->set('status', ':status')
                            ->where('id = :id')
                            ->setParameter('status', Status::ORDER_STATE_COMPLETED)
                            ->setParameter('id', $order->getId())
                            ->execute();
                    }
                } else {
                    $this->increaseTransmissionAttempt($order);
                }

                $this->updateOrderStatus($order);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e, [], 'order');
            }
        } else {
            /** @var Order $order */
            $order = $data['order'];

            if (array_key_exists($order->getNumber(), $data['messages'])) {
                /** @var Position $position */
                foreach ($order->getDetails()->getValues() as $position)
                {
                    $ordernumber = $position->getArticleNumber();
                    $attributes = $position->getAttribute();

                    if (
                        array_key_exists($ordernumber, $data['messages'][$order->getNumber()]) &&
                        method_exists($attributes,'setDcDropshipStatusMessage')
                    ) {
                        $errors = json_encode($data['messages'][$order->getNumber()][$ordernumber]);

                        Shopware()->Models()->getConnection()->createQueryBuilder()
                            ->update('s_order_details_attributes')
                            ->set('dc_dropship_status_message', ':dropshipStatusMessage')
                            ->where('id = :id')
                            ->setParameter('dropshipStatusMessage', $errors)
                            ->setParameter('id', $position->getId())
                            ->execute();

                        $this->logger->error(
                            'Send order failed',
                            null,
                            $data['messages'][$order->getNumber()][$ordernumber],
                            'order'
                        );
                    }
                }
            }
        }
    }

    /**
     * @param Position[] $positions
     * @param Order $order
     * @return array
     */
    private function getSimilaries($positions, $order)
    {
        $similarieSets = [];

        foreach ($positions as $position)
        {
            $similarieSets = array_merge($similarieSets, $this->getPositionSimilaries($position, $order));
        }

        return $similarieSets;
    }

    /**
     * @param Position[] $positions
     * @param array $similaries
     * @param Order $order
     * @return bool
     */
    private function checkAvailabilities($positions, $similaries, $order)
    {
        $availabilities = array_map(function ($position) use($similaries, $order) {
            $similaries = array_filter($similaries, function($similary) use($position) {
                return $similary['positionId'] === $position->getId();
            });

            $totalStocks = array_sum(array_column($similaries, 'instock'));

            $isAvailable = $totalStocks >= $position->getQuantity();

            if (!$isAvailable) {
                if (!array_key_exists($order->getNumber(), $this->messages)) {
                    $this->messages[$order->getNumber()] = [];
                }

                if (!array_key_exists($position->getArticleNumber(), $this->messages[$order->getNumber()])) {
                    $this->messages[$order->getNumber()][$position->getArticleNumber()] = [];
                }

                $this->messages[$order->getNumber()][$position->getArticleNumber()][] = [
                    'error' => 'not_availble',
                    'ordered' => $position->getQuantity(),
                    'instock' => $totalStocks < 0 ? 0 : $totalStocks
                ];
            }

            return $isAvailable;
        }, $positions);

        return array_sum($availabilities) === count($positions);
    }

    /**
     * @param Order $order
     * @return array
     */
    private function getSortedOrder($order)
    {
        $success = false;
        $positions = $this->getPositions($order);

        $positionIds = $this->getPositionIds($positions);
        $positionQuantities = $this->getPositionQuantities($positions);
        $similaries = $this->getSimilaries($positions, $order);

        $allAvailable = $this->checkAvailabilities($positions, $similaries, $order);

        if ($allAvailable) {
            $combinations = $this->getCombinations($similaries);

            $this->filterCombinationsByExpectedStocks($combinations, $positionQuantities);
            $this->filterCombinationsByPrice($combinations);
            $this->filterCombinationsByRequiredPositions($combinations, $positionIds);
            $optimalCombination = $this->findOptimalCombination($combinations);

            if (!empty($optimalCombination)) {
                /** @var Position[] $originalPositions */
                $originalPositions = $order->getDetails()->getValues();

                $masters = [];

                foreach ($originalPositions as $originalPosition)
                {
                    $masters[$originalPosition->getArticleDetail()->getId()] = $originalPosition;
                }

                foreach ($optimalCombination as $similary)
                {
                    $success = $this->createNewPosition($masters, $similary);
                }

                /** @var Order $order */
                $order = $this->em->getRepository(Order::class)->find($order->getId());

                if (!is_null($order)) {
                    /** @var Position[] $positions */
                    $positions = $order->getDetails()->getValues();

                    $dropshipActive = count(array_filter($positions, function ($position) {
                        return
                            !is_null($position->getAttribute()->getDcNameShort()) &&
                            !empty($position->getAttribute()->getDcNameShort());
                    }));

                    if ($dropshipActive) {
                        $order->getAttribute()->setDcDropshipActive($dropshipActive);

                        try {
                            $this->em->flush();
                        } catch (Exception $e) {
                            $this->logger->error($e->getMessage(), $e, [],'order');
                        }
                    }
                }
            }
        }

        return [
            'order' => $order,
            'success' => $success,
            'messages' => $this->messages
        ];
    }


    /**
     * @param Position[] $masters
     * @param array $similary
     * @return bool
     */
    private function createNewPosition($masters, $similary)
    {
        try {
            $master = $masters[$similary['articleDetailId']];

            $shortname = $similary['warehouse'] !== 'shop'
                ? $similary['warehouse']
                : '';

            $this->em->clear();

            $tax = $this->em->find(Tax::class, $master->getTax()->getId());
            $order = $this->em->find(Order::class, $master->getOrder()->getId());
            $articleDetail = $this->em->find(ArticleDetail::class, $master->getArticleDetail()->getId());

            $attribute = new PositionAttribute();
            $attribute->setDcNameShort($shortname);

            $position = new Position();
            $position->setOrder($order);
            $position->setNumber($master->getNumber());
            $position->setArticleId($master->getArticleId());
            $position->setArticleNumber($master->getArticleNumber());
            $position->setArticleName($master->getArticleName());
            $position->setArticleDetail($articleDetail);
            $position->setPrice($master->getPrice());
            $position->setQuantity($similary['quantity']);
            $position->setStatus($this->em->find(DetailStatus::class, 0));
            $position->setShipped($master->getShipped());
            $position->setShippedGroup($master->getShippedGroup());
            $position->setReleaseDate($master->getReleaseDate());
            $position->setMode($master->getMode());
            $position->setEsdArticle($master->getEsdArticle());
            $position->setEan($master->getEan());
            $position->setTax($tax);
            $position->setTaxRate($tax->getTax());
            $position->setConfig($master->getConfig());
            $position->setUnit($master->getUnit());
            $position->setPackUnit($master->getPackUnit());
            $position->setAttribute($attribute);

            $this->em->persist($position);
            $this->em->flush();
            $this->em->clear();

            /** @var Position $deletable */
            $deletable = $this->em->find(Position::class, $master->getId());

            if (!is_null($deletable) && $deletable instanceof Position) {
                $this->em->remove($deletable);
                $this->em->flush();
                $this->em->clear();
            }

            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e, [], 'order');
        }

        return false;
    }

    /**
     * @param Order $order
     * @return Position[]
     */
    private function getPositions($order)
    {
        /** @var Position[] $positions */
        $positions = [];

        /** @var Position $position */
        foreach ($order->getDetails()->getValues() as $position)
        {
            if (!is_null($position->getArticleDetail())) {
                if ($position->getStatus()->getId() !== Status::ORDER_STATE_COMPLETED) {
                    if (!array_key_exists($position->getArticleNumber(), $positions)) {
                        $positions[$position->getArticleNumber()] = $position;
                    } else {
                        $quantity = $positions[$position->getArticleNumber()]->getQuantity() + $position->getQuantity();
                        $positions[$position->getArticleNumber()]->setQuantity($quantity);

                        try {
                            $entity = $this->em->merge($position);
                            $this->em->remove($entity);
                            $this->em->flush();
                            $this->em->clear();
                        } catch (Exception $e) {
                            $this->logger->error($e->getMessage(), $e, [], 'order');
                        }
                    }
                }
            } else {
                $position->getAttribute()->setDcDropshipStatusMessage('Artikel wird nicht mehr geführt!');

                try {
                    $this->em->flush();
                    $this->em->clear();
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), $e, [], 'order');
                }
            }
        }

        return $positions;
    }

    /**
     * @param Position[] $positions
     * @return array
     */
    private function getPositionQuantities($positions)
    {
        $positionQuantities = [];

        foreach ($positions as $position)
        {
            $positionQuantities[$position->getId()] = [
                'expected' => $position->getQuantity(),
                'total_stock' => 0
            ];
        }

        return $positionQuantities;
    }

    /**
     * @param Position[] $positions
     * @return array
     */
    private function getPositionIds($positions)
    {
        $positionIds = [];

        foreach ($positions as $position)
        {
            $positionIds[$position->getId()] = 0;
        }

        return $positionIds;
    }

    /**
     * @param $combinations
     * @return array
     */
    private function findOptimalCombination($combinations)
    {
        $combinationPrices = [];

        foreach ($combinations as $key => &$combination)
        {
            $totalPrice = 0;
            $expectedQuantities = [];
            $shippingCosts = [];
            $wholesalerPrice = [];
            $excemptionLimits = [];

            foreach ($combination as $similary)
            {
                $expectedQuantities[$similary['positionId']] = $similary['quantity'];

                if (!in_array($similary['warehouse'], $shippingCosts)) {
                    $shippingCosts[$similary['warehouse']] = $similary['shippingCost'];
                }

                if (!in_array($similary['warehouse'], $excemptionLimits)) {
                    $excemptionLimits[$similary['warehouse']] = $similary['shippingExcemptionLimit'];
                }
            }

            foreach ($combination as &$similary)
            {
                $quantity = $similary['instock'] < $expectedQuantities[$similary['positionId']]
                    ? $similary['instock']
                    : $expectedQuantities[$similary['positionId']];

                $totalPrice += ($similary['price'] * $quantity);

                $expectedQuantities[$similary['positionId']] -= $quantity;

                if (!array_key_exists($similary['warehouse'], $wholesalerPrice)) {
                    $wholesalerPrice[$similary['warehouse']] = $totalPrice;
                } else {
                    $wholesalerPrice[$similary['warehouse']] += $totalPrice;
                }

                $similary['quantity'] = $quantity;
            }

            $shippingCosts = array_filter($shippingCosts, function($key) use($excemptionLimits, $wholesalerPrice) {
                return $wholesalerPrice[$key] < $excemptionLimits[$key];
            }, ARRAY_FILTER_USE_KEY);

            $combinationPrices[$key] = $totalPrice + array_sum($shippingCosts);
        }

        $positionCounts = array_map(function ($combination) {
            $warehouses = [];

            foreach ($combination as $similary)
            {
                if (!in_array($similary['warehouse'], $warehouses)) {
                    $warehouses[] = $similary['warehouse'];
                }
            }

            return count($warehouses);
        }, $combinations);

        asort($positionCounts);
        asort($combinationPrices);

        $qualities = [];
        $mainKey = 0;

        foreach ($combinationPrices as $key => $combinationPrice)
        {
            if (!$mainKey) {
                $qualities[$key] = 100;
                $mainKey = $key;
            } else {
                $qualities[$key] = (100 / $combinationPrices[$mainKey]) * $combinationPrice;
            }
        }

        $mainKey = 0;

        foreach ($positionCounts as $key => $positionCount)
        {
            if (!$mainKey) {
                $mainKey = $key;
            } else {
                $qualities[$key] += ($positionCount - $positionCounts[$mainKey]) * 15;
            }
        }

        asort($qualities);

        if (!empty($qualities)) {
            reset($qualities);
            return $combinations[key($qualities)];
        } else {
            return [];
        }
    }

    /**
     * @param array $combinations
     */
    private function filterCombinationsByPrice(&$combinations)
    {
        foreach ($combinations as &$combination)
        {
            $priceSets = [];

            foreach ($combination as $key => $similary)
            {
                $priceSets[$similary['positionId']][] = [
                    'warehouse' => $similary['warehouse'],
                    'price' => $similary['price'],
                    'key' => $key,
                    'instock' => $similary['instock'],
                    'quantity' => $similary['quantity']
                ];
            }

            array_walk($priceSets, function($values) {
                $price = array_column($values, 'price');
                array_multisort($price, SORT_ASC, $values);
            });

            foreach ($priceSets as $priceSet)
            {
                $deleteSimilaries = false;
                $totalStock = 0;

                foreach ($priceSet as $similary)
                {
                    $totalStock += $similary['instock'];

                    if ($totalStock >= $similary['quantity'] && !$deleteSimilaries) {
                        $deleteSimilaries = true;
                    } else if ($deleteSimilaries) {
                        unset($combination[$similary['key']]);
                    }
                }
            }
        }
    }

    /**
     * @param array $combinations
     * @param array $positionQuantities
     */
    private function filterCombinationsByExpectedStocks(&$combinations, $positionQuantities)
    {
        $combinations = array_filter($combinations, function($combination) use($positionQuantities) {
            $quantities = $positionQuantities;

            foreach ($combination as $similary)
            {
                $quantities[$similary['positionId']]['total_stock'] += (int)$similary['instock'];
            }

            foreach ($quantities as $quantity)
            {
                if ($quantity['total_stock'] < $quantity['expected']) {
                    return false;
                }
            }

            return true;
        });
    }

    /**
     * @param array $combinations
     * @param array $positionIds
     */
    private function filterCombinationsByRequiredPositions(&$combinations, $positionIds)
    {
        $combinations = array_filter($combinations, function($combination) use($positionIds) {
            $expected = $positionIds;

            foreach ($combination as $similary)
            {
                $expected[$similary['positionId']]++;
            }

            return array_sum($expected) >= count($expected);
        });
    }

    /**
     * @param array $similaries
     * @return array
     */
    private function getCombinations($similaries)
    {
        $results = [[]];

        foreach ($similaries as $similary) {
            foreach ($results as $combination) {
                array_push($results, array_merge([$similary], $combination));
            }
        }

        return $results;
    }

    /**
     * @param Wholesaler $wholesaler
     * @return IJsonResponse
     */
    private function getWholesalerData($wholesaler) {
        if (!array_key_exists($wholesaler->getShortname(), $this->quantities)) {
            $this->quantities[$wholesaler->getShortname()] = $wholesaler->getClient()->getAll();
        }

        return $this->quantities[$wholesaler->getShortname()];
    }

    /**
     * @param Position $position
     * @param Order $order
     * @return array
     */
    private function getPositionSimilaries($position, $order)
    {
        $similaries = [];
        $shippingCosts = 0.0;

        $articleDetail = $position->getArticleDetail();

        /** @var ShippingCost[] $costMatrix */
        $costMatrix = $order->getDispatch()->getCostsMatrix()->getValues();
        $excemptionLimit = $order->getDispatch()->getShippingFree();

        foreach ($costMatrix as $cost)
        {
            $shippingCosts += $cost->getValue();
        }

        if (($instock = $articleDetail->getInStock())) {
            $similaries[] = [
                'instock' => $instock,
                'price' => (float)number_format($position->getPrice(), 2),
                'articleDetailId' => $articleDetail->getId(),
                'ordernumber' => $articleDetail->getNumber(),
                'warehouse' => 'shop',
                'positionId' => $position->getId(),
                'quantity' => $position->getQuantity(),
                'shippingCost' => $shippingCosts,
                'shippingExcemptionLimit' => $excemptionLimit,
            ];
        }

        $articleDetailAttributes = $articleDetail->getAttribute();

        foreach ($this->wholesalers as $wholesaler)
        {
            $upperShortname = ucfirst(strtolower($wholesaler->getShortname()));

            if (
                method_exists($articleDetailAttributes,'getDc' . $upperShortname . 'Ordernumber') &&
                method_exists($articleDetailAttributes,'getDc' . $upperShortname . 'Ordernumber') &&
                $articleDetailAttributes->{'getDc' . $upperShortname . 'Active'}() &&
                !empty($articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}()) &&
                !is_null($articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}())
            ) {
                $ordernumber = $articleDetailAttributes->{'getDc' . $upperShortname . 'Ordernumber'}();

                if (($response = $this->getWholesalerData($wholesaler))->getStatusCode() === 200) {
                    $data = json_decode($response->getContent(), true);

                    if (
                        array_key_exists($ordernumber, $data) &&
                        array_key_exists('quantity', $data[$ordernumber]) &&
                        array_key_exists('purchasePrice', $data[$ordernumber]) &&
                        ($quantity = (int)$data[$ordernumber]['quantity']) > 0
                    ) {
                        $price = str_replace(',', '.', $data[$ordernumber]['purchasePrice']);

                        $similaries[] = [
                            'instock' => $quantity,
                            'price' => (float)number_format($price, 2),
                            'articleDetailId' => $articleDetail->getId(),
                            'ordernumber' => $ordernumber,
                            'warehouse' => $wholesaler->getShortname(),
                            'positionId' => $position->getId(),
                            'quantity' => $position->getQuantity(),
                            'shippingCost' => $wholesaler->getConfig()->shipping()->cost(),
                            'shippingExcemptionLimit' => $wholesaler->getConfig()->shipping()->excemptionLimit(),
                        ];
                    }
                }
            }
        }

        return $similaries;
    }


    /**
     * @param Order[] $orders
     */
    public function abortAutoOrder($orders)
    {
        foreach ($orders as $order)
        {
            try {
                $order->getAttribute()->setDcDropshipTransmissionAttempt(3);

                $this->em->flush();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), $e, [], 'order');
            }
        }
    }
}