import DefaultHeaders from "../../../data/ticket/headers";

export default {
  namespaced: true,
  state: {
    headers: []
  },
  getters: {
    getHeaders: (state) => state.headers
  },
  mutations: {
    setHeaders: (state, headers) => state.headers = headers,
  },
  actions: {
    save({commit, rootGetters}, headers) {
      return axios.post(rootGetters.getUrl('saveTicketHeaders'), {headers: headers})
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let headers = response.data.hasOwnProperty('headers') ? response.data.headers : [];

          if (headers.length === 0) {
            headers = DefaultHeaders
          }

          if (success) {
            commit('setHeaders', headers)
          } else {
            throw new Error();
          }

          return headers
        })
        .catch(() => {
          console.warn('Ticketheaders could not be loaded')
          commit('setHeaders', DefaultHeaders)
          return DefaultHeaders
        })
    },
    load({commit, rootGetters}) {
      return axios.get(rootGetters.getUrl('getTicketHeaders'))
        .then(response => {
          let success = response.data.hasOwnProperty('success') ? response.data.success : false;
          let headers = response.data.hasOwnProperty('headers') ? response.data.headers : [];

          if (headers.length === 0) {
            headers = DefaultHeaders
          }

          if (success) {
            commit('setHeaders', headers)
          } else {
            throw new Error();
          }

          return headers
        })
        .catch(() => {
          console.warn('Ticketheaders could not be loaded')
          commit('setHeaders', DefaultHeaders)
          return DefaultHeaders
        })
    },
  }
}
