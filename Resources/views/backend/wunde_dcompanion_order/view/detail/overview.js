//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/view/detail/overview"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionOrder.view.detail.Overview', {
    override: 'Shopware.apps.Order.view.detail.Overview',

    blockMsgTpl: [
        '{literal}<tpl for=".">',
            '<p>',
                '{text}',
            '</p>',
        '</tpl>{/literal}'
    ],

    initComponent: function() {
        var me = this;
        var dropshipInfos = me.record.get('dcDropshipInfos');

        me.callParent(arguments);

        Ext.each(dropshipInfos, function (info, key) {
            me.items.insert(key, me.createNoticeContainer(info));
        })
    },

    createNoticeContainer: function(info) {
        var me = this;
        var message = info.message;
        var color = info.color;

        return me.createBlockMessage(message, color);
    },

    createBlockMessage: function(text, color) {
        var me = this;
        var pnl;
        var msgData;
        var style;
        var innerPnl;

        msgData = {
            text: text
        };

        style = {
            "background-color": color
        }

        innerPnl = Ext.create('Ext.container.Container', {
            cls: [ 'block-message-inner', 'dc-dropship-info' ],
            style: style,
            data: msgData,
            margin: 1,
            padding: 7,
            plain: true,
            tpl: me.blockMsgTpl
        });

        pnl = Ext.create('Ext.container.Container', {
            cls: 'block-message',
            ui: 'shopware-ui',
            items: [ innerPnl ]
        });

        innerPnl.update(msgData);

        return pnl;
    },
})
//{/block}