<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 18:52
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Seeder;

use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use WundeDcompanion\Helpers\Database\Database;
use WundeDcompanion\Models\Order\Carrier;

/**
 * Class CarrierSeeder
 * @package WundeDcompanion\Seeder
 */
abstract class CarrierSeeder extends DatabaseSeeder
{
    const CARRIER_PATH = __DIR__ . '/../Data/carriers.json';

    public static function seed()
    {
        $em = Shopware()->Container()->get('models');
        $logger = Shopware()->Container()->get('pluginlogger');

        $carriers = file_exists(self::CARRIER_PATH)
            ? json_decode(file_get_contents(self::CARRIER_PATH), true)
            : [];

        if (Database::entityTableExists(Carrier::class)) {
            foreach ($carriers as $name => $data)
            {
                try {
                    /** @var Carrier $carrier */
                    $carrier = $em->getRepository(Carrier::class)->findOneBy([
                        'name' => $name
                    ]);

                    if (is_null($carrier)) {
                        $carrier = new Carrier($name, $data['trackingUrl'], $data['domain']);
                        $em->persist($carrier);
                    } else {
                        $carrier->setName($name);
                        $carrier->setTrackingUrl($data['trackingUrl']);
                        $carrier->setDomain($data['domain']);
                    }
                } catch (Exception $e) {
                    $logger->error($e->getMessage(), $e->getTrace());
                }
            }
        }
    }
}