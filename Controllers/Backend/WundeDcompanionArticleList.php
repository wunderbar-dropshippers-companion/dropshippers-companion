<?php

use WundeDcompanion\Models\Wholesaler\Repository;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\SchemaService;

/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:42
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

class Shopware_Controllers_Backend_WundeDcompanionArticleList extends Shopware_Controllers_Backend_ExtJs
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_article_list/main';

    /**
     * @var Repository
     */
    private $wholesalerRepository;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var SchemaService $schema
     */
    private $schema;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    private $snippets;

    /**
     * Shopware_Controllers_Backend_WundeDcompanionArticleList constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->wholesalerRepository = Shopware()->Container()->get('wunde_dcompanion.repositories.wholesalers');

        $this->logger = Shopware()->Container()->get('wunde_dcompanion.services.logger');
        $this->schema = Shopware()->Container()->get('wunde_dcompanion.services.schema');

        $this->snippets = Shopware()->Snippets()->getNamespace(self::SNIPPET_NAMESPACE);
    }

    private function getWholesalerList($shortname = null)
    {
        $wholesalers = is_null($shortname)
            ? $this->wholesalerRepository->findAll()
            : [$this->wholesalerRepository->findByShortname($shortname)];
        $data = [];

        foreach ($wholesalers as $wholesaler) {
            $data[] = [
                'longname' => $wholesaler->getLongname(),
                'shortname' => $wholesaler->getShortname(),
                'basename' => $wholesaler->getBasename()
            ];
        }

        return $data;
    }

    public function listAction()
    {
        $this->view->assign('data', $this->getWholesalerList());
    }
}