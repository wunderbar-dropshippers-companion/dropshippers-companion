<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 02/10/2020 08:29
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Subscriber;


use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Order;
use Shopware\Models\Order\Status;
use WundeDcompanion\Models\Core\Config as CoreConfig;
use WundeDcompanion\Services\CronService;
use WundeDcompanion\Services\LoggerService;
use Shopware\Models\Order\Detail as Position;
use WundeDcompanion\Services\OrderService;

class EventSubscriber implements SubscriberInterface
{
    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var CoreConfig $config
     */
    private $config;

    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * @var CronService $cron
     */
    private $cron;

    /**
     * EventSubscriber constructor.
     * @param LoggerService $logger
     * @param ModelManager $em
     * @param CoreConfig $config
     * @param OrderService $orderService
     * @param CronService $cron
     */
    public function __construct($logger, $em, $config, $orderService, $cron)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->config = $config;
        $this->orderService = $orderService;
        $this->cron = $cron;
    }

    public static function getSubscribedEvents()
    {
        return [
            'sOrder::sSaveOrder::after' => 'onSaveOrderAfter',
        ];
    }

    public function onSaveOrderAfter(Enlight_Event_EventArgs $args)
    {

        $this->cron->runSendOrderCronjob();
    }
}