<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 17/08/2020 14:45
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler;


use Shopware\Components\Plugin\ConfigReader;
use Shopware\Components\Plugin\DBALConfigReader;
use Shopware\Models\Plugin\Plugin;
use Shopware\Models\Shop\Repository as ShopRepository;
use Shopware\Models\Shop\Shop;
use WundeDcompanion\Interfaces\DateTimeInterface;
use WundeDcompanion\Interfaces\IClient;
use WundeDcompanion\Interfaces\IImportService;
use WundeDcompanion\Interfaces\IWholesaler;
use WundeDcompanion\Services\SchemaService;

/**
 * Class Wholesaler
 * @package WundeDcompanion\Models
 */
class Wholesaler implements IWholesaler
{
    /**
     * @var IClient $client
     */
    private $client;

    /**
     * @var Plugin $plugin
     */
    private $plugin;

    /**
     * @var string $shortname
     */
    private $shortname;

    /**
     * @var string $longname
     */
    private $longname;

    /**
     * @var string $basename
     */
    private $basename;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $identifier
     */
    private $identifier;

    /**
     * Wholesaler constructor.
     * @param IClient $client
     * @param Plugin $plugin
     * @param string $shortname
     * @param string $longname
     * @param string $basename
     * @param string $name
     * @param string $identifier
     */
    public function __construct($client, $plugin, $shortname, $longname, $basename, $name, $identifier)
    {
        $this->client = $client;
        $this->plugin = $plugin;
        $this->shortname = $shortname;
        $this->longname = $longname;
        $this->basename = $basename;
        $this->name = $name;
        $this->identifier = $identifier;
    }

    /**
     * @return Plugin
     */
    public function getPlugin()
    {
        return $this->plugin;
    }

    /**
     * @return string
     */
    public function getShortname()
    {
        return strtolower(trim($this->shortname));
    }

    /**
     * @return string
     */
    public function getUpperShortname()
    {
        return ucfirst(strtolower(trim($this->shortname)));
    }

    /**
     * @return string
     */
    public function getLongname()
    {
        return $this->longname;
    }

    /**
     * @return string
     */
    public function getBasename()
    {
        return $this->basename;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return IClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return ArticleRepository
     */
    public function articles()
    {
        return new ArticleRepository($this);
    }

    /**
     *
     */
    public function setImportVariants()
    {
        if (Shopware()->Container()->has($this->identifier . '.import_service')) {
            /** @var IImportService $service */
            $service = Shopware()->Container()->get($this->identifier . '.import_service');

            $service->setVariants();
        }
    }

    /**
     * @param bool $cached
     * @return Config
     */
    public function getConfig($cached = false)
    {
        /** @var ShopRepository $shopRepository */
        $shopRepository = Shopware()->Models()->getRepository(Shop::class);

        /** @var ConfigReader $configReader */
        $configReader = $cached
            ? Shopware()->Container()->get('shopware.plugin.cached_config_reader')
            : Shopware()->Container()->get('shopware.plugin.config_reader');

        $shop = Shopware()->Container()->has('shop')
            ? Shopware()->Shop()
            : $shopRepository->getActiveDefault();

        $config = $configReader->getByPluginName($this->plugin->getName(), $shop);

        return new Config($config, $this->getShortname());
    }

    /**
     * @return array
     */
    public function __debugInfo()
    {
        return [
            'name' => $this->name,
            'shortname' => $this->shortname,
            'basename' => $this->basename,
            'longname' => $this->longname,
            'identifier' => $this->identifier,
        ];
    }


}