<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 15:58
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Database;


/**
 * Class Table
 * @package WundeDcompanion\Helpers\Database
 */
class Column
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $oldName
     */
    private $oldName;

    /**
     * @var string $label
     */
    private $label;

    /**
     * @var string $type
     */
    private $type;

    /**
     * @var string $helpText
     */
    private $helpText;

    /**
     * @var string $supportText
     */
    private $supportText;

    /**
     * @var bool $translatable
     */
    private $translatable = false;

    /**
     * @var bool $displayInBackend
     */
    private $displayInBackend = false;

    /**
     * @var bool $custom
     */
    private $custom = false;

    /**
     * @var mixed $default
     */
    private $default;

    /**
     * Column constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->label = $data['label'];
        $this->type = $data['type'];
        $this->translatable = $data['translatable'];
        $this->displayInBackend = $data['displayInBackend'];
        $this->custom = $data['custom'];

        $this->default = isset($data['default'])
            ? $data['default']
            : null;

        $this->oldName = isset($data['oldName'])
            ? $data['oldName']
            : null;

        $this->helpText = isset($data['helpText'])
            ? $data['helpText']
            : '';

        $this->supportText = isset($data['supportText'])
            ? $data['supportText']
            : '';
    }

    /**
     * @param string|null $shortname
     * @return string
     */
    public function getName(string $shortname = null)
    {
        return !is_null($shortname)
            ? str_replace('%shortname%', trim(strtolower($shortname)), $this->name)
            : $this->name;
    }

    /**
     * @param string|null $shortname
     * @return string|null
     */
    public function getOldName(string $shortname = null)
    {
        return !is_null($shortname)
            ? str_replace('%shortname%', trim(strtolower($shortname)), $this->oldName)
            : $this->oldName;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isTranslatable()
    {
        return $this->translatable;
    }

    /**
     * @return bool
     */
    public function isDisplayInBackend()
    {
        return $this->displayInBackend;
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return $this->custom;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @return string
     */
    public function getHelpText()
    {
        return $this->helpText;
    }

    /**
     * @return string
     */
    public function getSupportText()
    {
        return $this->supportText;
    }

    public function getValues()
    {
        return [
            'label' => $this->getLabel(),
            'supportText' => $this->getSupportText(),
            'helpText' => $this->getHelpText(),
            'translatable' => $this->isTranslatable(),
            'displayInBackend' => $this->isDisplayInBackend(),
            'custom' => $this->isCustom()
        ];
    }
}