export default  {
  "main": [
    {
      "snippet": "menu.group.start",
      "items": [
        {
          "snippet": "menu.overview",
          "icon": "home",
          "color": "primary",
          "to": "main",
          "disabled": false,
          "hidden": false
        },
        {
          "snippet": "menu.settings",
          "icon": "settings",
          "color": "primary",
          "to": "settings",
          "disabled": false,
          "hidden": false
        },
        {
          "snippet": "menu.statistics",
          "icon": "leaderboard",
          "color": "primary",
          "to": "statistics",
          "disabled": true,
          "hidden": true
        }
      ]
    },
    {
      "snippet": "menu.group.support",
      "items": [
        {
          "snippet": "menu.tickets",
          "icon": "bugReport",
          "color": "primary",
          "to": "tickets",
          "disabled": false,
          "hidden": false
        },
        {
          "snippet": "menu.suggestions",
          "icon": "addBox",
          "color": "primary",
          "to": "suggestions",
          "disabled": true,
          "hidden": true
        },
        {
          "snippet": "menu.faq",
          "icon": "help",
          "color": "primary",
          "to": "faqs",
          "disabled": true,
          "hidden": true
        },
        {
          "snippet": "menu.privacy",
          "icon": "privacyTip",
          "color": "primary",
          "to": "privacy",
          "disabled": false,
          "hidden": false
        },
        {
          "snippet": "menu.contact",
          "icon": "email",
          "color": "primary",
          "to": "contact",
          "disabled": false,
          "hidden": false
        },
        {
          "snippet": "menu.imprint",
          "icon": "subject",
          "color": "primary",
          "to": "imprint",
          "disabled": false,
          "hidden": false
        }
      ]
    },
    {
      "snippet": "",
      "items": [
        {
          "snippet": "menu.trash",
          "icon": "delete",
          "color": "primary",
          "to": "trash",
          "disabled": true,
          "hidden": true
        }
      ]
    }
  ],
  "settings": [
    {
      "snippet": "menu.plugins",
      "icon": "settings",
      "color": "white",
      "to": "plugins",
      "disabled": false,
      "hidden": false
    },
    {
      "snippet": "menu.colors",
      "icon": "settings",
      "color": "white",
      "to": "colors",
      "disabled": true,
      "hidden": true
    }
  ]
}
