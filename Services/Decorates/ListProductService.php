<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 28/12/2020 17:06
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services\Decorates;


use Shopware\Bundle\StoreFrontBundle\Service\ListProductServiceInterface;
use Shopware\Bundle\StoreFrontBundle\Struct\ProductContextInterface;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Article\Detail;
use Shopware\Models\Article\Repository;
use WundeDcompanion\DC;
use WundeDcompanion\Models\Wholesaler\Repository as WholesalerRepository;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;

/**
 * Class ListProductService
 * @package WundeDcompanion\Services\Decorates
 */
class ListProductService implements ListProductServiceInterface
{

    /**
     * @var ListProductServiceInterface $originalService
     */
    private $originalService;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Wholesaler[] $wholesalers
     */
    private $wholesalers;

    /**
     * ListProductService constructor.
     * @param ListProductServiceInterface $service
     * @param LoggerService $logger
     * @param ModelManager $em
     * @param WholesalerRepository $wholesalerRepository
     */
    public function __construct(ListProductServiceInterface $service, LoggerService $logger, ModelManager $em, WholesalerRepository $wholesalerRepository)
    {
        $this->originalService = $service;
        $this->logger = $logger;
        $this->em = $em;
        $this->wholesalers = $wholesalerRepository->findAllActive();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(array $numbers, ProductContextInterface $context)
    {
        $products = $this->originalService->getList($numbers, $context);

        /** @var Repository $articleDetailRepository */
        $articleDetailRepository = $this->em->getRepository(Detail::class);

        foreach ($products as $ordernumber => &$product)
        {
            $wholesalerStocks = array_sum(array_map(function($wholesaler) use($ordernumber, $articleDetailRepository) {
                $detail = $articleDetailRepository->findOneBy([
                    'number' => $ordernumber
                ]);

                $attribute = $detail->getAttribute();

                return !is_null($detail) && method_exists($attribute, 'getDc'.$wholesaler->getUpperShortname().'Instock')
                    ? $attribute->{'getDc'.$wholesaler->getUpperShortname().'Instock'}()
                    : 0;
            }, $this->wholesalers));

            $product->setStock($product->getStock() + $wholesalerStocks);
        }

        return $products;
    }

    /**
     * {@inheritdoc}
     */
    public function get($number, ProductContextInterface $context)
    {
        $products = $this->getList([$number], $context);

        return array_shift($products);
    }
}