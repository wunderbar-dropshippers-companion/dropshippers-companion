import router from './router'
import vuetify from './vuetify'
import { i18n, setI18nLanguage } from './i18n'
import { db, firebase } from './firebase'
require('./editor');
require('./axios');
require('./lodash');

export {
  router,
  vuetify,
  db,
  i18n,
  setI18nLanguage,
  firebase
}
