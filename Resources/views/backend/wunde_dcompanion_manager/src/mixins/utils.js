import vColors from 'vuetify/lib/util/colors'

export default {
  methods: {
    hexToRgb(hex) {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    },
    getSystemRgba(color, alpha = 1) {
      var theme = this.$vuetify.theme.dark ? 'dark' : 'light'
      var sysColor = this.$vuetify.theme.themes[theme][color]

      return sysColor !== null && sysColor !== undefined
        ? Object.assign(this.hexToRgb(sysColor), {a: alpha})
        : null
    },
    getSystemRgbaString(color, alpha = 1) {
      var rgba = this.getSystemRgba(color, alpha)

      return rgba
        ? `rgba(${rgba.r},${rgba.g},${rgba.b},${rgba.a})`
        : null
    },
    getSystemColor(color) {
      var colors = color.split(' ')
      var theme = this.$vuetify.theme.dark ? 'dark' : 'light'

      if (colors.length >= 1) {
        colors[0] = colors[0].replace(/-([a-z])/g, function (g) { return g[1].toUpperCase(); })
      }

      if (this.$vuetify.theme.themes[theme].hasOwnProperty(colors[0])) {
        return this.$vuetify.theme.themes[theme][colors[0]]
      } else if (vColors.hasOwnProperty(colors[0])) {
        if (colors.length > 1) {
          let brightness = colors[1].split('-').join('')

          return vColors[colors[0]].hasOwnProperty(brightness)
            ? vColors[colors[0]][brightness]
            : (vColors[colors[0]].hasOwnProperty('base')
              ? vColors[colors[0]]['base']
              : this.$vuetify.theme.themes[theme]['primary'])
        } else {
          return vColors[colors[0]].hasOwnProperty('base')
            ? vColors[colors[0]]['base']
            : this.$vuetify.theme.themes[theme]['primary']
        }
      } else {
        return this.$vuetify.theme.themes[theme]['primary']
      }
    }
  }
}
