<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 27/08/2020 11:42
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\DBALConfigReader;
use Shopware\Models\Article\Price;
use Shopware\Models\Order\Detail as OrderDetail;
use Shopware\Models\Order\Order;
use WundeDcompanion\Models\Wholesaler\Repository;
use WundeDcompanion\Services\LoggerService;
use WundeDcompanion\Services\OrderService;
use WundeDcompanion\Services\SchemaService;
use Shopware\Models\Article\Detail as ArticleDetail;


class Shopware_Controllers_Backend_WundeDcompanionOrder extends Shopware_Controllers_Backend_ExtJs
{
    const SNIPPET_NAMESPACE = 'backend/wunde_dcompanion_order/main';

    /**
     * @var Repository
     */
    private $wholesalerRepository;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * @var SchemaService $schema
     */
    private $schema;

    /**
     * @var Enlight_Components_Snippet_Namespace $snippets
     */
    private $snippets;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var OrderService $orderService
     */
    private $orderService;

    /**
     * Shopware_Controllers_Backend_WundeDcompanionArticle constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->wholesalerRepository = Shopware()->Container()->get('wunde_dcompanion.repositories.wholesalers');


        /** @var DBALConfigReader $configReader */
        $configReader = Shopware()->Container()->get('shopware.plugin.config_reader');
        $plugiName = Shopware()->Container()->getParameter('wunde_dcompanion.plugin_name');

        $this->orderService = Shopware()->Container()->get('wunde_dcompanion.services.order');
        $this->logger = Shopware()->Container()->get('wunde_dcompanion.services.logger');
        $this->schema = Shopware()->Container()->get('wunde_dcompanion.services.schema');
        $this->em = Shopware()->Container()->get('models');

        $this->config = $configReader->getByPluginName($plugiName);

        $this->snippets = Shopware()->Snippets()->getNamespace(self::SNIPPET_NAMESPACE);

    }

    public function getSourceAction()
    {
        $sources = [];

        $sources[] = [
            'shortname' => '',
            'basename' => 'myShop',
            'longname' => $this->snippets->get('my_shop', 'My shop'),
        ];

        foreach ($this->wholesalerRepository->findAll() as $wholesaler)
        {
            $sources[] = [
                'shortname' => $wholesaler->getShortname(),
                'basename' => $wholesaler->getBasename(),
                'longname' => $wholesaler->getLongname(),
            ];
        }

        $this->view->assign([
            'success' => true,
            'sources' => $sources
        ]);
    }

    public function savePositionAttributesAction()
    {
        try {
            $longname = $this->Request()->getParam('longname', null);
            $shortname = $this->Request()->getParam('shortname', null);
            $purchasePrice = $this->Request()->getParam('purchasePrice', null);
            $quantity = $this->Request()->getParam('quantity', null);
            $detailId = $this->Request()->getParam('detailId', null);

            if (
                !is_null($longname) &&
                !is_null($shortname) &&
                !is_null($purchasePrice) &&
                !is_null($quantity) &&
                !is_null($detailId)
            ) {
                if (empty($shortname)) {
                    $longname = null;
                    $shortname = null;
                    $quantity = 0;
                    $purchasePrice = 0;
                }

                Shopware()->Models()->getConnection()->createQueryBuilder()
                    ->update('s_order_details_attributes')
                    ->set('dc_name_long', ':longname')
                    ->set('dc_name_short', ':shortname')
                    ->set('dc_stock', ':stock')
                    ->set('dc_purchasing_price', ':purchasePrice')
                    ->where('detailID = :detailId')
                    ->setParameter('longname', $longname)
                    ->setParameter('shortname', $shortname)
                    ->setParameter('stock', $quantity)
                    ->setParameter('purchasePrice', $purchasePrice)
                    ->setParameter('detailId', $detailId)
                    ->execute();


                $this->view->assign('success', true);
            } else {
                throw new Exception('Missing parameter');
            }
        } catch (Exception $e) {
            $this->view->assign('success', false);
        }
    }

    public function getLocaleAction()
    {
        $contextService = $this->container->get('shopware_storefront.context_service');
        $locale = $contextService->getShopContext()->getShop()->getLocale();

        $this->view->assign([
            'success' => true,
            'locale' => $locale
        ]);
    }

    public function getConfigAction()
    {
        $this->view->assign([
            'success' => true,
            'data' => $this->config
        ]);
    }

    public function sendOrderAction()
    {

    }

    public function abortOrderProcessAction()
    {
        $orderIds = json_decode($this->Request()->getParam('orderIds'));

        $orders = $this->em->getRepository(Order::class)->findBy([
            'id' => $orderIds
        ]);

        $this->orderService->abortAutoOrder($orders);
    }
}