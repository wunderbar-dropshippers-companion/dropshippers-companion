<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 15:50
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Database;


use Doctrine\DBAL\Schema\Table;
use Exception;
use Shopware\Components\Model\ModelManager;

class AttributeTableManager
{
    const ATTRIBUTE_TABLES_PATH = __DIR__ . '/../../Data/tables.json';

    const CONTEXT_EXTENSION = 'extension';
    const CONTEXT_CORE = 'core';

    /**
     * @var array $tableSchema
     */
    private $tableSchema;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * AttributeTableManager constructor.
     */
    public function __construct()
    {
        $this->em = Shopware()->Container()->get('models');

        $this->tableSchema = file_exists(self::ATTRIBUTE_TABLES_PATH)
            ? json_decode(file_get_contents(self::ATTRIBUTE_TABLES_PATH), true)
            : [];
    }

    /**
     * @return DatabaseTable[]
     */
    public function all()
    {
        return array_merge($this->core(), $this->extension());
    }

    /**
     * @return DatabaseTable[]
     */
    public function core()
    {
        return $this->getTables(__FUNCTION__);
    }

    /**
     * @return DatabaseTable[]
     */
    public function extension()
    {
        return $this->getTables(__FUNCTION__);
    }

    /**
     * @param array $column
     * @return bool
     */
    private function validColumnData($column = [])
    {
        return
            isset($column['name']) && is_string($column['name']) &&
            isset($column['label']) && is_string($column['label']) &&
            isset($column['type']) && is_string($column['type']) &&
            isset($column['translatable']) &&
            isset($column['displayInBackend']) &&
            isset($column['custom']);
    }

    /**
     * @param string $context
     * @return DatabaseTable[]
     */
    private function getTables(string $context)
    {
        $tables = [];

        foreach ($this->tableSchema[$context] as $tableName => $columns)
        {
            $columns = array_map(function ($data) {
                return new Column($data);
            }, array_filter($columns, function ($column) {
                return $this->validColumnData($column);
            }));

            $tables[] = new DatabaseTable($tableName, $columns);
        }

        return $tables;
    }

    public function generateAttributeModels()
    {
        $tables = array_map(function ($table) {
            return $table->getName();
        }, array_filter($this->all(), function ($table) {
            return Database::tableExists($table->getName());
        }));

        $this->em->getConfiguration()->getMetadataCacheImpl()->deleteAll();
        $this->em->generateAttributeModels($tables);
    }
}