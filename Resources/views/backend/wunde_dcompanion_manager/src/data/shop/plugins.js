export default [
  {
    "id": 2,
    "name": "ErrorHandler",
    "label": "ErrorHandler",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 7,
    "name": "Cron",
    "label": "Cron",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1608555604,
    "updated": 1608555604,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 8,
    "name": "Router",
    "label": "Router",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 9,
    "name": "CronBirthday",
    "label": "CronBirthday",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 10,
    "name": "System",
    "label": "System",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 11,
    "name": "ViewportForward",
    "label": "ViewportForward",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 12,
    "name": "Shop",
    "label": "Shop",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": null,
    "author": "shopware AG",
    "copyright": "Copyright © 2010, shopware AG",
    "license": "",
    "version": "1",
    "support": "https://docs.shopware.com/",
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 13,
    "name": "PostFilter",
    "label": "PostFilter",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 14,
    "name": "CronRating",
    "label": "CronRating",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 15,
    "name": "ControllerBase",
    "label": "ControllerBase",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 16,
    "name": "CronStock",
    "label": "CronStock",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 19,
    "name": "RouterRewrite",
    "label": "RouterRewrite",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 21,
    "name": "Facebook",
    "label": "Facebook",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": null,
    "author": "shopware AG",
    "copyright": "Copyright © 2010, shopware AG",
    "license": "",
    "version": "1",
    "support": "https://docs.shopware.com/",
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 22,
    "name": "Seo",
    "label": "Seo",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 29,
    "name": "AdvancedMenu",
    "label": "Erweitertes Menü",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 31,
    "name": "Statistics",
    "label": "Statistics",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 33,
    "name": "Notification",
    "label": "Notification",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 34,
    "name": "TagCloud",
    "label": "TagCloud",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": false,
    "added": 1346104800,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 35,
    "name": "InputFilter",
    "label": "InputFilter",
    "namespace": "Frontend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 36,
    "name": "Auth",
    "label": "Auth",
    "namespace": "Backend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 37,
    "name": "Menu",
    "label": "Menu",
    "namespace": "Backend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346104800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": null,
    "author": "shopware AG",
    "copyright": "Copyright © 2010, shopware AG",
    "license": "",
    "version": "1",
    "support": "https://docs.shopware.com/",
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 40,
    "name": "Check",
    "label": "Systeminfo",
    "namespace": "Backend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1287352800,
    "installed": 1287352800,
    "updated": 1287352800,
    "refreshed": null,
    "author": "shopware AG",
    "copyright": "Copyright © 2011, shopware AG",
    "license": "",
    "version": "1.0.0",
    "support": "https://docs.shopware.com/",
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 43,
    "name": "Locale",
    "label": "Locale",
    "namespace": "Backend",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1346099333,
    "installed": 1346099333,
    "updated": 1346099333,
    "refreshed": null,
    "author": "shopware AG",
    "copyright": "Copyright © 2011, shopware AG",
    "license": "",
    "version": "1.0.0",
    "support": "https://docs.shopware.com/",
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 44,
    "name": "RestApi",
    "label": "RestApi",
    "namespace": "Core",
    "source": "Default",
    "description": "",
    "active": true,
    "added": 1342173793,
    "installed": 1342173816,
    "updated": 1342173816,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 49,
    "name": "PasswordEncoder",
    "label": "PasswordEncoder",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": true,
    "added": 1366107234,
    "installed": 1366114043,
    "updated": 1366114043,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 50,
    "name": "MarketingAggregate",
    "label": "Shopware Marketing Aggregat Funktionen",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": true,
    "added": 1367324353,
    "installed": 1367324808,
    "updated": 1367324808,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 51,
    "name": "RebuildIndex",
    "label": "Shopware Such- und SEO-Index",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": true,
    "added": 1368953604,
    "installed": 1369135684,
    "updated": 1369135684,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "http://www.shopware.de/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 52,
    "name": "HttpCache",
    "label": "Frontendcache (HttpCache)",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": false,
    "added": 1369663079,
    "installed": 1369663089,
    "updated": 1369663089,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.1.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 53,
    "name": "PaymentMethods",
    "label": "Payment Methods",
    "namespace": "Core",
    "source": "Default",
    "description": "Shopware Payment Methods handling. This plugin is required to handle payment methods, and should not be deactivated",
    "active": true,
    "added": 1383117142,
    "installed": 1383117206,
    "updated": 1383117206,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.1",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": false,
    "capabilityInstall": false,
    "capabilityEnable": false,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 54,
    "name": "Debug",
    "label": "Debug",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": false,
    "added": 1389946745,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 55,
    "name": "SwagUpdate",
    "label": "Shopware Auto Update",
    "namespace": "Backend",
    "source": "Default",
    "description": null,
    "active": true,
    "added": 1399359781,
    "installed": 1399359786,
    "updated": 1399359786,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 56,
    "name": "PluginManager",
    "label": "Plugin Manager",
    "namespace": "Backend",
    "source": "Default",
    "description": null,
    "active": true,
    "added": 1415357746,
    "installed": 1415357754,
    "updated": 1415357754,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 60,
    "name": "CronProductExport",
    "label": "CronProductExport",
    "namespace": "Core",
    "source": "Default",
    "description": null,
    "active": false,
    "added": 1608054709,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 61,
    "name": "CronRefresh",
    "label": "CronRefresh",
    "namespace": "Frontend",
    "source": "Default",
    "description": null,
    "active": false,
    "added": 1608054709,
    "installed": null,
    "updated": null,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": false,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 80,
    "name": "WundeDcompanion",
    "label": "Dropshippers Companion",
    "namespace": "ShopwarePlugins",
    "source": "Community",
    "description": null,
    "active": true,
    "added": 1608715232,
    "installed": 1619191627,
    "updated": 1619191627,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "2.5.0",
    "support": "--",
    "changes": {
      "2.5.0": {
        "en": [
          "\n            - Multiple wholesalers are now possible\n            - Tracking information is automatically entered\n            - Completely new wholesaler management in the article overview\n        "
        ],
        "de": [
          "\n            - Mehrere Großhändler sind nun möglich\n            - Trackinginformationen werden automatisch eingepflegt\n            - Komplett neues Großhandelsmanagement in der Artikelübersicht\n        "
        ]
      }
    },
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion"
      },
      "en": {
        "label": "Dropshippers Companion"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 81,
    "name": "WundeDcInnoCigs",
    "label": "Dropshippers Companion (InnoCigs)",
    "namespace": "ShopwarePlugins",
    "source": "Community",
    "description": null,
    "active": true,
    "added": 1608715381,
    "installed": 1617225796,
    "updated": 1617225796,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "2.5.0",
    "support": "--",
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion (InnoCigs)"
      },
      "en": {
        "label": "Dropshippers Companion (InnoCigs)"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 83,
    "name": "DropshippersCompanion",
    "label": "Test",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": false,
    "added": 1609539588,
    "installed": null,
    "updated": 1609539592,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": null,
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Test"
      },
      "en": {
        "label": "Test"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 84,
    "name": "WundeDcompanionVaporExMachina",
    "label": "Dropshippers Companion (VaporExMachina)",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": true,
    "added": 1609540423,
    "installed": 1617699602,
    "updated": 1617699602,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": null,
    "license": null,
    "version": "2.5.0",
    "support": null,
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion (VaporExMachina)"
      },
      "en": {
        "label": "Dropshippers Companion (VaporExMachina)"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 85,
    "name": "SwagPaymentPayPalUnified",
    "label": "PayPal",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": "This module offers you the most popular PayPal products from a single source: the PayPal Express button, PayPal PLUS, and banners for the new PayPal Installment Payment. And best of all, this module lets you use all products individually or simultaneously in your online store.",
    "active": true,
    "added": 1609961821,
    "installed": 1609961834,
    "updated": 1616229544,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": null,
    "license": null,
    "version": "3.1.1",
    "support": null,
    "changes": {
      "3.1.1": {
        "de": [
          "\n            PT-12267 - Behebt Fehler im Risiko Management;\n        "
        ],
        "en": [
          "\n            PT-12267 - Fixes errors in risk management;\n        "
        ]
      },
      "3.1.0": {
        "de": [
          "\n            PT-10201 - PayPal Express Button berücksichtigt nun auch das Risiko Management;\n            PT-12236 - Korrigiert das Setzen des PayPal-Payment-Typs für Rechnungskauf;\n        "
        ],
        "en": [
          "\n            PT-10201 - PayPal Express button now also considers the risk management;\n            PT-12236 - Corrects setting the PayPal payment type for pay upon invoice;\n        "
        ]
      },
      "3.0.4": {
        "de": [
          "PT-12234 - Korrigiert das Setzen des PayPal-Payment-Typs für Rechnungskauf;"
        ],
        "en": [
          "PT-12234 - Corrects setting the PayPal payment type for pay upon invoice;"
        ]
      },
      "3.0.3": {
        "de": [
          "PT-12230 - Korrigiert das Setzen des PayPal-Payment-Typs;"
        ],
        "en": [
          "PT-12230 - Corrects setting the PayPal payment type;"
        ]
      },
      "3.0.2": {
        "de": [
          "\n            PT-12125 - Shopware 5.7 und PHP8 Kompatibilität;\n            PT-12178 - Der PayPal-Payment-Typ ist nun als Variable in der Bestellbestätigungs-Mail verfügbar;\n            PT-12208 - Fügt einen PayPal-Plus-Cookie zum Consent-Manager hinzu;\n        "
        ],
        "en": [
          "\n            PT-12125 - Shopware 5.7 and PHP8 compatibility;\n            PT-12178 - The PayPal payment type is now available as a variable in the order confirmation mail;\n            PT-12208 - Adds a PayPal Plus cookie to the Consent Manager;\n        "
        ]
      },
      "3.0.1": {
        "de": [
          "PT-12068 - Korrigiert Link in den Einstellungen für Ratenzahlung;"
        ],
        "en": [
          "PT-12068 - Corrects link in installments settings;"
        ]
      },
      "3.0.0": {
        "de": [
          "\n            PT-10770 - Composer-Name des Plugin ist nun mit Composer 2 kompatibel;\n            PT-11863 - Benötigte Mindestversion von Shopware auf 5.2.27 angehoben;\n            PT-12040 - In-Shop Ratenzahlung Integration entfernt;\n        "
        ],
        "en": [
          "\n            PT-10770 - Composer name of the plugin is now compatible with Composer 2;\n            PT-11863 - Increased required minimum version of shopware to 5.2.27;\n            PT-12040 - Removed in-shop installments integration;\n        "
        ]
      },
      "2.8.1": {
        "de": [
          "\n            PT-11760 - Behebt Fehler während der installation über den \"Shopware First Run Wizard\";\n            PT-11763 - Behebt fehlerhaftes Verhalten des Express-Checkout Buttons;\n            PT-11782 - Optimiert PayPal Link in Bestellbestätigungsmail;\n        "
        ],
        "en": [
          "\n            PT-11760 - Fixes error during installation via the \"Shopware First Run Wizard\";\n            PT-11763 - Fixes incorrect behavior of the Express Checkout Button;\n            PT-11782 - Optimizes PayPal link in order confirmation email;\n        "
        ]
      },
      "2.8.0": {
        "de": [
          "\n            PT-11491 - PayPal-Cookies zum Consent-Manager hinzugefügt;\n        "
        ],
        "en": [
          "\n            PT-11491 - Add PayPal cookies to consent manager;\n        "
        ]
      },
      "2.7.0": {
        "de": [
          "\n            PT-10536 - Behebt Anzeigefehler des Logos in Bestellbestätigungsmail;\n            PT-11361 - Entfernt Werbung für Kostenlose Retouren;\n            PT-11469 - Shop-Einstellungen werden im Firefox wieder korrekt geladen;\n            PT-11574 - Behebt Fehler des Express-Checkout-Buttons in Produkt-Listings;\n            PT-11575 - Javascript auf der Bestellbestätigungsseite verbessert, wenn der In-Context Modus benutzt wird;\n        "
        ],
        "en": [
          "\n            PT-10536 - Fixes display error of logo in order confirmation mail;\n            PT-11361 - Remove free return advertising;\n            PT-11469 - Shop settings are loaded correctly in Firefox again;\n            PT-11574 - Fixes error of Express Checkout button in product listings;\n            PT-11575 - Improve Javascript on confirm page, if in-context mode is used;\n        "
        ]
      },
      "2.6.5": {
        "de": [
          "Erweiterbarkeit durch Dritt-Plugins verbessert;"
        ],
        "en": [
          "Improved extensibility via third-party plugins;"
        ]
      },
      "2.6.4": {
        "de": [
          "PT-11190 - Kompatibilität mit älteren Shopware-Versionen wiederhergestellt;"
        ],
        "en": [
          "PT-11190 - Restore compatibility with older Shopware versions;"
        ]
      },
      "2.6.3": {
        "de": [
          "PT-11166 - Express Checkout unterstützt nun wieder korrekt die \"Warenkorb übertragen\" Option;"
        ],
        "en": [
          "PT-11166 - Express Checkout correctly considers the \"Submit cart\" configuration again;"
        ]
      },
      "2.6.2": {
        "de": [
          "PT-11003 - Fehler im Javascript behoben;"
        ],
        "en": [
          "PT-11003 - Fix error in Javascript;"
        ]
      },
      "2.6.1": {
        "de": [
          "PT-11003 - Fehler beim Update behoben;"
        ],
        "en": [
          "PT-11003 - Fix error on update;"
        ]
      },
      "2.6.0": {
        "de": [
          "\n            PT-11003 - Neue Option für das Bewerben von Ratenzahlung;\n            PT-11082 - Umsatzsteuer-ID wird nun wieder korrekt berücksichtigt;\n        "
        ],
        "en": [
          "\n            PT-11003 - New option for advertising installment payment;\n            PT-11082 - VAT ID is now correctly considered again;\n        "
        ]
      },
      "2.5.2": {
        "de": [
          "PT-11021 - Fehler im Express Checkout behoben;"
        ],
        "en": [
          "PT-11021 - Fix error in Express Checkout;"
        ]
      },
      "2.5.1": {
        "de": [
          "\n            PT-10636 - Berücksichtigt nun Kundengruppen Einstellungen korrekt;\n            PT-10996 - Setzt nun mindestens Shopware 5.6.3 für das Payment-Token-Feature voraus;\n        "
        ],
        "en": [
          "\n            PT-10636 - Now considers customer group settings correctly;\n            PT-10996 - Now requires at least Shopware 5.6.3 for the payment token feature;\n        "
        ]
      },
      "2.5.0": {
        "de": [
          "\n            PT-9896 - Neue allgemeine Option für das Warenkorb übertragen hinzugefügt;\n            PT-10621 - Mit Shopware 5.6 kann nun die Session wiederhergestellt werden, falls diese verloren geht, während der Kunde sich auf der PayPal Seite befindet;\n            PT-10823 - Wenn verschiedene Aktionen über das PayPal Bestellmodul ausgelöst werden, wird nun auch der Zahlungsstatus der Bestellung aktualisiert;\n            PT-10825 - Anzeige der Smart Payment Buttons verbessert;\n            PT-10855 - Problem behoben, dass manchmal beim Erstellen einer Zahlung für den In-Context-Modus aufgetreten ist;\n            PT-10919 - Fehlermeldungen im Bestellmodul sind nun fest gesetzt;\n            PT-10953 - Anzeige des Maximalbetrags zur Wiedergutschrift im Bestellmodul wurde korrigiert;\n        "
        ],
        "en": [
          "\n            PT-9896 - Add new general option for shopping cart submit;\n            PT-10621 - With Shopware 5.6 the session can now be restored if it gets lost while the customer is on the PayPal page;\n            PT-10823 - If several actions are triggered via the PayPal order module, the payment status of the order is now also updated;\n            PT-10825 - Improve displaying of Smart Payment Buttons;\n            PT-10855 - Fixed problem that sometimes occurred when creating a payment for in-context mode;\n            PT-10919 - Error messages in the order module are now sticky;\n            PT-10953 - Fix displaying of maximum amount for refund in order module;\n        "
        ]
      },
      "2.4.1": {
        "de": [
          "PT-10794 - Anzeigefehler mit aktivierten SPB auf der Bestätigungsseite behoben;"
        ],
        "en": [
          "PT-10794 - Fix display bug with activated SPB on confirm page;"
        ]
      },
      "2.4.0": {
        "de": [
          "PT-10573, PT-10724, PT-10732, PT-10765 - Smart Payment Buttons integriert;"
        ],
        "en": [
          "PT-10573, PT-10724, PT-10732, PT-10765 - Introduced Smart Payment Buttons;"
        ]
      },
      "2.3.0": {
        "de": [
          "SW-24197, PT-10430 - Shopware 5.6 Kompatibilität;"
        ],
        "en": [
          "SW-24197, PT-10430 - Shopware 5.6 compatibility;"
        ]
      },
      "2.2.4": {
        "de": [
          "PT-10584 - Fehler in der Suche im Bestellmodul behoben;"
        ],
        "en": [
          "PT-10584 - Fixed search in order module;"
        ]
      },
      "2.2.3": {
        "de": [
          "PT-10562 - Bestellbestätigung wird nun trotzdem wieder versendet, auch wenn der Bezahlvorgang fehlgeschlagen ist. Stellt Kompatibilität mit anderen Plugins wieder her;"
        ],
        "en": [
          "PT-10562 - Order confirmation will now be sent again, even if the payment process has failed. Restores compatibility with other plugins;"
        ]
      },
      "2.2.2": {
        "de": [
          "PT-10330 - Wenn die Bestellnummer mitgesendet wird, wird nun in einem Fehlerfall zur Abschlussseite, statt zur Zahlungsartenauswahlseite, umgeleitet;"
        ],
        "en": [
          "PT-10330 - If the order number gets sent and an error occurs, a redirect to the finish page is done, instead of the payment method selection page;"
        ]
      },
      "2.2.1": {
        "de": [
          "PT-10471 - Ajax Calls des Plugins verbessert;"
        ],
        "en": [
          "PT-10471 - Improve ajax calls of the plugin;"
        ]
      },
      "2.2.0": {
        "de": [
          "\n            PT-10162 - Die Sprache für den Express-Checkout kann jetzt bei Bedarf individuell eingestellt werden;\n            PT-10335 - Problem mit Bestellnummern mit Prefix in der PayPal Bestellübersicht behoben;\n            PT-10349 - Das Bundesland wird nun beim Express Checkout korrekt gespeichert;\n            PT-10371 - PayPal Dokument Erweiterungen werden nur noch geladen, wenn der richtige Dokumententyp ausgewählt wurde;\n            PT-10373 - Ajax Calls des Plugins verbessert;\n        "
        ],
        "en": [
          "\n            PT-10162 - If required the locale code for the express checkout can now be set individually;\n            PT-10335 - Solved problem with order numbers with prefix in PayPal order overview;\n            PT-10349 - The country state is now saved correctly during express checkout;\n            PT-10371 - PayPal document extensions are only loaded if the correct document type has been selected;\n            PT-10373 - Improve ajax calls of the plugin;\n        "
        ]
      },
      "2.1.3": {
        "de": [
          "\n            PT-7290 - Bestellungen ohne Versandart sind nun nicht mehr möglich;\n            PT-10236 - Adresse des Kunden wird nun wieder korrekt auf der Abschlussseite angezeigt;\n        "
        ],
        "en": [
          "\n            PT-7290 - Orders without shipping method are no longer possible;\n            PT-10236 - Customer's address is now correctly displayed on the finish page again;\n        "
        ]
      },
      "2.1.2": {
        "de": [
          "PT-10176 - Prüfung des Risk-Management-Auschlusses verbessert;"
        ],
        "en": [
          "PT-10176 - Improved risk management exclusion check;"
        ]
      },
      "2.1.1": {
        "de": [
          "\n            PT-9974 - Kompatibilität mit Microsoft Browsern verbessert;\n            PT-10102 - Javascript Fehler behoben, falls PayPal über das Risk-Management ausgeschlossen wurde;\n        "
        ],
        "en": [
          "\n            PT-9974 - Improve compatibility with Microsoft browsers;\n            PT-10102 - Fix javascript error, if PayPal was excluded via risk management;\n        "
        ]
      },
      "2.1.0": {
        "de": [
          "\n            PT-9419 - Der Express-Checkout Button kann nun optional auf Kategorieseiten angezeigt werden. Die Shopware-Einstellung \"Kaufenbutton im Listing anzeigen\" muss aktiviert sein;\n            PT-9754 - Verbessert die Kompatibilität mit anderen Plugins in der PDF-Belegerstellung;\n            PT-9965 - Verhalten Express Checkout Button auf der Registrierungsseite verbessert;\n            PT-9989 - Der Kunde hat nun nicht mehr die Möglichkeit auf der PayPal Seite seine Lieferadresse zu ändern;\n        "
        ],
        "en": [
          "\n            PT-9419 - The Express-Checkout button can now be displayed optionally on category pages. The Shopware setting \"Display buy button in listing\" must be activated;\n            PT-9754 - Improve compatibility with other plugins in PDF document creation;\n            PT-9965 - Improve behaviour of Express Checkout button on registration page;\n            PT-9989 - The customer has no longer the option to change the shipping address on the PayPal page;\n        "
        ]
      },
      "2.0.3": {
        "de": [
          "PT-9990 - Kundenkommentare werden bei Zahlungen über Plus wieder korrekt gespeichert;"
        ],
        "en": [
          "PT-9990 - Customer comments are saved correctly again for payments via Plus;"
        ]
      },
      "2.0.2": {
        "de": [
          "PT-9947 - Übertragung der Sprachcodes für deutschsprachige Länder korrigiert;"
        ],
        "en": [
          "PT-9947 - Fixed language codes transmission for German speaking countries;"
        ]
      },
      "2.0.1": {
        "de": [
          "\n            PT-9838 - Zum Ändern des Bezahlstatus wird nun die Core-Funktionalität genutzt;\n            PT-9942 - Kompatibilität mit Custom Products verbessert;\n            PT-9945 - Abweichende Steuerregeln werden nun korrekt angewendet, wenn mit Express Checkout bezahlt wird;\n        "
        ],
        "en": [
          "\n            PT-9838 - The core functionality is now used to change the payment status;\n            PT-9942 - Improved compatibility with Custom Products;\n            PT-9945 - Different tax rules are now correctly applied when paying with Express Checkout;\n        "
        ]
      },
      "2.0.0": {
        "de": [
          "\n            PT-9704 - Paypal Expresscheckout Konfiguration optimiert\n            PT-9705 - Google Pagespeed Kritieren optimiert\n            PT-9738 - Fehler in der Suche der Paypal Bestellübersicht behoben\n            PT-9752 - Bessere Fehlermeldungen bei der Übermittlung von inkorrekten Adressdaten\n        "
        ],
        "en": [
          "\n            PT-9704 - Optimized Paypal expresscheckout configuration\n            PT-9705 - Optimization for Google pagespeed\n            PT-9738 - Fixed an error in order overview search\n            PT-9752 - Optimized error messages when submitting incorrect address data\n        "
        ]
      },
      "1.1.1": {
        "de": [
          "\n            PT-8708 - Obsoleten Code und Tabellenspalte entfernt;\n            PT-9564 - Kompatibilität mit Safari auf iOS und MacOS verbessert;\n            PT-9710 - Übertragung von Preisen zu PayPal verbessert;\n        "
        ],
        "en": [
          "\n            PT-8708 - Removed obsolete code and table column;\n            PT-9564 - Improved compatibility with Safari on iOS and MacOS;\n            PT-9710 - Improved transmission of prices to PayPal;\n        "
        ]
      },
      "1.1.0": {
        "de": [
          "\n            PT-8708, PT-9707 - Die PayPal Experience Profile Logik wurde durch den Application Context ersetzt. Dadurch entfällt die Bildauswahl für das Logo auf der PayPal-Seite. Dies wird nun über das Händlerkonto bezogen;\n            PT-9269 - Englische Übersetzungen korrigiert;\n            PT-9649 - Nach einer Rückerstattung wird der Status der Bestellung korrekt gesetzt;\n            PT-9710 - Übertragung von Preisen zu PayPal verbessert;\n        "
        ],
        "en": [
          "\n            PT-8708, PT-9707 - The PayPal Experience Profile logic has been replaced by the Application Context. Therefore the image selection for the logo on the PayPal page is no longer necessary. This will now be retrieved via the merchant account;\n            PT-9269 - Fixed English translation;\n            PT-9649 - The status of the order will be set correctly after a refund;\n            PT-9710 - Improved transmission of prices to PayPal;\n        "
        ]
      },
      "1.0.7": {
        "de": [
          "\n            PT-9420 - Ratenzahlung wird nun korrekt nach der Registrierung angezeigt;\n            PT-9542 - Anzeige Fehler beim Zahlungsartwechsel behoben;\n            PT-9590 - Netto/Brutto Behandlung bei Express Checkout verbessert;\n            PT-9596 - Behandlung von 'Steuerfrei für Unternehmen' verbessert;\n            PT-9604 - Versandkosten werden nun bei Nutzung von Plus korrekt zu PayPal übertragen;\n        "
        ],
        "en": [
          "\n            PT-9420 - After registration installments is shown correctly;\n            PT-9542 - Fix display bug on payment method change;\n            PT-9590 - Improve net/gross handling on Express Checkout;\n            PT-9596 - Improve handling of tax free for companies;\n            PT-9604 - Shipping costs are now transferred correctly to PayPal if Plus is used;\n        "
        ]
      },
      "1.0.6": {
        "de": [
          "\n            PT-9367 - Aufgrund des PayPal Risk-Managements werden Custom Products Optionen nicht mehr als einzelne Warenkorb Positionen übertragen. Evtl. Aufschläge werden auf den Produktpreis aufgerechnet;\n            PT-9383 - Workflow mit steuerfreien oder Netto-Bestellungen verbessert;\n            PT-9424 - AGB-Checkbox Validierung verbessert;\n            PT-9546 - Setzen des Ländersprachcodes korrigiert;\n        "
        ],
        "en": [
          "\n            PT-9367 - Due to PayPal's risk management, Custom Products options are no longer transferred as single shopping cart items. Possible surcharges will be added to the product price;\n            PT-9383 - Improve workflow with tax free and net orders;\n            PT-9424 - Improve ToS checkbox validation;\n            PT-9546 - Fix setting of country language code;\n        "
        ]
      },
      "1.0.5": {
        "de": [
          "\n            PT-9318 - Sprache der Buttons für Express Checkout und In-Context richtet sich nun nach der Shopsprache;\n            PT-9366 - Javascript Warnung korrigiert;\n            Datum in der Bestellübersicht korrigiert;\n        "
        ],
        "en": [
          "\n            PT-9318 - The shop language is now considered for the language of the express checkout and in-context buttons;\n            PT-9366 - Fix Javascript warning;\n            Fix date in order overview;\n        "
        ]
      },
      "1.0.4": {
        "de": [
          "PT-9241 - Optimierung von PayPal Bannern und Buttons;"
        ],
        "en": [
          "PT-9241 - Optimize PayPal banners and buttons;"
        ]
      },
      "1.0.3": {
        "de": [
          "\n            PT-9252 - Anzeige des Express Checkout Buttons korrigiert;\n            PT-9285 - Logo für Drittanbieter Zahlungsart im Plus iFrame nun pflegbar;\n            PT-9290 - Einstellungen werden jetzt entfernt, wenn dies bei der Deinstallation ausgewählt wird;\n        "
        ],
        "en": [
          "\n            PT-9252 - Fix displaying of express checkout button;\n            PT-9285 - Logo for third party payment in Plus iFrame method can now be maintained;\n            PT-9290 - If selected on uninstallation, the settings now will be removed;\n        "
        ]
      },
      "1.0.2": {
        "de": [
          "PT-9245 - Bezahldatum wird nun korrekt gesetzt;"
        ],
        "en": [
          "PT-9245 - Date of payment is now set correctly;"
        ]
      },
      "1.0.1": {
        "de": [
          "\n            PT-9236 - PDF Elemente lassen sich nun korrekt editieren;\n            PT-9267 - Benutzt nun minifizierte Javascript Library;\n        "
        ],
        "en": [
          "\n            PT-9236 - PDF elements could now edited correctly;\n            PT-9267 - Now uses minified Javascript library;\n        "
        ]
      },
      "1.0.0": {
        "de": [
          "Erstveröffentlichung;"
        ],
        "en": [
          "First release;"
        ]
      }
    },
    "link": "http://store.shopware.com",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "PayPal",
        "description": "Dieses Modul bietet Ihnen die beliebtesten PayPal-Produkte aus einer Hand: Den PayPal Express-Button, PayPal PLUS, und Banner für die neue PayPal Ratenzahlung. Und das Beste daran: Mit diesem Modul können Sie alle Produkte einzeln oder gleichzeitig in Ihrem Online-Shop nutzen."
      },
      "en": {
        "label": "PayPal",
        "description": "This module offers you the most popular PayPal products from a single source: the PayPal Express button, PayPal PLUS, and banners for the new PayPal Installment Payment. And best of all, this module lets you use all products individually or simultaneously in your online store."
      }
    },
    "inSafeMode": false
  },
  {
    "id": 86,
    "name": "FroshProfiler",
    "label": "Profiler",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": "The Profiler shows many informations like Timing of Events, Template Variables and much more in a simple Toolbar",
    "active": false,
    "added": 1611235396,
    "installed": null,
    "updated": 1611235406,
    "refreshed": 1619219383,
    "author": "Friends of Shopware",
    "copyright": null,
    "license": null,
    "version": "1.4.3",
    "support": null,
    "changes": {
      "1.4.3": {
        "en": [
          "\n            \n                <ul>\n                    <li>Support Shopware 5.7.0</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Support Shopware 5.7.0</li>\n                </ul>\n            \n        "
        ]
      },
      "1.4.2": {
        "en": [
          "\n            \n                <ul>\n                    <li>Fixed Symfony Var-Dump Server</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Symfony VarDump Server behoben</li>\n                </ul>\n            \n        "
        ]
      },
      "1.4.1": {
        "en": [
          "\n            \n                <ul>\n                    <li>Compability to Shopware 5.6</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Kompatiblität zu Shopware 5.6</li>\n                </ul>\n            \n        "
        ]
      },
      "1.4.0": {
        "en": [
          "\n            \n                <ul>\n                    <li>Compability to Shopware 5.6</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Kompatiblität zu Shopware 5.6</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.8": {
        "en": [
          "\n            \n                <ul>\n                    <li>Added escapes to outputs, thanks to @hlohaus</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Escape zu den Ausgaben hinzugefügt, Danke an @hlohaus</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.7": {
        "en": [
          "\n            \n                <ul>\n                    <li>Improved block comments for Dreischild Seo Plugin</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Block Kommentare verbessert für Dreischilds Seo Plugin</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.6": {
        "en": [
          "\n            \n                <ul>\n                    <li>VarDumper improved</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Vardumper Support verbessert</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.5": {
        "en": [
          "\n            \n                <ul>\n                    <li>Cart loading fix</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Warenkorb Laden behoben</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.4": {
        "en": [
          "\n            \n                <ul>\n                    <li>Show cart items in Profiler page</li>\n                    <li>Added backend profiling</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Warenkorb Einträge werden in der Profiler Seite dargestellt</li>\n                    <li>Backend Profiling eingefügt</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.3": {
        "en": [
          "\n            \n                <ul>\n                    <li>Fix issue why the file \"127.0.0.1:9912\" is created in the root dir</li>\n                    <li>Added dumping content to toolbar and detail page, thanks to <a href=\"https://github.com/afoeder\">@afroder</a></li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Die Datei 127.0.0.1:9912 wird nicht mehr im Root des Shop Ordners erstellt</li>\n                    <li>Gedumptes Inhalt wird nun in er Toolbar dargestellt <a href=\"https://github.com/afoeder\">@afroder</a></li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.2": {
        "en": [
          "\n            \n                <ul>\n                    <li>Decorate again the logger, because plugins dont use interfaces</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Der Logger wird nun wieder dekoriert</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.1": {
        "en": [
          "\n            \n                <ul>\n                    <li>Added trace option of services</li>\n                    <li>Added filter to events tab</li>\n                    <li>Fixed php module detection</li>\n                    <li>Fixed Logging decoration, thanks to @afroder</li>\n                    <li>Fixed Subrequest Collecting with async javascript</li>\n                    <li>Moved Subrequest Page to Request / Response</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Services können nun getract werden</li>\n                    <li>Filters events tauchen nun auch in Event tab auf</li>\n                    <li>PHP Module detection korrigiert</li>\n                    <li>Logger decoration behoben @afroder</li>\n                    <li>Subrequest Bearbeitung behoben</li>\n                </ul>\n            \n        "
        ]
      },
      "1.3.0": {
        "en": [
          "\n            \n                <ul>\n                    <li>Added var-dump server</li>\n                    <li>Improved data normalizer for objects</li>\n                    <li>Added stopwatch service to add custom events</li>\n                    <li>Allow users to stop profiling</li>\n                    <li>Advanced js events logging, thanks to @dneustadt</li>\n                    <li>Fixed whitespace problem with whitelistIP, thanks to @JoshuaBehrens</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Var-Dump Server hinzugefügt</li>\n                    <li>Data normalizer für Objekte verbessert</li>\n                    <li>Stopwatch Event eingefügt</li>\n                    <li>Profiling kann nun via Toolbar deaktiviert werden</li>\n                    <li>Erweitertes Javascript Event logging eingebaut, thanks to @dneustadt</li>\n                    <li>Whitespace Problem mit IP Begrenzung behoben, thanks to @JoshuaBehrens</li>\n                </ul>\n            \n        "
        ]
      },
      "1.2.2": {
        "en": [
          "\n            \n                <ul>\n                    <li>Add plugin icon for the plugin manager</li>\n                    <li>Update name to a user friendly notation</li>\n                    <li>Change order of changelog list items</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Plugin Icon hinzugefügt</li>\n                    <li>Plugin.xml Changelog korrigiert</li>\n                </ul>\n            \n        "
        ]
      },
      "1.2.1": {
        "en": [
          "\n            \n                <ul>\n                    <li>Fixed units of query time, added config for dumpSize</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Einheiten behoben in Query Zeit</li>\n                </ul>\n            \n        "
        ]
      },
      "1.2.0": {
        "en": [
          "\n            \n                <ul>\n                    <li>Profiler plugin is now part of Friends of Shopware group. Repository moved to https://github.com/FriendsOfShopware/FroshProfiler</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Der Profiler ist nun Teil von FriendsOfShopware </li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.6": {
        "en": [
          "\n            \n                <ul>\n                    <li>Fixed template name column size, thanks to larsbo <a href=\"https://github.com/shyim/shopware-profiler/issues/64\">#64</a></li>\n                    <li>Profiler reachable also if ip not on whitelist <a href=\"https://github.com/shyim/shopware-profiler/issues/63\">#63</a></li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Template Namen Breite behoben, danke an larsbo <a href=\"https://github.com/shyim/shopware-profiler/issues/64\">#64</a></li>\n                    <li>Profiler nun auch erreichbar ohne IP Whitelist <a href=\"https://github.com/shyim/shopware-profiler/issues/63\">#63</a></li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.5": {
        "en": [
          "\n            \n                <ul>\n                    <li>Fixed Fatal Error while Profiling Closure Event Listener</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Fatal Error bei Closure Events behoben</li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.4": {
        "en": [
          "\n            \n                <ul>\n                    <li>Added new configuration option to log javascript events</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Javascript Event Logging eingefügt</li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.3": {
        "en": [
          "\n            \n                <ul>\n                    <li>Added support for symfony forms collector</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                    <li>Symfony Forms Collector hinzugefügt</li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.2": {
        "en": [
          "\n            \n                <ul>\n                <li>Added overview of used snippets per page</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                <li>Alle benutzen Snippets werden nun dargestellt auf der Seite</li>\n                </ul>\n            \n        "
        ]
      },
      "1.1.1": {
        "en": [
          "\n            \n                <ul>\n                <li>Shopware 5.3 compatibility</li>\n                </ul>\n            \n        "
        ],
        "de": [
          "\n            \n                <ul>\n                <li>Shopware 5.3 kompatiblität</li>\n                </ul>\n            \n        "
        ]
      }
    },
    "link": "https://friendsofshopware.github.io/",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Profiler",
        "description": "Der Profiler zeigt viele Informationen wie Zeiten von Events, Template Variabeln und vieles mehr in einer Toolbar"
      },
      "en": {
        "label": "Profiler",
        "description": "The Profiler shows many informations like Timing of Events, Template Variables and much more in a simple Toolbar"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 87,
    "name": "WundeDcompanionArticleImport",
    "label": "Dropshippers Companion - Article import",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": false,
    "added": 1611405214,
    "installed": null,
    "updated": 1617715474,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": null,
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion - Artikelimport"
      },
      "en": {
        "label": "Dropshippers Companion - Article import"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 90,
    "name": "WundeDcompanionManager",
    "label": "Dropshippers Companion - Manager",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": false,
    "added": 1613921319,
    "installed": null,
    "updated": 1615374190,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": null,
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion - Manager"
      },
      "en": {
        "label": "Dropshippers Companion - Manager"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 93,
    "name": "WundeDcompanionCommitNotifier",
    "label": "Dropshippers Companion - GitLab Service",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": true,
    "added": 1617188577,
    "installed": 1617211124,
    "updated": 1617211124,
    "refreshed": 1619219383,
    "author": "wunderbarmedien",
    "copyright": null,
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": "https://wunderbar-medienagentur.de",
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "Dropshippers Companion - GitLab Service"
      },
      "en": {
        "label": "Dropshippers Companion - GitLab Service"
      }
    },
    "inSafeMode": false
  },
  {
    "id": 94,
    "name": "SwagImportExport",
    "label": "Shopware Import/Export",
    "namespace": "Backend",
    "source": "Community",
    "description": null,
    "active": false,
    "added": 1617715007,
    "installed": null,
    "updated": 1617715010,
    "refreshed": 1619219383,
    "author": "shopware AG",
    "copyright": "Copyright © 2012, shopware AG",
    "license": null,
    "version": "2.10.7",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": null,
    "inSafeMode": false
  },
  {
    "id": 95,
    "name": "VueJsBackendPlugin",
    "label": "VueJs Backend Plugin",
    "namespace": "ShopwarePlugins",
    "source": "",
    "description": null,
    "active": false,
    "added": 1619214443,
    "installed": null,
    "updated": 1619214448,
    "refreshed": 1619219383,
    "author": "Andreas Gerhardt",
    "copyright": null,
    "license": null,
    "version": "1.0.0",
    "support": null,
    "changes": null,
    "link": null,
    "updateVersion": null,
    "updateSource": null,
    "capabilityUpdate": true,
    "capabilityInstall": true,
    "capabilityEnable": true,
    "capabilitySecureUninstall": true,
    "translations": {
      "de": {
        "label": "VueJs Backend Plugin"
      },
      "en": {
        "label": "VueJs Backend Plugin"
      }
    },
    "inSafeMode": false
  }
]
