export default {
  namespaced: true,
  state: {
    instance: null,
    backgroundColor: 'grey lighten-3',
    color: 'grey darken-1',
    hoverColor: 'black',
    dark: false,
    extensions: [],
    extensionGroups: [],
    dense: false,
    small: false,
    divider: {
      show: false,
      inset: false,
      color: ''
    },
  },
  getters: {
    getColor: (state) => state.color,
    getHoverColor: (state) => state.hoverColor,
    getBackgroundColor: (state) => state.backgroundColor,
    getDark: (state) => state.dark,
    getExtensions: (state) => state.extensions,
    getExtensionGroups: (state) => state.extensionGroups,
    getDivider: (state) => state.divider,
    getDense: (state) => state.dense,
    getSmall: (state) => state.small,
    getInstance: (state) => state.instance,
  },
  mutations: {
    setColor: (state, color) => state.color = color,
    setHoverColor: (state, hoverColor) => state.hoverColor = hoverColor,
    setBackgroundColor: (state, backgroundColor) => state.backgroundColor = backgroundColor,
    setDark: (state, dark) => state.dark = dark,
    setShowDivider: (state, show) => state.divider.show = show,
    setDividerColor: (state, color) => state.divider.color = color,
    setDividerInset: (state, inset) => state.divider.inset = inset,
    setDense: (state, dense) => state.dense = dense,
    setSmall: (state, small) => state.small = small,
    setInstance: (state, instance) => state.instance = instance,
    setExtensions: (state, extensions) => {
      state.extensions = extensions

      extensions.forEach(extension => {
        if (extension.group !== undefined) {
          var group = state.extensionGroups.find(extensionGroup => extensionGroup.name === extension.group.name)

          if (group === undefined) {
            extension.group['components'] = [extension]

            state.extensionGroups.push(extension.group)
          } else {
            group.components.push(extension)
          }
        }
      })
    },
  },
}
