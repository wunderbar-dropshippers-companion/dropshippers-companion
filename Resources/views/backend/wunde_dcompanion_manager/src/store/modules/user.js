import SystemUser from '../../data/systemUser'

export default {
  namespaced: true,
  state: {
    user: null,
    users: []
  },
  getters: {
    getUser: (state) => state.user,
    findUser: (state) => (id) => {
      let user = state.users.find(user => user.id = id)

      return user === null || user === undefined
        ? SystemUser
        : user
    }
  },
  mutations: {
    setUser: (state, user) => state.user = user,
  },
  actions: {
    load({commit, getters, rootGetters}) {
      // if (getters['getUser'] === null && !_.isEqual(getters['getUser'], SystemUser)) {
      if (getters['getUser'] === null && !_.isEqual(getters['getUser'], SystemUser)) {
        return axios.get(rootGetters.getUrl('getBackendUser'))
          .then(response => {
            let success = response.data.hasOwnProperty('success') ? response.data.success : false;
            let user = response.data.hasOwnProperty('user') ? response.data.user : null

            if (success && user !== null) {
              commit('setUser', user)
              return user
            } else {
              throw new Error()
            }
          })
          .catch(() => {
            console.warn('User not found! Using dummy user')
            commit('setUser', SystemUser)
            return SystemUser
          })
      } else {
        return getters['getUser']
      }
    },
  }
}
