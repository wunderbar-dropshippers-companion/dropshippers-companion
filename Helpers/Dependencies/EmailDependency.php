<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 14/12/2020 17:01
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Helpers\Dependencies;


use DirectoryIterator;
use Doctrine\ORM\Query\Expr;
use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Mail\Mail;
use WundeDcompanion\Helpers\Email\Template;
use WundeDcompanion\Services\MailService;

/**
 * Class EmailDependency
 * @package WundeDcompanion\Helpers\Dependencies
 */
class EmailDependency
{

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var Expr $expr
     */
    private $expr;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * EmailDependency constructor.
     */
    public function __construct()
    {
        $di = Shopware()->Container();

        $this->em = $di->get('models');
        $this->logger = $di->get('pluginlogger');

        $this->expr = $this->em->getExpressionBuilder();
    }

    /**
     * @return Mail[]
     */
    private function getEmailTemplates()
    {
        try {
            /** @var Mail[] $templates */
            $templates = $this->em->getRepository(Mail::class)
                ->createQueryBuilder('mail')
                ->select(['mail'])
                ->indexBy('mail', 'mail.name')
                ->getQuery()
                ->getResult();

            return $templates;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return [];
    }

    /**
     * @return Mail[]
     */
    public function registerEmailTemplates()
    {
        $path = __DIR__ . '/../../Email';

        $registeredTemplates = $this->getEmailTemplates();

        try {
            if (file_exists($path)) {
                $iterator = new DirectoryIterator($path);

                /** @var Template[] $templates */
                $templates = [];

                foreach ($iterator as $fileInfo)
                {
                    if ($fileInfo->isFile()) {
                        $basename = $fileInfo->getBasename('.' .$fileInfo->getExtension());

                        if (!array_key_exists($basename, $templates)) {
                            $templates[$basename] = new Template();
                        }

                        $templates[$basename]->setValue($fileInfo);
                    }
                }

                $emailTemplates = [];

                foreach ($templates as $template)
                {
                    $emailTemplate = $template->register($registeredTemplates);

                    if (!is_null($emailTemplate)) {
                        $emailTemplates[] = $emailTemplate;
                    }
                }

                return $emailTemplates;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        return [];
    }

    /**
     * @return bool
     */
    public function unregisterEmailTemplates()
    {
        $path = __DIR__ . '/../../Email';

        $registeredTemplates = $this->getEmailTemplates();

        $success = false;

        try {
            if (file_exists($path)) {
                $iterator = new DirectoryIterator($path);

                foreach ($iterator as $fileInfo)
                {
                    if ($fileInfo->isFile()) {
                        $extension = $fileInfo->getExtension();
                        $basename = Template::PREFIX . strtoupper($fileInfo->getBasename('.' . $extension));

                        if (array_key_exists($basename, $registeredTemplates)) {
                            if ($basename === 'DC_UNINSTALL') {
                                $mailService = new MailService();
                                $mailService->sendUninstallMail($registeredTemplates[$basename]);
                            }

                            $this->em->remove($registeredTemplates[$basename]);
                            unset($registeredTemplates[$basename]);
                        }
                    }
                }

                $success = true;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            $success = false;
        }

        return $success;
    }
}