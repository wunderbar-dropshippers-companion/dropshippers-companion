//{namespace name=backend/wunde_dcompanion_order/main}
Ext.define('Shopware.apps.Order.view.list.Dropship', {

    extend:'Ext.grid.Panel',
    alias:'widget.order-dropship-grid',
    stateful: true,
    stateId: 'order-dropship-grid',
    minHeight: 90,
    autoScroll:true,

    snippets: {
        wholesaler: '{s name=column/wholesaler}AWholesaler{/s}',
        articleNumber: '{s name=column/article_number}Article number{/s}',
        articleName: '{s name=column/article_name}Article name{/s}',
        dropshipDate: '{s name=column/dropship_date}Dropship date{/s}',
        dropshipStatus: '{s name=column/dropship_status}Status{/s}',
        dropshipStatusMessage: '{s name=column/dropship_status_message}Status message{/s}',
        dropshipTrackingCarrier: '{s name=column/dropship_tracking_carrier}Carrier{/s}',
        dropshipTrackingCode: '{s name=column/dropship_tracking_code}Tracking code{/s}',
    },

    viewConfig: {
        enableTextSelection: true
    },

    initComponent: function() {
        var me = this;

        me.registerEvents();
        me.columns = me.getColumns();
        me.callParent(arguments);
    },

    registerEvents: function() {
        this.addEvents(
            'openArticle',
        );
    },

    getColumns:function () {
        var me = this;

        return [
            {
                header: me.snippets.wholesaler,
                dataIndex: 'dc_name_long',
                width: 140,
                renderer: function(value) {
                    return value.length
                        ? value
                        : '---';
                }
            },
            {
                header: me.snippets.articleNumber,
                dataIndex: 'articleNumber',
                width: 150,
            },
            {
                header: me.snippets.articleName,
                dataIndex: 'articleName',
                width: 360,
            },
            {
                header: me.snippets.dropshipDate,
                dataIndex: 'dc_dropship_date',
                align: 'center',
                width: 130,
                renderer: function(value) {
                    return value.length
                        ? value
                        : '---';
                }
            },
            {
                header: me.snippets.dropshipStatus,
                dataIndex: 'dc_dropship_status',
                flex: 2,
                renderer: function(value) {
                    if (parseInt(value) === -100) {
                        return 'Fehler in der Übermittlung des Dropshipping-Auftrages. Bitte kontrollieren Sie Ihre Mails.';
                    } else if (parseInt(value) === 0) {
                        return 'Dropshipping-Auftrag ist zur Übermittlung an Großhandelspartner vorgemerkt.';
                    } else if (parseInt(value) === 100) {
                        return 'Dropshipping-Auftrag an Großhandelspartner übermittelt.';
                    } else if (parseInt(value) === 200) {
                        return 'Dropshipping-Auftrag Tracking-Informationen vorhanden. Bitte kontrollieren Sie Ihre E-Mails.';
                    } else {
                        return '---';
                    }
                }
            },
            {
                header: me.snippets.dropshipStatusMessage,
                dataIndex: 'dc_dropship_status_message',
                flex: 2,
                renderer: function(value) {
                    return value.length ? value : '---';
                }
            },
            {
                header: me.snippets.dropshipTrackingCarrier,
                dataIndex: 'dc_dropship_tracking_carrier',
                align: 'center',
                width: 130,
                renderer: me.linkRenderer
            },
            {
                header: me.snippets.dropshipTrackingCode,
                dataIndex: 'dc_dropship_tracking_code',
                align: 'center',
                width: 130,
                renderer: me.linkRenderer
            },
            {
                xtype: 'actioncolumn',
                width: 30,
                align: 'center',
                items: [
                    {
                        iconCls: 'sprite-inbox',
                        action: 'openArticle',
                        tooltip: 'open article',
                        handler: function (view, rowIndex, colIndex, item) {
                            var store = view.getStore(),
                                record = store.getAt(rowIndex);

                            me.fireEvent('openArticle', record);
                        },
                        getClass: function (value, metadata, record) {
                            if (!record.get('articleId') || record.get('mode') !== 0) {
                                return 'x-hidden';
                            }
                        }
                    }
                ]
            }
        ];
    },

    linkRenderer: function (value) {
        try {
            var data = JSON.parse(value);

            var links = Ext.Array.map(data, function (item) {
                return item.hasOwnProperty('carrier')
                    ? '<a href="'+item.url+'" target="_blank" title="'+item.carrier+'">'+item.value+'</a>'
                    : '<a href="'+item.url+'" target="_blank">'+item.value+'</a>';
            })

            return links.length ? links.join(', ') : '---'
        } catch (error) {
            return '---'
        }
    }
});
