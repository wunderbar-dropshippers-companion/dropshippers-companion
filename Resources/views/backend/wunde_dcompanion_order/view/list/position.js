//{namespace name=backend/wunde_dcompanion_order/main}
//{block name="backend/order/view/list/position"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionOrder.view.list.Position', {

    override: 'Shopware.apps.Order.view.list.Position',

    dcSnippets: {
        source: '{s name=source}Source{/s}',
        dcPrice: '{s name=dc_price}DC price{/s}',
        dcInstock: '{s name=dc_instock}DC instock{/s}',
    },

    getColumns: function () {
        var me = this;
        var columns = me.callOverridden(arguments);

        me.dcDropshipActive = {
            header: 'Dropship',
            dataIndex: 'dc_dropship_active',
            width: 60,
            tooltip: 'Dropshipping article',
            align: 'center',
            renderer: function(value, item, record) {
                if (value === 'true' || value === 'false') {
                    return '';
                }

                return record.get('dc_dropship_active');
            }
        };

        me.wholesalerCombobox = {
            header: me.dcSnippets.source,
            id: 'dcSourceField',
            width: 200,
            dataIndex: 'dc_name_long',
            renderer: function(value) {
                return value.length ? value : 'Mein Shop'
            }
        };

        me.dcStock = {
            header: me.dcSnippets.dcInstock,
            dataIndex: 'dc_stock',
            flex:1,
            align: 'center',
        };

        me.dcPurchasePrice = {
            header: me.dcSnippets.dcPrice,
            dataIndex: 'dc_purchase_price',
            width: 120,
            align: 'center',
            renderer: me.priceColumn
        };

        Ext.Array.insert(columns, 0,  [me.dcDropshipActive]);

        return Ext.Array.insert(columns, 9,  [
            me.dcStock,
            me.dcPurchasePrice,
            me.wholesalerCombobox
        ]);
    },
});
//{/block}