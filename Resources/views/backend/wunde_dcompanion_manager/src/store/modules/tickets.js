import { db } from '../../plugins'

import PriorityModule from './ticket/priorities'
import StateModule from './ticket/states'
import HeaderModule from './ticket/headers'

import DefaultTicket from '../../data/tickets'

export default {
  namespaced: true,
  state: {
    tickets: []
  },
  getters: {
    getTickets: (state) => state.tickets,
    getDefaultTicket: () => DefaultTicket
  },
  mutations: {
    setTickets: (state, tickets) => state.tickets = tickets
  },
  actions: {
    delete({commit, rootGetters}, ticket) {
      if (ticket.hasOwnProperty('uid')) {
        let uid = ticket.uid

        return db.collection('tickets')
          .doc(uid)
          .delete()
      } else {
        return false
      }
    },
    deleteMultiple({commit, rootGetters}, tickets = []) {
      let batch = db.batch()

      tickets.forEach(ticket => {
        if (ticket.hasOwnProperty('uid')) {
          let reference = db.collection('tickets').doc(ticket.uid)
          batch.delete(reference)
        }
      })

      return batch.commit()
    },
    clone({dispatch}, ticket) {
      delete ticket.uid
      delete ticket.id

      return dispatch('create', ticket)
    },
    create({commit, rootGetters}, data = null) {
      if (data === null) {
        data = DefaultTicket

        data.rating = Math.floor(Math.random() * 10) / 2
      }

      var user = rootGetters['user/getUser']

      if (user !== null && user !== undefined) {
        data.creatorId = user.id
      }

      data['shop'] = rootGetters['shop/getShop'] !== null
        ? rootGetters['shop/getShop'].ref
        : null

      if (data['shop'] !== null) {
        return db.collection('tickets')
          .add(data)
      }
    },
    listen({commit, rootGetters}, shop) {
      db.collection('tickets')
        .where('shop', '==', shop.ref)
        .onSnapshot(querySnapshot => {
          commit('setTickets', querySnapshot.docs)
        })
    },
    load({commit, rootGetters, dispatch}, shop) {
      var user = rootGetters['user/getUser']
      user.email = 'a.gerhardt1987@gmail.com'

      if (shop !== null) {
        return db.collection('tickets')
          .where('shop', '==', shop.ref)
          .get()
          .then(querySnapshot => {
            commit('setTickets', querySnapshot.docs)
            dispatch('listen', shop)
            return querySnapshot.docs
          })
      } else {
        return []
      }
    },
    save({commit, rootGetters, dispatch}, ticket) {
      if (ticket.hasOwnProperty('uid')) {
        let uid = ticket.uid

        return db.collection('tickets')
          .doc(uid)
          .set(ticket)
      }

      return false
    },
  },
  modules: {
    priorities: PriorityModule,
    states: StateModule,
    headers: HeaderModule
  }
}
