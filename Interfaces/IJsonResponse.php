<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 17/08/2020 15:51
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Interfaces;


/**
 * Interface IJsonResponse
 * @package WundeDcompanion\Interfaces
 */
interface IJsonResponse
{
    /**
     * @return string
     */
    public function getContent();

    /**
     * @return int
     */
    public function getStatusCode();

    /**
     * @return float
     */
    public function getTotalTime();
}