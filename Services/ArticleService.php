<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 20/09/2020 04:46
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use Exception;
use Shopware\Components\Logger;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Attribute\Article as ArticleAttribute;
use Shopware\Models\Order\Detail as OrderDetail;
use WundeDcompanion\Models\Wholesaler\Repository;

/**
 * Class ArticleService
 * @package WundeDcompanion\Services
 */
class ArticleService
{

    /**
     * @var Repository $repository
     */
    private $repository;

    /**
     * @var ModelManager $em
     */
    private $em;

    /**
     * @var LoggerService $logger
     */
    private $logger;

    /**
     * ArticleService constructor.
     * @param Repository $repository
     * @param ModelManager $em
     * @param LoggerService $logger
     */
    public function __construct(Repository $repository, ModelManager $em, LoggerService $logger)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @param string $ordernumber
     * @param string $articleDetailId
     * @return bool
     */
    public function updateAll($ordernumber = null, $articleDetailId = null)
    {
        try {
            $wholesalers = $this->repository->findAll();

            /** @var ArticleAttribute[] $articleAttributes */
            $articleAttributes = $this->em->getRepository(ArticleAttribute::class)->findAll();

            foreach ($wholesalers as $wholesaler)
            {
                $reponse = is_null($ordernumber)
                    ? $wholesaler->getClient()->getAll()
                    : $wholesaler->getClient()->getAll($ordernumber);

                if ($reponse->getStatusCode() === 200) {
                    $wholesalerArticles = json_decode($reponse->getContent(), true);

                    $ordernumbers = array_keys($wholesalerArticles);

                    if (!empty($ordernumbers)) {

                        $upperShortname = ucfirst(strtolower($wholesaler->getShortname()));

                        foreach ($articleAttributes as $articleAttribute)
                        {
                            if (!is_null($articleDetailId) && $articleAttribute->getArticleDetailId() !== (int)$articleDetailId) {
                                continue;
                            }

                            $getDcOrdernumber = 'getDc'.$upperShortname.'Ordernumber';

                            if (
                                method_exists($articleAttribute,'getDc' . $upperShortname . 'Ordernumber') &&
                                method_exists($articleAttribute,'setDc' . $upperShortname . 'Instock') &&
                                method_exists($articleAttribute,'setDc' . $upperShortname . 'PriceRecommended') &&
                                method_exists($articleAttribute,'setDc' . $upperShortname . 'PurchasingPrice') &&
                                method_exists($articleAttribute,'setDc' . $upperShortname . 'Articlename') &&
                                method_exists($articleAttribute,'setDc' . $upperShortname . 'Description') &&
                                in_array($articleAttribute->$getDcOrdernumber(), $ordernumbers)
                            ) {
                                $setInstock = 'setDc'.$upperShortname.'Instock';
                                $setPriceRecommended = 'setDc'.$upperShortname.'PriceRecommended';
                                $setPurchasingPrice = 'setDc'.$upperShortname.'PurchasingPrice';
                                $setArticlename = 'setDc'.$upperShortname.'Articlename';
                                $setDescription = 'setDc'.$upperShortname.'Description';

                                $wholesalerArticle = $wholesalerArticles[$articleAttribute->$getDcOrdernumber()];

                                if (array_key_exists('quantity', $wholesalerArticle)) {
                                    $articleAttribute->$setInstock($wholesalerArticle['quantity']);
                                }

                                if (array_key_exists('purchasePrice', $wholesalerArticle)) {
                                    $articleAttribute->$setPurchasingPrice((float)number_format($wholesalerArticle['purchasePrice'], 2));
                                }

                                if (array_key_exists('priceRecommended', $wholesalerArticle)) {
                                    $articleAttribute->$setPriceRecommended((float)number_format($wholesalerArticle['priceRecommended'], 2));
                                }

                                if (array_key_exists('name', $wholesalerArticle)) {
                                    $articleAttribute->$setArticlename($wholesalerArticle['name']);
                                }

                                if (array_key_exists('description', $wholesalerArticle)) {
                                    $articleAttribute->$setDescription($wholesalerArticle['description']);
                                }
                            }
                        }

                        $this->em->flush();
                    }
                }
            }

            return true;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e);
        }

        return false;
    }
}