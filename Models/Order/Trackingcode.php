<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 09/10/2020 13:31
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Order;

use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\Order\Detail as Position;

/**
 * @ORM\Entity()
 * @ORM\Table(name="s_dcompanion_trackingcodes")
 */
class Trackingcode extends ModelEntity
{

    /**
     * Primary Key - autoincrement value
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $code
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @var int $positionId
     * @ORM\Column(name="position_id", type="integer", nullable=true)
     */
    private $positionId = 0;

    /**
     * @var Position $position
     *
     * @ORM\ManyToOne(targetEntity="Shopware\Models\Order\Detail", inversedBy="trackingcodes")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     */
    protected $position;

    /**
     * @var int $carrierId
     * @ORM\Column(name="carrier_id", type="integer", nullable=true)
     */
    private $carrierId = 0;

    /**
     * @var Carrier $carrier
     *
     * @ORM\ManyToOne(targetEntity="WundeDcompanion\Models\Order\Carrier")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     */
    protected $carrier;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return Position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param Position $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * @param Carrier $carrier
     */
    public function setCarrier($carrier)
    {
        $this->carrier = $carrier;
    }

    /**
     * @return int
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * @return int
     */
    public function getCarrierId()
    {
        return $this->carrierId;
    }
}