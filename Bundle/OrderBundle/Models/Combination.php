<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 23:57
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle\OrderBundle\Models;


use Shopware\Models\Order\Detail as Position;
use Shopware\Models\Order\Order;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Combination
 * @package WundeDcompanion\Bundle\OrderBundle\Models
 */
class Combination
{
    /**
     * @var Similary[] $similaries
     */
    private $similaries = [];

    /**
     * @var Order $order
     */
    private $order;

    /**
     * @var OutputInterface|null $output
     */
    private $output;

    /**
     * @var int $uid
     */
    private $uid = 0;

    /**
     * @var array $expectedQuantities
     */
    private $expectedQuantities = [];

    /**
     * Combination constructor.
     * @param Order $order
     * @param OutputInterface|null $output
     */
    public function __construct(Order $order, ?OutputInterface $output)
    {
        $this->order = $order;
        $this->output = $output;
    }

    /**
     * @return Similary[]
     */
    public function getSimilaries(): array
    {
        return $this->similaries;
    }

    /**
     * @param Similary[] $similaries
     * @return Combination
     */
    public function setSimilaries(array $similaries): Combination
    {
        $this->similaries = $similaries;
        $this->setExpectedQuantities();
        $this->distributeQuantitiesToDealers();
//        $this->removeUnusedSimilaries();

        return $this;
    }

    /**
     *
     */
    private function setExpectedQuantities()
    {
        foreach ($this->similaries as $similary)
        {
            if (!array_key_exists($similary->getArticleDetailId(), $this->expectedQuantities)) {
                $this->expectedQuantities[$similary->getArticleDetailId()] = $similary->getQuantity();
            }
        }
    }

    /**
     * @return int
     */
    public function getUid(): int
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     *
     */
    private function removeUnusedSimilaries()
    {
        $articleDetailSimilaries = [];

        foreach ($this->getSimilaries() as $similary)
        {
            $articleDetailSimilaries[$similary->getArticleDetailId()][] = $similary;
        }

        /** @var Similary[] $similaries */
        foreach ($articleDetailSimilaries as &$similaries)
        {
            usort($similaries, function (Similary $a, Similary $b) {
                return $a->getTotalPurchasePrice() <=> $b->getTotalPurchasePrice();
            });

            if (count($similaries) > 1) {
                $expectedQuantity = $this->expectedQuantities[$similaries[0]->getArticleDetailId()];

                foreach ($similaries as $key => $similary)
                {
                    $expectedQuantity -= $similary->getQuantity();

                    if ($expectedQuantity <= 0) {
//                        $similaries = array_splice($similaries, 0, $key + 1);
                    }
                }
            }
        }

        $this->similaries = [];

        /** @var Similary[] $similaries */
        foreach ($articleDetailSimilaries as &$similaries)
        {
            foreach ($similaries as $similary)
            {
                $this->similaries[] = $similary;
            }
        }
    }

    /**
     *
     */
    private function distributeQuantitiesToDealers()
    {
        $articleDetailSimilaries = [];

        foreach ($this->similaries as $similary)
        {
            $articleDetailSimilaries[$similary->getArticleDetailId()][] = $similary;
        }

        $distributedSimilaries = [];

        /** @var Similary[] $similaries */
        foreach ($articleDetailSimilaries as &$similaries)
        {
            usort($similaries, function (Similary $a, Similary $b) {
                return $a->getTotalPurchasePrice() > $b->getTotalPurchasePrice();
            });

            if (count($similaries) > 1 && $similaries[0]->getInstock() < $similaries[0]->getExpectedQuantity()) {
                $expectedQuantity = $similaries[0]->getExpectedQuantity();
                $providedInstock = $similaries[0]->getInstock();


                foreach ($similaries as $key => &$similary)
                {
                    if ($key) {
                        $difference = $expectedQuantity - $providedInstock;

                        if ($similary->getInstock() >= $difference) {
                            $similary->setQuantity($difference);

                            if (count($similaries) > $key + 1) {
                                array_splice($similaries, $key + 1);
                            }
                        } else {
                            $providedInstock += $similary->getInstock();
                        }
                    } else {
                        $similary->setQuantity($similary->getInstock());
                    }

                    $distributedSimilaries[] = $similary;
                }
            } else {
                foreach ($similaries as $similary)
                {
                    $distributedSimilaries[] = $similary;
                }
            }
        }

        $this->similaries = $distributedSimilaries;
    }

    /**
     * @param Similary $similary
     * @return Combination
     */
    public function addSimilary(Similary $similary): Combination
    {
        $this->similaries[] = $similary;
        return $this;
    }

    /**
     * @return float|int
     */
    public function getPrice()
    {
        return array_sum(array_map(function($similary) {
            return $similary->getPrice() * $similary->getQuantity();
        }, $this->similaries));
    }

    /**
     * @return float|int
     */
    public function getPurchasePrice()
    {
        return array_sum(array_map(function($similary) {
            return $similary->getTotalPurchasePrice();
        }, $this->similaries));
    }

    /**
     * @return float|int
     */
    public function getTotalPurchasePrice()
    {
        return $this->getPurchasePrice() + $this->getShippingCosts();
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        $hashes = array_map(function($similary) {
            return $similary->getHash();
        }, $this->getSimilaries());

        sort($hashes);

        return md5(serialize($hashes));
    }

    /**
     * @return float|int
     */
    public function getShippingCosts()
    {
        $shippingCosts = [];

        foreach ($this->similaries as $similary)
        {
            $context = is_null($similary->getWholesaler()) ? 'shop' : $similary->getWholesaler()->getShortname();

            if (!array_key_exists($context, $shippingCosts)) {
                $shippingCosts[$context] = $similary->getShippingCost();
            }
        }

        return count($shippingCosts)
            ? array_sum($shippingCosts)
            : 0;
    }

    /**
     * @return array
     */
    public function getExpectedQuantities(): array
    {
        $expected = [];

        if ($this->order instanceof Order) {
            /** @var Position $position */
            foreach ($this->order->getDetails()->getValues() as $position)
            {
                $expected[$position->getId()] = $position->getQuantity();
            }
        }

        return $expected;
    }

    /**
     * @return array
     */
    public function getProvidedQuantities(): array
    {
        $provided = [];

        foreach ($this->getSimilaries() as $similary)
        {
//            if (array_key_exists($similary->getPositionId(), $provided)) {
//                $provided[$similary->getPositionId()] += (int)$similary->getInstock();
//            } else {
//                $provided[$similary->getPositionId()] = (int)$similary->getInstock();
//            }
            if (array_key_exists($similary->getPositionId(), $provided)) {
                $provided[$similary->getPositionId()] += (int)$similary->getQuantity();
            } else {
                $provided[$similary->getPositionId()] = (int)$similary->getQuantity();
            }
        }

        return $provided;
    }

    /**
     * @return int
     */
    public function getPackageFactor()
    {
        return $this->getPackageCount() > 1 ? $this->getPackageCount() / 10 + 1 : 1;
    }

    /**
     * @return bool
     */
    public function allArticlesAvailable(): bool
    {
        $expectedQuantities = $this->getExpectedQuantities();
        $providedQuantities = $this->getProvidedQuantities();

        ksort($expectedQuantities);
        ksort($providedQuantities);

        $subtracted = array_map(function($x, $y) { return $y-$x; }, $expectedQuantities, $providedQuantities);

        return is_array($subtracted) && !empty($subtracted)
            ? min($subtracted) >= 0
            : false;
    }

    /**
     * @return int
     */
    public function getPackageCount(): int
    {
        $packages = [];

        foreach ($this->similaries as $similary)
        {
            $context = is_null($similary->getWholesaler()) ? 'shop' : $similary->getWholesaler()->getShortname();

            $packages[$context] = array_key_exists($context, $packages)
                ? $packages[$context]++
                : 1;
        }

        return count($packages)
            ? array_sum($packages)
            : 0;
    }

    protected function clearConsole()
    {
        echo sprintf("\033\143");
    }

    /**
     * @param mixed $value
     * @param string $color
     */
    protected function print($value, $color = 'info')
    {
        if (!is_null($this->output)) {
            $this->output->writeln('<'.$color.'>'.print_r($value, true).'</'.$color.'>');
        } else {
            echo "\e[4m\033[31mOUTPUTINTERFACE NOT FOUND!\033\e[0m" . PHP_EOL . PHP_EOL;
            echo print_r($value, true) . PHP_EOL;
        }
    }

    /**
     * @return array
     */
    public function __debugInfo(): array
    {
        return [
            'id' => $this->getUid(),
//            'count' => count($this->similaries),
            'packages' => $this->getPackageCount(),
            'allVailable' => $this->allArticlesAvailable(),
            'similaries' => $this->getSimilaries(),
//            'dealer' => array_map(function ($similary) {
//                return !is_null($similary->getWholesaler())
//                    ? $similary->getWholesaler()->getLongname()
//                    : Shopware()->Config()->get('shopName');
//            }, $this->getSimilaries()),
            'prices' => [
                'shipping' => $this->getShippingCosts(),
                'salePrice' => $this->getPrice(),
                'totalPurchasePrice' => $this->getTotalPurchasePrice(),
                'purchasePrice' => $this->getPurchasePrice(),
            ]
        ];
    }
}