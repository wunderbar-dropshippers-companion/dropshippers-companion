<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 29/09/2020 23:49
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Models\Wholesaler\Config;


/**
 * Class Shipping
 * @package WundeDcompanion\Models\Wholesaler\Config
 */
class Shipping
{

    /**
     * @var float $cost
     */
    private $cost = 0.0;

    /**
     * @var float $excemptionLimit
     */
    private $excemptionLimit = 0.0;

    /**
     * Shipping constructor.
     * @param float $cost
     * @param float $excemptionLimit
     */
    public function __construct($cost, $excemptionLimit)
    {
        $this->cost = $cost;
        $this->excemptionLimit = $excemptionLimit;
    }

    /**
     * @return float
     */
    public function cost()
    {
        return $this->cost;
    }

    /**
     * @return float
     */
    public function excemptionLimit()
    {
        return $this->excemptionLimit;
    }
}