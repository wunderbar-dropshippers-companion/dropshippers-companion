<?php
/**
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 20/08/2020 23:24
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Services;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Shopware\Components\Model\ModelEntity;

/**
 * Class SchemaService
 * @package WundeDcompanion\Services
 */
class SchemaService
{
    /**
     * @var Connection $connection
     */
    private $connection;

    /**
     * @var AbstractSchemaManager $schemaManager
     */
    private $schemaManager;

    /**
     * SchemaService constructor.
     */
    public function __construct()
    {
        $this->connection = Shopware()->Models()->getConnection();
        $this->schemaManager = $this->connection->getSchemaManager();
    }

    /**
     * @param array $tables
     * @return bool
     */
    public function tablesExist($tables = [])
    {
        return $this->schemaManager->tablesExist($tables);
    }


    /**
     * @param string $table
     * @param array $values
     * @param string $prefix
     * @return array
     */
    public function castValues($table, $values, $prefix = '')
    {
        if (is_string($table) && is_array($values) && $this->tablesExist([$table])) {
            $columns = $this->schemaManager->listTableColumns($table);

            foreach ($columns as $column)
            {
                $type = $column->getType()->getName();
                $name = $column->getName();

                foreach ($values as $key => &$value)
                {
                    $key = $prefix . $key;
                    if ($key === $name) {
                        switch ($type) {
                            case 'integer':
                                $value = (int)$value;
                                break;
                            case 'float':
                                $value = (float)$value;
                                break;
                            default:
                                $value = (string)$value;
                        }
                    }
                }
            }
        }

        return $values;
    }

    /**
     * @param ModelEntity $entity
     * @return string
     */
    public function getTableNameFromEntity($entity)
    {
        $meta = Shopware()->Models()->getClassMetadata($entity);

        return !is_null($meta)
            ? $meta->getTableName()
            : null;
    }

    /**
     * @param string|ModelEntity $table
     * @param array $columns
     * @return bool
     */
    public function columnsExist($table, $columns = [])
    {
        if ($table instanceof ModelEntity) {
            $table = $this->getTableNameFromEntity($table);
        }

        if ($this->tablesExist([$table])) {
            $tableColumns = $this->schemaManager->listTableColumns($table);

            foreach ($columns as $column)
            {
                if (!array_key_exists($column, $tableColumns)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}