/*
 * @project Dropshippers Companion EXP
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 07/09/2020 09:55
 *
 * @supplier Dropshippers Companion
 * @copyright 2020 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

//{namespace name=backend/wunde_dcompanion_article/main}
//{block name="backend/article/view/variant/detail"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.WundeDcompanionArticle.view.variant.PickwareDetail', {
    override: 'Shopware.apps.Article.view.variant.Detail',

    requires: [
        'Shopware.apps.WundeDcompanionArticle.view.detail.List'
    ],

    dcSnippets: {
        dcWholesaler: '{s name=dc/wholesalers}DC Wholesalers{/s}'
    },

    initComponent: function() {
        var me = this;

        me.callParent(arguments);

        me.width = '80%';
        me.height = '90%';

        me.formPanel.insert(0, me.getWholesalerFieldSet());
    },

    getWholesalerFieldSet: function () {
        var me = this;

        var store = Ext.create('Shopware.apps.WundeDcompanionArticle.store.Wholesaler');

        store.getProxy().setExtraParam('detailId', me.record.get('id'));

        var grid = Ext.create('Shopware.apps.WundeDcompanionArticle.view.detail.List', {
            store: store.load(),
            height: '100%',
            article: me.record,
            detailId: me.record.get('id'),
            variant: true,
            region: 'center',
            border: false,
            forceFit: false,
            minHeight: 150,
        })

        return Ext.create('Ext.form.FieldSet', {
            title: me.dcSnippets.dcWholesaler,
            layout: 'anchor',
            margin: '15 0 15',
            defaults: {
                anchor: '100%'
            },
            items: [
                grid
            ]
        });
    }
});
//{/block}