//{namespace name=backend/wunde_dcompanion_article/main}
//{block name="backend/article/controller/detail"}
Ext.define('Shopware.apps.WundeDcompanionArticle.controller.Detail', {
    override: 'Shopware.apps.Article.controller.Detail',

    dcSnippets: {
        hint: '{s name=hint}hint{/s}',
        abort: '{s name=abort}Abort{/s}',
        close: '{s name=close}Close{/s}',
        error: '{s name=error}Error{/s}',
        success: '{s name=success}Success{/s}',
        wholesalerActivateHint: '{s name=wholesaler/activate/hint}You have to activate the wholesaler to be able to edit it!{/s}',
        wholesalerActivate: '{s name=wholesaler/activate}Activate wholesaler{/s}',
        wholesalerSaved: '{s name=wholesaler/saved}Saved [0]{/s}',
        overwrite: '{s name=overwrite}Overwrite{/s}',
        overridePricePermanentlyHint: '{s name=override/price/permanently/hint}Thus the purchase price is permanently overwritten by the purchase price of [0] with each update!{/s}',
        overrideNamePermanentlyHint: '{s name=override/name/permanently/hint}Thereby the article name and the article description is permanently overwritten by the data of [0] with each update!{/s}',
        overridePermanently: '{s name=override/permanently}Permanently overwrite{/s}',
        overwriteReallyQuestionTitle: '{s name=overwrite/really/question/title}Really overwrite?{/s}',
        overwriteNameReallyQuestionMessage: '{s name=overwrite/name/really/question/message}Now, if you click on save the item name and the item description will be overwritten permanently!{/s}',
        overwritePriceReallyQuestionMessage: '{s name=overwrite/price/really/question/message}If you now click on "Overwrite", the purchase price is transferred to the master data. The old purchase price will be overwritten and after clicking on "Save article" it will be permanently transferred.{/s}',
        saveArticleHint: '{s name=save/article/hint}You must save the article for the data to be transferred!{/s}',
        wholesalerHasNoDescription: '{s name=wholesaler/has_no_description}The wholesaler [0] did not provide a description for this item. Therefore the current description is not overwritten!{/s}',
        deleteArticleDataReally: '{s name=delete/article/wholesaler/data/really}Do you really want to delete the [0] data of this article irrevocably?{/s}',
        deleteIrrevocably: '{s name=delete/irrevocably}Delete irrevocably{/s}',
        deleteDataFailure:'{s name=delete/data/failure}Data could not be deleted!{/s}',
        deleteDataSuccess:'{s name=delete/data/success}"Data successfully deleted{/s}'
    },

    init: function () {
        var me = this;

        me.refs.push({
            ref:'wholesalerGrid', selector:'multi-edit-wholesaler-grid'
        })

        me.control({
            'multi-edit-wholesaler-grid': {
                setActive: me.onSetActive,
                setPermanentlyOverridePurchasePrice: me.onSetPermanentlyOverridePurchasePrice,
                setPermanentlyOverrideNameAndDescription: me.onSetPermanentlyOverrideNameAndDescription,
                editWholesaler: me.onEditWholesaler,
                saveWholesaler: me.onSaveWholesaler,
                updateArticleNameAndDescription: me.onUpdateArticleNameAndDescription,
                updatePurchasePrice: me.onUpdatePurchasePrice,
                deleteData: me.onDeleteData,
                refresh: me.onRefresh
            }
        });

        me.callParent(arguments);
    },

    onRefresh: function(store, variant) {
        var me = this;
        store.getProxy().setExtraParam('showUpdateMessage', true);
        store.getProxy().setExtraParam('live', true);
        store.reload({
            success: function() {
                if (variant) {
                    me.refreshArticleVariantList();
                } else {
                    me.refreshArticleList();
                }
            }
        });
    },

    onDeleteData: function(record, store, detailId, variant = false) {
        var me = this;

        if (record.get('ordernumber').length) {
            Ext.Msg.show({
                title : me.dcSnippets.hint,
                msg: Ext.String.format(me.dcSnippets.deleteArticleDataReally, record.get('basename')),
                width : 300,
                closable : false,
                buttons : Ext.Msg.YESNO,
                buttonText :
                    {
                        yes : me.dcSnippets.deleteIrrevocably,
                        no : me.dcSnippets.abort
                    },
                multiline : false,
                fn : function(value){
                    if (value === 'yes') {
                        record.set('active', false);
                        record.set('articlename', null);
                        record.set('description', null);
                        record.set('instock', 0);
                        record.set('ordernumber', null);
                        record.set('override_name', false);
                        record.set('override_price', false);
                        record.set('price_recommended', 0);
                        record.set('purchasing_price', 0);
                        record.commit();
                        record.save({
                            params: {
                                detailId: detailId,
                                delete: true,
                            },
                            success: function() {
                                Shopware.Notification.createStickyGrowlMessage({
                                        title: me.dcSnippets.success,
                                        text: me.dcSnippets.deleteDataSuccess,
                                        log: true
                                    },
                                    'Wholesaler');

                                if (variant) {
                                    me.refreshArticleVariantList();
                                } else {
                                    me.refreshArticleList();
                                }
                            },
                            failure: function() {
                                Shopware.Notification.createStickyGrowlMessage({
                                        title: me.dcSnippets.error,
                                        text: me.dcSnippets.deleteDataFailure,
                                        log: true
                                    },
                                    'Wholesaler');

                                store.reload();
                            }
                        });
                    }
                },
                icon : Ext.Msg.QUESTION
            });
        }
    },

    onSetActive: function(record, detailId, variant = false) {
        var me = this;

        record.set('active', !record.get('active'));
        record.commit();
        record.save({
            params: {
                detailId: detailId
            },
            success: function() {
                if (variant) {
                    me.refreshArticleVariantList();
                } else {
                    me.refreshArticleList();
                }
            }
        });
    },

    refreshArticleVariantList: function() {
        var me = this;
        var listing = me.getVariantListing();

        listing.store.reload();
    },

    onSetPermanentlyOverridePurchasePrice: function(grid, record, detailId) {
        var me = this;

        if (record.get('ordernumber').length && record.get('active')) {
            if (!record.get('override_price')) {
                Ext.Msg.show({
                    title : me.dcSnippets.hint,
                    msg: Ext.String.format(me.dcSnippets.overridePricePermanentlyHint, record.get('basename')),
                    width : 300,
                    closable : false,
                    buttons : Ext.Msg.YESNO,
                    buttonText :
                        {
                            yes : me.dcSnippets.overridePermanently,
                            no : me.dcSnippets.abort
                        },
                    multiline : false,
                    fn : function(value){
                        if (value === 'yes') {
                            var records = grid.getStore().getRange();

                            Ext.Array.each(records, function(rec) {
                                rec.set('override_price', rec.get('shortname') === record.get('shortname'));
                                rec.commit();
                                rec.save({
                                    params: {
                                        detailId: detailId
                                    }
                                });
                            });
                        }
                    },
                    icon : Ext.Msg.QUESTION
                });
            } else {
                record.set('override_price', !record.get('override_price'));
                record.commit();
                record.save({
                    params: {
                        detailId: detailId
                    }
                });
            }
        }
    },

    onSetPermanentlyOverrideNameAndDescription: function(grid, record, detailId) {
        var me = this;

        if (record.get('ordernumber').length && record.get('active')) {
            if (!record.get('override_name')) {
                Ext.Msg.show({
                    title : me.dcSnippets.hint,
                    msg: Ext.String.format(
                        me.dcSnippets.overrideNamePermanentlyHint,
                        record.get('basename')
                    ),
                    width : 300,
                    closable : false,
                    buttons : Ext.Msg.YESNO,
                    buttonText :
                        {
                            yes : me.dcSnippets.overridePermanently,
                            no : me.dcSnippets.abort
                        },
                    multiline : false,
                    fn : function(value) {
                        if (value === 'yes') {
                            var records = grid.getStore().getRange();

                            Ext.Array.each(records, function(rec, index) {
                                rec.set('override_name', rec.get('shortname') === record.get('shortname'));
                                rec.commit();
                                rec.save({
                                    params: {
                                        detailId: detailId
                                    }
                                });
                            });
                        }
                    },
                    icon : Ext.Msg.QUESTION
                });
            } else {
                record.set('override_name', !record.get('override_name'));
                record.commit();
                record.save({
                    params: {
                        detailId: detailId
                    }
                });
            }
        }
    },

    onUpdatePurchasePrice: function(record) {
        var me = this;
        var mainWindow = me.getMainWindow();
        var form = mainWindow.detailForm.getForm();

        if (record.get('ordernumber').length && record.get('purchasing_price') && record.get('active')) {
            Ext.Msg.show({
                title : me.dcSnippets.overwriteReallyQuestionTitle,
                msg : me.dcSnippets.overwritePriceReallyQuestionMessage,
                width : 300,
                closable : false,
                buttons : Ext.Msg.YESNO,
                buttonText :
                    {
                        yes : me.dcSnippets.overwrite,
                        no : me.dcSnippets.abort
                    },
                multiline : false,
                fn : function(value){
                    if (value === 'yes') {
                        form.findField('mainDetail[purchasePrice]').setValue(record.get('purchasing_price'))

                        Ext.Msg.confirm({
                            title : me.dcSnippets.hint,
                            msg : '<p><b>' + me.dcSnippets.saveArticleHint + '</b></p>',
                            width : 300,
                            closable : true,
                            buttons : Ext.Msg.CANCEL,
                            buttonText :
                                {
                                    cancel : me.dcSnippets.close
                                },
                            multiline : false,
                            icon : Ext.Msg.INFO
                        });
                    }
                },
                icon : Ext.Msg.QUESTION
            });
        }
    },

    onUpdateArticleNameAndDescription: function(record) {
        var me = this;
        var message;
        var mainWindow = me.getMainWindow();
        var form = mainWindow.detailForm.getForm();

        if (record.get('ordernumber').length && record.get('articlename').length && record.get('active')) {
            Ext.Msg.show({
                title : me.dcSnippets.overwriteReallyQuestionTitle,
                msg : me.dcSnippets.overwriteNameReallyQuestionMessage,
                width : 300,
                closable : false,
                buttons : Ext.Msg.YESNO,
                buttonText :
                    {
                        yes : me.dcSnippets.overwrite,
                        no : me.dcSnippets.abort
                    },
                multiline : false,
                fn : function(value){
                    if (value === 'yes') {
                        form.findField('name').setValue(record.get('articlename'))
                        mainWindow.setTitle(record.get('articlename'));

                        if (record.get('description').trim().length) {
                            form.findField('descriptionLong').setValue(record.get('description'))
                            message = '<p><b>' + me.dcSnippets.saveArticleHint + '</b></p>';
                        } else {
                            message = '<p>' + Ext.String.format(me.dcSnippets.wholesalerHasNoDescription, record.get('longname')) + '</p><br>' +
                                '<p><b>' + me.dcSnippets.saveArticleHint + '</b></p>';
                        }

                        Ext.Msg.confirm({
                            title : me.dcSnippets.hint,
                            msg : message,
                            width : 300,
                            closable : true,
                            buttons : Ext.Msg.CANCEL,
                            buttonText :
                                {
                                    cancel : me.dcSnippets.close
                                },
                            multiline : false,
                            icon : Ext.Msg.INFO
                        });
                    }
                },
                icon : Ext.Msg.QUESTION
            });
        }
    },

    onEditWholesaler: function(record, detailId) {
        var me = this;
        var active = record.get('active');

        if (!active) {
            Ext.Msg.show({
                title : me.dcSnippets.hint,
                msg : me.dcSnippets.wholesalerActivateHint,
                width : 300,
                closable : false,
                buttons : Ext.Msg.YESNO,
                buttonText :
                    {
                        yes : me.dcSnippets.wholesalerActivate,
                        no : me.dcSnippets.abort
                    },
                multiline : false,
                fn : function(value){
                    if (value === 'yes') {
                        record.set('active', true);
                        record.commit();
                        record.save({
                            params: {
                                detailId: detailId
                            },
                        });
                    }
                },
                icon : Ext.Msg.QUESTION
            });

            return false;
        } else {
            return true;
        }
    },

    onSaveWholesaler: function(record, detailId, variant = false, showNotification = false) {
        var me = this;

        record.commit();
        record.save({
            params: {
                detailId: detailId
            },
            success: function() {
                if (showNotification) {
                    Shopware.Notification.createGrowlMessage(
                        me.dcSnippets.success,
                        Ext.String.format(me.dcSnippets.wholesalerSaved, record.get('longname')),
                        'Wholesaler',
                        'growl',
                        true
                    );
                }

                if (variant) {
                    me.refreshArticleVariantList();
                }

                me.refreshArticleList();
            },
            failure: function(store) {
                if (showNotification) {
                    var response = store.proxy.reader.rawData

                    Shopware.Notification.createStickyGrowlMessage({
                            title: '{s name=error}Error{/s}',
                            text: Ext.String.format(
                                '{s name=' + response.messageNamespace + '}' + response.message + '{/s}',
                                record.get('longname')
                            ),
                            log: true
                        },
                        'Wholesaler');
                }

                record.store.reload();
            }
        });
    }
});
//{/block}