<?php
/*
 * @project Dropshippers Companion 2.0
 * @author Thomas Hamich & Andreas Gerhardt
 * @created 12/10/2019 22:38
 * @updated 21/01/2021 20:34
 *
 * @supplier Dropshippers Companion
 * @copyright 2021 © Dropshippers Companion
 *
 * @link https://dropshippers-companion.de
 * @support support@dropshippers-companion.de
 *
 * @license proprietary
 */

namespace WundeDcompanion\Bundle;


use DateTime;
use DateTimeZone;
use Exception;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Order\Detail;
use Shopware\Models\Order\DetailStatus;
use Symfony\Component\Console\Output\OutputInterface;
use WundeDcompanion\Bundle\OrderBundle\CombinationService;
use WundeDcompanion\Bundle\OrderBundle\SimilaryService;
use WundeDcompanion\Bundle\WholesalerBundle\WholesalerDataService;
use WundeDcompanion\Models\Core\Config as CoreConfig;
use WundeDcompanion\Models\Wholesaler\Wholesaler;
use WundeDcompanion\Services\LoggerService;

/**
 * Class AbstractService
 * @package WundeDcompanion\Bundle
 */
abstract class AbstractService
{
    /**
     * @var ModelManager $em
     */
    protected $em;

    /**
     * @var LoggerService $logger
     */
    protected $logger;

    /**
     * @var CoreConfig $config
     */
    protected $config;

    /**
     * @var Wholesaler[] $wholesalers
     */
    protected $wholesalers = [];

    /**
     * @var WholesalerDataService $wholesalerDataService
     */
    protected $wholesalerDataService;

    /**
     * @var array $wholesalerData
     */
    protected $wholesalerData = [];

    /**
     * @var SimilaryService $similaryService
     */
    protected $similaryService;

    /**
     * @var string $pluginDirectoy
     */
    private $pluginDirectory;

    /**
     * @var int $updateInterval
     */
    private $updateInterval;

    /**
     * @var CombinationService $combinationService
     */
    protected $combinationService;

    /**
     * @var OutputInterface|null $output
     */
    protected $output;

    /**
     * AbstractService constructor.
     * @param ModelManager $em
     * @param LoggerService $logger
     * @param CoreConfig $config
     * @param WholesalerDataService $wholesalerDataService
     * @param CombinationService $combinationService
     * @param string $pluginDirectory
     * @param int $updateInterval
     */
    public function __construct(
        ModelManager $em,
        LoggerService $logger,
        CoreConfig $config,
        WholesalerDataService $wholesalerDataService,
        CombinationService $combinationService,
        string $pluginDirectory,
        $updateInterval = 300
    )
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->config = $config;
        $this->wholesalerDataService = $wholesalerDataService;
        $this->wholesalers = $wholesalerDataService->findAllActive();
        $this->pluginDirectory = $pluginDirectory;
        $this->updateInterval = (int)$updateInterval;

        $this->wholesalerData = $this->receiveWholesalerData();

        /** @var DetailStatus $detailStatus */
        $detailStatus = $this->em->getRepository(DetailStatus::class)->find(0);

        $this->similaryService = new SimilaryService($this->em, $this->logger, $detailStatus, $this->wholesalers, $this->wholesalerData);
        $this->combinationService = $combinationService;
    }

    /**
     * @param array $content
     * @return boolean
     */
    private function wholesalerDataNeedUpdate(array $content): bool
    {

        if (count($this->wholesalers) !== count($content)) {
            return true;
        }

        foreach ($this->wholesalers as $wholesaler)
        {
            if (!array_key_exists($wholesaler->getShortname(), $content)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param boolean $update
     * @return mixed
     */
    private function receiveWholesalerData($update = false): array
    {
        $path = $this->pluginDirectory . '/Data/wholesalerData.json';
        $timezone = new DateTimeZone('Europe/Berlin');

        if (file_exists($path) && !$update) {
            $content = json_decode(file_get_contents($path), true);
            $date = new DateTime('now');

            if (
                is_array($content) &&
                isset($content['last_update']) &&
                isset($content['data']) &&
                is_array($content['last_update']) &&
                is_array($content['data']) &&
                isset($content['last_update']['date']) &&
                !$this->wholesalerDataNeedUpdate($content['data'])
            ) {
                try {
                    $lastUpdate = new DateTime($content['last_update']['date']);
                } catch (Exception $e) {
                    $lastUpdate = null;
                }

                if (!is_null($lastUpdate) && $date->getTimestamp() - $lastUpdate->getTimestamp() <= $this->updateInterval) {
                    return $content['data'];
                }
            }
        }

        $data = $this->wholesalerDataService->getAll($update);

        try {
            $date = new DateTime('now', $timezone);
        } catch (Exception $e) {
            $date = new DateTime('now');
        }

        $content = [
            'last_update' => $date,
            'data' => $data
        ];

        file_put_contents($path, json_encode($content));

        return $data;
    }

    /**
     * @param OutputInterface|null $output
     */
    public function setOutput(?OutputInterface $output): void
    {
        $this->output = $output;
        $this->combinationService->setOutput($output);
    }

    protected function clearConsole()
    {
        echo sprintf("\033\143");
    }

    /**
     * @param mixed $value
     * @param string $color
     */
    protected function print($value, $color = 'info')
    {
        if (!is_null($this->output)) {
            $this->output->writeln('<'.$color.'>'.print_r($value, true).'</'.$color.'>');
        } else {
            echo "\e[4m\033[31mOUTPUTINTERFACE NOT FOUND!\033\e[0m" . PHP_EOL . PHP_EOL;
            echo print_r($value, true) . PHP_EOL;
        }
    }
}